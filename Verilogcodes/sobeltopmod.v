
module sobelfilteraxistream(

  input clock,reset,
  //slave ports
  input s_axis_tlast,
  input s_axis_tvalid,
  output s_axis_tready,
  input [127:0] s_axis_tdata,
  input [15:0] s_axis_tstrb,

  //master ports
  output m_axis_tlast,
  output m_axis_tvalid,
  input m_axis_tready,
  output [39:0] m_axis_tdata,
  output [4:0] m_axis_tkeep
  );
  //ready is done
  //reg ready = 1'b1;
  reg lastpck = 1'b0;
  reg hold =1'b0;
  reg [47:0] temp; //temp register to store the last two columns to be used by the next convolutions
  reg f = 1'b1; //determines if it is the first packet of each row of image
  reg [167:0] inputvals; //register to store incoming data from fifo can store max 7x3 matrix (column wise)
  wire [2:0] full[4:0];
  wire [7:0] ans[4:0];
  reg [4:0] valid;
  wire [4:0] validout;
  //reg [4:0] anskeep;

  //convolutions
  convolution c1(inputvals[167:160],inputvals[159:152],inputvals[151:144],inputvals[143:136],inputvals[135:128],inputvals[127:120],
  inputvals[119:112],inputvals[111:104],inputvals[103:96],clock,ans[0],full[0],valid[0],validout[0],reset,m_axis_tready,hold);

  convolution c2(inputvals[143:136],inputvals[135:128],inputvals[127:120],inputvals[119:112],inputvals[111:104],inputvals[103:96],
  inputvals[95:88],inputvals[87:80],inputvals[79:72],clock,ans[1],full[1],valid[1],validout[1],reset,m_axis_tready,hold);

  convolution c3(inputvals[119:112],inputvals[111:104],inputvals[103:96],inputvals[95:88],inputvals[87:80],inputvals[79:72],
  inputvals[71:64],inputvals[63:56],inputvals[55:48],clock,ans[2],full[2],valid[2],validout[2],reset,m_axis_tready,hold);

  convolution c4(inputvals[95:88],inputvals[87:80],inputvals[79:72],inputvals[71:64],inputvals[63:56],inputvals[55:48],
  inputvals[47:40],inputvals[39:32],inputvals[31:24],clock,ans[3],full[3],valid[3],validout[3],reset,m_axis_tready,hold);

  convolution c5(inputvals[71:64],inputvals[63:56],inputvals[55:48],inputvals[47:40],inputvals[39:32],inputvals[31:24],
  inputvals[23:16],inputvals[15:8],inputvals[7:0],clock,ans[4],full[4],valid[4],validout[4],reset,m_axis_tready,hold);

  //to code output and f for the next row
  always @ (posedge clock)
  begin


    if((s_axis_tvalid == 1'b1) && ((m_axis_tready == 1'b1 | full[0] != 3'b111) ))
    begin

      if((s_axis_tstrb[14:9] == 6'b111111)) //checking if last two columns are valid
        temp <= {s_axis_tdata[79:72],s_axis_tdata[87:80],s_axis_tdata[95:88],
          s_axis_tdata[103:96],s_axis_tdata[111:104],s_axis_tdata[119:112]}; //ignoring the first 8 bits of input data as they just mark the end of packet
      else
        temp <= {47{1'b0}};


      if(f == 1'b1)
      begin
          inputvals[167:48] <= {s_axis_tdata[7:0],s_axis_tdata[15:8],s_axis_tdata[23:16],
            s_axis_tdata[31:24],s_axis_tdata[39:32],s_axis_tdata[47:40],
            s_axis_tdata[55:48],s_axis_tdata[63:56],s_axis_tdata[71:64],
            s_axis_tdata[79:72],s_axis_tdata[87:80],s_axis_tdata[95:88],
            s_axis_tdata[103:96],s_axis_tdata[111:104],s_axis_tdata[119:112]};
        // if((s_axis_tstrb[14:0] != 15'b111111111111111) | (s_axis_tdata[127:120] == 8'b11111111))
        //   f <= 1'b1;
        // else
        //   f <= 1'b0;
      end
      else
        inputvals[167:0] <= {temp,s_axis_tdata[7:0],s_axis_tdata[15:8],s_axis_tdata[23:16],
          s_axis_tdata[31:24],s_axis_tdata[39:32],s_axis_tdata[47:40],
          s_axis_tdata[55:48],s_axis_tdata[63:56],s_axis_tdata[71:64],
          s_axis_tdata[79:72],s_axis_tdata[87:80],s_axis_tdata[95:88],
          s_axis_tdata[103:96],s_axis_tdata[111:104],s_axis_tdata[119:112]};


      if((s_axis_tstrb[14:0] != 15'b111111111111111) | (s_axis_tdata[127:120] == 8'b11111111 && s_axis_tstrb[15] == 1'b1))
        f <= 1'b1;
      else
        f <= 1'b0;

      if((s_axis_tstrb[2:0] == 3'b111 && f == 1'b0) || (s_axis_tstrb[8:0] == 9'b111111111 && f == 1'b1))
        valid[0] <= 1'b1;
      else
      begin
        valid[0] <= 1'b0;
      end
      if((s_axis_tstrb[5:3] == 3'b111 && f == 1'b0) || (s_axis_tstrb[11:3] == 9'b111111111 && f == 1'b1))
        valid[1] <= 1'b1;
      else
      begin
        valid[1] <= 1'b0;
      end

      if((s_axis_tstrb[8:6] == 3'b111 && f == 1'b0) || (s_axis_tstrb[14:6] == 9'b111111111 && f == 1'b1))
        valid[2] <= 1'b1;
      else
      begin
        valid[2] <= 1'b0;
      end

      if((s_axis_tstrb[11:9] == 3'b111) && (f == 1'b0))
        valid[3] <= 1'b1;
      else
      begin
        valid[3] <= 1'b0;
      end

      if(s_axis_tstrb[14:12] == 3'b111 && f == 1'b0)
        valid[4] <= 1'b1;
      else
      begin
        valid[4] <= 1'b0;
      end

      if(full[0] == 3'b011 && m_axis_tready == 1'b0)
        hold <= 1'b1;
      else
        hold <= 1'b0;

      if(s_axis_tlast == 1'b1)
        lastpck <= 1'b1;
      /*if(validout[0] == 1'b1)
        anskeep[4] <= 1'b1;
      else
        anskeep[4] <= 1'b0;

      if(validout[1] == 1'b1)
        anskeep[3] <= 1'b1;
      else
        anskeep[3] <= 1'b0;

      if(validout[2] == 1'b1)
        anskeep[2] <= 1'b1;
      else
        anskeep[2] <= 1'b0;

      if(validout[3] == 1'b1)
        anskeep[1] <= 1'b1;
      else
        anskeep[1] <= 1'b0;

      if(validout[4] == 1'b1)
        anskeep[0] <= 1'b1;
      else
        anskeep[0] <= 1'b0;*/
    end
    else if(s_axis_tvalid == 1'b0)
        valid <= 5'b0;
  end

  assign m_axis_tlast = lastpck;
  assign m_axis_tvalid = ((validout[0] || validout[1] || validout[2] || validout[3] || validout[4]) && !hold);
  assign s_axis_tready = ( full[0] != 3'b111 | m_axis_tready==1'b1 );
  assign m_axis_tkeep = {validout[4],validout[3],validout[2],validout[1],validout[0]};
  assign m_axis_tdata = {ans[4],ans[3],ans[2],ans[1],ans[0]};
endmodule
