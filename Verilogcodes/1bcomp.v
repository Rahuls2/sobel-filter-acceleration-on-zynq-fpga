module comp1b(A,B,agt,eq,bgt);
    input A,B;
    output agt,eq,bgt;
    assign agt=A&(~B);
    assign eq=A~^B;
    assign bgt=B&(~A);
endmodule    