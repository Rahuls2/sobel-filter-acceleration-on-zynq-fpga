
module test ();

  reg [7:0] a1,b1,c1,a2,b2,c2,a3,b3,c3,a4,b4,c4,a5,b5,c5,last;
  reg clk,m_valid,s_ready,reset;
  reg [15:0] m_strb;
  wire m_ready,s_valid;
  wire [39:0] s_ans;
  wire [4:0] s_strb;
  reg lastvalin;
  wire lastout;

  sobelfilteraxistream s1(clk,reset,lastvalin,m_valid,m_ready,{last,c5,b5,a5,c4,b4,a4,c3,b3,a3,c2,b2,a2,c1,b1,a1},m_strb,lastout,s_valid,s_ready,s_ans,s_strb);
  always #1 clk = ~clk;
  initial begin
    lastvalin = 1'b0;
    reset = 1'b1;
    last = 0;
    clk = 1'b0;
    s_ready = 1'b1;
    #1;
    #1;
    #1;
    if (m_ready == 1'b1)
    begin
      a1 = 255;
      b1 = 255;
      c1 = 220;
      a2 = 255;
      b2 = 255;
      c2 = 220;
      a3 = 255;
      b3 = 255;
      c3 = 220;
      a4 = 5;
      b4 = 2;
      c4 = 0;
      a5 = 255;
      b5 = 255;
      c5 = 220;
      m_valid = 1'b1;
      m_strb = 16'b0111111111111111;
    end
    #1;
    #1;
    if (m_ready == 1'b1)
    begin
      a1 = 0;
      b1 = 0;
      c1 = 0;
      a2 = 0;
      b2 = 0;
      c2 = 0;
      a3 = 0;
      b3 = 0;
      c3 = 0;
      a4 = 25;
      b4 = 25;
      c4 = 20;
      a5 = 55;
      b5 = 55;
      c5 = 20;
      m_valid = 1'b1;
      m_strb = 16'b0111111111111111;
    end
    #1;
    #1;
    if (m_ready == 1'b1)
    begin
      a1 = 2;
      b1 = 3;
      c1 = 0;
      a2 = 0;
      b2 = 1;
      c2 = 0;
      a3 = 0;
      b3 = 0;
      c3 = 0;
      a4 = 255;
      b4 = 25;
      c4 = 220;
      a5 = 255;
      b5 = 255;
      c5 = 22;
      last = 8'b11111111;
      m_valid = 1'b1;
      m_strb = 16'b1111111111111111;
    end
    #1;
    #1;
    #1;
    #1;
  end
  initial begin
    $dumpfile("sobeloutputtop.vcd");
    $dumpvars(0,test);
  end
endmodule //test
