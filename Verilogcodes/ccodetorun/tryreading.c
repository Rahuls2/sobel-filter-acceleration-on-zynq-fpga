#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"

int main()
{
  FILE *ptr;
  uint8_t y;
  int i;
  int j;
  char num[15];
  ptr = fopen("trialinput","r");
  while (!feof(ptr))
  {
    i = 1;
    while (i <= 5)
    {
      j = 1;
      while(j <= 3)
      {
        fscanf(ptr,"%s",num);
        y = atoi(num);
        printf("%u ",y);
        j++;
      }
      if(fgetc(ptr) == '\n')
      {
        if (i == 5)
          printf("255 \n");
        else
        {
          printf("\n");
          i = 6;
        }
      }
      i++;
    }
  }
  return 0;
}
