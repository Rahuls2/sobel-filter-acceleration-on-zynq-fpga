//`include "1bcomp.v"
module comp2b(A,B,agt,eq,bgt);
    input [1:0] A,B;
    wire agt1,agt2,eq1,eq2,bgt1,bgt2;
    output agt,eq,bgt;
    comp1b cp21(A[0],B[0],agt1,eq1,bgt1);
    comp1b cp22(A[1],B[1],agt2,eq2,bgt2);
    assign agt=agt2 | ((A[1] | ~B[1])&agt1);
    assign eq=eq1&eq2;
    assign bgt=bgt2 | ((B[1] | ~A[1])&bgt1);
endmodule
