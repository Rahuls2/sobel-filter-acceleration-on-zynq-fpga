module HA(inp1,inp2,s,c);
    input inp1,inp2;
    output s,c;
    assign s=inp1^inp2;
    assign c=inp1&inp2;
endmodule    
