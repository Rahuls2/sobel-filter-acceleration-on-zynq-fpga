INSTRUCTIONS TO TEST AND DEBUG MODULES

Testing the convolution module
Testbench: tbconvolution.v
File to test: convolution.v (contains convolution module)

Compiling:
iverilog -o convolutionexe tbconvolution.v convolution.v 10bitsubtractor.v 4bsubtractor.v FA.v HA.v 2bcomp.v 1bcomp.v

Running executable (press ctrl-z to stop running the executable):
vvp convolutionexe

Checking the wave:
gtkwave convolutiontestwave.vcd



Testing top module
Testbench: tbsobeltopmod.v
File to test: sobeltopmod.v (contains sobelfilteraxistream module)

Compiling:
iverilog -o sobelfiltertopmodule tbsobeltopmod.v sobeltopmod.v convolution.v HA.v FA.v 1bcomp.v 2bcomp.v 10bitsubtractor.v 4bsubtractor.v

Runnning executable (press ctrl-z to stop running the executable):
vvp sobelfiltertopmodule

Checking the wave:
gtkwave sobeloutputtop.vcd &
