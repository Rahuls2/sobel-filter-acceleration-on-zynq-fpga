

module  comp10b(
  input wire [9:0] A,B,
  output wire agt,eq,bgt
  );
comp2b comp21(A[1:0],B[1:0],agt1,eq1,bgt1);
comp2b comp22(A[3:2],B[3:2],agt2,eq2,bgt2);
comp2b comp23({agt2,agt1},{bgt2,bgt1},agt3,eq3,bgt3);
comp2b comp24(A[5:4],B[5:4],agt4,eq4,bgt4);
comp2b comp25(A[7:6],B[7:6],agt5,eq5,bgt5);
comp2b comp26({agt5,agt4},{bgt5,bgt4},agt6,eq6,bgt6);
comp2b comp27(A[9:8],B[9:8],agt7,eq7,bgt7);
comp2b comp28({agt6,agt3},{bgt6,bgt3},agt8,eq8,bgt8);
comp2b comp29({agt7,agt8},{bgt7,bgt8},agt,eq,bgt);
endmodule


module adder128b (
  input [9:0] A,
  input [9:0] B,
  input C_in,
  output [9:0] ans,
  output C_out
  );

  wire [10:0] C;

  FA f1(A[0],B[0],C_in,ans[0],C[0]);
  FA f2(A[1],B[1],C[0],ans[1],C[1]);
  FA f3(A[2],B[2],C[1],ans[2],C[2]);
  FA f4(A[3],B[3],C[2],ans[3],C[3]);
  FA f5(A[4],B[4],C[3],ans[4],C[4]);
  FA f6(A[5],B[5],C[4],ans[5],C[5]);
  FA f7(A[6],B[6],C[5],ans[6],C[6]);
  FA f8(A[7],B[7],C[6],ans[7],C[7]);
  FA f9(A[8],B[8],C[7],ans[8],C[8]);
  FA f10(A[9],B[9],C[8],ans[9],C_out);
  //FA f11(A[10],1'b0,C[9],ans[10],C[10]);
  //FA f12(A[11],1'b0,C[10],ans[11],C_out);

endmodule //adder10-8b

module sub10b (
  input [9:0] A,B,
  input C_in,
  output wire [9:0] ans,
  output wire C_out
  );

  sub4b s1(A[3:0],B[3:0],C_in,ans[3:0],C1);
  sub4b s2(A[7:4],B[7:4],C1,ans[7:4],C2);
  FA f5(A[8],~B[8],C2,ans[8],C3);
  FA F6(A[9],~B[9],C3,ans[9],C4);
  assign C_out = C4;
endmodule //sub10b
