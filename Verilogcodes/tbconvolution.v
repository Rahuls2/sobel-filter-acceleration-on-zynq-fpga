//`include "convolution.v"

module test();
  reg hold;
  reg [7:0] a1,b1,c1,a2,b2,c2,a3,b3,c3;
  wire [7:0] ans;
  wire [2:0] full;
  wire validout;
  reg clk,valid,reset,ready;
  always #1 clk = ~clk;
  convolution DUT(a1,b1,c1,a2,b2,c2,a3,b3,c3,clk,ans,full,valid,validout,reset,ready,hold);
  initial begin
    hold =  1'b0;
    reset = 1'b1;
    ready = 1'b1;
    valid = 1'b1;
    clk = 1'b0;
    a1 = 8'b0;
    b1 = 8'b0;
    c1 = 8'b0;
    a2 = 8'b0;
    b2 = 8'b0;
    c2 = 8'b0;
    a3 = 8'b0;
    b3 = 8'b0;
    c3 = 8'b0;
    #2;
    valid = 1'b0;
    a1 = 1;
    b1 = 2;
    c1 = 3;
    a2 = 0;
    b2 = 0;
    c2 = 0;
    a3 = 1;
    b3 = 2;
    c3 = 3;
    #2;
    valid = 1'b1;
    a1 = 2;
    b1 = 3;
    c1 = 4;
    a2 = 1;
    b2 = 0;
    c2 = 1;
    a3 = 3;
    b3 = 2;
    c3 = 4;
    #2;
    a1 = 8'b0;
    b1 = 8'b0;
    c1 = 8'b0;
    a2 = 8'b0;
    b2 = 8'b0;
    c2 = 8'b0;
    a3 = 8'b0;
    b3 = 8'b0;
    c3 = 8'b0;
    #2;
    a1 = 1;
    b1 = 2;
    c1 = 3;
    a2 = 0;
    b2 = 0;
    c2 = 0;
    a3 = 1;
    b3 = 2;
    c3 = 3;
    #2;
  end
  initial begin
    $dumpfile("convolutiontestwave.vcd");
    $dumpvars(0,test);
  end
endmodule
