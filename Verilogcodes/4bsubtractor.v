
module sub4b(A,B,C_in,ans,C_out);
    input [3:0] A,B;
    input C_in;
    wire [3:0] C;
    output [3:0] ans;
    output C_out;
    FA f1(A[0],~B[0],C_in,ans[0],C[0]);
    FA f2(A[1],~B[1],C[0],ans[1],C[1]);
    FA f3(A[2],~B[2],C[1],ans[2],C[2]);
    FA f4(A[3],~B[3],C[2],ans[3],C[3]);
    assign C_out=C[3];
endmodule
