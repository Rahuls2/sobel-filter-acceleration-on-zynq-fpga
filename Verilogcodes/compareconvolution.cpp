#include <iostream>
#include <cmath>
#include <ctime>

using namespace std;
int ar[3][7];
int ans;
int ans2;

int main()
{
  clock_t begin = clock();
  int k = 12;
  for(int i = 2;i<7;i++)
  {
    for(int j = 0;j<3;j++)
    {
      ar[j][i] = k;
      k++;
    }
  }
  k = 21;
  for(int i = 0;i<2;i++)
  {
    for(int j = 0;j<3;j++)
    {
      ar[j][i] = k;
      k++;
    }
  }
  for(int m = 0;m < 10000000;m++)
  {
    k = 0;
    for(k = 0;k<5;k++)
    {
      ans = 0;
      ans2 = 0;
      for(int i = 0;i<3;i+=2)
      {
        for(int j = 0;j < 3;j++)
        {
          if (i == 2 && j != 1)
            ans -= ar[j][i+k];
          else if(i == 2 && j == 1)
            ans -= 2*ar[j][i+k];
          else if(i == 0 && j != 1)
            ans += ar[j][i+k];
          else
            ans += 2*ar[j][i+k];
        }
      }
      ans = abs(ans);
      for(int i = 0;i<3;i+=2)
      {
        for(int j = 0;j < 3;j++)
        {
          if (i == 2 && j != 1)
            ans2 -= ar[i][j+k];
          else if(i == 2 && j == 1)
            ans2 -= 2*ar[i][j+k];
          else if(i == 0 && j != 1)
            ans2 += ar[i][j+k];
          else
            ans2 += 2*ar[i][j+k];
        }
      }
      ans2 = abs(ans2);
      cout<<ans+ans2<<"\n";
    }
  }
  // for(int i = 0;i<7;i++)
  // {
  //   for(int j = 0;j<3;j++)
  //   {
  //     cout<<ar[j][i]<<" ";
  //   }
  // }

  clock_t end = clock();
  double elapsedtime = double(end - begin) / CLOCKS_PER_SEC;
  cout<<elapsedtime<<" secs";
  return 0;
}
