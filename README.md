# Sobel filter acceleration on zynq FPGA

Accelerating Sobel filter on FPGA using pipelining and parallelism.

Verilog files with testbench are in the folder Verilogcodes.

Readme files that tell the user how to run the testbench and check the waves using gtkwave is given. You can use a different software to check the output waves.

projectalmostdone is the folder having all files with exported hardware and c code written to test on the FPGA
