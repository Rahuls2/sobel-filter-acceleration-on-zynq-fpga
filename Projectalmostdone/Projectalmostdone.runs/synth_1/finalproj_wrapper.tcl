# 
# Synthesis run script generated by Vivado
# 

proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
set_msg_config -id {HDL-1065} -limit 10000
create_project -in_memory -part xc7z020clg484-1

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.cache/wt [current_project]
set_property parent.project_path /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.xpr [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_FIFO XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language Verilog [current_project]
set_property board_part em.avnet.com:zed:part0:1.3 [current_project]
set_property ip_output_repo /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.cache/ip [current_project]
set_property ip_cache_permissions {read write} [current_project]
read_verilog -library xil_defaultlib /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/hdl/finalproj_wrapper.v
add_files /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/finalproj.bd
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_processing_system7_0_0/finalproj_processing_system7_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_dma_0_0/finalproj_axi_dma_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_dma_0_0/finalproj_axi_dma_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_dma_0_0/finalproj_axi_dma_0_0_clocks.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_rst_ps7_0_100M_0/finalproj_rst_ps7_0_100M_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_rst_ps7_0_100M_0/finalproj_rst_ps7_0_100M_0.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_rst_ps7_0_100M_0/finalproj_rst_ps7_0_100M_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axis_data_fifo_0_0/finalproj_axis_data_fifo_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axis_data_fifo_0_0/finalproj_axis_data_fifo_0_0/finalproj_axis_data_fifo_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axis_data_fifo_0_1/finalproj_axis_data_fifo_0_1_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axis_data_fifo_0_1/finalproj_axis_data_fifo_0_1/finalproj_axis_data_fifo_0_1.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axis_dwidth_converter_0_0/finalproj_axis_dwidth_converter_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_10/bd_3225_s00a2s_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_16/bd_3225_s01a2s_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_20/bd_3225_m00s2a_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_21/bd_3225_m00arn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_22/bd_3225_m00rn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_23/bd_3225_m00awn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_24/bd_3225_m00wn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_25/bd_3225_m00bn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_17/bd_3225_sawn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_18/bd_3225_swn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_19/bd_3225_sbn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_11/bd_3225_sarn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_12/bd_3225_srn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_2/bd_3225_arsw_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_3/bd_3225_rsw_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_4/bd_3225_awsw_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_5/bd_3225_wsw_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_6/bd_3225_bsw_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_1/bd_3225_psr_aclk_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/bd_0/ip/ip_1/bd_3225_psr_aclk_0.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_axi_smc_0/ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_auto_pc_0/finalproj_auto_pc_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/finalproj_ooc.xdc]

# Mark all dcp files as not used in implementation to prevent them from being
# stitched into the results of this synthesis run. Any black boxes in the
# design are intentionally left as such for best results. Dcp files will be
# stitched into the design at a later time, either when this synthesis run is
# opened, or when it is stitched into a dependent implementation run.
foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
read_xdc dont_touch.xdc
set_property used_in_implementation false [get_files dont_touch.xdc]

synth_design -top finalproj_wrapper -part xc7z020clg484-1


# disable binary constraint mode for synth run checkpoints
set_param constraints.enableBinaryConstraints false
write_checkpoint -force -noxdef finalproj_wrapper.dcp
create_report "synth_1_synth_report_utilization_0" "report_utilization -file finalproj_wrapper_utilization_synth.rpt -pb finalproj_wrapper_utilization_synth.pb"
