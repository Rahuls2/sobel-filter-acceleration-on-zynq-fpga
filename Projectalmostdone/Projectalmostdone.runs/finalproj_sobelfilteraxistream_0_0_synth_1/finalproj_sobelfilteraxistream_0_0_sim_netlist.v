// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Thu Oct 25 18:25:06 2018
// Host        : dell-inspiron-3567 running 64-bit Ubuntu 16.04.5 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ finalproj_sobelfilteraxistream_0_0_sim_netlist.v
// Design      : finalproj_sobelfilteraxistream_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convolution
   (D,
    \xreg1_reg[8]_0 ,
    \temp_reg[0] ,
    temp0,
    hold0,
    \m_axis_tkeep[0] ,
    lastpck_reg,
    E,
    s_axis_tready,
    f_reg,
    m_axis_tdata,
    Q,
    reset,
    \s_axis_tstrb[9] ,
    s_axis_tstrb,
    m_axis_tready,
    s_axis_tlast,
    m_axis_tlast,
    f,
    s_axis_tvalid,
    \s_axis_tstrb[6] ,
    clock,
    hold,
    \valid_reg[0] ,
    reset_0,
    hold_reg);
  output [9:0]D;
  output \xreg1_reg[8]_0 ;
  output \temp_reg[0] ;
  output temp0;
  output hold0;
  output \m_axis_tkeep[0] ;
  output lastpck_reg;
  output [0:0]E;
  output s_axis_tready;
  output f_reg;
  output [7:0]m_axis_tdata;
  input [63:0]Q;
  input reset;
  input \s_axis_tstrb[9] ;
  input [5:0]s_axis_tstrb;
  input m_axis_tready;
  input s_axis_tlast;
  input m_axis_tlast;
  input f;
  input s_axis_tvalid;
  input \s_axis_tstrb[6] ;
  input clock;
  input hold;
  input [0:0]\valid_reg[0] ;
  input reset_0;
  input hold_reg;

  wire [9:0]D;
  wire [0:0]E;
  wire [63:0]Q;
  wire [7:1]ans1;
  wire [9:1]ans2;
  wire [5:2]\c3/outx11 ;
  wire \c3/x11/C_3 ;
  wire \c3/x11/C_5 ;
  wire \c3/x11/C_6 ;
  wire \c3/x12/C_1 ;
  wire \c3/x12/C_2 ;
  wire \c3/x12/C_3 ;
  wire \c3/x12/C_5 ;
  wire \c3/x12/C_6 ;
  wire \c3/x12/f6/S ;
  wire \c3/x12/f8/S ;
  wire clock;
  wire f;
  wire f_reg;
  wire \final/C_1 ;
  wire \final/C_2 ;
  wire \final/C_4 ;
  wire \final/C_6 ;
  wire \final/f1/Ca ;
  wire \final/f2/S ;
  wire \final/f4/S ;
  wire \final/f5/S ;
  wire \final/f6/S ;
  wire [1:0]\full[0]_0 ;
  wire \full[0]_i_1_n_0 ;
  wire \full[1]_i_1_n_0 ;
  wire \full[2]_i_1_n_0 ;
  wire hold;
  wire hold0;
  wire hold_reg;
  wire lastpck_reg;
  wire [7:0]m_axis_tdata;
  wire \m_axis_tkeep[0] ;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire out;
  wire \out[0]_i_1_n_0 ;
  wire \out[0]_i_2_n_0 ;
  wire \out[1]_i_1_n_0 ;
  wire \out[1]_i_2_n_0 ;
  wire \out[2]_i_1_n_0 ;
  wire \out[2]_i_2_n_0 ;
  wire \out[3]_i_1_n_0 ;
  wire \out[3]_i_2_n_0 ;
  wire \out[4]_i_1_n_0 ;
  wire \out[4]_i_2_n_0 ;
  wire \out[5]_i_1_n_0 ;
  wire \out[5]_i_2_n_0 ;
  wire \out[6]_i_1_n_0 ;
  wire \out[6]_i_2_n_0 ;
  wire \out[7]_i_26_n_0 ;
  wire \out[7]_i_27_n_0 ;
  wire \out[7]_i_28_n_0 ;
  wire \out[7]_i_29_n_0 ;
  wire \out[7]_i_2_n_0 ;
  wire \out[7]_i_3_n_0 ;
  wire \out[7]_i_8_n_0 ;
  wire [5:2]outx11;
  wire [9:1]outx12;
  wire [5:2]outy11;
  wire [9:1]outy12;
  wire [5:2]outy21;
  wire [9:1]outy22;
  wire reset;
  wire reset_0;
  wire \s1/C2 ;
  wire \s1/F6/S ;
  wire \s1/s1/C_2 ;
  wire \s1/s2/C_0 ;
  wire \s1/s2/C_2 ;
  wire \s1/s2/f2/S ;
  wire \s11[0]_i_1_n_0 ;
  wire \s11[1]_i_1_n_0 ;
  wire \s11[2]_i_1_n_0 ;
  wire \s11[3]_i_1_n_0 ;
  wire \s11[4]_i_1_n_0 ;
  wire \s11[5]_i_1_n_0 ;
  wire \s11[6]_i_1_n_0 ;
  wire \s11[7]_i_1_n_0 ;
  wire \s11[8]_i_1_n_0 ;
  wire \s11[9]_i_1_n_0 ;
  wire \s11_reg_n_0_[0] ;
  wire \s11_reg_n_0_[1] ;
  wire \s11_reg_n_0_[2] ;
  wire \s11_reg_n_0_[3] ;
  wire \s11_reg_n_0_[4] ;
  wire \s11_reg_n_0_[5] ;
  wire \s11_reg_n_0_[6] ;
  wire \s11_reg_n_0_[7] ;
  wire \s11_reg_n_0_[8] ;
  wire \s11_reg_n_0_[9] ;
  wire [9:0]s12;
  wire \s12[0]_i_1_n_0 ;
  wire \s12[1]_i_1_n_0 ;
  wire \s12[2]_i_1_n_0 ;
  wire \s12[3]_i_1_n_0 ;
  wire \s12[4]_i_1_n_0 ;
  wire \s12[5]_i_1_n_0 ;
  wire \s12[6]_i_1_n_0 ;
  wire \s12[7]_i_1_n_0 ;
  wire \s12[8]_i_1_n_0 ;
  wire \s12[8]_i_2_n_0 ;
  wire \s12[8]_i_3_n_0 ;
  wire \s12[8]_i_4_n_0 ;
  wire \s12[8]_i_5_n_0 ;
  wire \s12[8]_i_6_n_0 ;
  wire \s12[8]_i_7_n_0 ;
  wire \s12[8]_i_8_n_0 ;
  wire \s12[8]_i_9_n_0 ;
  wire \s12[9]_i_2_n_0 ;
  wire \s2/C2 ;
  wire \s2/F6/S ;
  wire \s2/f5/S ;
  wire \s2/s1/C_2 ;
  wire \s2/s1/f1/S ;
  wire \s2/s2/C_0 ;
  wire \s2/s2/C_2 ;
  wire \s2/s2/f2/S ;
  wire s21;
  wire \s21[0]_i_1_n_0 ;
  wire \s21[1]_i_1_n_0 ;
  wire \s21[2]_i_1_n_0 ;
  wire \s21[3]_i_1_n_0 ;
  wire \s21[4]_i_1_n_0 ;
  wire \s21[5]_i_1_n_0 ;
  wire \s21[6]_i_1_n_0 ;
  wire \s21[7]_i_1_n_0 ;
  wire \s21[8]_i_1_n_0 ;
  wire \s21[9]_i_1_n_0 ;
  wire \s21_reg_n_0_[0] ;
  wire \s21_reg_n_0_[1] ;
  wire \s21_reg_n_0_[2] ;
  wire \s21_reg_n_0_[3] ;
  wire \s21_reg_n_0_[4] ;
  wire \s21_reg_n_0_[5] ;
  wire \s21_reg_n_0_[6] ;
  wire \s21_reg_n_0_[7] ;
  wire \s21_reg_n_0_[8] ;
  wire \s21_reg_n_0_[9] ;
  wire [9:0]s22;
  wire \s22[0]_i_1_n_0 ;
  wire \s22[1]_i_1_n_0 ;
  wire \s22[2]_i_1_n_0 ;
  wire \s22[3]_i_1_n_0 ;
  wire \s22[4]_i_1_n_0 ;
  wire \s22[5]_i_1_n_0 ;
  wire \s22[6]_i_1_n_0 ;
  wire \s22[7]_i_1_n_0 ;
  wire \s22[8]_i_1_n_0 ;
  wire \s22[8]_i_2_n_0 ;
  wire \s22[8]_i_3_n_0 ;
  wire \s22[8]_i_4_n_0 ;
  wire \s22[8]_i_5_n_0 ;
  wire \s22[8]_i_6_n_0 ;
  wire \s22[8]_i_7_n_0 ;
  wire \s22[8]_i_8_n_0 ;
  wire \s22[8]_i_9_n_0 ;
  wire \s22[9]_i_1_n_0 ;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire [5:0]s_axis_tstrb;
  wire \s_axis_tstrb[6] ;
  wire \s_axis_tstrb[9] ;
  wire s_axis_tvalid;
  wire temp0;
  wire \temp_reg[0] ;
  wire [0:0]\valid_reg[0] ;
  wire \x11/C_3 ;
  wire \x11/C_5 ;
  wire \x11/C_6 ;
  wire \x12/C_1 ;
  wire \x12/C_2 ;
  wire \x12/C_3 ;
  wire \x12/C_5 ;
  wire \x12/C_6 ;
  wire \x12/f1/S ;
  wire \x12/f6/S ;
  wire \x12/f8/S ;
  wire xreg1;
  wire \xreg1_reg[8]_0 ;
  wire \xreg1_reg_n_0_[0] ;
  wire \xreg1_reg_n_0_[1] ;
  wire \xreg1_reg_n_0_[2] ;
  wire \xreg1_reg_n_0_[3] ;
  wire \xreg1_reg_n_0_[4] ;
  wire \xreg1_reg_n_0_[5] ;
  wire \xreg1_reg_n_0_[6] ;
  wire \xreg1_reg_n_0_[7] ;
  wire \xreg1_reg_n_0_[8] ;
  wire \xreg1_reg_n_0_[9] ;
  wire [9:0]xreg2;
  wire \y11/C_3 ;
  wire \y11/C_5 ;
  wire \y11/C_6 ;
  wire \y12/C_1 ;
  wire \y12/C_2 ;
  wire \y12/C_3 ;
  wire \y12/C_5 ;
  wire \y12/C_6 ;
  wire \y12/f1/S ;
  wire \y12/f6/S ;
  wire \y12/f8/S ;
  wire \y21/C_3 ;
  wire \y21/C_5 ;
  wire \y21/C_6 ;
  wire \y22/C_1 ;
  wire \y22/C_2 ;
  wire \y22/C_3 ;
  wire \y22/C_5 ;
  wire \y22/C_6 ;
  wire \y22/f1/S ;
  wire \y22/f6/S ;
  wire \y22/f8/S ;
  wire yreg1;
  wire \yreg1_reg_n_0_[0] ;
  wire \yreg1_reg_n_0_[1] ;
  wire \yreg1_reg_n_0_[2] ;
  wire \yreg1_reg_n_0_[3] ;
  wire \yreg1_reg_n_0_[4] ;
  wire \yreg1_reg_n_0_[5] ;
  wire \yreg1_reg_n_0_[6] ;
  wire \yreg1_reg_n_0_[7] ;
  wire \yreg1_reg_n_0_[8] ;
  wire \yreg1_reg_n_0_[9] ;
  wire [9:0]yreg2;

  LUT6 #(
    .INIT(64'hFF7FFFFF00000000)) 
    f_i_1
       (.I0(s_axis_tstrb[2]),
        .I1(s_axis_tstrb[1]),
        .I2(s_axis_tstrb[0]),
        .I3(\s_axis_tstrb[6] ),
        .I4(temp0),
        .I5(f),
        .O(f_reg));
  LUT6 #(
    .INIT(64'hEEFEEEEE44444444)) 
    \full[0]_i_1 
       (.I0(hold),
        .I1(\valid_reg[0] ),
        .I2(\full[0]_0 [1]),
        .I3(m_axis_tready),
        .I4(\m_axis_tkeep[0] ),
        .I5(\full[0]_0 [0]),
        .O(\full[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'hEEFE4444)) 
    \full[1]_i_1 
       (.I0(hold),
        .I1(\full[0]_0 [0]),
        .I2(\m_axis_tkeep[0] ),
        .I3(m_axis_tready),
        .I4(\full[0]_0 [1]),
        .O(\full[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFBFF4040)) 
    \full[2]_i_1 
       (.I0(hold),
        .I1(reset),
        .I2(\full[0]_0 [1]),
        .I3(m_axis_tready),
        .I4(\m_axis_tkeep[0] ),
        .O(\full[2]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[0] 
       (.C(clock),
        .CE(1'b1),
        .CLR(reset_0),
        .D(\full[0]_i_1_n_0 ),
        .Q(\full[0]_0 [0]));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[1] 
       (.C(clock),
        .CE(1'b1),
        .CLR(reset_0),
        .D(\full[1]_i_1_n_0 ),
        .Q(\full[0]_0 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[2] 
       (.C(clock),
        .CE(1'b1),
        .CLR(reset_0),
        .D(\full[2]_i_1_n_0 ),
        .Q(\m_axis_tkeep[0] ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hF7FF0000)) 
    hold_i_1
       (.I0(\m_axis_tkeep[0] ),
        .I1(\full[0]_0 [1]),
        .I2(m_axis_tready),
        .I3(\full[0]_0 [0]),
        .I4(s_axis_tvalid),
        .O(temp0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    hold_i_2
       (.I0(\full[0]_0 [0]),
        .I1(m_axis_tready),
        .I2(\full[0]_0 [1]),
        .I3(\m_axis_tkeep[0] ),
        .O(hold0));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \inputvals[47]_i_1 
       (.I0(temp0),
        .I1(f),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    lastpck_i_1
       (.I0(s_axis_tlast),
        .I1(temp0),
        .I2(m_axis_tlast),
        .O(lastpck_reg));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[0]_i_1 
       (.I0(\out[0]_i_2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8_n_0 ),
        .O(\out[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    \out[0]_i_2 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s11_reg_n_0_[0] ),
        .O(\out[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[1]_i_1 
       (.I0(\out[1]_i_2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8_n_0 ),
        .O(\out[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'hAA9696AA)) 
    \out[1]_i_2 
       (.I0(\final/f2/S ),
        .I1(\s11_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s21_reg_n_0_[0] ),
        .I4(s22[0]),
        .O(\out[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'hD22D2DD2)) 
    \out[1]_i_3 
       (.I0(s12[0]),
        .I1(\s11_reg_n_0_[0] ),
        .I2(\s11_reg_n_0_[1] ),
        .I3(s12[1]),
        .I4(ans2[1]),
        .O(\final/f2/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[2]_i_1 
       (.I0(\out[2]_i_2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8_n_0 ),
        .O(\out[2]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \out[2]_i_2 
       (.I0(ans1[2]),
        .I1(ans2[2]),
        .I2(\final/C_1 ),
        .O(\out[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h6966996969666966)) 
    \out[2]_i_3 
       (.I0(s12[2]),
        .I1(\s11_reg_n_0_[2] ),
        .I2(\s11_reg_n_0_[1] ),
        .I3(s12[1]),
        .I4(\s11_reg_n_0_[0] ),
        .I5(s12[0]),
        .O(ans1[2]));
  LUT6 #(
    .INIT(64'h6966996969666966)) 
    \out[2]_i_4 
       (.I0(s22[2]),
        .I1(\s21_reg_n_0_[2] ),
        .I2(\s21_reg_n_0_[1] ),
        .I3(s22[1]),
        .I4(\s21_reg_n_0_[0] ),
        .I5(s22[0]),
        .O(ans2[2]));
  LUT6 #(
    .INIT(64'h0CC0D44D4DD40CC0)) 
    \out[2]_i_5 
       (.I0(\s2/s1/f1/S ),
        .I1(ans2[1]),
        .I2(s12[1]),
        .I3(\s11_reg_n_0_[1] ),
        .I4(\s11_reg_n_0_[0] ),
        .I5(s12[0]),
        .O(\final/C_1 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \out[2]_i_6 
       (.I0(\s21_reg_n_0_[0] ),
        .I1(s22[0]),
        .O(\s2/s1/f1/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[3]_i_1 
       (.I0(\out[3]_i_2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8_n_0 ),
        .O(\out[3]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out[3]_i_2 
       (.I0(\final/f4/S ),
        .I1(\final/C_2 ),
        .O(\out[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    \out[3]_i_3 
       (.I0(\s1/s1/C_2 ),
        .I1(\s11_reg_n_0_[3] ),
        .I2(s12[3]),
        .I3(\s2/s1/C_2 ),
        .I4(\s21_reg_n_0_[3] ),
        .I5(s22[3]),
        .O(\final/f4/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[4]_i_1 
       (.I0(\out[4]_i_2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8_n_0 ),
        .O(\out[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hDF0DFFFF0000DF0D)) 
    \out[4]_i_10 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s22[1]),
        .I3(\s21_reg_n_0_[1] ),
        .I4(s22[2]),
        .I5(\s21_reg_n_0_[2] ),
        .O(\s2/s1/C_2 ));
  LUT6 #(
    .INIT(64'h711717718EE8E88E)) 
    \out[4]_i_2 
       (.I0(\final/C_2 ),
        .I1(ans2[3]),
        .I2(s12[3]),
        .I3(\s11_reg_n_0_[3] ),
        .I4(\s1/s1/C_2 ),
        .I5(\final/f5/S ),
        .O(\out[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[4]_i_3 
       (.I0(\final/f1/Ca ),
        .I1(ans2[1]),
        .I2(ans1[1]),
        .I3(ans2[2]),
        .I4(ans1[2]),
        .O(\final/C_2 ));
  LUT3 #(
    .INIT(8'h69)) 
    \out[4]_i_4 
       (.I0(s22[3]),
        .I1(\s21_reg_n_0_[3] ),
        .I2(\s2/s1/C_2 ),
        .O(ans2[3]));
  LUT6 #(
    .INIT(64'hDF0DFFFF0000DF0D)) 
    \out[4]_i_5 
       (.I0(s12[0]),
        .I1(\s11_reg_n_0_[0] ),
        .I2(s12[1]),
        .I3(\s11_reg_n_0_[1] ),
        .I4(s12[2]),
        .I5(\s11_reg_n_0_[2] ),
        .O(\s1/s1/C_2 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \out[4]_i_6 
       (.I0(\s1/s1/C_2 ),
        .I1(s12[3]),
        .I2(\s11_reg_n_0_[3] ),
        .I3(\s11_reg_n_0_[4] ),
        .I4(s12[4]),
        .I5(ans2[4]),
        .O(\final/f5/S ));
  LUT4 #(
    .INIT(16'h0660)) 
    \out[4]_i_7 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s11_reg_n_0_[0] ),
        .O(\final/f1/Ca ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h6966)) 
    \out[4]_i_8 
       (.I0(s22[1]),
        .I1(\s21_reg_n_0_[1] ),
        .I2(\s21_reg_n_0_[0] ),
        .I3(s22[0]),
        .O(ans2[1]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'h6966)) 
    \out[4]_i_9 
       (.I0(s12[1]),
        .I1(\s11_reg_n_0_[1] ),
        .I2(\s11_reg_n_0_[0] ),
        .I3(s12[0]),
        .O(ans1[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[5]_i_1 
       (.I0(\out[5]_i_2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8_n_0 ),
        .O(\out[5]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out[5]_i_2 
       (.I0(\final/f6/S ),
        .I1(\final/C_4 ),
        .O(\out[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    \out[5]_i_3 
       (.I0(\s1/s2/C_0 ),
        .I1(\s11_reg_n_0_[5] ),
        .I2(s12[5]),
        .I3(\s2/s2/C_0 ),
        .I4(\s21_reg_n_0_[5] ),
        .I5(s22[5]),
        .O(\final/f6/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[6]_i_1 
       (.I0(\out[6]_i_2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8_n_0 ),
        .O(\out[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \out[6]_i_2 
       (.I0(ans2[6]),
        .I1(ans1[6]),
        .I2(ans1[5]),
        .I3(ans2[5]),
        .I4(\final/C_4 ),
        .O(\out[6]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h00D0)) 
    \out[7]_i_1 
       (.I0(\m_axis_tkeep[0] ),
        .I1(m_axis_tready),
        .I2(\full[0]_0 [1]),
        .I3(hold),
        .O(out));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_10 
       (.I0(\s2/s2/C_0 ),
        .I1(s22[5]),
        .I2(\s21_reg_n_0_[5] ),
        .I3(s22[6]),
        .I4(\s21_reg_n_0_[6] ),
        .O(\s2/s2/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_11 
       (.I0(\s1/s2/C_0 ),
        .I1(s12[5]),
        .I2(\s11_reg_n_0_[5] ),
        .I3(s12[6]),
        .I4(\s11_reg_n_0_[6] ),
        .O(\s1/s2/C_2 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[7]_i_12 
       (.I0(\final/C_2 ),
        .I1(ans2[3]),
        .I2(ans1[3]),
        .I3(ans2[4]),
        .I4(ans1[4]),
        .O(\final/C_4 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_13 
       (.I0(s22[5]),
        .I1(\s21_reg_n_0_[5] ),
        .I2(\s2/s2/C_0 ),
        .O(ans2[5]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_14 
       (.I0(s12[5]),
        .I1(\s11_reg_n_0_[5] ),
        .I2(\s1/s2/C_0 ),
        .O(ans1[5]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_15 
       (.I0(s22[6]),
        .I1(\s21_reg_n_0_[6] ),
        .I2(\s21_reg_n_0_[5] ),
        .I3(s22[5]),
        .I4(\s2/s2/C_0 ),
        .O(ans2[6]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_16 
       (.I0(s12[6]),
        .I1(\s11_reg_n_0_[6] ),
        .I2(\s11_reg_n_0_[5] ),
        .I3(s12[5]),
        .I4(\s1/s2/C_0 ),
        .O(ans1[6]));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_17 
       (.I0(\s11_reg_n_0_[9] ),
        .I1(s12[9]),
        .O(\s1/F6/S ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_18 
       (.I0(\s21_reg_n_0_[8] ),
        .I1(s22[8]),
        .O(\s2/f5/S ));
  LUT6 #(
    .INIT(64'hEF0EFFFF0000EF0E)) 
    \out[7]_i_19 
       (.I0(\out[7]_i_26_n_0 ),
        .I1(\out[7]_i_27_n_0 ),
        .I2(s22[6]),
        .I3(\s21_reg_n_0_[6] ),
        .I4(s22[7]),
        .I5(\s21_reg_n_0_[7] ),
        .O(\s2/C2 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \out[7]_i_2 
       (.I0(ans2[9]),
        .I1(ans1[7]),
        .I2(ans2[7]),
        .I3(\final/C_6 ),
        .I4(\out[7]_i_8_n_0 ),
        .O(\out[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEF0EFFFF0000EF0E)) 
    \out[7]_i_20 
       (.I0(\out[7]_i_28_n_0 ),
        .I1(\out[7]_i_29_n_0 ),
        .I2(s12[6]),
        .I3(\s11_reg_n_0_[6] ),
        .I4(s12[7]),
        .I5(\s11_reg_n_0_[7] ),
        .O(\s1/C2 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_21 
       (.I0(\s2/s1/C_2 ),
        .I1(s22[3]),
        .I2(\s21_reg_n_0_[3] ),
        .I3(s22[4]),
        .I4(\s21_reg_n_0_[4] ),
        .O(\s2/s2/C_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_22 
       (.I0(\s1/s1/C_2 ),
        .I1(s12[3]),
        .I2(\s11_reg_n_0_[3] ),
        .I3(s12[4]),
        .I4(\s11_reg_n_0_[4] ),
        .O(\s1/s2/C_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_23 
       (.I0(s12[3]),
        .I1(\s11_reg_n_0_[3] ),
        .I2(\s1/s1/C_2 ),
        .O(ans1[3]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_24 
       (.I0(s22[4]),
        .I1(\s21_reg_n_0_[4] ),
        .I2(\s21_reg_n_0_[3] ),
        .I3(s22[3]),
        .I4(\s2/s1/C_2 ),
        .O(ans2[4]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_25 
       (.I0(s12[4]),
        .I1(\s11_reg_n_0_[4] ),
        .I2(\s11_reg_n_0_[3] ),
        .I3(s12[3]),
        .I4(\s1/s1/C_2 ),
        .O(ans1[4]));
  LUT6 #(
    .INIT(64'h8A088A8A08088A08)) 
    \out[7]_i_26 
       (.I0(\s2/s2/f2/S ),
        .I1(\s21_reg_n_0_[4] ),
        .I2(s22[4]),
        .I3(\s21_reg_n_0_[3] ),
        .I4(s22[3]),
        .I5(\s2/s1/C_2 ),
        .O(\out[7]_i_26_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \out[7]_i_27 
       (.I0(\s21_reg_n_0_[5] ),
        .I1(s22[5]),
        .O(\out[7]_i_27_n_0 ));
  LUT6 #(
    .INIT(64'h8A088A8A08088A08)) 
    \out[7]_i_28 
       (.I0(\s1/s2/f2/S ),
        .I1(\s11_reg_n_0_[4] ),
        .I2(s12[4]),
        .I3(\s11_reg_n_0_[3] ),
        .I4(s12[3]),
        .I5(\s1/s1/C_2 ),
        .O(\out[7]_i_28_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \out[7]_i_29 
       (.I0(\s11_reg_n_0_[5] ),
        .I1(s12[5]),
        .O(\out[7]_i_29_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \out[7]_i_3 
       (.I0(reset),
        .O(\out[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_30 
       (.I0(\s21_reg_n_0_[5] ),
        .I1(s22[5]),
        .O(\s2/s2/f2/S ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_31 
       (.I0(\s11_reg_n_0_[5] ),
        .I1(s12[5]),
        .O(\s1/s2/f2/S ));
  LUT6 #(
    .INIT(64'h65A66565A6A665A6)) 
    \out[7]_i_4 
       (.I0(\s2/F6/S ),
        .I1(\s21_reg_n_0_[8] ),
        .I2(s22[8]),
        .I3(\s21_reg_n_0_[7] ),
        .I4(s22[7]),
        .I5(\s2/s2/C_2 ),
        .O(ans2[9]));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_5 
       (.I0(s12[7]),
        .I1(\s11_reg_n_0_[7] ),
        .I2(\s1/s2/C_2 ),
        .O(ans1[7]));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_6 
       (.I0(s22[7]),
        .I1(\s21_reg_n_0_[7] ),
        .I2(\s2/s2/C_2 ),
        .O(ans2[7]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[7]_i_7 
       (.I0(\final/C_4 ),
        .I1(ans2[5]),
        .I2(ans1[5]),
        .I3(ans2[6]),
        .I4(ans1[6]),
        .O(\final/C_6 ));
  LUT6 #(
    .INIT(64'h7DFFFF7DFF7DBEFF)) 
    \out[7]_i_8 
       (.I0(\s1/F6/S ),
        .I1(\s2/f5/S ),
        .I2(\s2/C2 ),
        .I3(s12[8]),
        .I4(\s11_reg_n_0_[8] ),
        .I5(\s1/C2 ),
        .O(\out[7]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_9 
       (.I0(\s21_reg_n_0_[9] ),
        .I1(s22[9]),
        .O(\s2/F6/S ));
  FDCE \out_reg[0] 
       (.C(clock),
        .CE(out),
        .CLR(\out[7]_i_3_n_0 ),
        .D(\out[0]_i_1_n_0 ),
        .Q(m_axis_tdata[0]));
  FDCE \out_reg[1] 
       (.C(clock),
        .CE(out),
        .CLR(\out[7]_i_3_n_0 ),
        .D(\out[1]_i_1_n_0 ),
        .Q(m_axis_tdata[1]));
  FDCE \out_reg[2] 
       (.C(clock),
        .CE(out),
        .CLR(\out[7]_i_3_n_0 ),
        .D(\out[2]_i_1_n_0 ),
        .Q(m_axis_tdata[2]));
  FDCE \out_reg[3] 
       (.C(clock),
        .CE(out),
        .CLR(\out[7]_i_3_n_0 ),
        .D(\out[3]_i_1_n_0 ),
        .Q(m_axis_tdata[3]));
  FDCE \out_reg[4] 
       (.C(clock),
        .CE(out),
        .CLR(\out[7]_i_3_n_0 ),
        .D(\out[4]_i_1_n_0 ),
        .Q(m_axis_tdata[4]));
  FDCE \out_reg[5] 
       (.C(clock),
        .CE(out),
        .CLR(\out[7]_i_3_n_0 ),
        .D(\out[5]_i_1_n_0 ),
        .Q(m_axis_tdata[5]));
  FDCE \out_reg[6] 
       (.C(clock),
        .CE(out),
        .CLR(\out[7]_i_3_n_0 ),
        .D(\out[6]_i_1_n_0 ),
        .Q(m_axis_tdata[6]));
  FDCE \out_reg[7] 
       (.C(clock),
        .CE(out),
        .CLR(\out[7]_i_3_n_0 ),
        .D(\out[7]_i_2_n_0 ),
        .Q(m_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[0]_i_1 
       (.I0(xreg2[0]),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(\xreg1_reg_n_0_[0] ),
        .O(\s11[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[1]_i_1 
       (.I0(xreg2[1]),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(\xreg1_reg_n_0_[1] ),
        .O(\s11[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[2]_i_1 
       (.I0(xreg2[2]),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(\xreg1_reg_n_0_[2] ),
        .O(\s11[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[3]_i_1 
       (.I0(xreg2[3]),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(\xreg1_reg_n_0_[3] ),
        .O(\s11[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[4]_i_1 
       (.I0(xreg2[4]),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(\xreg1_reg_n_0_[4] ),
        .O(\s11[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[5]_i_1 
       (.I0(xreg2[5]),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(\xreg1_reg_n_0_[5] ),
        .O(\s11[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[6]_i_1 
       (.I0(xreg2[6]),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(\xreg1_reg_n_0_[6] ),
        .O(\s11[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[7]_i_1 
       (.I0(xreg2[7]),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(\xreg1_reg_n_0_[7] ),
        .O(\s11[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[8]_i_1 
       (.I0(xreg2[8]),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(\xreg1_reg_n_0_[8] ),
        .O(\s11[8]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \s11[9]_i_1 
       (.I0(\xreg1_reg_n_0_[9] ),
        .I1(xreg2[9]),
        .O(\s11[9]_i_1_n_0 ));
  FDCE \s11_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s11[0]_i_1_n_0 ),
        .Q(\s11_reg_n_0_[0] ));
  FDCE \s11_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s11[1]_i_1_n_0 ),
        .Q(\s11_reg_n_0_[1] ));
  FDCE \s11_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s11[2]_i_1_n_0 ),
        .Q(\s11_reg_n_0_[2] ));
  FDCE \s11_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s11[3]_i_1_n_0 ),
        .Q(\s11_reg_n_0_[3] ));
  FDCE \s11_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s11[4]_i_1_n_0 ),
        .Q(\s11_reg_n_0_[4] ));
  FDCE \s11_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s11[5]_i_1_n_0 ),
        .Q(\s11_reg_n_0_[5] ));
  FDCE \s11_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s11[6]_i_1_n_0 ),
        .Q(\s11_reg_n_0_[6] ));
  FDCE \s11_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s11[7]_i_1_n_0 ),
        .Q(\s11_reg_n_0_[7] ));
  FDCE \s11_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s11[8]_i_1_n_0 ),
        .Q(\s11_reg_n_0_[8] ));
  FDCE \s11_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s11[9]_i_1_n_0 ),
        .Q(\s11_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[0]_i_1 
       (.I0(\xreg1_reg_n_0_[0] ),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(xreg2[0]),
        .O(\s12[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[1]_i_1 
       (.I0(\xreg1_reg_n_0_[1] ),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(xreg2[1]),
        .O(\s12[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[2]_i_1 
       (.I0(\xreg1_reg_n_0_[2] ),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(xreg2[2]),
        .O(\s12[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[3]_i_1 
       (.I0(\xreg1_reg_n_0_[3] ),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(xreg2[3]),
        .O(\s12[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[4]_i_1 
       (.I0(\xreg1_reg_n_0_[4] ),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(xreg2[4]),
        .O(\s12[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[5]_i_1 
       (.I0(\xreg1_reg_n_0_[5] ),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(xreg2[5]),
        .O(\s12[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[6]_i_1 
       (.I0(\xreg1_reg_n_0_[6] ),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(xreg2[6]),
        .O(\s12[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[7]_i_1 
       (.I0(\xreg1_reg_n_0_[7] ),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(xreg2[7]),
        .O(\s12[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[8]_i_1 
       (.I0(\xreg1_reg_n_0_[8] ),
        .I1(\s12[8]_i_2_n_0 ),
        .I2(xreg2[8]),
        .O(\s12[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'hB200FFB2)) 
    \s12[8]_i_2 
       (.I0(\s12[8]_i_3_n_0 ),
        .I1(\xreg1_reg_n_0_[8] ),
        .I2(xreg2[8]),
        .I3(xreg2[9]),
        .I4(\xreg1_reg_n_0_[9] ),
        .O(\s12[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h7050FFFF)) 
    \s12[8]_i_3 
       (.I0(\s12[8]_i_4_n_0 ),
        .I1(\s12[8]_i_5_n_0 ),
        .I2(\s12[8]_i_6_n_0 ),
        .I3(\s12[8]_i_7_n_0 ),
        .I4(\s12[8]_i_8_n_0 ),
        .O(\s12[8]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s12[8]_i_4 
       (.I0(\xreg1_reg_n_0_[4] ),
        .I1(xreg2[4]),
        .I2(xreg2[5]),
        .I3(\xreg1_reg_n_0_[5] ),
        .O(\s12[8]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h2F22BF2F)) 
    \s12[8]_i_5 
       (.I0(\xreg1_reg_n_0_[3] ),
        .I1(xreg2[3]),
        .I2(\s12[8]_i_9_n_0 ),
        .I3(\xreg1_reg_n_0_[2] ),
        .I4(xreg2[2]),
        .O(\s12[8]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'hBB2B)) 
    \s12[8]_i_6 
       (.I0(xreg2[7]),
        .I1(\xreg1_reg_n_0_[7] ),
        .I2(\xreg1_reg_n_0_[6] ),
        .I3(xreg2[6]),
        .O(\s12[8]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s12[8]_i_7 
       (.I0(xreg2[4]),
        .I1(\xreg1_reg_n_0_[4] ),
        .I2(\xreg1_reg_n_0_[5] ),
        .I3(xreg2[5]),
        .O(\s12[8]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'hB0FB)) 
    \s12[8]_i_8 
       (.I0(\xreg1_reg_n_0_[6] ),
        .I1(xreg2[6]),
        .I2(\xreg1_reg_n_0_[7] ),
        .I3(xreg2[7]),
        .O(\s12[8]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h40F440F4FFFF40F4)) 
    \s12[8]_i_9 
       (.I0(\xreg1_reg_n_0_[0] ),
        .I1(xreg2[0]),
        .I2(xreg2[1]),
        .I3(\xreg1_reg_n_0_[1] ),
        .I4(xreg2[3]),
        .I5(\xreg1_reg_n_0_[3] ),
        .O(\s12[8]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'h0000DF00)) 
    \s12[9]_i_1 
       (.I0(\full[0]_0 [1]),
        .I1(m_axis_tready),
        .I2(\m_axis_tkeep[0] ),
        .I3(\full[0]_0 [0]),
        .I4(hold),
        .O(s21));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s12[9]_i_2 
       (.I0(\xreg1_reg_n_0_[9] ),
        .I1(xreg2[9]),
        .O(\s12[9]_i_2_n_0 ));
  FDCE \s12_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s12[0]_i_1_n_0 ),
        .Q(s12[0]));
  FDCE \s12_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s12[1]_i_1_n_0 ),
        .Q(s12[1]));
  FDCE \s12_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s12[2]_i_1_n_0 ),
        .Q(s12[2]));
  FDCE \s12_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s12[3]_i_1_n_0 ),
        .Q(s12[3]));
  FDCE \s12_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s12[4]_i_1_n_0 ),
        .Q(s12[4]));
  FDCE \s12_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s12[5]_i_1_n_0 ),
        .Q(s12[5]));
  FDCE \s12_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s12[6]_i_1_n_0 ),
        .Q(s12[6]));
  FDCE \s12_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s12[7]_i_1_n_0 ),
        .Q(s12[7]));
  FDCE \s12_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s12[8]_i_1_n_0 ),
        .Q(s12[8]));
  FDCE \s12_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s12[9]_i_2_n_0 ),
        .Q(s12[9]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[0]_i_1 
       (.I0(yreg2[0]),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(\yreg1_reg_n_0_[0] ),
        .O(\s21[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[1]_i_1 
       (.I0(yreg2[1]),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(\yreg1_reg_n_0_[1] ),
        .O(\s21[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[2]_i_1 
       (.I0(yreg2[2]),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(\yreg1_reg_n_0_[2] ),
        .O(\s21[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[3]_i_1 
       (.I0(yreg2[3]),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(\yreg1_reg_n_0_[3] ),
        .O(\s21[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[4]_i_1 
       (.I0(yreg2[4]),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(\yreg1_reg_n_0_[4] ),
        .O(\s21[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[5]_i_1 
       (.I0(yreg2[5]),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(\yreg1_reg_n_0_[5] ),
        .O(\s21[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[6]_i_1 
       (.I0(yreg2[6]),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(\yreg1_reg_n_0_[6] ),
        .O(\s21[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[7]_i_1 
       (.I0(yreg2[7]),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(\yreg1_reg_n_0_[7] ),
        .O(\s21[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[8]_i_1 
       (.I0(yreg2[8]),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(\yreg1_reg_n_0_[8] ),
        .O(\s21[8]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \s21[9]_i_1 
       (.I0(\yreg1_reg_n_0_[9] ),
        .I1(yreg2[9]),
        .O(\s21[9]_i_1_n_0 ));
  FDCE \s21_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(\out[7]_i_3_n_0 ),
        .D(\s21[0]_i_1_n_0 ),
        .Q(\s21_reg_n_0_[0] ));
  FDCE \s21_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(\out[7]_i_3_n_0 ),
        .D(\s21[1]_i_1_n_0 ),
        .Q(\s21_reg_n_0_[1] ));
  FDCE \s21_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s21[2]_i_1_n_0 ),
        .Q(\s21_reg_n_0_[2] ));
  FDCE \s21_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s21[3]_i_1_n_0 ),
        .Q(\s21_reg_n_0_[3] ));
  FDCE \s21_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s21[4]_i_1_n_0 ),
        .Q(\s21_reg_n_0_[4] ));
  FDCE \s21_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s21[5]_i_1_n_0 ),
        .Q(\s21_reg_n_0_[5] ));
  FDCE \s21_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s21[6]_i_1_n_0 ),
        .Q(\s21_reg_n_0_[6] ));
  FDCE \s21_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s21[7]_i_1_n_0 ),
        .Q(\s21_reg_n_0_[7] ));
  FDCE \s21_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s21[8]_i_1_n_0 ),
        .Q(\s21_reg_n_0_[8] ));
  FDCE \s21_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s21[9]_i_1_n_0 ),
        .Q(\s21_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[0]_i_1 
       (.I0(\yreg1_reg_n_0_[0] ),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(yreg2[0]),
        .O(\s22[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[1]_i_1 
       (.I0(\yreg1_reg_n_0_[1] ),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(yreg2[1]),
        .O(\s22[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[2]_i_1 
       (.I0(\yreg1_reg_n_0_[2] ),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(yreg2[2]),
        .O(\s22[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[3]_i_1 
       (.I0(\yreg1_reg_n_0_[3] ),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(yreg2[3]),
        .O(\s22[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[4]_i_1 
       (.I0(\yreg1_reg_n_0_[4] ),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(yreg2[4]),
        .O(\s22[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[5]_i_1 
       (.I0(\yreg1_reg_n_0_[5] ),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(yreg2[5]),
        .O(\s22[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[6]_i_1 
       (.I0(\yreg1_reg_n_0_[6] ),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(yreg2[6]),
        .O(\s22[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[7]_i_1 
       (.I0(\yreg1_reg_n_0_[7] ),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(yreg2[7]),
        .O(\s22[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[8]_i_1 
       (.I0(\yreg1_reg_n_0_[8] ),
        .I1(\s22[8]_i_2_n_0 ),
        .I2(yreg2[8]),
        .O(\s22[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'hB200FFB2)) 
    \s22[8]_i_2 
       (.I0(\s22[8]_i_3_n_0 ),
        .I1(\yreg1_reg_n_0_[8] ),
        .I2(yreg2[8]),
        .I3(yreg2[9]),
        .I4(\yreg1_reg_n_0_[9] ),
        .O(\s22[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h7050FFFF)) 
    \s22[8]_i_3 
       (.I0(\s22[8]_i_4_n_0 ),
        .I1(\s22[8]_i_5_n_0 ),
        .I2(\s22[8]_i_6_n_0 ),
        .I3(\s22[8]_i_7_n_0 ),
        .I4(\s22[8]_i_8_n_0 ),
        .O(\s22[8]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s22[8]_i_4 
       (.I0(\yreg1_reg_n_0_[4] ),
        .I1(yreg2[4]),
        .I2(yreg2[5]),
        .I3(\yreg1_reg_n_0_[5] ),
        .O(\s22[8]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h4D44DDDD)) 
    \s22[8]_i_5 
       (.I0(yreg2[3]),
        .I1(\yreg1_reg_n_0_[3] ),
        .I2(yreg2[2]),
        .I3(\yreg1_reg_n_0_[2] ),
        .I4(\s22[8]_i_9_n_0 ),
        .O(\s22[8]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'hBB2B)) 
    \s22[8]_i_6 
       (.I0(yreg2[7]),
        .I1(\yreg1_reg_n_0_[7] ),
        .I2(\yreg1_reg_n_0_[6] ),
        .I3(yreg2[6]),
        .O(\s22[8]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s22[8]_i_7 
       (.I0(yreg2[4]),
        .I1(\yreg1_reg_n_0_[4] ),
        .I2(\yreg1_reg_n_0_[5] ),
        .I3(yreg2[5]),
        .O(\s22[8]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'hB0FB)) 
    \s22[8]_i_8 
       (.I0(\yreg1_reg_n_0_[6] ),
        .I1(yreg2[6]),
        .I2(\yreg1_reg_n_0_[7] ),
        .I3(yreg2[7]),
        .O(\s22[8]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h40F440F4FFFF40F4)) 
    \s22[8]_i_9 
       (.I0(\yreg1_reg_n_0_[0] ),
        .I1(yreg2[0]),
        .I2(yreg2[1]),
        .I3(\yreg1_reg_n_0_[1] ),
        .I4(yreg2[2]),
        .I5(\yreg1_reg_n_0_[2] ),
        .O(\s22[8]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s22[9]_i_1 
       (.I0(\yreg1_reg_n_0_[9] ),
        .I1(yreg2[9]),
        .O(\s22[9]_i_1_n_0 ));
  FDCE \s22_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s22[0]_i_1_n_0 ),
        .Q(s22[0]));
  FDCE \s22_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s22[1]_i_1_n_0 ),
        .Q(s22[1]));
  FDCE \s22_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s22[2]_i_1_n_0 ),
        .Q(s22[2]));
  FDCE \s22_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s22[3]_i_1_n_0 ),
        .Q(s22[3]));
  FDCE \s22_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s22[4]_i_1_n_0 ),
        .Q(s22[4]));
  FDCE \s22_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s22[5]_i_1_n_0 ),
        .Q(s22[5]));
  FDCE \s22_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s22[6]_i_1_n_0 ),
        .Q(s22[6]));
  FDCE \s22_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s22[7]_i_1_n_0 ),
        .Q(s22[7]));
  FDCE \s22_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s22[8]_i_1_n_0 ),
        .Q(s22[8]));
  FDCE \s22_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\s22[9]_i_1_n_0 ),
        .Q(s22[9]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    s_axis_tready_INST_0
       (.I0(\full[0]_0 [0]),
        .I1(m_axis_tready),
        .I2(\full[0]_0 [1]),
        .I3(\m_axis_tkeep[0] ),
        .O(s_axis_tready));
  LUT5 #(
    .INIT(32'hBFFF0000)) 
    \temp[47]_i_1 
       (.I0(\s_axis_tstrb[9] ),
        .I1(s_axis_tstrb[5]),
        .I2(s_axis_tstrb[4]),
        .I3(s_axis_tstrb[3]),
        .I4(temp0),
        .O(\temp_reg[0] ));
  LUT2 #(
    .INIT(4'h6)) 
    \xreg1[0]_i_1 
       (.I0(Q[56]),
        .I1(Q[40]),
        .O(\x12/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \xreg1[1]_i_1 
       (.I0(Q[41]),
        .I1(Q[57]),
        .I2(Q[48]),
        .I3(Q[40]),
        .I4(Q[56]),
        .O(outx12[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \xreg1[2]_i_1 
       (.I0(Q[42]),
        .I1(Q[49]),
        .I2(Q[58]),
        .I3(Q[48]),
        .I4(Q[57]),
        .I5(\x12/C_1 ),
        .O(outx12[2]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \xreg1[2]_i_2 
       (.I0(Q[56]),
        .I1(Q[40]),
        .I2(Q[48]),
        .I3(Q[57]),
        .I4(Q[41]),
        .O(\x12/C_1 ));
  LUT3 #(
    .INIT(8'h96)) 
    \xreg1[3]_i_1 
       (.I0(Q[43]),
        .I1(outx11[3]),
        .I2(\x12/C_2 ),
        .O(outx12[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \xreg1[3]_i_2 
       (.I0(Q[50]),
        .I1(Q[59]),
        .I2(Q[49]),
        .I3(Q[58]),
        .I4(Q[48]),
        .I5(Q[57]),
        .O(outx11[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \xreg1[3]_i_3 
       (.I0(\x12/C_1 ),
        .I1(Q[57]),
        .I2(Q[48]),
        .I3(Q[58]),
        .I4(Q[49]),
        .I5(Q[42]),
        .O(\x12/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg1[4]_i_1 
       (.I0(Q[44]),
        .I1(Q[51]),
        .I2(Q[60]),
        .I3(\x11/C_3 ),
        .I4(\x12/C_3 ),
        .O(outx12[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \xreg1[5]_i_1 
       (.I0(\x12/C_3 ),
        .I1(\x11/C_3 ),
        .I2(Q[60]),
        .I3(Q[51]),
        .I4(Q[44]),
        .I5(\x12/f6/S ),
        .O(outx12[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg1[5]_i_2 
       (.I0(\x12/C_1 ),
        .I1(outx11[2]),
        .I2(Q[42]),
        .I3(outx11[3]),
        .I4(Q[43]),
        .O(\x12/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \xreg1[5]_i_3 
       (.I0(Q[57]),
        .I1(Q[48]),
        .I2(Q[58]),
        .I3(Q[49]),
        .I4(Q[59]),
        .I5(Q[50]),
        .O(\x11/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg1[5]_i_4 
       (.I0(\x11/C_3 ),
        .I1(Q[60]),
        .I2(Q[51]),
        .I3(Q[61]),
        .I4(Q[52]),
        .I5(Q[45]),
        .O(\x12/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \xreg1[5]_i_5 
       (.I0(Q[49]),
        .I1(Q[58]),
        .I2(Q[48]),
        .I3(Q[57]),
        .O(outx11[2]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg1[6]_i_1 
       (.I0(Q[46]),
        .I1(Q[53]),
        .I2(Q[62]),
        .I3(\x11/C_5 ),
        .I4(\x12/C_5 ),
        .O(outx12[6]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg1[6]_i_2 
       (.I0(\x11/C_3 ),
        .I1(Q[60]),
        .I2(Q[51]),
        .I3(Q[61]),
        .I4(Q[52]),
        .O(\x11/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg1[6]_i_3 
       (.I0(\x12/C_3 ),
        .I1(outx11[4]),
        .I2(Q[44]),
        .I3(outx11[5]),
        .I4(Q[45]),
        .O(\x12/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \xreg1[6]_i_4 
       (.I0(Q[51]),
        .I1(Q[60]),
        .I2(\x11/C_3 ),
        .O(outx11[4]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \xreg1[6]_i_5 
       (.I0(Q[52]),
        .I1(Q[61]),
        .I2(Q[51]),
        .I3(Q[60]),
        .I4(\x11/C_3 ),
        .O(outx11[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \xreg1[7]_i_1 
       (.I0(\x12/f8/S ),
        .I1(\x12/C_6 ),
        .O(outx12[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg1[7]_i_2 
       (.I0(\x11/C_5 ),
        .I1(Q[62]),
        .I2(Q[53]),
        .I3(Q[63]),
        .I4(Q[54]),
        .I5(Q[47]),
        .O(\x12/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \xreg1[8]_i_1 
       (.I0(Q[55]),
        .I1(Q[47]),
        .I2(Q[54]),
        .I3(Q[63]),
        .I4(\x11/C_6 ),
        .I5(\x12/C_6 ),
        .O(outx12[8]));
  LUT6 #(
    .INIT(64'h00000000F7FF0000)) 
    \xreg1[9]_i_1 
       (.I0(\full[0]_0 [0]),
        .I1(\m_axis_tkeep[0] ),
        .I2(m_axis_tready),
        .I3(\full[0]_0 [1]),
        .I4(\valid_reg[0] ),
        .I5(hold),
        .O(xreg1));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \xreg1[9]_i_2 
       (.I0(\x12/C_6 ),
        .I1(\x11/C_6 ),
        .I2(Q[63]),
        .I3(Q[54]),
        .I4(Q[47]),
        .I5(Q[55]),
        .O(outx12[9]));
  LUT1 #(
    .INIT(2'h1)) 
    \xreg1[9]_i_3 
       (.I0(reset),
        .O(\xreg1_reg[8]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \xreg1[9]_i_4 
       (.I0(\x12/C_5 ),
        .I1(\x11/C_5 ),
        .I2(Q[62]),
        .I3(Q[53]),
        .I4(Q[46]),
        .O(\x12/C_6 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \xreg1[9]_i_5 
       (.I0(\x11/C_5 ),
        .I1(Q[62]),
        .I2(Q[53]),
        .O(\x11/C_6 ));
  FDCE \xreg1_reg[0] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(\x12/f1/S ),
        .Q(\xreg1_reg_n_0_[0] ));
  FDCE \xreg1_reg[1] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(outx12[1]),
        .Q(\xreg1_reg_n_0_[1] ));
  FDCE \xreg1_reg[2] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(outx12[2]),
        .Q(\xreg1_reg_n_0_[2] ));
  FDCE \xreg1_reg[3] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(outx12[3]),
        .Q(\xreg1_reg_n_0_[3] ));
  FDCE \xreg1_reg[4] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(outx12[4]),
        .Q(\xreg1_reg_n_0_[4] ));
  FDCE \xreg1_reg[5] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(outx12[5]),
        .Q(\xreg1_reg_n_0_[5] ));
  FDCE \xreg1_reg[6] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(outx12[6]),
        .Q(\xreg1_reg_n_0_[6] ));
  FDCE \xreg1_reg[7] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(outx12[7]),
        .Q(\xreg1_reg_n_0_[7] ));
  FDCE \xreg1_reg[8] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(outx12[8]),
        .Q(\xreg1_reg_n_0_[8] ));
  FDCE \xreg1_reg[9] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(outx12[9]),
        .Q(\xreg1_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \xreg2[0]_i_1__3 
       (.I0(Q[16]),
        .I1(Q[0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \xreg2[1]_i_1__3 
       (.I0(Q[1]),
        .I1(Q[17]),
        .I2(Q[8]),
        .I3(Q[0]),
        .I4(Q[16]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \xreg2[2]_i_1__3 
       (.I0(Q[2]),
        .I1(Q[9]),
        .I2(Q[18]),
        .I3(Q[8]),
        .I4(Q[17]),
        .I5(\c3/x12/C_1 ),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \xreg2[2]_i_2__3 
       (.I0(Q[16]),
        .I1(Q[0]),
        .I2(Q[8]),
        .I3(Q[17]),
        .I4(Q[1]),
        .O(\c3/x12/C_1 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \xreg2[3]_i_1__3 
       (.I0(Q[3]),
        .I1(\c3/outx11 [3]),
        .I2(\c3/x12/C_2 ),
        .O(D[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \xreg2[3]_i_2__3 
       (.I0(Q[10]),
        .I1(Q[19]),
        .I2(Q[9]),
        .I3(Q[18]),
        .I4(Q[8]),
        .I5(Q[17]),
        .O(\c3/outx11 [3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \xreg2[3]_i_3__3 
       (.I0(\c3/x12/C_1 ),
        .I1(Q[17]),
        .I2(Q[8]),
        .I3(Q[18]),
        .I4(Q[9]),
        .I5(Q[2]),
        .O(\c3/x12/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg2[4]_i_1__3 
       (.I0(Q[4]),
        .I1(Q[11]),
        .I2(Q[20]),
        .I3(\c3/x11/C_3 ),
        .I4(\c3/x12/C_3 ),
        .O(D[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \xreg2[5]_i_1__3 
       (.I0(\c3/x12/C_3 ),
        .I1(\c3/x11/C_3 ),
        .I2(Q[20]),
        .I3(Q[11]),
        .I4(Q[4]),
        .I5(\c3/x12/f6/S ),
        .O(D[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[5]_i_2__3 
       (.I0(\c3/x12/C_1 ),
        .I1(\c3/outx11 [2]),
        .I2(Q[2]),
        .I3(\c3/outx11 [3]),
        .I4(Q[3]),
        .O(\c3/x12/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \xreg2[5]_i_3__3 
       (.I0(Q[17]),
        .I1(Q[8]),
        .I2(Q[18]),
        .I3(Q[9]),
        .I4(Q[19]),
        .I5(Q[10]),
        .O(\c3/x11/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg2[5]_i_4__3 
       (.I0(\c3/x11/C_3 ),
        .I1(Q[20]),
        .I2(Q[11]),
        .I3(Q[21]),
        .I4(Q[12]),
        .I5(Q[5]),
        .O(\c3/x12/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \xreg2[5]_i_5__3 
       (.I0(Q[9]),
        .I1(Q[18]),
        .I2(Q[8]),
        .I3(Q[17]),
        .O(\c3/outx11 [2]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg2[6]_i_1__3 
       (.I0(Q[6]),
        .I1(Q[13]),
        .I2(Q[22]),
        .I3(\c3/x11/C_5 ),
        .I4(\c3/x12/C_5 ),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[6]_i_2__3 
       (.I0(\c3/x11/C_3 ),
        .I1(Q[20]),
        .I2(Q[11]),
        .I3(Q[21]),
        .I4(Q[12]),
        .O(\c3/x11/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[6]_i_3__3 
       (.I0(\c3/x12/C_3 ),
        .I1(\c3/outx11 [4]),
        .I2(Q[4]),
        .I3(\c3/outx11 [5]),
        .I4(Q[5]),
        .O(\c3/x12/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \xreg2[6]_i_4__3 
       (.I0(Q[11]),
        .I1(Q[20]),
        .I2(\c3/x11/C_3 ),
        .O(\c3/outx11 [4]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \xreg2[6]_i_5__3 
       (.I0(Q[12]),
        .I1(Q[21]),
        .I2(Q[11]),
        .I3(Q[20]),
        .I4(\c3/x11/C_3 ),
        .O(\c3/outx11 [5]));
  LUT2 #(
    .INIT(4'h6)) 
    \xreg2[7]_i_1__3 
       (.I0(\c3/x12/f8/S ),
        .I1(\c3/x12/C_6 ),
        .O(D[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg2[7]_i_2__3 
       (.I0(\c3/x11/C_5 ),
        .I1(Q[22]),
        .I2(Q[13]),
        .I3(Q[23]),
        .I4(Q[14]),
        .I5(Q[7]),
        .O(\c3/x12/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \xreg2[8]_i_1__3 
       (.I0(Q[15]),
        .I1(Q[7]),
        .I2(Q[14]),
        .I3(Q[23]),
        .I4(\c3/x11/C_6 ),
        .I5(\c3/x12/C_6 ),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \xreg2[9]_i_1__3 
       (.I0(\c3/x12/C_6 ),
        .I1(\c3/x11/C_6 ),
        .I2(Q[23]),
        .I3(Q[14]),
        .I4(Q[7]),
        .I5(Q[15]),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \xreg2[9]_i_2__3 
       (.I0(\c3/x12/C_5 ),
        .I1(\c3/x11/C_5 ),
        .I2(Q[22]),
        .I3(Q[13]),
        .I4(Q[6]),
        .O(\c3/x12/C_6 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \xreg2[9]_i_3__3 
       (.I0(\c3/x11/C_5 ),
        .I1(Q[22]),
        .I2(Q[13]),
        .O(\c3/x11/C_6 ));
  FDCE \xreg2_reg[0] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(D[0]),
        .Q(xreg2[0]));
  FDCE \xreg2_reg[1] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(D[1]),
        .Q(xreg2[1]));
  FDCE \xreg2_reg[2] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(D[2]),
        .Q(xreg2[2]));
  FDCE \xreg2_reg[3] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(D[3]),
        .Q(xreg2[3]));
  FDCE \xreg2_reg[4] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(D[4]),
        .Q(xreg2[4]));
  FDCE \xreg2_reg[5] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(D[5]),
        .Q(xreg2[5]));
  FDCE \xreg2_reg[6] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(D[6]),
        .Q(xreg2[6]));
  FDCE \xreg2_reg[7] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(D[7]),
        .Q(xreg2[7]));
  FDCE \xreg2_reg[8] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(D[8]),
        .Q(xreg2[8]));
  FDCE \xreg2_reg[9] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\xreg1_reg[8]_0 ),
        .D(D[9]),
        .Q(xreg2[9]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \yreg1[0]_i_1 
       (.I0(Q[56]),
        .I1(Q[16]),
        .O(\y12/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \yreg1[1]_i_1 
       (.I0(Q[17]),
        .I1(Q[57]),
        .I2(Q[32]),
        .I3(Q[16]),
        .I4(Q[56]),
        .O(outy12[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \yreg1[2]_i_1 
       (.I0(Q[18]),
        .I1(Q[33]),
        .I2(Q[58]),
        .I3(Q[32]),
        .I4(Q[57]),
        .I5(\y12/C_1 ),
        .O(outy12[2]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \yreg1[2]_i_2 
       (.I0(Q[56]),
        .I1(Q[16]),
        .I2(Q[32]),
        .I3(Q[57]),
        .I4(Q[17]),
        .O(\y12/C_1 ));
  LUT3 #(
    .INIT(8'h96)) 
    \yreg1[3]_i_1 
       (.I0(Q[19]),
        .I1(outy11[3]),
        .I2(\y12/C_2 ),
        .O(outy12[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \yreg1[3]_i_2 
       (.I0(Q[34]),
        .I1(Q[59]),
        .I2(Q[33]),
        .I3(Q[58]),
        .I4(Q[32]),
        .I5(Q[57]),
        .O(outy11[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \yreg1[3]_i_3 
       (.I0(\y12/C_1 ),
        .I1(Q[57]),
        .I2(Q[32]),
        .I3(Q[58]),
        .I4(Q[33]),
        .I5(Q[18]),
        .O(\y12/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg1[4]_i_1 
       (.I0(Q[20]),
        .I1(Q[35]),
        .I2(Q[60]),
        .I3(\y11/C_3 ),
        .I4(\y12/C_3 ),
        .O(outy12[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \yreg1[5]_i_1 
       (.I0(\y12/C_3 ),
        .I1(\y11/C_3 ),
        .I2(Q[60]),
        .I3(Q[35]),
        .I4(Q[20]),
        .I5(\y12/f6/S ),
        .O(outy12[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[5]_i_2 
       (.I0(\y12/C_1 ),
        .I1(outy11[2]),
        .I2(Q[18]),
        .I3(outy11[3]),
        .I4(Q[19]),
        .O(\y12/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \yreg1[5]_i_3 
       (.I0(Q[57]),
        .I1(Q[32]),
        .I2(Q[58]),
        .I3(Q[33]),
        .I4(Q[59]),
        .I5(Q[34]),
        .O(\y11/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg1[5]_i_4 
       (.I0(\y11/C_3 ),
        .I1(Q[60]),
        .I2(Q[35]),
        .I3(Q[61]),
        .I4(Q[36]),
        .I5(Q[21]),
        .O(\y12/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \yreg1[5]_i_5 
       (.I0(Q[33]),
        .I1(Q[58]),
        .I2(Q[32]),
        .I3(Q[57]),
        .O(outy11[2]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg1[6]_i_1 
       (.I0(Q[22]),
        .I1(Q[37]),
        .I2(Q[62]),
        .I3(\y11/C_5 ),
        .I4(\y12/C_5 ),
        .O(outy12[6]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[6]_i_2 
       (.I0(\y11/C_3 ),
        .I1(Q[60]),
        .I2(Q[35]),
        .I3(Q[61]),
        .I4(Q[36]),
        .O(\y11/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[6]_i_3 
       (.I0(\y12/C_3 ),
        .I1(outy11[4]),
        .I2(Q[20]),
        .I3(outy11[5]),
        .I4(Q[21]),
        .O(\y12/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg1[6]_i_4 
       (.I0(Q[35]),
        .I1(Q[60]),
        .I2(\y11/C_3 ),
        .O(outy11[4]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \yreg1[6]_i_5 
       (.I0(Q[36]),
        .I1(Q[61]),
        .I2(Q[35]),
        .I3(Q[60]),
        .I4(\y11/C_3 ),
        .O(outy11[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg1[7]_i_1 
       (.I0(\y12/f8/S ),
        .I1(\y12/C_6 ),
        .O(outy12[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg1[7]_i_2 
       (.I0(\y11/C_5 ),
        .I1(Q[62]),
        .I2(Q[37]),
        .I3(Q[63]),
        .I4(Q[38]),
        .I5(Q[23]),
        .O(\y12/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \yreg1[8]_i_1 
       (.I0(Q[39]),
        .I1(Q[23]),
        .I2(Q[38]),
        .I3(Q[63]),
        .I4(\y11/C_6 ),
        .I5(\y12/C_6 ),
        .O(outy12[8]));
  LUT6 #(
    .INIT(64'hF7FF000000000000)) 
    \yreg1[9]_i_1 
       (.I0(\full[0]_0 [0]),
        .I1(\m_axis_tkeep[0] ),
        .I2(m_axis_tready),
        .I3(\full[0]_0 [1]),
        .I4(\valid_reg[0] ),
        .I5(hold_reg),
        .O(yreg1));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \yreg1[9]_i_2 
       (.I0(\y12/C_6 ),
        .I1(\y11/C_6 ),
        .I2(Q[63]),
        .I3(Q[38]),
        .I4(Q[23]),
        .I5(Q[39]),
        .O(outy12[9]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \yreg1[9]_i_4 
       (.I0(\y12/C_5 ),
        .I1(\y11/C_5 ),
        .I2(Q[62]),
        .I3(Q[37]),
        .I4(Q[22]),
        .O(\y12/C_6 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \yreg1[9]_i_5 
       (.I0(\y11/C_5 ),
        .I1(Q[62]),
        .I2(Q[37]),
        .O(\y11/C_6 ));
  FDRE \yreg1_reg[0] 
       (.C(clock),
        .CE(yreg1),
        .D(\y12/f1/S ),
        .Q(\yreg1_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \yreg1_reg[1] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[1]),
        .Q(\yreg1_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \yreg1_reg[2] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[2]),
        .Q(\yreg1_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \yreg1_reg[3] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[3]),
        .Q(\yreg1_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \yreg1_reg[4] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[4]),
        .Q(\yreg1_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \yreg1_reg[5] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[5]),
        .Q(\yreg1_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \yreg1_reg[6] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[6]),
        .Q(\yreg1_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \yreg1_reg[7] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[7]),
        .Q(\yreg1_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \yreg1_reg[8] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[8]),
        .Q(\yreg1_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \yreg1_reg[9] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[9]),
        .Q(\yreg1_reg_n_0_[9] ),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg2[0]_i_1 
       (.I0(Q[40]),
        .I1(Q[0]),
        .O(\y22/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \yreg2[1]_i_1 
       (.I0(Q[1]),
        .I1(Q[41]),
        .I2(Q[24]),
        .I3(Q[0]),
        .I4(Q[40]),
        .O(outy22[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \yreg2[2]_i_1 
       (.I0(Q[2]),
        .I1(Q[25]),
        .I2(Q[42]),
        .I3(Q[24]),
        .I4(Q[41]),
        .I5(\y22/C_1 ),
        .O(outy22[2]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \yreg2[2]_i_2 
       (.I0(Q[40]),
        .I1(Q[0]),
        .I2(Q[24]),
        .I3(Q[41]),
        .I4(Q[1]),
        .O(\y22/C_1 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg2[3]_i_1 
       (.I0(Q[3]),
        .I1(outy21[3]),
        .I2(\y22/C_2 ),
        .O(outy22[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \yreg2[3]_i_2 
       (.I0(Q[26]),
        .I1(Q[43]),
        .I2(Q[25]),
        .I3(Q[42]),
        .I4(Q[24]),
        .I5(Q[41]),
        .O(outy21[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \yreg2[3]_i_3 
       (.I0(\y22/C_1 ),
        .I1(Q[41]),
        .I2(Q[24]),
        .I3(Q[42]),
        .I4(Q[25]),
        .I5(Q[2]),
        .O(\y22/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg2[4]_i_1 
       (.I0(Q[4]),
        .I1(Q[27]),
        .I2(Q[44]),
        .I3(\y21/C_3 ),
        .I4(\y22/C_3 ),
        .O(outy22[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \yreg2[5]_i_1 
       (.I0(\y22/C_3 ),
        .I1(\y21/C_3 ),
        .I2(Q[44]),
        .I3(Q[27]),
        .I4(Q[4]),
        .I5(\y22/f6/S ),
        .O(outy22[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[5]_i_2 
       (.I0(\y22/C_1 ),
        .I1(outy21[2]),
        .I2(Q[2]),
        .I3(outy21[3]),
        .I4(Q[3]),
        .O(\y22/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \yreg2[5]_i_3 
       (.I0(Q[41]),
        .I1(Q[24]),
        .I2(Q[42]),
        .I3(Q[25]),
        .I4(Q[43]),
        .I5(Q[26]),
        .O(\y21/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg2[5]_i_4 
       (.I0(\y21/C_3 ),
        .I1(Q[44]),
        .I2(Q[27]),
        .I3(Q[45]),
        .I4(Q[28]),
        .I5(Q[5]),
        .O(\y22/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \yreg2[5]_i_5 
       (.I0(Q[25]),
        .I1(Q[42]),
        .I2(Q[24]),
        .I3(Q[41]),
        .O(outy21[2]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg2[6]_i_1 
       (.I0(Q[6]),
        .I1(Q[29]),
        .I2(Q[46]),
        .I3(\y21/C_5 ),
        .I4(\y22/C_5 ),
        .O(outy22[6]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[6]_i_2 
       (.I0(\y21/C_3 ),
        .I1(Q[44]),
        .I2(Q[27]),
        .I3(Q[45]),
        .I4(Q[28]),
        .O(\y21/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[6]_i_3 
       (.I0(\y22/C_3 ),
        .I1(outy21[4]),
        .I2(Q[4]),
        .I3(outy21[5]),
        .I4(Q[5]),
        .O(\y22/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg2[6]_i_4 
       (.I0(Q[27]),
        .I1(Q[44]),
        .I2(\y21/C_3 ),
        .O(outy21[4]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \yreg2[6]_i_5 
       (.I0(Q[28]),
        .I1(Q[45]),
        .I2(Q[27]),
        .I3(Q[44]),
        .I4(\y21/C_3 ),
        .O(outy21[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg2[7]_i_1 
       (.I0(\y22/f8/S ),
        .I1(\y22/C_6 ),
        .O(outy22[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg2[7]_i_2 
       (.I0(\y21/C_5 ),
        .I1(Q[46]),
        .I2(Q[29]),
        .I3(Q[47]),
        .I4(Q[30]),
        .I5(Q[7]),
        .O(\y22/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \yreg2[8]_i_1 
       (.I0(Q[31]),
        .I1(Q[7]),
        .I2(Q[30]),
        .I3(Q[47]),
        .I4(\y21/C_6 ),
        .I5(\y22/C_6 ),
        .O(outy22[8]));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \yreg2[9]_i_1 
       (.I0(\y22/C_6 ),
        .I1(\y21/C_6 ),
        .I2(Q[47]),
        .I3(Q[30]),
        .I4(Q[7]),
        .I5(Q[31]),
        .O(outy22[9]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \yreg2[9]_i_2 
       (.I0(\y22/C_5 ),
        .I1(\y21/C_5 ),
        .I2(Q[46]),
        .I3(Q[29]),
        .I4(Q[6]),
        .O(\y22/C_6 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \yreg2[9]_i_3 
       (.I0(\y21/C_5 ),
        .I1(Q[46]),
        .I2(Q[29]),
        .O(\y21/C_6 ));
  FDRE \yreg2_reg[0] 
       (.C(clock),
        .CE(yreg1),
        .D(\y22/f1/S ),
        .Q(yreg2[0]),
        .R(1'b0));
  FDRE \yreg2_reg[1] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[1]),
        .Q(yreg2[1]),
        .R(1'b0));
  FDRE \yreg2_reg[2] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[2]),
        .Q(yreg2[2]),
        .R(1'b0));
  FDRE \yreg2_reg[3] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[3]),
        .Q(yreg2[3]),
        .R(1'b0));
  FDRE \yreg2_reg[4] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[4]),
        .Q(yreg2[4]),
        .R(1'b0));
  FDRE \yreg2_reg[5] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[5]),
        .Q(yreg2[5]),
        .R(1'b0));
  FDRE \yreg2_reg[6] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[6]),
        .Q(yreg2[6]),
        .R(1'b0));
  FDRE \yreg2_reg[7] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[7]),
        .Q(yreg2[7]),
        .R(1'b0));
  FDRE \yreg2_reg[8] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[8]),
        .Q(yreg2[8]),
        .R(1'b0));
  FDRE \yreg2_reg[9] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[9]),
        .Q(yreg2[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "convolution" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convolution_0
   (D,
    m_axis_tvalid,
    \m_axis_tkeep[1] ,
    m_axis_tdata,
    Q,
    \full_reg[2]_0 ,
    \full_reg[2]_1 ,
    \full_reg[2]_2 ,
    hold,
    \full_reg[2]_3 ,
    clock,
    reset_0,
    reset_1,
    reset,
    m_axis_tready,
    \valid_reg[1] ,
    hold_reg);
  output [9:0]D;
  output m_axis_tvalid;
  output \m_axis_tkeep[1] ;
  output [7:0]m_axis_tdata;
  input [63:0]Q;
  input \full_reg[2]_0 ;
  input \full_reg[2]_1 ;
  input \full_reg[2]_2 ;
  input hold;
  input \full_reg[2]_3 ;
  input clock;
  input reset_0;
  input reset_1;
  input reset;
  input m_axis_tready;
  input [0:0]\valid_reg[1] ;
  input hold_reg;

  wire [9:0]D;
  wire [63:0]Q;
  wire [7:1]ans1;
  wire [9:1]ans2;
  wire [5:2]\c4/outx11 ;
  wire \c4/x11/C_3 ;
  wire \c4/x11/C_5 ;
  wire \c4/x11/C_6 ;
  wire \c4/x12/C_1 ;
  wire \c4/x12/C_2 ;
  wire \c4/x12/C_3 ;
  wire \c4/x12/C_5 ;
  wire \c4/x12/C_6 ;
  wire \c4/x12/f6/S ;
  wire \c4/x12/f8/S ;
  wire clock;
  wire \final/C_1 ;
  wire \final/C_2 ;
  wire \final/C_4 ;
  wire \final/C_6 ;
  wire \final/f1/Ca ;
  wire \final/f2/S ;
  wire \final/f4/S ;
  wire \final/f5/S ;
  wire \final/f6/S ;
  wire \full[0]_i_1__0_n_0 ;
  wire \full[1]_i_1__0_n_0 ;
  wire \full[2]_i_1__0_n_0 ;
  wire \full_reg[2]_0 ;
  wire \full_reg[2]_1 ;
  wire \full_reg[2]_2 ;
  wire \full_reg[2]_3 ;
  wire \full_reg_n_0_[0] ;
  wire \full_reg_n_0_[1] ;
  wire hold;
  wire hold_reg;
  wire [7:0]m_axis_tdata;
  wire \m_axis_tkeep[1] ;
  wire m_axis_tready;
  wire m_axis_tvalid;
  wire out;
  wire \out[0]_i_1__0_n_0 ;
  wire \out[0]_i_2__0_n_0 ;
  wire \out[1]_i_1__0_n_0 ;
  wire \out[1]_i_2__0_n_0 ;
  wire \out[2]_i_1__0_n_0 ;
  wire \out[2]_i_2__0_n_0 ;
  wire \out[3]_i_1__0_n_0 ;
  wire \out[3]_i_2__0_n_0 ;
  wire \out[4]_i_1__0_n_0 ;
  wire \out[4]_i_2__0_n_0 ;
  wire \out[5]_i_1__0_n_0 ;
  wire \out[5]_i_2__0_n_0 ;
  wire \out[6]_i_1__0_n_0 ;
  wire \out[6]_i_2__0_n_0 ;
  wire \out[7]_i_25__0_n_0 ;
  wire \out[7]_i_26__0_n_0 ;
  wire \out[7]_i_27__0_n_0 ;
  wire \out[7]_i_28__0_n_0 ;
  wire \out[7]_i_2__0_n_0 ;
  wire \out[7]_i_7__0_n_0 ;
  wire [5:2]outx11;
  wire [9:1]outx12;
  wire [5:2]outy11;
  wire [9:1]outy12;
  wire [5:2]outy21;
  wire [9:1]outy22;
  wire reset;
  wire reset_0;
  wire reset_1;
  wire \s1/C2 ;
  wire \s1/F6/S ;
  wire \s1/s1/C_2 ;
  wire \s1/s2/C_0 ;
  wire \s1/s2/C_2 ;
  wire \s1/s2/f2/S ;
  wire \s11[0]_i_1__0_n_0 ;
  wire \s11[1]_i_1__0_n_0 ;
  wire \s11[2]_i_1__0_n_0 ;
  wire \s11[3]_i_1__0_n_0 ;
  wire \s11[4]_i_1__0_n_0 ;
  wire \s11[5]_i_1__0_n_0 ;
  wire \s11[6]_i_1__0_n_0 ;
  wire \s11[7]_i_1__0_n_0 ;
  wire \s11[8]_i_1__0_n_0 ;
  wire \s11[9]_i_1__0_n_0 ;
  wire \s11_reg_n_0_[0] ;
  wire \s11_reg_n_0_[1] ;
  wire \s11_reg_n_0_[2] ;
  wire \s11_reg_n_0_[3] ;
  wire \s11_reg_n_0_[4] ;
  wire \s11_reg_n_0_[5] ;
  wire \s11_reg_n_0_[6] ;
  wire \s11_reg_n_0_[7] ;
  wire \s11_reg_n_0_[8] ;
  wire \s11_reg_n_0_[9] ;
  wire [9:0]s12;
  wire \s12[0]_i_1__0_n_0 ;
  wire \s12[1]_i_1__0_n_0 ;
  wire \s12[2]_i_1__0_n_0 ;
  wire \s12[3]_i_1__0_n_0 ;
  wire \s12[4]_i_1__0_n_0 ;
  wire \s12[5]_i_1__0_n_0 ;
  wire \s12[6]_i_1__0_n_0 ;
  wire \s12[7]_i_1__0_n_0 ;
  wire \s12[8]_i_1__0_n_0 ;
  wire \s12[8]_i_2__0_n_0 ;
  wire \s12[8]_i_3__0_n_0 ;
  wire \s12[8]_i_4__0_n_0 ;
  wire \s12[8]_i_5__0_n_0 ;
  wire \s12[8]_i_6__0_n_0 ;
  wire \s12[8]_i_7__0_n_0 ;
  wire \s12[8]_i_8__0_n_0 ;
  wire \s12[8]_i_9__0_n_0 ;
  wire \s12[9]_i_2__0_n_0 ;
  wire \s2/C2 ;
  wire \s2/F6/S ;
  wire \s2/f5/S ;
  wire \s2/s1/C_2 ;
  wire \s2/s1/f1/S ;
  wire \s2/s2/C_0 ;
  wire \s2/s2/C_2 ;
  wire \s2/s2/f2/S ;
  wire s21;
  wire \s21[0]_i_1__0_n_0 ;
  wire \s21[1]_i_1__0_n_0 ;
  wire \s21[2]_i_1__0_n_0 ;
  wire \s21[3]_i_1__0_n_0 ;
  wire \s21[4]_i_1__0_n_0 ;
  wire \s21[5]_i_1__0_n_0 ;
  wire \s21[6]_i_1__0_n_0 ;
  wire \s21[7]_i_1__0_n_0 ;
  wire \s21[8]_i_1__0_n_0 ;
  wire \s21[9]_i_1__0_n_0 ;
  wire \s21_reg_n_0_[0] ;
  wire \s21_reg_n_0_[1] ;
  wire \s21_reg_n_0_[2] ;
  wire \s21_reg_n_0_[3] ;
  wire \s21_reg_n_0_[4] ;
  wire \s21_reg_n_0_[5] ;
  wire \s21_reg_n_0_[6] ;
  wire \s21_reg_n_0_[7] ;
  wire \s21_reg_n_0_[8] ;
  wire \s21_reg_n_0_[9] ;
  wire [9:0]s22;
  wire \s22[0]_i_1__0_n_0 ;
  wire \s22[1]_i_1__0_n_0 ;
  wire \s22[2]_i_1__0_n_0 ;
  wire \s22[3]_i_1__0_n_0 ;
  wire \s22[4]_i_1__0_n_0 ;
  wire \s22[5]_i_1__0_n_0 ;
  wire \s22[6]_i_1__0_n_0 ;
  wire \s22[7]_i_1__0_n_0 ;
  wire \s22[8]_i_1__0_n_0 ;
  wire \s22[8]_i_2__0_n_0 ;
  wire \s22[8]_i_3__0_n_0 ;
  wire \s22[8]_i_4__0_n_0 ;
  wire \s22[8]_i_5__0_n_0 ;
  wire \s22[8]_i_6__0_n_0 ;
  wire \s22[8]_i_7__0_n_0 ;
  wire \s22[8]_i_8__0_n_0 ;
  wire \s22[8]_i_9__0_n_0 ;
  wire \s22[9]_i_1__0_n_0 ;
  wire [0:0]\valid_reg[1] ;
  wire \x11/C_3 ;
  wire \x11/C_5 ;
  wire \x11/C_6 ;
  wire \x12/C_1 ;
  wire \x12/C_2 ;
  wire \x12/C_3 ;
  wire \x12/C_5 ;
  wire \x12/C_6 ;
  wire \x12/f1/S ;
  wire \x12/f6/S ;
  wire \x12/f8/S ;
  wire xreg1;
  wire \xreg1_reg_n_0_[0] ;
  wire \xreg1_reg_n_0_[1] ;
  wire \xreg1_reg_n_0_[2] ;
  wire \xreg1_reg_n_0_[3] ;
  wire \xreg1_reg_n_0_[4] ;
  wire \xreg1_reg_n_0_[5] ;
  wire \xreg1_reg_n_0_[6] ;
  wire \xreg1_reg_n_0_[7] ;
  wire \xreg1_reg_n_0_[8] ;
  wire \xreg1_reg_n_0_[9] ;
  wire [9:0]xreg2;
  wire \y11/C_3 ;
  wire \y11/C_5 ;
  wire \y11/C_6 ;
  wire \y12/C_1 ;
  wire \y12/C_2 ;
  wire \y12/C_3 ;
  wire \y12/C_5 ;
  wire \y12/C_6 ;
  wire \y12/f1/S ;
  wire \y12/f6/S ;
  wire \y12/f8/S ;
  wire \y21/C_3 ;
  wire \y21/C_5 ;
  wire \y21/C_6 ;
  wire \y22/C_1 ;
  wire \y22/C_2 ;
  wire \y22/C_3 ;
  wire \y22/C_5 ;
  wire \y22/C_6 ;
  wire \y22/f1/S ;
  wire \y22/f6/S ;
  wire \y22/f8/S ;
  wire yreg1;
  wire \yreg1_reg_n_0_[0] ;
  wire \yreg1_reg_n_0_[1] ;
  wire \yreg1_reg_n_0_[2] ;
  wire \yreg1_reg_n_0_[3] ;
  wire \yreg1_reg_n_0_[4] ;
  wire \yreg1_reg_n_0_[5] ;
  wire \yreg1_reg_n_0_[6] ;
  wire \yreg1_reg_n_0_[7] ;
  wire \yreg1_reg_n_0_[8] ;
  wire \yreg1_reg_n_0_[9] ;
  wire [9:0]yreg2;

  LUT6 #(
    .INIT(64'hEEFEEEEE44444444)) 
    \full[0]_i_1__0 
       (.I0(hold),
        .I1(\valid_reg[1] ),
        .I2(\full_reg_n_0_[1] ),
        .I3(m_axis_tready),
        .I4(\m_axis_tkeep[1] ),
        .I5(\full_reg_n_0_[0] ),
        .O(\full[0]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'hEEFE4444)) 
    \full[1]_i_1__0 
       (.I0(hold),
        .I1(\full_reg_n_0_[0] ),
        .I2(\m_axis_tkeep[1] ),
        .I3(m_axis_tready),
        .I4(\full_reg_n_0_[1] ),
        .O(\full[1]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'hFBFF4040)) 
    \full[2]_i_1__0 
       (.I0(hold),
        .I1(reset),
        .I2(\full_reg_n_0_[1] ),
        .I3(m_axis_tready),
        .I4(\m_axis_tkeep[1] ),
        .O(\full[2]_i_1__0_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[0] 
       (.C(clock),
        .CE(1'b1),
        .CLR(reset_0),
        .D(\full[0]_i_1__0_n_0 ),
        .Q(\full_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[1] 
       (.C(clock),
        .CE(1'b1),
        .CLR(reset_0),
        .D(\full[1]_i_1__0_n_0 ),
        .Q(\full_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[2] 
       (.C(clock),
        .CE(1'b1),
        .CLR(reset_0),
        .D(\full[2]_i_1__0_n_0 ),
        .Q(\m_axis_tkeep[1] ));
  LUT6 #(
    .INIT(64'h0000FFFF0000FFFE)) 
    m_axis_tvalid_INST_0
       (.I0(\m_axis_tkeep[1] ),
        .I1(\full_reg[2]_0 ),
        .I2(\full_reg[2]_1 ),
        .I3(\full_reg[2]_2 ),
        .I4(hold),
        .I5(\full_reg[2]_3 ),
        .O(m_axis_tvalid));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[0]_i_1__0 
       (.I0(\out[0]_i_2__0_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__0_n_0 ),
        .O(\out[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    \out[0]_i_2__0 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s11_reg_n_0_[0] ),
        .O(\out[0]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[1]_i_1__0 
       (.I0(\out[1]_i_2__0_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__0_n_0 ),
        .O(\out[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT5 #(
    .INIT(32'hAA9696AA)) 
    \out[1]_i_2__0 
       (.I0(\final/f2/S ),
        .I1(\s11_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s21_reg_n_0_[0] ),
        .I4(s22[0]),
        .O(\out[1]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT5 #(
    .INIT(32'hD22D2DD2)) 
    \out[1]_i_3__0 
       (.I0(s12[0]),
        .I1(\s11_reg_n_0_[0] ),
        .I2(\s11_reg_n_0_[1] ),
        .I3(s12[1]),
        .I4(ans2[1]),
        .O(\final/f2/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[2]_i_1__0 
       (.I0(\out[2]_i_2__0_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__0_n_0 ),
        .O(\out[2]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \out[2]_i_2__0 
       (.I0(ans1[2]),
        .I1(ans2[2]),
        .I2(\final/C_1 ),
        .O(\out[2]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h6966996969666966)) 
    \out[2]_i_3__0 
       (.I0(s12[2]),
        .I1(\s11_reg_n_0_[2] ),
        .I2(\s11_reg_n_0_[1] ),
        .I3(s12[1]),
        .I4(\s11_reg_n_0_[0] ),
        .I5(s12[0]),
        .O(ans1[2]));
  LUT6 #(
    .INIT(64'h6966996969666966)) 
    \out[2]_i_4__0 
       (.I0(s22[2]),
        .I1(\s21_reg_n_0_[2] ),
        .I2(\s21_reg_n_0_[1] ),
        .I3(s22[1]),
        .I4(\s21_reg_n_0_[0] ),
        .I5(s22[0]),
        .O(ans2[2]));
  LUT6 #(
    .INIT(64'h0CC0D44D4DD40CC0)) 
    \out[2]_i_5__0 
       (.I0(\s2/s1/f1/S ),
        .I1(ans2[1]),
        .I2(s12[1]),
        .I3(\s11_reg_n_0_[1] ),
        .I4(\s11_reg_n_0_[0] ),
        .I5(s12[0]),
        .O(\final/C_1 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \out[2]_i_6__0 
       (.I0(\s21_reg_n_0_[0] ),
        .I1(s22[0]),
        .O(\s2/s1/f1/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[3]_i_1__0 
       (.I0(\out[3]_i_2__0_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__0_n_0 ),
        .O(\out[3]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out[3]_i_2__0 
       (.I0(\final/f4/S ),
        .I1(\final/C_2 ),
        .O(\out[3]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    \out[3]_i_3__0 
       (.I0(\s1/s1/C_2 ),
        .I1(\s11_reg_n_0_[3] ),
        .I2(s12[3]),
        .I3(\s2/s1/C_2 ),
        .I4(\s21_reg_n_0_[3] ),
        .I5(s22[3]),
        .O(\final/f4/S ));
  LUT6 #(
    .INIT(64'hDF0DFFFF0000DF0D)) 
    \out[4]_i_10__0 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s22[1]),
        .I3(\s21_reg_n_0_[1] ),
        .I4(s22[2]),
        .I5(\s21_reg_n_0_[2] ),
        .O(\s2/s1/C_2 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[4]_i_1__0 
       (.I0(\out[4]_i_2__0_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__0_n_0 ),
        .O(\out[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h711717718EE8E88E)) 
    \out[4]_i_2__0 
       (.I0(\final/C_2 ),
        .I1(ans2[3]),
        .I2(s12[3]),
        .I3(\s11_reg_n_0_[3] ),
        .I4(\s1/s1/C_2 ),
        .I5(\final/f5/S ),
        .O(\out[4]_i_2__0_n_0 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[4]_i_3__0 
       (.I0(\final/f1/Ca ),
        .I1(ans2[1]),
        .I2(ans1[1]),
        .I3(ans2[2]),
        .I4(ans1[2]),
        .O(\final/C_2 ));
  LUT3 #(
    .INIT(8'h69)) 
    \out[4]_i_4__0 
       (.I0(s22[3]),
        .I1(\s21_reg_n_0_[3] ),
        .I2(\s2/s1/C_2 ),
        .O(ans2[3]));
  LUT6 #(
    .INIT(64'hDF0DFFFF0000DF0D)) 
    \out[4]_i_5__0 
       (.I0(s12[0]),
        .I1(\s11_reg_n_0_[0] ),
        .I2(s12[1]),
        .I3(\s11_reg_n_0_[1] ),
        .I4(s12[2]),
        .I5(\s11_reg_n_0_[2] ),
        .O(\s1/s1/C_2 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \out[4]_i_6__0 
       (.I0(\s1/s1/C_2 ),
        .I1(s12[3]),
        .I2(\s11_reg_n_0_[3] ),
        .I3(\s11_reg_n_0_[4] ),
        .I4(s12[4]),
        .I5(ans2[4]),
        .O(\final/f5/S ));
  LUT4 #(
    .INIT(16'h0660)) 
    \out[4]_i_7__0 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s11_reg_n_0_[0] ),
        .O(\final/f1/Ca ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT4 #(
    .INIT(16'h6966)) 
    \out[4]_i_8__0 
       (.I0(s22[1]),
        .I1(\s21_reg_n_0_[1] ),
        .I2(\s21_reg_n_0_[0] ),
        .I3(s22[0]),
        .O(ans2[1]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT4 #(
    .INIT(16'h6966)) 
    \out[4]_i_9__0 
       (.I0(s12[1]),
        .I1(\s11_reg_n_0_[1] ),
        .I2(\s11_reg_n_0_[0] ),
        .I3(s12[0]),
        .O(ans1[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[5]_i_1__0 
       (.I0(\out[5]_i_2__0_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__0_n_0 ),
        .O(\out[5]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out[5]_i_2__0 
       (.I0(\final/f6/S ),
        .I1(\final/C_4 ),
        .O(\out[5]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    \out[5]_i_3__0 
       (.I0(\s1/s2/C_0 ),
        .I1(\s11_reg_n_0_[5] ),
        .I2(s12[5]),
        .I3(\s2/s2/C_0 ),
        .I4(\s21_reg_n_0_[5] ),
        .I5(s22[5]),
        .O(\final/f6/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[6]_i_1__0 
       (.I0(\out[6]_i_2__0_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__0_n_0 ),
        .O(\out[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \out[6]_i_2__0 
       (.I0(ans2[6]),
        .I1(ans1[6]),
        .I2(ans1[5]),
        .I3(ans2[5]),
        .I4(\final/C_4 ),
        .O(\out[6]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_10__0 
       (.I0(\s1/s2/C_0 ),
        .I1(s12[5]),
        .I2(\s11_reg_n_0_[5] ),
        .I3(s12[6]),
        .I4(\s11_reg_n_0_[6] ),
        .O(\s1/s2/C_2 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[7]_i_11__0 
       (.I0(\final/C_2 ),
        .I1(ans2[3]),
        .I2(ans1[3]),
        .I3(ans2[4]),
        .I4(ans1[4]),
        .O(\final/C_4 ));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_12__0 
       (.I0(s22[5]),
        .I1(\s21_reg_n_0_[5] ),
        .I2(\s2/s2/C_0 ),
        .O(ans2[5]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_13__0 
       (.I0(s12[5]),
        .I1(\s11_reg_n_0_[5] ),
        .I2(\s1/s2/C_0 ),
        .O(ans1[5]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_14__0 
       (.I0(s22[6]),
        .I1(\s21_reg_n_0_[6] ),
        .I2(\s21_reg_n_0_[5] ),
        .I3(s22[5]),
        .I4(\s2/s2/C_0 ),
        .O(ans2[6]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_15__0 
       (.I0(s12[6]),
        .I1(\s11_reg_n_0_[6] ),
        .I2(\s11_reg_n_0_[5] ),
        .I3(s12[5]),
        .I4(\s1/s2/C_0 ),
        .O(ans1[6]));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_16__0 
       (.I0(\s11_reg_n_0_[9] ),
        .I1(s12[9]),
        .O(\s1/F6/S ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_17__0 
       (.I0(\s21_reg_n_0_[8] ),
        .I1(s22[8]),
        .O(\s2/f5/S ));
  LUT6 #(
    .INIT(64'hEF0EFFFF0000EF0E)) 
    \out[7]_i_18__0 
       (.I0(\out[7]_i_25__0_n_0 ),
        .I1(\out[7]_i_26__0_n_0 ),
        .I2(s22[6]),
        .I3(\s21_reg_n_0_[6] ),
        .I4(s22[7]),
        .I5(\s21_reg_n_0_[7] ),
        .O(\s2/C2 ));
  LUT6 #(
    .INIT(64'hEF0EFFFF0000EF0E)) 
    \out[7]_i_19__0 
       (.I0(\out[7]_i_27__0_n_0 ),
        .I1(\out[7]_i_28__0_n_0 ),
        .I2(s12[6]),
        .I3(\s11_reg_n_0_[6] ),
        .I4(s12[7]),
        .I5(\s11_reg_n_0_[7] ),
        .O(\s1/C2 ));
  LUT4 #(
    .INIT(16'h00D0)) 
    \out[7]_i_1__0 
       (.I0(\m_axis_tkeep[1] ),
        .I1(m_axis_tready),
        .I2(\full_reg_n_0_[1] ),
        .I3(hold),
        .O(out));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_20__0 
       (.I0(\s2/s1/C_2 ),
        .I1(s22[3]),
        .I2(\s21_reg_n_0_[3] ),
        .I3(s22[4]),
        .I4(\s21_reg_n_0_[4] ),
        .O(\s2/s2/C_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_21__0 
       (.I0(\s1/s1/C_2 ),
        .I1(s12[3]),
        .I2(\s11_reg_n_0_[3] ),
        .I3(s12[4]),
        .I4(\s11_reg_n_0_[4] ),
        .O(\s1/s2/C_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_22__0 
       (.I0(s12[3]),
        .I1(\s11_reg_n_0_[3] ),
        .I2(\s1/s1/C_2 ),
        .O(ans1[3]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_23__0 
       (.I0(s22[4]),
        .I1(\s21_reg_n_0_[4] ),
        .I2(\s21_reg_n_0_[3] ),
        .I3(s22[3]),
        .I4(\s2/s1/C_2 ),
        .O(ans2[4]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_24__0 
       (.I0(s12[4]),
        .I1(\s11_reg_n_0_[4] ),
        .I2(\s11_reg_n_0_[3] ),
        .I3(s12[3]),
        .I4(\s1/s1/C_2 ),
        .O(ans1[4]));
  LUT6 #(
    .INIT(64'h8A088A8A08088A08)) 
    \out[7]_i_25__0 
       (.I0(\s2/s2/f2/S ),
        .I1(\s21_reg_n_0_[4] ),
        .I2(s22[4]),
        .I3(\s21_reg_n_0_[3] ),
        .I4(s22[3]),
        .I5(\s2/s1/C_2 ),
        .O(\out[7]_i_25__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \out[7]_i_26__0 
       (.I0(\s21_reg_n_0_[5] ),
        .I1(s22[5]),
        .O(\out[7]_i_26__0_n_0 ));
  LUT6 #(
    .INIT(64'h8A088A8A08088A08)) 
    \out[7]_i_27__0 
       (.I0(\s1/s2/f2/S ),
        .I1(\s11_reg_n_0_[4] ),
        .I2(s12[4]),
        .I3(\s11_reg_n_0_[3] ),
        .I4(s12[3]),
        .I5(\s1/s1/C_2 ),
        .O(\out[7]_i_27__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \out[7]_i_28__0 
       (.I0(\s11_reg_n_0_[5] ),
        .I1(s12[5]),
        .O(\out[7]_i_28__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_29__0 
       (.I0(\s21_reg_n_0_[5] ),
        .I1(s22[5]),
        .O(\s2/s2/f2/S ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \out[7]_i_2__0 
       (.I0(ans2[9]),
        .I1(ans1[7]),
        .I2(ans2[7]),
        .I3(\final/C_6 ),
        .I4(\out[7]_i_7__0_n_0 ),
        .O(\out[7]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_30__0 
       (.I0(\s11_reg_n_0_[5] ),
        .I1(s12[5]),
        .O(\s1/s2/f2/S ));
  LUT6 #(
    .INIT(64'h65A66565A6A665A6)) 
    \out[7]_i_3__2 
       (.I0(\s2/F6/S ),
        .I1(\s21_reg_n_0_[8] ),
        .I2(s22[8]),
        .I3(\s21_reg_n_0_[7] ),
        .I4(s22[7]),
        .I5(\s2/s2/C_2 ),
        .O(ans2[9]));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_4__0 
       (.I0(s12[7]),
        .I1(\s11_reg_n_0_[7] ),
        .I2(\s1/s2/C_2 ),
        .O(ans1[7]));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_5__0 
       (.I0(s22[7]),
        .I1(\s21_reg_n_0_[7] ),
        .I2(\s2/s2/C_2 ),
        .O(ans2[7]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[7]_i_6__0 
       (.I0(\final/C_4 ),
        .I1(ans2[5]),
        .I2(ans1[5]),
        .I3(ans2[6]),
        .I4(ans1[6]),
        .O(\final/C_6 ));
  LUT6 #(
    .INIT(64'h7DFFFF7DFF7DBEFF)) 
    \out[7]_i_7__0 
       (.I0(\s1/F6/S ),
        .I1(\s2/f5/S ),
        .I2(\s2/C2 ),
        .I3(s12[8]),
        .I4(\s11_reg_n_0_[8] ),
        .I5(\s1/C2 ),
        .O(\out[7]_i_7__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_8__0 
       (.I0(\s21_reg_n_0_[9] ),
        .I1(s22[9]),
        .O(\s2/F6/S ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_9__0 
       (.I0(\s2/s2/C_0 ),
        .I1(s22[5]),
        .I2(\s21_reg_n_0_[5] ),
        .I3(s22[6]),
        .I4(\s21_reg_n_0_[6] ),
        .O(\s2/s2/C_2 ));
  FDCE \out_reg[0] 
       (.C(clock),
        .CE(out),
        .CLR(reset_0),
        .D(\out[0]_i_1__0_n_0 ),
        .Q(m_axis_tdata[0]));
  FDCE \out_reg[1] 
       (.C(clock),
        .CE(out),
        .CLR(reset_0),
        .D(\out[1]_i_1__0_n_0 ),
        .Q(m_axis_tdata[1]));
  FDCE \out_reg[2] 
       (.C(clock),
        .CE(out),
        .CLR(reset_0),
        .D(\out[2]_i_1__0_n_0 ),
        .Q(m_axis_tdata[2]));
  FDCE \out_reg[3] 
       (.C(clock),
        .CE(out),
        .CLR(reset_0),
        .D(\out[3]_i_1__0_n_0 ),
        .Q(m_axis_tdata[3]));
  FDCE \out_reg[4] 
       (.C(clock),
        .CE(out),
        .CLR(reset_0),
        .D(\out[4]_i_1__0_n_0 ),
        .Q(m_axis_tdata[4]));
  FDCE \out_reg[5] 
       (.C(clock),
        .CE(out),
        .CLR(reset_0),
        .D(\out[5]_i_1__0_n_0 ),
        .Q(m_axis_tdata[5]));
  FDCE \out_reg[6] 
       (.C(clock),
        .CE(out),
        .CLR(reset_0),
        .D(\out[6]_i_1__0_n_0 ),
        .Q(m_axis_tdata[6]));
  FDCE \out_reg[7] 
       (.C(clock),
        .CE(out),
        .CLR(reset_0),
        .D(\out[7]_i_2__0_n_0 ),
        .Q(m_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[0]_i_1__0 
       (.I0(xreg2[0]),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(\xreg1_reg_n_0_[0] ),
        .O(\s11[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[1]_i_1__0 
       (.I0(xreg2[1]),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(\xreg1_reg_n_0_[1] ),
        .O(\s11[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[2]_i_1__0 
       (.I0(xreg2[2]),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(\xreg1_reg_n_0_[2] ),
        .O(\s11[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[3]_i_1__0 
       (.I0(xreg2[3]),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(\xreg1_reg_n_0_[3] ),
        .O(\s11[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[4]_i_1__0 
       (.I0(xreg2[4]),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(\xreg1_reg_n_0_[4] ),
        .O(\s11[4]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[5]_i_1__0 
       (.I0(xreg2[5]),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(\xreg1_reg_n_0_[5] ),
        .O(\s11[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[6]_i_1__0 
       (.I0(xreg2[6]),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(\xreg1_reg_n_0_[6] ),
        .O(\s11[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[7]_i_1__0 
       (.I0(xreg2[7]),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(\xreg1_reg_n_0_[7] ),
        .O(\s11[7]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[8]_i_1__0 
       (.I0(xreg2[8]),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(\xreg1_reg_n_0_[8] ),
        .O(\s11[8]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \s11[9]_i_1__0 
       (.I0(\xreg1_reg_n_0_[9] ),
        .I1(xreg2[9]),
        .O(\s11[9]_i_1__0_n_0 ));
  FDCE \s11_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[0]_i_1__0_n_0 ),
        .Q(\s11_reg_n_0_[0] ));
  FDCE \s11_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[1]_i_1__0_n_0 ),
        .Q(\s11_reg_n_0_[1] ));
  FDCE \s11_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[2]_i_1__0_n_0 ),
        .Q(\s11_reg_n_0_[2] ));
  FDCE \s11_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[3]_i_1__0_n_0 ),
        .Q(\s11_reg_n_0_[3] ));
  FDCE \s11_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[4]_i_1__0_n_0 ),
        .Q(\s11_reg_n_0_[4] ));
  FDCE \s11_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s11[5]_i_1__0_n_0 ),
        .Q(\s11_reg_n_0_[5] ));
  FDCE \s11_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s11[6]_i_1__0_n_0 ),
        .Q(\s11_reg_n_0_[6] ));
  FDCE \s11_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s11[7]_i_1__0_n_0 ),
        .Q(\s11_reg_n_0_[7] ));
  FDCE \s11_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s11[8]_i_1__0_n_0 ),
        .Q(\s11_reg_n_0_[8] ));
  FDCE \s11_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s11[9]_i_1__0_n_0 ),
        .Q(\s11_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[0]_i_1__0 
       (.I0(\xreg1_reg_n_0_[0] ),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(xreg2[0]),
        .O(\s12[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[1]_i_1__0 
       (.I0(\xreg1_reg_n_0_[1] ),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(xreg2[1]),
        .O(\s12[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[2]_i_1__0 
       (.I0(\xreg1_reg_n_0_[2] ),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(xreg2[2]),
        .O(\s12[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[3]_i_1__0 
       (.I0(\xreg1_reg_n_0_[3] ),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(xreg2[3]),
        .O(\s12[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[4]_i_1__0 
       (.I0(\xreg1_reg_n_0_[4] ),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(xreg2[4]),
        .O(\s12[4]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[5]_i_1__0 
       (.I0(\xreg1_reg_n_0_[5] ),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(xreg2[5]),
        .O(\s12[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[6]_i_1__0 
       (.I0(\xreg1_reg_n_0_[6] ),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(xreg2[6]),
        .O(\s12[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[7]_i_1__0 
       (.I0(\xreg1_reg_n_0_[7] ),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(xreg2[7]),
        .O(\s12[7]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[8]_i_1__0 
       (.I0(\xreg1_reg_n_0_[8] ),
        .I1(\s12[8]_i_2__0_n_0 ),
        .I2(xreg2[8]),
        .O(\s12[8]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT5 #(
    .INIT(32'hB200FFB2)) 
    \s12[8]_i_2__0 
       (.I0(\s12[8]_i_3__0_n_0 ),
        .I1(\xreg1_reg_n_0_[8] ),
        .I2(xreg2[8]),
        .I3(xreg2[9]),
        .I4(\xreg1_reg_n_0_[9] ),
        .O(\s12[8]_i_2__0_n_0 ));
  LUT5 #(
    .INIT(32'h7050FFFF)) 
    \s12[8]_i_3__0 
       (.I0(\s12[8]_i_4__0_n_0 ),
        .I1(\s12[8]_i_5__0_n_0 ),
        .I2(\s12[8]_i_6__0_n_0 ),
        .I3(\s12[8]_i_7__0_n_0 ),
        .I4(\s12[8]_i_8__0_n_0 ),
        .O(\s12[8]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s12[8]_i_4__0 
       (.I0(\xreg1_reg_n_0_[4] ),
        .I1(xreg2[4]),
        .I2(xreg2[5]),
        .I3(\xreg1_reg_n_0_[5] ),
        .O(\s12[8]_i_4__0_n_0 ));
  LUT5 #(
    .INIT(32'h2F22BF2F)) 
    \s12[8]_i_5__0 
       (.I0(\xreg1_reg_n_0_[3] ),
        .I1(xreg2[3]),
        .I2(\s12[8]_i_9__0_n_0 ),
        .I3(\xreg1_reg_n_0_[2] ),
        .I4(xreg2[2]),
        .O(\s12[8]_i_5__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT4 #(
    .INIT(16'hBB2B)) 
    \s12[8]_i_6__0 
       (.I0(xreg2[7]),
        .I1(\xreg1_reg_n_0_[7] ),
        .I2(\xreg1_reg_n_0_[6] ),
        .I3(xreg2[6]),
        .O(\s12[8]_i_6__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s12[8]_i_7__0 
       (.I0(xreg2[4]),
        .I1(\xreg1_reg_n_0_[4] ),
        .I2(\xreg1_reg_n_0_[5] ),
        .I3(xreg2[5]),
        .O(\s12[8]_i_7__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT4 #(
    .INIT(16'hB0FB)) 
    \s12[8]_i_8__0 
       (.I0(\xreg1_reg_n_0_[6] ),
        .I1(xreg2[6]),
        .I2(\xreg1_reg_n_0_[7] ),
        .I3(xreg2[7]),
        .O(\s12[8]_i_8__0_n_0 ));
  LUT6 #(
    .INIT(64'h40F440F4FFFF40F4)) 
    \s12[8]_i_9__0 
       (.I0(\xreg1_reg_n_0_[0] ),
        .I1(xreg2[0]),
        .I2(xreg2[1]),
        .I3(\xreg1_reg_n_0_[1] ),
        .I4(xreg2[3]),
        .I5(\xreg1_reg_n_0_[3] ),
        .O(\s12[8]_i_9__0_n_0 ));
  LUT5 #(
    .INIT(32'h0000DF00)) 
    \s12[9]_i_1__0 
       (.I0(\full_reg_n_0_[1] ),
        .I1(m_axis_tready),
        .I2(\m_axis_tkeep[1] ),
        .I3(\full_reg_n_0_[0] ),
        .I4(hold),
        .O(s21));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s12[9]_i_2__0 
       (.I0(\xreg1_reg_n_0_[9] ),
        .I1(xreg2[9]),
        .O(\s12[9]_i_2__0_n_0 ));
  FDCE \s12_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[0]_i_1__0_n_0 ),
        .Q(s12[0]));
  FDCE \s12_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[1]_i_1__0_n_0 ),
        .Q(s12[1]));
  FDCE \s12_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[2]_i_1__0_n_0 ),
        .Q(s12[2]));
  FDCE \s12_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[3]_i_1__0_n_0 ),
        .Q(s12[3]));
  FDCE \s12_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[4]_i_1__0_n_0 ),
        .Q(s12[4]));
  FDCE \s12_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s12[5]_i_1__0_n_0 ),
        .Q(s12[5]));
  FDCE \s12_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s12[6]_i_1__0_n_0 ),
        .Q(s12[6]));
  FDCE \s12_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s12[7]_i_1__0_n_0 ),
        .Q(s12[7]));
  FDCE \s12_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s12[8]_i_1__0_n_0 ),
        .Q(s12[8]));
  FDCE \s12_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s12[9]_i_2__0_n_0 ),
        .Q(s12[9]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[0]_i_1__0 
       (.I0(yreg2[0]),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(\yreg1_reg_n_0_[0] ),
        .O(\s21[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[1]_i_1__0 
       (.I0(yreg2[1]),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(\yreg1_reg_n_0_[1] ),
        .O(\s21[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[2]_i_1__0 
       (.I0(yreg2[2]),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(\yreg1_reg_n_0_[2] ),
        .O(\s21[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[3]_i_1__0 
       (.I0(yreg2[3]),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(\yreg1_reg_n_0_[3] ),
        .O(\s21[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[4]_i_1__0 
       (.I0(yreg2[4]),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(\yreg1_reg_n_0_[4] ),
        .O(\s21[4]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[5]_i_1__0 
       (.I0(yreg2[5]),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(\yreg1_reg_n_0_[5] ),
        .O(\s21[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[6]_i_1__0 
       (.I0(yreg2[6]),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(\yreg1_reg_n_0_[6] ),
        .O(\s21[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[7]_i_1__0 
       (.I0(yreg2[7]),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(\yreg1_reg_n_0_[7] ),
        .O(\s21[7]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[8]_i_1__0 
       (.I0(yreg2[8]),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(\yreg1_reg_n_0_[8] ),
        .O(\s21[8]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \s21[9]_i_1__0 
       (.I0(\yreg1_reg_n_0_[9] ),
        .I1(yreg2[9]),
        .O(\s21[9]_i_1__0_n_0 ));
  FDCE \s21_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s21[0]_i_1__0_n_0 ),
        .Q(\s21_reg_n_0_[0] ));
  FDCE \s21_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s21[1]_i_1__0_n_0 ),
        .Q(\s21_reg_n_0_[1] ));
  FDCE \s21_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s21[2]_i_1__0_n_0 ),
        .Q(\s21_reg_n_0_[2] ));
  FDCE \s21_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s21[3]_i_1__0_n_0 ),
        .Q(\s21_reg_n_0_[3] ));
  FDCE \s21_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s21[4]_i_1__0_n_0 ),
        .Q(\s21_reg_n_0_[4] ));
  FDCE \s21_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s21[5]_i_1__0_n_0 ),
        .Q(\s21_reg_n_0_[5] ));
  FDCE \s21_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s21[6]_i_1__0_n_0 ),
        .Q(\s21_reg_n_0_[6] ));
  FDCE \s21_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s21[7]_i_1__0_n_0 ),
        .Q(\s21_reg_n_0_[7] ));
  FDCE \s21_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s21[8]_i_1__0_n_0 ),
        .Q(\s21_reg_n_0_[8] ));
  FDCE \s21_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s21[9]_i_1__0_n_0 ),
        .Q(\s21_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[0]_i_1__0 
       (.I0(\yreg1_reg_n_0_[0] ),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(yreg2[0]),
        .O(\s22[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[1]_i_1__0 
       (.I0(\yreg1_reg_n_0_[1] ),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(yreg2[1]),
        .O(\s22[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[2]_i_1__0 
       (.I0(\yreg1_reg_n_0_[2] ),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(yreg2[2]),
        .O(\s22[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[3]_i_1__0 
       (.I0(\yreg1_reg_n_0_[3] ),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(yreg2[3]),
        .O(\s22[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[4]_i_1__0 
       (.I0(\yreg1_reg_n_0_[4] ),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(yreg2[4]),
        .O(\s22[4]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[5]_i_1__0 
       (.I0(\yreg1_reg_n_0_[5] ),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(yreg2[5]),
        .O(\s22[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[6]_i_1__0 
       (.I0(\yreg1_reg_n_0_[6] ),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(yreg2[6]),
        .O(\s22[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[7]_i_1__0 
       (.I0(\yreg1_reg_n_0_[7] ),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(yreg2[7]),
        .O(\s22[7]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[8]_i_1__0 
       (.I0(\yreg1_reg_n_0_[8] ),
        .I1(\s22[8]_i_2__0_n_0 ),
        .I2(yreg2[8]),
        .O(\s22[8]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT5 #(
    .INIT(32'hB200FFB2)) 
    \s22[8]_i_2__0 
       (.I0(\s22[8]_i_3__0_n_0 ),
        .I1(\yreg1_reg_n_0_[8] ),
        .I2(yreg2[8]),
        .I3(yreg2[9]),
        .I4(\yreg1_reg_n_0_[9] ),
        .O(\s22[8]_i_2__0_n_0 ));
  LUT5 #(
    .INIT(32'h7050FFFF)) 
    \s22[8]_i_3__0 
       (.I0(\s22[8]_i_4__0_n_0 ),
        .I1(\s22[8]_i_5__0_n_0 ),
        .I2(\s22[8]_i_6__0_n_0 ),
        .I3(\s22[8]_i_7__0_n_0 ),
        .I4(\s22[8]_i_8__0_n_0 ),
        .O(\s22[8]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s22[8]_i_4__0 
       (.I0(\yreg1_reg_n_0_[4] ),
        .I1(yreg2[4]),
        .I2(yreg2[5]),
        .I3(\yreg1_reg_n_0_[5] ),
        .O(\s22[8]_i_4__0_n_0 ));
  LUT5 #(
    .INIT(32'h4D44DDDD)) 
    \s22[8]_i_5__0 
       (.I0(yreg2[3]),
        .I1(\yreg1_reg_n_0_[3] ),
        .I2(yreg2[2]),
        .I3(\yreg1_reg_n_0_[2] ),
        .I4(\s22[8]_i_9__0_n_0 ),
        .O(\s22[8]_i_5__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT4 #(
    .INIT(16'hBB2B)) 
    \s22[8]_i_6__0 
       (.I0(yreg2[7]),
        .I1(\yreg1_reg_n_0_[7] ),
        .I2(\yreg1_reg_n_0_[6] ),
        .I3(yreg2[6]),
        .O(\s22[8]_i_6__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s22[8]_i_7__0 
       (.I0(yreg2[4]),
        .I1(\yreg1_reg_n_0_[4] ),
        .I2(\yreg1_reg_n_0_[5] ),
        .I3(yreg2[5]),
        .O(\s22[8]_i_7__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT4 #(
    .INIT(16'hB0FB)) 
    \s22[8]_i_8__0 
       (.I0(\yreg1_reg_n_0_[6] ),
        .I1(yreg2[6]),
        .I2(\yreg1_reg_n_0_[7] ),
        .I3(yreg2[7]),
        .O(\s22[8]_i_8__0_n_0 ));
  LUT6 #(
    .INIT(64'h40F440F4FFFF40F4)) 
    \s22[8]_i_9__0 
       (.I0(\yreg1_reg_n_0_[0] ),
        .I1(yreg2[0]),
        .I2(yreg2[1]),
        .I3(\yreg1_reg_n_0_[1] ),
        .I4(yreg2[2]),
        .I5(\yreg1_reg_n_0_[2] ),
        .O(\s22[8]_i_9__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s22[9]_i_1__0 
       (.I0(\yreg1_reg_n_0_[9] ),
        .I1(yreg2[9]),
        .O(\s22[9]_i_1__0_n_0 ));
  FDCE \s22_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[0]_i_1__0_n_0 ),
        .Q(s22[0]));
  FDCE \s22_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[1]_i_1__0_n_0 ),
        .Q(s22[1]));
  FDCE \s22_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[2]_i_1__0_n_0 ),
        .Q(s22[2]));
  FDCE \s22_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[3]_i_1__0_n_0 ),
        .Q(s22[3]));
  FDCE \s22_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[4]_i_1__0_n_0 ),
        .Q(s22[4]));
  FDCE \s22_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s22[5]_i_1__0_n_0 ),
        .Q(s22[5]));
  FDCE \s22_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s22[6]_i_1__0_n_0 ),
        .Q(s22[6]));
  FDCE \s22_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s22[7]_i_1__0_n_0 ),
        .Q(s22[7]));
  FDCE \s22_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s22[8]_i_1__0_n_0 ),
        .Q(s22[8]));
  FDCE \s22_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_1),
        .D(\s22[9]_i_1__0_n_0 ),
        .Q(s22[9]));
  LUT2 #(
    .INIT(4'h6)) 
    \xreg1[0]_i_1__0 
       (.I0(Q[56]),
        .I1(Q[40]),
        .O(\x12/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \xreg1[1]_i_1__0 
       (.I0(Q[41]),
        .I1(Q[57]),
        .I2(Q[48]),
        .I3(Q[40]),
        .I4(Q[56]),
        .O(outx12[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \xreg1[2]_i_1__0 
       (.I0(Q[42]),
        .I1(Q[49]),
        .I2(Q[58]),
        .I3(Q[48]),
        .I4(Q[57]),
        .I5(\x12/C_1 ),
        .O(outx12[2]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \xreg1[2]_i_2__0 
       (.I0(Q[56]),
        .I1(Q[40]),
        .I2(Q[48]),
        .I3(Q[57]),
        .I4(Q[41]),
        .O(\x12/C_1 ));
  LUT3 #(
    .INIT(8'h96)) 
    \xreg1[3]_i_1__0 
       (.I0(Q[43]),
        .I1(outx11[3]),
        .I2(\x12/C_2 ),
        .O(outx12[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \xreg1[3]_i_2__0 
       (.I0(Q[50]),
        .I1(Q[59]),
        .I2(Q[49]),
        .I3(Q[58]),
        .I4(Q[48]),
        .I5(Q[57]),
        .O(outx11[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \xreg1[3]_i_3__0 
       (.I0(\x12/C_1 ),
        .I1(Q[57]),
        .I2(Q[48]),
        .I3(Q[58]),
        .I4(Q[49]),
        .I5(Q[42]),
        .O(\x12/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg1[4]_i_1__0 
       (.I0(Q[44]),
        .I1(Q[51]),
        .I2(Q[60]),
        .I3(\x11/C_3 ),
        .I4(\x12/C_3 ),
        .O(outx12[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \xreg1[5]_i_1__0 
       (.I0(\x12/C_3 ),
        .I1(\x11/C_3 ),
        .I2(Q[60]),
        .I3(Q[51]),
        .I4(Q[44]),
        .I5(\x12/f6/S ),
        .O(outx12[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg1[5]_i_2__0 
       (.I0(\x12/C_1 ),
        .I1(outx11[2]),
        .I2(Q[42]),
        .I3(outx11[3]),
        .I4(Q[43]),
        .O(\x12/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \xreg1[5]_i_3__0 
       (.I0(Q[57]),
        .I1(Q[48]),
        .I2(Q[58]),
        .I3(Q[49]),
        .I4(Q[59]),
        .I5(Q[50]),
        .O(\x11/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg1[5]_i_4__0 
       (.I0(\x11/C_3 ),
        .I1(Q[60]),
        .I2(Q[51]),
        .I3(Q[61]),
        .I4(Q[52]),
        .I5(Q[45]),
        .O(\x12/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \xreg1[5]_i_5__0 
       (.I0(Q[49]),
        .I1(Q[58]),
        .I2(Q[48]),
        .I3(Q[57]),
        .O(outx11[2]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg1[6]_i_1__0 
       (.I0(Q[46]),
        .I1(Q[53]),
        .I2(Q[62]),
        .I3(\x11/C_5 ),
        .I4(\x12/C_5 ),
        .O(outx12[6]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg1[6]_i_2__0 
       (.I0(\x11/C_3 ),
        .I1(Q[60]),
        .I2(Q[51]),
        .I3(Q[61]),
        .I4(Q[52]),
        .O(\x11/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg1[6]_i_3__0 
       (.I0(\x12/C_3 ),
        .I1(outx11[4]),
        .I2(Q[44]),
        .I3(outx11[5]),
        .I4(Q[45]),
        .O(\x12/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \xreg1[6]_i_4__0 
       (.I0(Q[51]),
        .I1(Q[60]),
        .I2(\x11/C_3 ),
        .O(outx11[4]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \xreg1[6]_i_5__0 
       (.I0(Q[52]),
        .I1(Q[61]),
        .I2(Q[51]),
        .I3(Q[60]),
        .I4(\x11/C_3 ),
        .O(outx11[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \xreg1[7]_i_1__0 
       (.I0(\x12/f8/S ),
        .I1(\x12/C_6 ),
        .O(outx12[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg1[7]_i_2__0 
       (.I0(\x11/C_5 ),
        .I1(Q[62]),
        .I2(Q[53]),
        .I3(Q[63]),
        .I4(Q[54]),
        .I5(Q[47]),
        .O(\x12/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \xreg1[8]_i_1__0 
       (.I0(Q[55]),
        .I1(Q[47]),
        .I2(Q[54]),
        .I3(Q[63]),
        .I4(\x11/C_6 ),
        .I5(\x12/C_6 ),
        .O(outx12[8]));
  LUT6 #(
    .INIT(64'h00000000F7FF0000)) 
    \xreg1[9]_i_1__0 
       (.I0(\full_reg_n_0_[0] ),
        .I1(\m_axis_tkeep[1] ),
        .I2(m_axis_tready),
        .I3(\full_reg_n_0_[1] ),
        .I4(\valid_reg[1] ),
        .I5(hold),
        .O(xreg1));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \xreg1[9]_i_2__0 
       (.I0(\x12/C_6 ),
        .I1(\x11/C_6 ),
        .I2(Q[63]),
        .I3(Q[54]),
        .I4(Q[47]),
        .I5(Q[55]),
        .O(outx12[9]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \xreg1[9]_i_3__0 
       (.I0(\x12/C_5 ),
        .I1(\x11/C_5 ),
        .I2(Q[62]),
        .I3(Q[53]),
        .I4(Q[46]),
        .O(\x12/C_6 ));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \xreg1[9]_i_4__0 
       (.I0(\x11/C_5 ),
        .I1(Q[62]),
        .I2(Q[53]),
        .O(\x11/C_6 ));
  FDCE \xreg1_reg[0] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(\x12/f1/S ),
        .Q(\xreg1_reg_n_0_[0] ));
  FDCE \xreg1_reg[1] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(outx12[1]),
        .Q(\xreg1_reg_n_0_[1] ));
  FDCE \xreg1_reg[2] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(outx12[2]),
        .Q(\xreg1_reg_n_0_[2] ));
  FDCE \xreg1_reg[3] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(outx12[3]),
        .Q(\xreg1_reg_n_0_[3] ));
  FDCE \xreg1_reg[4] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(outx12[4]),
        .Q(\xreg1_reg_n_0_[4] ));
  FDCE \xreg1_reg[5] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(outx12[5]),
        .Q(\xreg1_reg_n_0_[5] ));
  FDCE \xreg1_reg[6] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(outx12[6]),
        .Q(\xreg1_reg_n_0_[6] ));
  FDCE \xreg1_reg[7] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(outx12[7]),
        .Q(\xreg1_reg_n_0_[7] ));
  FDCE \xreg1_reg[8] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(outx12[8]),
        .Q(\xreg1_reg_n_0_[8] ));
  FDCE \xreg1_reg[9] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(outx12[9]),
        .Q(\xreg1_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \xreg2[0]_i_1__2 
       (.I0(Q[16]),
        .I1(Q[0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \xreg2[1]_i_1__2 
       (.I0(Q[1]),
        .I1(Q[17]),
        .I2(Q[8]),
        .I3(Q[0]),
        .I4(Q[16]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \xreg2[2]_i_1__2 
       (.I0(Q[2]),
        .I1(Q[9]),
        .I2(Q[18]),
        .I3(Q[8]),
        .I4(Q[17]),
        .I5(\c4/x12/C_1 ),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \xreg2[2]_i_2__2 
       (.I0(Q[16]),
        .I1(Q[0]),
        .I2(Q[8]),
        .I3(Q[17]),
        .I4(Q[1]),
        .O(\c4/x12/C_1 ));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \xreg2[3]_i_1__2 
       (.I0(Q[3]),
        .I1(\c4/outx11 [3]),
        .I2(\c4/x12/C_2 ),
        .O(D[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \xreg2[3]_i_2__2 
       (.I0(Q[10]),
        .I1(Q[19]),
        .I2(Q[9]),
        .I3(Q[18]),
        .I4(Q[8]),
        .I5(Q[17]),
        .O(\c4/outx11 [3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \xreg2[3]_i_3__2 
       (.I0(\c4/x12/C_1 ),
        .I1(Q[17]),
        .I2(Q[8]),
        .I3(Q[18]),
        .I4(Q[9]),
        .I5(Q[2]),
        .O(\c4/x12/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg2[4]_i_1__2 
       (.I0(Q[4]),
        .I1(Q[11]),
        .I2(Q[20]),
        .I3(\c4/x11/C_3 ),
        .I4(\c4/x12/C_3 ),
        .O(D[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \xreg2[5]_i_1__2 
       (.I0(\c4/x12/C_3 ),
        .I1(\c4/x11/C_3 ),
        .I2(Q[20]),
        .I3(Q[11]),
        .I4(Q[4]),
        .I5(\c4/x12/f6/S ),
        .O(D[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[5]_i_2__2 
       (.I0(\c4/x12/C_1 ),
        .I1(\c4/outx11 [2]),
        .I2(Q[2]),
        .I3(\c4/outx11 [3]),
        .I4(Q[3]),
        .O(\c4/x12/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \xreg2[5]_i_3__2 
       (.I0(Q[17]),
        .I1(Q[8]),
        .I2(Q[18]),
        .I3(Q[9]),
        .I4(Q[19]),
        .I5(Q[10]),
        .O(\c4/x11/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg2[5]_i_4__2 
       (.I0(\c4/x11/C_3 ),
        .I1(Q[20]),
        .I2(Q[11]),
        .I3(Q[21]),
        .I4(Q[12]),
        .I5(Q[5]),
        .O(\c4/x12/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \xreg2[5]_i_5__2 
       (.I0(Q[9]),
        .I1(Q[18]),
        .I2(Q[8]),
        .I3(Q[17]),
        .O(\c4/outx11 [2]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg2[6]_i_1__2 
       (.I0(Q[6]),
        .I1(Q[13]),
        .I2(Q[22]),
        .I3(\c4/x11/C_5 ),
        .I4(\c4/x12/C_5 ),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[6]_i_2__2 
       (.I0(\c4/x11/C_3 ),
        .I1(Q[20]),
        .I2(Q[11]),
        .I3(Q[21]),
        .I4(Q[12]),
        .O(\c4/x11/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[6]_i_3__2 
       (.I0(\c4/x12/C_3 ),
        .I1(\c4/outx11 [4]),
        .I2(Q[4]),
        .I3(\c4/outx11 [5]),
        .I4(Q[5]),
        .O(\c4/x12/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \xreg2[6]_i_4__2 
       (.I0(Q[11]),
        .I1(Q[20]),
        .I2(\c4/x11/C_3 ),
        .O(\c4/outx11 [4]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \xreg2[6]_i_5__2 
       (.I0(Q[12]),
        .I1(Q[21]),
        .I2(Q[11]),
        .I3(Q[20]),
        .I4(\c4/x11/C_3 ),
        .O(\c4/outx11 [5]));
  LUT2 #(
    .INIT(4'h6)) 
    \xreg2[7]_i_1__2 
       (.I0(\c4/x12/f8/S ),
        .I1(\c4/x12/C_6 ),
        .O(D[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg2[7]_i_2__2 
       (.I0(\c4/x11/C_5 ),
        .I1(Q[22]),
        .I2(Q[13]),
        .I3(Q[23]),
        .I4(Q[14]),
        .I5(Q[7]),
        .O(\c4/x12/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \xreg2[8]_i_1__2 
       (.I0(Q[15]),
        .I1(Q[7]),
        .I2(Q[14]),
        .I3(Q[23]),
        .I4(\c4/x11/C_6 ),
        .I5(\c4/x12/C_6 ),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \xreg2[9]_i_1__2 
       (.I0(\c4/x12/C_6 ),
        .I1(\c4/x11/C_6 ),
        .I2(Q[23]),
        .I3(Q[14]),
        .I4(Q[7]),
        .I5(Q[15]),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \xreg2[9]_i_2__2 
       (.I0(\c4/x12/C_5 ),
        .I1(\c4/x11/C_5 ),
        .I2(Q[22]),
        .I3(Q[13]),
        .I4(Q[6]),
        .O(\c4/x12/C_6 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \xreg2[9]_i_3__2 
       (.I0(\c4/x11/C_5 ),
        .I1(Q[22]),
        .I2(Q[13]),
        .O(\c4/x11/C_6 ));
  FDCE \xreg2_reg[0] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(D[0]),
        .Q(xreg2[0]));
  FDCE \xreg2_reg[1] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(D[1]),
        .Q(xreg2[1]));
  FDCE \xreg2_reg[2] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(D[2]),
        .Q(xreg2[2]));
  FDCE \xreg2_reg[3] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(D[3]),
        .Q(xreg2[3]));
  FDCE \xreg2_reg[4] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(D[4]),
        .Q(xreg2[4]));
  FDCE \xreg2_reg[5] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(D[5]),
        .Q(xreg2[5]));
  FDCE \xreg2_reg[6] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(D[6]),
        .Q(xreg2[6]));
  FDCE \xreg2_reg[7] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_1),
        .D(D[7]),
        .Q(xreg2[7]));
  FDCE \xreg2_reg[8] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(D[8]),
        .Q(xreg2[8]));
  FDCE \xreg2_reg[9] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(D[9]),
        .Q(xreg2[9]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg1[0]_i_1__0 
       (.I0(Q[56]),
        .I1(Q[16]),
        .O(\y12/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \yreg1[1]_i_1__0 
       (.I0(Q[17]),
        .I1(Q[57]),
        .I2(Q[32]),
        .I3(Q[16]),
        .I4(Q[56]),
        .O(outy12[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \yreg1[2]_i_1__0 
       (.I0(Q[18]),
        .I1(Q[33]),
        .I2(Q[58]),
        .I3(Q[32]),
        .I4(Q[57]),
        .I5(\y12/C_1 ),
        .O(outy12[2]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \yreg1[2]_i_2__0 
       (.I0(Q[56]),
        .I1(Q[16]),
        .I2(Q[32]),
        .I3(Q[57]),
        .I4(Q[17]),
        .O(\y12/C_1 ));
  LUT3 #(
    .INIT(8'h96)) 
    \yreg1[3]_i_1__0 
       (.I0(Q[19]),
        .I1(outy11[3]),
        .I2(\y12/C_2 ),
        .O(outy12[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \yreg1[3]_i_2__0 
       (.I0(Q[34]),
        .I1(Q[59]),
        .I2(Q[33]),
        .I3(Q[58]),
        .I4(Q[32]),
        .I5(Q[57]),
        .O(outy11[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \yreg1[3]_i_3__0 
       (.I0(\y12/C_1 ),
        .I1(Q[57]),
        .I2(Q[32]),
        .I3(Q[58]),
        .I4(Q[33]),
        .I5(Q[18]),
        .O(\y12/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg1[4]_i_1__0 
       (.I0(Q[20]),
        .I1(Q[35]),
        .I2(Q[60]),
        .I3(\y11/C_3 ),
        .I4(\y12/C_3 ),
        .O(outy12[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \yreg1[5]_i_1__0 
       (.I0(\y12/C_3 ),
        .I1(\y11/C_3 ),
        .I2(Q[60]),
        .I3(Q[35]),
        .I4(Q[20]),
        .I5(\y12/f6/S ),
        .O(outy12[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[5]_i_2__0 
       (.I0(\y12/C_1 ),
        .I1(outy11[2]),
        .I2(Q[18]),
        .I3(outy11[3]),
        .I4(Q[19]),
        .O(\y12/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \yreg1[5]_i_3__0 
       (.I0(Q[57]),
        .I1(Q[32]),
        .I2(Q[58]),
        .I3(Q[33]),
        .I4(Q[59]),
        .I5(Q[34]),
        .O(\y11/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg1[5]_i_4__0 
       (.I0(\y11/C_3 ),
        .I1(Q[60]),
        .I2(Q[35]),
        .I3(Q[61]),
        .I4(Q[36]),
        .I5(Q[21]),
        .O(\y12/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \yreg1[5]_i_5__0 
       (.I0(Q[33]),
        .I1(Q[58]),
        .I2(Q[32]),
        .I3(Q[57]),
        .O(outy11[2]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg1[6]_i_1__0 
       (.I0(Q[22]),
        .I1(Q[37]),
        .I2(Q[62]),
        .I3(\y11/C_5 ),
        .I4(\y12/C_5 ),
        .O(outy12[6]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[6]_i_2__0 
       (.I0(\y11/C_3 ),
        .I1(Q[60]),
        .I2(Q[35]),
        .I3(Q[61]),
        .I4(Q[36]),
        .O(\y11/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[6]_i_3__0 
       (.I0(\y12/C_3 ),
        .I1(outy11[4]),
        .I2(Q[20]),
        .I3(outy11[5]),
        .I4(Q[21]),
        .O(\y12/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg1[6]_i_4__0 
       (.I0(Q[35]),
        .I1(Q[60]),
        .I2(\y11/C_3 ),
        .O(outy11[4]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \yreg1[6]_i_5__0 
       (.I0(Q[36]),
        .I1(Q[61]),
        .I2(Q[35]),
        .I3(Q[60]),
        .I4(\y11/C_3 ),
        .O(outy11[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg1[7]_i_1__0 
       (.I0(\y12/f8/S ),
        .I1(\y12/C_6 ),
        .O(outy12[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg1[7]_i_2__0 
       (.I0(\y11/C_5 ),
        .I1(Q[62]),
        .I2(Q[37]),
        .I3(Q[63]),
        .I4(Q[38]),
        .I5(Q[23]),
        .O(\y12/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \yreg1[8]_i_1__0 
       (.I0(Q[39]),
        .I1(Q[23]),
        .I2(Q[38]),
        .I3(Q[63]),
        .I4(\y11/C_6 ),
        .I5(\y12/C_6 ),
        .O(outy12[8]));
  LUT6 #(
    .INIT(64'hF7FF000000000000)) 
    \yreg1[9]_i_1__0 
       (.I0(\full_reg_n_0_[0] ),
        .I1(\m_axis_tkeep[1] ),
        .I2(m_axis_tready),
        .I3(\full_reg_n_0_[1] ),
        .I4(\valid_reg[1] ),
        .I5(hold_reg),
        .O(yreg1));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \yreg1[9]_i_2__0 
       (.I0(\y12/C_6 ),
        .I1(\y11/C_6 ),
        .I2(Q[63]),
        .I3(Q[38]),
        .I4(Q[23]),
        .I5(Q[39]),
        .O(outy12[9]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \yreg1[9]_i_3 
       (.I0(\y12/C_5 ),
        .I1(\y11/C_5 ),
        .I2(Q[62]),
        .I3(Q[37]),
        .I4(Q[22]),
        .O(\y12/C_6 ));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \yreg1[9]_i_4__0 
       (.I0(\y11/C_5 ),
        .I1(Q[62]),
        .I2(Q[37]),
        .O(\y11/C_6 ));
  FDRE \yreg1_reg[0] 
       (.C(clock),
        .CE(yreg1),
        .D(\y12/f1/S ),
        .Q(\yreg1_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \yreg1_reg[1] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[1]),
        .Q(\yreg1_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \yreg1_reg[2] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[2]),
        .Q(\yreg1_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \yreg1_reg[3] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[3]),
        .Q(\yreg1_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \yreg1_reg[4] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[4]),
        .Q(\yreg1_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \yreg1_reg[5] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[5]),
        .Q(\yreg1_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \yreg1_reg[6] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[6]),
        .Q(\yreg1_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \yreg1_reg[7] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[7]),
        .Q(\yreg1_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \yreg1_reg[8] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[8]),
        .Q(\yreg1_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \yreg1_reg[9] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[9]),
        .Q(\yreg1_reg_n_0_[9] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \yreg2[0]_i_1__0 
       (.I0(Q[40]),
        .I1(Q[0]),
        .O(\y22/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \yreg2[1]_i_1__0 
       (.I0(Q[1]),
        .I1(Q[41]),
        .I2(Q[24]),
        .I3(Q[0]),
        .I4(Q[40]),
        .O(outy22[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \yreg2[2]_i_1__0 
       (.I0(Q[2]),
        .I1(Q[25]),
        .I2(Q[42]),
        .I3(Q[24]),
        .I4(Q[41]),
        .I5(\y22/C_1 ),
        .O(outy22[2]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \yreg2[2]_i_2__0 
       (.I0(Q[40]),
        .I1(Q[0]),
        .I2(Q[24]),
        .I3(Q[41]),
        .I4(Q[1]),
        .O(\y22/C_1 ));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg2[3]_i_1__0 
       (.I0(Q[3]),
        .I1(outy21[3]),
        .I2(\y22/C_2 ),
        .O(outy22[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \yreg2[3]_i_2__0 
       (.I0(Q[26]),
        .I1(Q[43]),
        .I2(Q[25]),
        .I3(Q[42]),
        .I4(Q[24]),
        .I5(Q[41]),
        .O(outy21[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \yreg2[3]_i_3__0 
       (.I0(\y22/C_1 ),
        .I1(Q[41]),
        .I2(Q[24]),
        .I3(Q[42]),
        .I4(Q[25]),
        .I5(Q[2]),
        .O(\y22/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg2[4]_i_1__0 
       (.I0(Q[4]),
        .I1(Q[27]),
        .I2(Q[44]),
        .I3(\y21/C_3 ),
        .I4(\y22/C_3 ),
        .O(outy22[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \yreg2[5]_i_1__0 
       (.I0(\y22/C_3 ),
        .I1(\y21/C_3 ),
        .I2(Q[44]),
        .I3(Q[27]),
        .I4(Q[4]),
        .I5(\y22/f6/S ),
        .O(outy22[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[5]_i_2__0 
       (.I0(\y22/C_1 ),
        .I1(outy21[2]),
        .I2(Q[2]),
        .I3(outy21[3]),
        .I4(Q[3]),
        .O(\y22/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \yreg2[5]_i_3__0 
       (.I0(Q[41]),
        .I1(Q[24]),
        .I2(Q[42]),
        .I3(Q[25]),
        .I4(Q[43]),
        .I5(Q[26]),
        .O(\y21/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg2[5]_i_4__0 
       (.I0(\y21/C_3 ),
        .I1(Q[44]),
        .I2(Q[27]),
        .I3(Q[45]),
        .I4(Q[28]),
        .I5(Q[5]),
        .O(\y22/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \yreg2[5]_i_5__0 
       (.I0(Q[25]),
        .I1(Q[42]),
        .I2(Q[24]),
        .I3(Q[41]),
        .O(outy21[2]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg2[6]_i_1__0 
       (.I0(Q[6]),
        .I1(Q[29]),
        .I2(Q[46]),
        .I3(\y21/C_5 ),
        .I4(\y22/C_5 ),
        .O(outy22[6]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[6]_i_2__0 
       (.I0(\y21/C_3 ),
        .I1(Q[44]),
        .I2(Q[27]),
        .I3(Q[45]),
        .I4(Q[28]),
        .O(\y21/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[6]_i_3__0 
       (.I0(\y22/C_3 ),
        .I1(outy21[4]),
        .I2(Q[4]),
        .I3(outy21[5]),
        .I4(Q[5]),
        .O(\y22/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg2[6]_i_4__0 
       (.I0(Q[27]),
        .I1(Q[44]),
        .I2(\y21/C_3 ),
        .O(outy21[4]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \yreg2[6]_i_5__0 
       (.I0(Q[28]),
        .I1(Q[45]),
        .I2(Q[27]),
        .I3(Q[44]),
        .I4(\y21/C_3 ),
        .O(outy21[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg2[7]_i_1__0 
       (.I0(\y22/f8/S ),
        .I1(\y22/C_6 ),
        .O(outy22[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg2[7]_i_2__0 
       (.I0(\y21/C_5 ),
        .I1(Q[46]),
        .I2(Q[29]),
        .I3(Q[47]),
        .I4(Q[30]),
        .I5(Q[7]),
        .O(\y22/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \yreg2[8]_i_1__0 
       (.I0(Q[31]),
        .I1(Q[7]),
        .I2(Q[30]),
        .I3(Q[47]),
        .I4(\y21/C_6 ),
        .I5(\y22/C_6 ),
        .O(outy22[8]));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \yreg2[9]_i_1__0 
       (.I0(\y22/C_6 ),
        .I1(\y21/C_6 ),
        .I2(Q[47]),
        .I3(Q[30]),
        .I4(Q[7]),
        .I5(Q[31]),
        .O(outy22[9]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \yreg2[9]_i_2__0 
       (.I0(\y22/C_5 ),
        .I1(\y21/C_5 ),
        .I2(Q[46]),
        .I3(Q[29]),
        .I4(Q[6]),
        .O(\y22/C_6 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \yreg2[9]_i_3__0 
       (.I0(\y21/C_5 ),
        .I1(Q[46]),
        .I2(Q[29]),
        .O(\y21/C_6 ));
  FDRE \yreg2_reg[0] 
       (.C(clock),
        .CE(yreg1),
        .D(\y22/f1/S ),
        .Q(yreg2[0]),
        .R(1'b0));
  FDRE \yreg2_reg[1] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[1]),
        .Q(yreg2[1]),
        .R(1'b0));
  FDRE \yreg2_reg[2] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[2]),
        .Q(yreg2[2]),
        .R(1'b0));
  FDRE \yreg2_reg[3] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[3]),
        .Q(yreg2[3]),
        .R(1'b0));
  FDRE \yreg2_reg[4] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[4]),
        .Q(yreg2[4]),
        .R(1'b0));
  FDRE \yreg2_reg[5] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[5]),
        .Q(yreg2[5]),
        .R(1'b0));
  FDRE \yreg2_reg[6] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[6]),
        .Q(yreg2[6]),
        .R(1'b0));
  FDRE \yreg2_reg[7] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[7]),
        .Q(yreg2[7]),
        .R(1'b0));
  FDRE \yreg2_reg[8] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[8]),
        .Q(yreg2[8]),
        .R(1'b0));
  FDRE \yreg2_reg[9] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[9]),
        .Q(yreg2[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "convolution" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convolution_1
   (D,
    \out_reg[0]_0 ,
    \m_axis_tkeep[2] ,
    m_axis_tdata,
    Q,
    reset,
    \inputvals_reg[119] ,
    clock,
    hold,
    m_axis_tready,
    \valid_reg[2] ,
    reset_0,
    hold_reg);
  output [9:0]D;
  output \out_reg[0]_0 ;
  output \m_axis_tkeep[2] ;
  output [7:0]m_axis_tdata;
  input [55:0]Q;
  input reset;
  input [9:0]\inputvals_reg[119] ;
  input clock;
  input hold;
  input m_axis_tready;
  input [0:0]\valid_reg[2] ;
  input reset_0;
  input hold_reg;

  wire [9:0]D;
  wire [55:0]Q;
  wire [7:1]ans1;
  wire [9:1]ans2;
  wire [5:2]\c5/outx11 ;
  wire \c5/x11/C_3 ;
  wire \c5/x11/C_5 ;
  wire \c5/x11/C_6 ;
  wire \c5/x12/C_1 ;
  wire \c5/x12/C_2 ;
  wire \c5/x12/C_3 ;
  wire \c5/x12/C_5 ;
  wire \c5/x12/C_6 ;
  wire \c5/x12/f6/S ;
  wire \c5/x12/f8/S ;
  wire clock;
  wire \final/C_1 ;
  wire \final/C_2 ;
  wire \final/C_4 ;
  wire \final/C_6 ;
  wire \final/f1/Ca ;
  wire \final/f2/S ;
  wire \final/f4/S ;
  wire \final/f5/S ;
  wire \final/f6/S ;
  wire \full[0]_i_1__1_n_0 ;
  wire \full[1]_i_1__1_n_0 ;
  wire \full[2]_i_1__1_n_0 ;
  wire \full_reg_n_0_[0] ;
  wire \full_reg_n_0_[1] ;
  wire hold;
  wire hold_reg;
  wire [9:0]\inputvals_reg[119] ;
  wire [7:0]m_axis_tdata;
  wire \m_axis_tkeep[2] ;
  wire m_axis_tready;
  wire out;
  wire \out[0]_i_1__1_n_0 ;
  wire \out[0]_i_2__1_n_0 ;
  wire \out[1]_i_1__1_n_0 ;
  wire \out[1]_i_2__1_n_0 ;
  wire \out[2]_i_1__1_n_0 ;
  wire \out[2]_i_2__1_n_0 ;
  wire \out[3]_i_1__1_n_0 ;
  wire \out[3]_i_2__1_n_0 ;
  wire \out[4]_i_1__1_n_0 ;
  wire \out[4]_i_2__1_n_0 ;
  wire \out[5]_i_1__1_n_0 ;
  wire \out[5]_i_2__1_n_0 ;
  wire \out[6]_i_1__1_n_0 ;
  wire \out[6]_i_2__1_n_0 ;
  wire \out[7]_i_25__1_n_0 ;
  wire \out[7]_i_26__1_n_0 ;
  wire \out[7]_i_27__1_n_0 ;
  wire \out[7]_i_28__1_n_0 ;
  wire \out[7]_i_2__1_n_0 ;
  wire \out[7]_i_7__1_n_0 ;
  wire \out_reg[0]_0 ;
  wire [5:2]outy11;
  wire [9:1]outy12;
  wire [5:2]outy21;
  wire [9:1]outy22;
  wire reset;
  wire reset_0;
  wire \s1/C2 ;
  wire \s1/F6/S ;
  wire \s1/s1/C_2 ;
  wire \s1/s2/C_0 ;
  wire \s1/s2/C_2 ;
  wire \s1/s2/f2/S ;
  wire \s11[0]_i_1__1_n_0 ;
  wire \s11[1]_i_1__1_n_0 ;
  wire \s11[2]_i_1__1_n_0 ;
  wire \s11[3]_i_1__1_n_0 ;
  wire \s11[4]_i_1__1_n_0 ;
  wire \s11[5]_i_1__1_n_0 ;
  wire \s11[6]_i_1__1_n_0 ;
  wire \s11[7]_i_1__1_n_0 ;
  wire \s11[8]_i_1__1_n_0 ;
  wire \s11[9]_i_1__1_n_0 ;
  wire \s11_reg_n_0_[0] ;
  wire \s11_reg_n_0_[1] ;
  wire \s11_reg_n_0_[2] ;
  wire \s11_reg_n_0_[3] ;
  wire \s11_reg_n_0_[4] ;
  wire \s11_reg_n_0_[5] ;
  wire \s11_reg_n_0_[6] ;
  wire \s11_reg_n_0_[7] ;
  wire \s11_reg_n_0_[8] ;
  wire \s11_reg_n_0_[9] ;
  wire [9:0]s12;
  wire \s12[0]_i_1__1_n_0 ;
  wire \s12[1]_i_1__1_n_0 ;
  wire \s12[2]_i_1__1_n_0 ;
  wire \s12[3]_i_1__1_n_0 ;
  wire \s12[4]_i_1__1_n_0 ;
  wire \s12[5]_i_1__1_n_0 ;
  wire \s12[6]_i_1__1_n_0 ;
  wire \s12[7]_i_1__1_n_0 ;
  wire \s12[8]_i_1__1_n_0 ;
  wire \s12[8]_i_2__1_n_0 ;
  wire \s12[8]_i_3__1_n_0 ;
  wire \s12[8]_i_4__1_n_0 ;
  wire \s12[8]_i_5__1_n_0 ;
  wire \s12[8]_i_6__1_n_0 ;
  wire \s12[8]_i_7__1_n_0 ;
  wire \s12[8]_i_8__1_n_0 ;
  wire \s12[8]_i_9__1_n_0 ;
  wire \s12[9]_i_2__1_n_0 ;
  wire \s2/C2 ;
  wire \s2/F6/S ;
  wire \s2/f5/S ;
  wire \s2/s1/C_2 ;
  wire \s2/s1/f1/S ;
  wire \s2/s2/C_0 ;
  wire \s2/s2/C_2 ;
  wire \s2/s2/f2/S ;
  wire s21;
  wire \s21[0]_i_1__1_n_0 ;
  wire \s21[1]_i_1__1_n_0 ;
  wire \s21[2]_i_1__1_n_0 ;
  wire \s21[3]_i_1__1_n_0 ;
  wire \s21[4]_i_1__1_n_0 ;
  wire \s21[5]_i_1__1_n_0 ;
  wire \s21[6]_i_1__1_n_0 ;
  wire \s21[7]_i_1__1_n_0 ;
  wire \s21[8]_i_1__1_n_0 ;
  wire \s21[9]_i_1__1_n_0 ;
  wire \s21_reg_n_0_[0] ;
  wire \s21_reg_n_0_[1] ;
  wire \s21_reg_n_0_[2] ;
  wire \s21_reg_n_0_[3] ;
  wire \s21_reg_n_0_[4] ;
  wire \s21_reg_n_0_[5] ;
  wire \s21_reg_n_0_[6] ;
  wire \s21_reg_n_0_[7] ;
  wire \s21_reg_n_0_[8] ;
  wire \s21_reg_n_0_[9] ;
  wire [9:0]s22;
  wire \s22[0]_i_1__1_n_0 ;
  wire \s22[1]_i_1__1_n_0 ;
  wire \s22[2]_i_1__1_n_0 ;
  wire \s22[3]_i_1__1_n_0 ;
  wire \s22[4]_i_1__1_n_0 ;
  wire \s22[5]_i_1__1_n_0 ;
  wire \s22[6]_i_1__1_n_0 ;
  wire \s22[7]_i_1__1_n_0 ;
  wire \s22[8]_i_1__1_n_0 ;
  wire \s22[8]_i_2__1_n_0 ;
  wire \s22[8]_i_3__1_n_0 ;
  wire \s22[8]_i_4__1_n_0 ;
  wire \s22[8]_i_5__1_n_0 ;
  wire \s22[8]_i_6__1_n_0 ;
  wire \s22[8]_i_7__1_n_0 ;
  wire \s22[8]_i_8__1_n_0 ;
  wire \s22[8]_i_9__1_n_0 ;
  wire \s22[9]_i_1__1_n_0 ;
  wire [0:0]\valid_reg[2] ;
  wire xreg1;
  wire \xreg1_reg_n_0_[0] ;
  wire \xreg1_reg_n_0_[1] ;
  wire \xreg1_reg_n_0_[2] ;
  wire \xreg1_reg_n_0_[3] ;
  wire \xreg1_reg_n_0_[4] ;
  wire \xreg1_reg_n_0_[5] ;
  wire \xreg1_reg_n_0_[6] ;
  wire \xreg1_reg_n_0_[7] ;
  wire \xreg1_reg_n_0_[8] ;
  wire \xreg1_reg_n_0_[9] ;
  wire [9:0]xreg2;
  wire \y11/C_3 ;
  wire \y11/C_5 ;
  wire \y11/C_6 ;
  wire \y12/C_1 ;
  wire \y12/C_2 ;
  wire \y12/C_3 ;
  wire \y12/C_5 ;
  wire \y12/C_6 ;
  wire \y12/f1/S ;
  wire \y12/f6/S ;
  wire \y12/f8/S ;
  wire \y21/C_3 ;
  wire \y21/C_5 ;
  wire \y21/C_6 ;
  wire \y22/C_1 ;
  wire \y22/C_2 ;
  wire \y22/C_3 ;
  wire \y22/C_5 ;
  wire \y22/C_6 ;
  wire \y22/f1/S ;
  wire \y22/f6/S ;
  wire \y22/f8/S ;
  wire yreg1;
  wire \yreg1_reg_n_0_[0] ;
  wire \yreg1_reg_n_0_[1] ;
  wire \yreg1_reg_n_0_[2] ;
  wire \yreg1_reg_n_0_[3] ;
  wire \yreg1_reg_n_0_[4] ;
  wire \yreg1_reg_n_0_[5] ;
  wire \yreg1_reg_n_0_[6] ;
  wire \yreg1_reg_n_0_[7] ;
  wire \yreg1_reg_n_0_[8] ;
  wire \yreg1_reg_n_0_[9] ;
  wire [9:0]yreg2;

  LUT6 #(
    .INIT(64'hEEFEEEEE44444444)) 
    \full[0]_i_1__1 
       (.I0(hold),
        .I1(\valid_reg[2] ),
        .I2(\full_reg_n_0_[1] ),
        .I3(m_axis_tready),
        .I4(\m_axis_tkeep[2] ),
        .I5(\full_reg_n_0_[0] ),
        .O(\full[0]_i_1__1_n_0 ));
  LUT5 #(
    .INIT(32'hEEFE4444)) 
    \full[1]_i_1__1 
       (.I0(hold),
        .I1(\full_reg_n_0_[0] ),
        .I2(\m_axis_tkeep[2] ),
        .I3(m_axis_tready),
        .I4(\full_reg_n_0_[1] ),
        .O(\full[1]_i_1__1_n_0 ));
  LUT5 #(
    .INIT(32'hFBFF4040)) 
    \full[2]_i_1__1 
       (.I0(hold),
        .I1(reset),
        .I2(\full_reg_n_0_[1] ),
        .I3(m_axis_tready),
        .I4(\m_axis_tkeep[2] ),
        .O(\full[2]_i_1__1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[0] 
       (.C(clock),
        .CE(1'b1),
        .CLR(reset_0),
        .D(\full[0]_i_1__1_n_0 ),
        .Q(\full_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[1] 
       (.C(clock),
        .CE(1'b1),
        .CLR(reset_0),
        .D(\full[1]_i_1__1_n_0 ),
        .Q(\full_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[2] 
       (.C(clock),
        .CE(1'b1),
        .CLR(reset_0),
        .D(\full[2]_i_1__1_n_0 ),
        .Q(\m_axis_tkeep[2] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[0]_i_1__1 
       (.I0(\out[0]_i_2__1_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__1_n_0 ),
        .O(\out[0]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    \out[0]_i_2__1 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s11_reg_n_0_[0] ),
        .O(\out[0]_i_2__1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[1]_i_1__1 
       (.I0(\out[1]_i_2__1_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__1_n_0 ),
        .O(\out[1]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT5 #(
    .INIT(32'hAA9696AA)) 
    \out[1]_i_2__1 
       (.I0(\final/f2/S ),
        .I1(\s11_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s21_reg_n_0_[0] ),
        .I4(s22[0]),
        .O(\out[1]_i_2__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT5 #(
    .INIT(32'hD22D2DD2)) 
    \out[1]_i_3__1 
       (.I0(s12[0]),
        .I1(\s11_reg_n_0_[0] ),
        .I2(\s11_reg_n_0_[1] ),
        .I3(s12[1]),
        .I4(ans2[1]),
        .O(\final/f2/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[2]_i_1__1 
       (.I0(\out[2]_i_2__1_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__1_n_0 ),
        .O(\out[2]_i_1__1_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \out[2]_i_2__1 
       (.I0(ans1[2]),
        .I1(ans2[2]),
        .I2(\final/C_1 ),
        .O(\out[2]_i_2__1_n_0 ));
  LUT6 #(
    .INIT(64'h6966996969666966)) 
    \out[2]_i_3__1 
       (.I0(s12[2]),
        .I1(\s11_reg_n_0_[2] ),
        .I2(\s11_reg_n_0_[1] ),
        .I3(s12[1]),
        .I4(\s11_reg_n_0_[0] ),
        .I5(s12[0]),
        .O(ans1[2]));
  LUT6 #(
    .INIT(64'h6966996969666966)) 
    \out[2]_i_4__1 
       (.I0(s22[2]),
        .I1(\s21_reg_n_0_[2] ),
        .I2(\s21_reg_n_0_[1] ),
        .I3(s22[1]),
        .I4(\s21_reg_n_0_[0] ),
        .I5(s22[0]),
        .O(ans2[2]));
  LUT6 #(
    .INIT(64'h0CC0D44D4DD40CC0)) 
    \out[2]_i_5__1 
       (.I0(\s2/s1/f1/S ),
        .I1(ans2[1]),
        .I2(s12[1]),
        .I3(\s11_reg_n_0_[1] ),
        .I4(\s11_reg_n_0_[0] ),
        .I5(s12[0]),
        .O(\final/C_1 ));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \out[2]_i_6__1 
       (.I0(\s21_reg_n_0_[0] ),
        .I1(s22[0]),
        .O(\s2/s1/f1/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[3]_i_1__1 
       (.I0(\out[3]_i_2__1_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__1_n_0 ),
        .O(\out[3]_i_1__1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out[3]_i_2__1 
       (.I0(\final/f4/S ),
        .I1(\final/C_2 ),
        .O(\out[3]_i_2__1_n_0 ));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    \out[3]_i_3__1 
       (.I0(\s1/s1/C_2 ),
        .I1(\s11_reg_n_0_[3] ),
        .I2(s12[3]),
        .I3(\s2/s1/C_2 ),
        .I4(\s21_reg_n_0_[3] ),
        .I5(s22[3]),
        .O(\final/f4/S ));
  LUT6 #(
    .INIT(64'hDF0DFFFF0000DF0D)) 
    \out[4]_i_10__1 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s22[1]),
        .I3(\s21_reg_n_0_[1] ),
        .I4(s22[2]),
        .I5(\s21_reg_n_0_[2] ),
        .O(\s2/s1/C_2 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[4]_i_1__1 
       (.I0(\out[4]_i_2__1_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__1_n_0 ),
        .O(\out[4]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h711717718EE8E88E)) 
    \out[4]_i_2__1 
       (.I0(\final/C_2 ),
        .I1(ans2[3]),
        .I2(s12[3]),
        .I3(\s11_reg_n_0_[3] ),
        .I4(\s1/s1/C_2 ),
        .I5(\final/f5/S ),
        .O(\out[4]_i_2__1_n_0 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[4]_i_3__1 
       (.I0(\final/f1/Ca ),
        .I1(ans2[1]),
        .I2(ans1[1]),
        .I3(ans2[2]),
        .I4(ans1[2]),
        .O(\final/C_2 ));
  LUT3 #(
    .INIT(8'h69)) 
    \out[4]_i_4__1 
       (.I0(s22[3]),
        .I1(\s21_reg_n_0_[3] ),
        .I2(\s2/s1/C_2 ),
        .O(ans2[3]));
  LUT6 #(
    .INIT(64'hDF0DFFFF0000DF0D)) 
    \out[4]_i_5__1 
       (.I0(s12[0]),
        .I1(\s11_reg_n_0_[0] ),
        .I2(s12[1]),
        .I3(\s11_reg_n_0_[1] ),
        .I4(s12[2]),
        .I5(\s11_reg_n_0_[2] ),
        .O(\s1/s1/C_2 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \out[4]_i_6__1 
       (.I0(\s1/s1/C_2 ),
        .I1(s12[3]),
        .I2(\s11_reg_n_0_[3] ),
        .I3(\s11_reg_n_0_[4] ),
        .I4(s12[4]),
        .I5(ans2[4]),
        .O(\final/f5/S ));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT4 #(
    .INIT(16'h0660)) 
    \out[4]_i_7__1 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s11_reg_n_0_[0] ),
        .O(\final/f1/Ca ));
  LUT4 #(
    .INIT(16'h6966)) 
    \out[4]_i_8__1 
       (.I0(s22[1]),
        .I1(\s21_reg_n_0_[1] ),
        .I2(\s21_reg_n_0_[0] ),
        .I3(s22[0]),
        .O(ans2[1]));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT4 #(
    .INIT(16'h6966)) 
    \out[4]_i_9__1 
       (.I0(s12[1]),
        .I1(\s11_reg_n_0_[1] ),
        .I2(\s11_reg_n_0_[0] ),
        .I3(s12[0]),
        .O(ans1[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[5]_i_1__1 
       (.I0(\out[5]_i_2__1_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__1_n_0 ),
        .O(\out[5]_i_1__1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out[5]_i_2__1 
       (.I0(\final/f6/S ),
        .I1(\final/C_4 ),
        .O(\out[5]_i_2__1_n_0 ));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    \out[5]_i_3__1 
       (.I0(\s1/s2/C_0 ),
        .I1(\s11_reg_n_0_[5] ),
        .I2(s12[5]),
        .I3(\s2/s2/C_0 ),
        .I4(\s21_reg_n_0_[5] ),
        .I5(s22[5]),
        .O(\final/f6/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[6]_i_1__1 
       (.I0(\out[6]_i_2__1_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_7__1_n_0 ),
        .O(\out[6]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \out[6]_i_2__1 
       (.I0(ans2[6]),
        .I1(ans1[6]),
        .I2(ans1[5]),
        .I3(ans2[5]),
        .I4(\final/C_4 ),
        .O(\out[6]_i_2__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_10__1 
       (.I0(\s1/s2/C_0 ),
        .I1(s12[5]),
        .I2(\s11_reg_n_0_[5] ),
        .I3(s12[6]),
        .I4(\s11_reg_n_0_[6] ),
        .O(\s1/s2/C_2 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[7]_i_11__1 
       (.I0(\final/C_2 ),
        .I1(ans2[3]),
        .I2(ans1[3]),
        .I3(ans2[4]),
        .I4(ans1[4]),
        .O(\final/C_4 ));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_12__1 
       (.I0(s22[5]),
        .I1(\s21_reg_n_0_[5] ),
        .I2(\s2/s2/C_0 ),
        .O(ans2[5]));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_13__1 
       (.I0(s12[5]),
        .I1(\s11_reg_n_0_[5] ),
        .I2(\s1/s2/C_0 ),
        .O(ans1[5]));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_14__1 
       (.I0(s22[6]),
        .I1(\s21_reg_n_0_[6] ),
        .I2(\s21_reg_n_0_[5] ),
        .I3(s22[5]),
        .I4(\s2/s2/C_0 ),
        .O(ans2[6]));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_15__1 
       (.I0(s12[6]),
        .I1(\s11_reg_n_0_[6] ),
        .I2(\s11_reg_n_0_[5] ),
        .I3(s12[5]),
        .I4(\s1/s2/C_0 ),
        .O(ans1[6]));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_16__1 
       (.I0(\s11_reg_n_0_[9] ),
        .I1(s12[9]),
        .O(\s1/F6/S ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_17__1 
       (.I0(\s21_reg_n_0_[8] ),
        .I1(s22[8]),
        .O(\s2/f5/S ));
  LUT6 #(
    .INIT(64'hEF0EFFFF0000EF0E)) 
    \out[7]_i_18__1 
       (.I0(\out[7]_i_25__1_n_0 ),
        .I1(\out[7]_i_26__1_n_0 ),
        .I2(s22[6]),
        .I3(\s21_reg_n_0_[6] ),
        .I4(s22[7]),
        .I5(\s21_reg_n_0_[7] ),
        .O(\s2/C2 ));
  LUT6 #(
    .INIT(64'hEF0EFFFF0000EF0E)) 
    \out[7]_i_19__1 
       (.I0(\out[7]_i_27__1_n_0 ),
        .I1(\out[7]_i_28__1_n_0 ),
        .I2(s12[6]),
        .I3(\s11_reg_n_0_[6] ),
        .I4(s12[7]),
        .I5(\s11_reg_n_0_[7] ),
        .O(\s1/C2 ));
  LUT4 #(
    .INIT(16'h00D0)) 
    \out[7]_i_1__1 
       (.I0(\m_axis_tkeep[2] ),
        .I1(m_axis_tready),
        .I2(\full_reg_n_0_[1] ),
        .I3(hold),
        .O(out));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_20__1 
       (.I0(\s2/s1/C_2 ),
        .I1(s22[3]),
        .I2(\s21_reg_n_0_[3] ),
        .I3(s22[4]),
        .I4(\s21_reg_n_0_[4] ),
        .O(\s2/s2/C_0 ));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_21__1 
       (.I0(\s1/s1/C_2 ),
        .I1(s12[3]),
        .I2(\s11_reg_n_0_[3] ),
        .I3(s12[4]),
        .I4(\s11_reg_n_0_[4] ),
        .O(\s1/s2/C_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_22__1 
       (.I0(s12[3]),
        .I1(\s11_reg_n_0_[3] ),
        .I2(\s1/s1/C_2 ),
        .O(ans1[3]));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_23__1 
       (.I0(s22[4]),
        .I1(\s21_reg_n_0_[4] ),
        .I2(\s21_reg_n_0_[3] ),
        .I3(s22[3]),
        .I4(\s2/s1/C_2 ),
        .O(ans2[4]));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_24__1 
       (.I0(s12[4]),
        .I1(\s11_reg_n_0_[4] ),
        .I2(\s11_reg_n_0_[3] ),
        .I3(s12[3]),
        .I4(\s1/s1/C_2 ),
        .O(ans1[4]));
  LUT6 #(
    .INIT(64'h8A088A8A08088A08)) 
    \out[7]_i_25__1 
       (.I0(\s2/s2/f2/S ),
        .I1(\s21_reg_n_0_[4] ),
        .I2(s22[4]),
        .I3(\s21_reg_n_0_[3] ),
        .I4(s22[3]),
        .I5(\s2/s1/C_2 ),
        .O(\out[7]_i_25__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \out[7]_i_26__1 
       (.I0(\s21_reg_n_0_[5] ),
        .I1(s22[5]),
        .O(\out[7]_i_26__1_n_0 ));
  LUT6 #(
    .INIT(64'h8A088A8A08088A08)) 
    \out[7]_i_27__1 
       (.I0(\s1/s2/f2/S ),
        .I1(\s11_reg_n_0_[4] ),
        .I2(s12[4]),
        .I3(\s11_reg_n_0_[3] ),
        .I4(s12[3]),
        .I5(\s1/s1/C_2 ),
        .O(\out[7]_i_27__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \out[7]_i_28__1 
       (.I0(\s11_reg_n_0_[5] ),
        .I1(s12[5]),
        .O(\out[7]_i_28__1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_29__1 
       (.I0(\s21_reg_n_0_[5] ),
        .I1(s22[5]),
        .O(\s2/s2/f2/S ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \out[7]_i_2__1 
       (.I0(ans2[9]),
        .I1(ans1[7]),
        .I2(ans2[7]),
        .I3(\final/C_6 ),
        .I4(\out[7]_i_7__1_n_0 ),
        .O(\out[7]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_30__1 
       (.I0(\s11_reg_n_0_[5] ),
        .I1(s12[5]),
        .O(\s1/s2/f2/S ));
  LUT1 #(
    .INIT(2'h1)) 
    \out[7]_i_3__1 
       (.I0(reset),
        .O(\out_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h65A66565A6A665A6)) 
    \out[7]_i_3__3 
       (.I0(\s2/F6/S ),
        .I1(\s21_reg_n_0_[8] ),
        .I2(s22[8]),
        .I3(\s21_reg_n_0_[7] ),
        .I4(s22[7]),
        .I5(\s2/s2/C_2 ),
        .O(ans2[9]));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_4__1 
       (.I0(s12[7]),
        .I1(\s11_reg_n_0_[7] ),
        .I2(\s1/s2/C_2 ),
        .O(ans1[7]));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_5__1 
       (.I0(s22[7]),
        .I1(\s21_reg_n_0_[7] ),
        .I2(\s2/s2/C_2 ),
        .O(ans2[7]));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[7]_i_6__1 
       (.I0(\final/C_4 ),
        .I1(ans2[5]),
        .I2(ans1[5]),
        .I3(ans2[6]),
        .I4(ans1[6]),
        .O(\final/C_6 ));
  LUT6 #(
    .INIT(64'h7DFFFF7DFF7DBEFF)) 
    \out[7]_i_7__1 
       (.I0(\s1/F6/S ),
        .I1(\s2/f5/S ),
        .I2(\s2/C2 ),
        .I3(s12[8]),
        .I4(\s11_reg_n_0_[8] ),
        .I5(\s1/C2 ),
        .O(\out[7]_i_7__1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_8__1 
       (.I0(\s21_reg_n_0_[9] ),
        .I1(s22[9]),
        .O(\s2/F6/S ));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_9__1 
       (.I0(\s2/s2/C_0 ),
        .I1(s22[5]),
        .I2(\s21_reg_n_0_[5] ),
        .I3(s22[6]),
        .I4(\s21_reg_n_0_[6] ),
        .O(\s2/s2/C_2 ));
  FDCE \out_reg[0] 
       (.C(clock),
        .CE(out),
        .CLR(\out_reg[0]_0 ),
        .D(\out[0]_i_1__1_n_0 ),
        .Q(m_axis_tdata[0]));
  FDCE \out_reg[1] 
       (.C(clock),
        .CE(out),
        .CLR(\out_reg[0]_0 ),
        .D(\out[1]_i_1__1_n_0 ),
        .Q(m_axis_tdata[1]));
  FDCE \out_reg[2] 
       (.C(clock),
        .CE(out),
        .CLR(\out_reg[0]_0 ),
        .D(\out[2]_i_1__1_n_0 ),
        .Q(m_axis_tdata[2]));
  FDCE \out_reg[3] 
       (.C(clock),
        .CE(out),
        .CLR(\out_reg[0]_0 ),
        .D(\out[3]_i_1__1_n_0 ),
        .Q(m_axis_tdata[3]));
  FDCE \out_reg[4] 
       (.C(clock),
        .CE(out),
        .CLR(\out_reg[0]_0 ),
        .D(\out[4]_i_1__1_n_0 ),
        .Q(m_axis_tdata[4]));
  FDCE \out_reg[5] 
       (.C(clock),
        .CE(out),
        .CLR(\out_reg[0]_0 ),
        .D(\out[5]_i_1__1_n_0 ),
        .Q(m_axis_tdata[5]));
  FDCE \out_reg[6] 
       (.C(clock),
        .CE(out),
        .CLR(\out_reg[0]_0 ),
        .D(\out[6]_i_1__1_n_0 ),
        .Q(m_axis_tdata[6]));
  FDCE \out_reg[7] 
       (.C(clock),
        .CE(out),
        .CLR(\out_reg[0]_0 ),
        .D(\out[7]_i_2__1_n_0 ),
        .Q(m_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[0]_i_1__1 
       (.I0(xreg2[0]),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(\xreg1_reg_n_0_[0] ),
        .O(\s11[0]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[1]_i_1__1 
       (.I0(xreg2[1]),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(\xreg1_reg_n_0_[1] ),
        .O(\s11[1]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[2]_i_1__1 
       (.I0(xreg2[2]),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(\xreg1_reg_n_0_[2] ),
        .O(\s11[2]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[3]_i_1__1 
       (.I0(xreg2[3]),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(\xreg1_reg_n_0_[3] ),
        .O(\s11[3]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[4]_i_1__1 
       (.I0(xreg2[4]),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(\xreg1_reg_n_0_[4] ),
        .O(\s11[4]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[5]_i_1__1 
       (.I0(xreg2[5]),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(\xreg1_reg_n_0_[5] ),
        .O(\s11[5]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[6]_i_1__1 
       (.I0(xreg2[6]),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(\xreg1_reg_n_0_[6] ),
        .O(\s11[6]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[7]_i_1__1 
       (.I0(xreg2[7]),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(\xreg1_reg_n_0_[7] ),
        .O(\s11[7]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[8]_i_1__1 
       (.I0(xreg2[8]),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(\xreg1_reg_n_0_[8] ),
        .O(\s11[8]_i_1__1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \s11[9]_i_1__1 
       (.I0(\xreg1_reg_n_0_[9] ),
        .I1(xreg2[9]),
        .O(\s11[9]_i_1__1_n_0 ));
  FDCE \s11_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s11[0]_i_1__1_n_0 ),
        .Q(\s11_reg_n_0_[0] ));
  FDCE \s11_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s11[1]_i_1__1_n_0 ),
        .Q(\s11_reg_n_0_[1] ));
  FDCE \s11_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s11[2]_i_1__1_n_0 ),
        .Q(\s11_reg_n_0_[2] ));
  FDCE \s11_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s11[3]_i_1__1_n_0 ),
        .Q(\s11_reg_n_0_[3] ));
  FDCE \s11_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s11[4]_i_1__1_n_0 ),
        .Q(\s11_reg_n_0_[4] ));
  FDCE \s11_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s11[5]_i_1__1_n_0 ),
        .Q(\s11_reg_n_0_[5] ));
  FDCE \s11_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s11[6]_i_1__1_n_0 ),
        .Q(\s11_reg_n_0_[6] ));
  FDCE \s11_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s11[7]_i_1__1_n_0 ),
        .Q(\s11_reg_n_0_[7] ));
  FDCE \s11_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s11[8]_i_1__1_n_0 ),
        .Q(\s11_reg_n_0_[8] ));
  FDCE \s11_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s11[9]_i_1__1_n_0 ),
        .Q(\s11_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[0]_i_1__1 
       (.I0(\xreg1_reg_n_0_[0] ),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(xreg2[0]),
        .O(\s12[0]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[1]_i_1__1 
       (.I0(\xreg1_reg_n_0_[1] ),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(xreg2[1]),
        .O(\s12[1]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[2]_i_1__1 
       (.I0(\xreg1_reg_n_0_[2] ),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(xreg2[2]),
        .O(\s12[2]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[3]_i_1__1 
       (.I0(\xreg1_reg_n_0_[3] ),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(xreg2[3]),
        .O(\s12[3]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[4]_i_1__1 
       (.I0(\xreg1_reg_n_0_[4] ),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(xreg2[4]),
        .O(\s12[4]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[5]_i_1__1 
       (.I0(\xreg1_reg_n_0_[5] ),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(xreg2[5]),
        .O(\s12[5]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[6]_i_1__1 
       (.I0(\xreg1_reg_n_0_[6] ),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(xreg2[6]),
        .O(\s12[6]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[7]_i_1__1 
       (.I0(\xreg1_reg_n_0_[7] ),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(xreg2[7]),
        .O(\s12[7]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[8]_i_1__1 
       (.I0(\xreg1_reg_n_0_[8] ),
        .I1(\s12[8]_i_2__1_n_0 ),
        .I2(xreg2[8]),
        .O(\s12[8]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT5 #(
    .INIT(32'hB200FFB2)) 
    \s12[8]_i_2__1 
       (.I0(\s12[8]_i_3__1_n_0 ),
        .I1(\xreg1_reg_n_0_[8] ),
        .I2(xreg2[8]),
        .I3(xreg2[9]),
        .I4(\xreg1_reg_n_0_[9] ),
        .O(\s12[8]_i_2__1_n_0 ));
  LUT5 #(
    .INIT(32'h7050FFFF)) 
    \s12[8]_i_3__1 
       (.I0(\s12[8]_i_4__1_n_0 ),
        .I1(\s12[8]_i_5__1_n_0 ),
        .I2(\s12[8]_i_6__1_n_0 ),
        .I3(\s12[8]_i_7__1_n_0 ),
        .I4(\s12[8]_i_8__1_n_0 ),
        .O(\s12[8]_i_3__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s12[8]_i_4__1 
       (.I0(\xreg1_reg_n_0_[4] ),
        .I1(xreg2[4]),
        .I2(xreg2[5]),
        .I3(\xreg1_reg_n_0_[5] ),
        .O(\s12[8]_i_4__1_n_0 ));
  LUT5 #(
    .INIT(32'h2F22BF2F)) 
    \s12[8]_i_5__1 
       (.I0(\xreg1_reg_n_0_[3] ),
        .I1(xreg2[3]),
        .I2(\s12[8]_i_9__1_n_0 ),
        .I3(\xreg1_reg_n_0_[2] ),
        .I4(xreg2[2]),
        .O(\s12[8]_i_5__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT4 #(
    .INIT(16'hBB2B)) 
    \s12[8]_i_6__1 
       (.I0(xreg2[7]),
        .I1(\xreg1_reg_n_0_[7] ),
        .I2(\xreg1_reg_n_0_[6] ),
        .I3(xreg2[6]),
        .O(\s12[8]_i_6__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s12[8]_i_7__1 
       (.I0(xreg2[4]),
        .I1(\xreg1_reg_n_0_[4] ),
        .I2(\xreg1_reg_n_0_[5] ),
        .I3(xreg2[5]),
        .O(\s12[8]_i_7__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT4 #(
    .INIT(16'hB0FB)) 
    \s12[8]_i_8__1 
       (.I0(\xreg1_reg_n_0_[6] ),
        .I1(xreg2[6]),
        .I2(\xreg1_reg_n_0_[7] ),
        .I3(xreg2[7]),
        .O(\s12[8]_i_8__1_n_0 ));
  LUT6 #(
    .INIT(64'h40F440F4FFFF40F4)) 
    \s12[8]_i_9__1 
       (.I0(\xreg1_reg_n_0_[0] ),
        .I1(xreg2[0]),
        .I2(xreg2[1]),
        .I3(\xreg1_reg_n_0_[1] ),
        .I4(xreg2[3]),
        .I5(\xreg1_reg_n_0_[3] ),
        .O(\s12[8]_i_9__1_n_0 ));
  LUT5 #(
    .INIT(32'h0000DF00)) 
    \s12[9]_i_1__1 
       (.I0(\full_reg_n_0_[1] ),
        .I1(m_axis_tready),
        .I2(\m_axis_tkeep[2] ),
        .I3(\full_reg_n_0_[0] ),
        .I4(hold),
        .O(s21));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s12[9]_i_2__1 
       (.I0(\xreg1_reg_n_0_[9] ),
        .I1(xreg2[9]),
        .O(\s12[9]_i_2__1_n_0 ));
  FDCE \s12_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s12[0]_i_1__1_n_0 ),
        .Q(s12[0]));
  FDCE \s12_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s12[1]_i_1__1_n_0 ),
        .Q(s12[1]));
  FDCE \s12_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s12[2]_i_1__1_n_0 ),
        .Q(s12[2]));
  FDCE \s12_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s12[3]_i_1__1_n_0 ),
        .Q(s12[3]));
  FDCE \s12_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s12[4]_i_1__1_n_0 ),
        .Q(s12[4]));
  FDCE \s12_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s12[5]_i_1__1_n_0 ),
        .Q(s12[5]));
  FDCE \s12_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s12[6]_i_1__1_n_0 ),
        .Q(s12[6]));
  FDCE \s12_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s12[7]_i_1__1_n_0 ),
        .Q(s12[7]));
  FDCE \s12_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s12[8]_i_1__1_n_0 ),
        .Q(s12[8]));
  FDCE \s12_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s12[9]_i_2__1_n_0 ),
        .Q(s12[9]));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[0]_i_1__1 
       (.I0(yreg2[0]),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(\yreg1_reg_n_0_[0] ),
        .O(\s21[0]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[1]_i_1__1 
       (.I0(yreg2[1]),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(\yreg1_reg_n_0_[1] ),
        .O(\s21[1]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[2]_i_1__1 
       (.I0(yreg2[2]),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(\yreg1_reg_n_0_[2] ),
        .O(\s21[2]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[3]_i_1__1 
       (.I0(yreg2[3]),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(\yreg1_reg_n_0_[3] ),
        .O(\s21[3]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[4]_i_1__1 
       (.I0(yreg2[4]),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(\yreg1_reg_n_0_[4] ),
        .O(\s21[4]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[5]_i_1__1 
       (.I0(yreg2[5]),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(\yreg1_reg_n_0_[5] ),
        .O(\s21[5]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[6]_i_1__1 
       (.I0(yreg2[6]),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(\yreg1_reg_n_0_[6] ),
        .O(\s21[6]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[7]_i_1__1 
       (.I0(yreg2[7]),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(\yreg1_reg_n_0_[7] ),
        .O(\s21[7]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[8]_i_1__1 
       (.I0(yreg2[8]),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(\yreg1_reg_n_0_[8] ),
        .O(\s21[8]_i_1__1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \s21[9]_i_1__1 
       (.I0(\yreg1_reg_n_0_[9] ),
        .I1(yreg2[9]),
        .O(\s21[9]_i_1__1_n_0 ));
  FDCE \s21_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s21[0]_i_1__1_n_0 ),
        .Q(\s21_reg_n_0_[0] ));
  FDCE \s21_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s21[1]_i_1__1_n_0 ),
        .Q(\s21_reg_n_0_[1] ));
  FDCE \s21_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s21[2]_i_1__1_n_0 ),
        .Q(\s21_reg_n_0_[2] ));
  FDCE \s21_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s21[3]_i_1__1_n_0 ),
        .Q(\s21_reg_n_0_[3] ));
  FDCE \s21_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s21[4]_i_1__1_n_0 ),
        .Q(\s21_reg_n_0_[4] ));
  FDCE \s21_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s21[5]_i_1__1_n_0 ),
        .Q(\s21_reg_n_0_[5] ));
  FDCE \s21_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s21[6]_i_1__1_n_0 ),
        .Q(\s21_reg_n_0_[6] ));
  FDCE \s21_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s21[7]_i_1__1_n_0 ),
        .Q(\s21_reg_n_0_[7] ));
  FDCE \s21_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s21[8]_i_1__1_n_0 ),
        .Q(\s21_reg_n_0_[8] ));
  FDCE \s21_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s21[9]_i_1__1_n_0 ),
        .Q(\s21_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[0]_i_1__1 
       (.I0(\yreg1_reg_n_0_[0] ),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(yreg2[0]),
        .O(\s22[0]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[1]_i_1__1 
       (.I0(\yreg1_reg_n_0_[1] ),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(yreg2[1]),
        .O(\s22[1]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[2]_i_1__1 
       (.I0(\yreg1_reg_n_0_[2] ),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(yreg2[2]),
        .O(\s22[2]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[3]_i_1__1 
       (.I0(\yreg1_reg_n_0_[3] ),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(yreg2[3]),
        .O(\s22[3]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[4]_i_1__1 
       (.I0(\yreg1_reg_n_0_[4] ),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(yreg2[4]),
        .O(\s22[4]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[5]_i_1__1 
       (.I0(\yreg1_reg_n_0_[5] ),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(yreg2[5]),
        .O(\s22[5]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[6]_i_1__1 
       (.I0(\yreg1_reg_n_0_[6] ),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(yreg2[6]),
        .O(\s22[6]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[7]_i_1__1 
       (.I0(\yreg1_reg_n_0_[7] ),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(yreg2[7]),
        .O(\s22[7]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[8]_i_1__1 
       (.I0(\yreg1_reg_n_0_[8] ),
        .I1(\s22[8]_i_2__1_n_0 ),
        .I2(yreg2[8]),
        .O(\s22[8]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT5 #(
    .INIT(32'hB200FFB2)) 
    \s22[8]_i_2__1 
       (.I0(\s22[8]_i_3__1_n_0 ),
        .I1(\yreg1_reg_n_0_[8] ),
        .I2(yreg2[8]),
        .I3(yreg2[9]),
        .I4(\yreg1_reg_n_0_[9] ),
        .O(\s22[8]_i_2__1_n_0 ));
  LUT5 #(
    .INIT(32'h7050FFFF)) 
    \s22[8]_i_3__1 
       (.I0(\s22[8]_i_4__1_n_0 ),
        .I1(\s22[8]_i_5__1_n_0 ),
        .I2(\s22[8]_i_6__1_n_0 ),
        .I3(\s22[8]_i_7__1_n_0 ),
        .I4(\s22[8]_i_8__1_n_0 ),
        .O(\s22[8]_i_3__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s22[8]_i_4__1 
       (.I0(\yreg1_reg_n_0_[4] ),
        .I1(yreg2[4]),
        .I2(yreg2[5]),
        .I3(\yreg1_reg_n_0_[5] ),
        .O(\s22[8]_i_4__1_n_0 ));
  LUT5 #(
    .INIT(32'h4D44DDDD)) 
    \s22[8]_i_5__1 
       (.I0(yreg2[3]),
        .I1(\yreg1_reg_n_0_[3] ),
        .I2(yreg2[2]),
        .I3(\yreg1_reg_n_0_[2] ),
        .I4(\s22[8]_i_9__1_n_0 ),
        .O(\s22[8]_i_5__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT4 #(
    .INIT(16'hBB2B)) 
    \s22[8]_i_6__1 
       (.I0(yreg2[7]),
        .I1(\yreg1_reg_n_0_[7] ),
        .I2(\yreg1_reg_n_0_[6] ),
        .I3(yreg2[6]),
        .O(\s22[8]_i_6__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s22[8]_i_7__1 
       (.I0(yreg2[4]),
        .I1(\yreg1_reg_n_0_[4] ),
        .I2(\yreg1_reg_n_0_[5] ),
        .I3(yreg2[5]),
        .O(\s22[8]_i_7__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT4 #(
    .INIT(16'hB0FB)) 
    \s22[8]_i_8__1 
       (.I0(\yreg1_reg_n_0_[6] ),
        .I1(yreg2[6]),
        .I2(\yreg1_reg_n_0_[7] ),
        .I3(yreg2[7]),
        .O(\s22[8]_i_8__1_n_0 ));
  LUT6 #(
    .INIT(64'h40F440F4FFFF40F4)) 
    \s22[8]_i_9__1 
       (.I0(\yreg1_reg_n_0_[0] ),
        .I1(yreg2[0]),
        .I2(yreg2[1]),
        .I3(\yreg1_reg_n_0_[1] ),
        .I4(yreg2[2]),
        .I5(\yreg1_reg_n_0_[2] ),
        .O(\s22[8]_i_9__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s22[9]_i_1__1 
       (.I0(\yreg1_reg_n_0_[9] ),
        .I1(yreg2[9]),
        .O(\s22[9]_i_1__1_n_0 ));
  FDCE \s22_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s22[0]_i_1__1_n_0 ),
        .Q(s22[0]));
  FDCE \s22_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s22[1]_i_1__1_n_0 ),
        .Q(s22[1]));
  FDCE \s22_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s22[2]_i_1__1_n_0 ),
        .Q(s22[2]));
  FDCE \s22_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s22[3]_i_1__1_n_0 ),
        .Q(s22[3]));
  FDCE \s22_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s22[4]_i_1__1_n_0 ),
        .Q(s22[4]));
  FDCE \s22_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s22[5]_i_1__1_n_0 ),
        .Q(s22[5]));
  FDCE \s22_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s22[6]_i_1__1_n_0 ),
        .Q(s22[6]));
  FDCE \s22_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s22[7]_i_1__1_n_0 ),
        .Q(s22[7]));
  FDCE \s22_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s22[8]_i_1__1_n_0 ),
        .Q(s22[8]));
  FDCE \s22_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(\out_reg[0]_0 ),
        .D(\s22[9]_i_1__1_n_0 ),
        .Q(s22[9]));
  LUT6 #(
    .INIT(64'h00000000F7FF0000)) 
    \xreg1[9]_i_1__1 
       (.I0(\full_reg_n_0_[0] ),
        .I1(\m_axis_tkeep[2] ),
        .I2(m_axis_tready),
        .I3(\full_reg_n_0_[1] ),
        .I4(\valid_reg[2] ),
        .I5(hold),
        .O(xreg1));
  FDCE \xreg1_reg[0] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(\inputvals_reg[119] [0]),
        .Q(\xreg1_reg_n_0_[0] ));
  FDCE \xreg1_reg[1] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(\inputvals_reg[119] [1]),
        .Q(\xreg1_reg_n_0_[1] ));
  FDCE \xreg1_reg[2] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(\inputvals_reg[119] [2]),
        .Q(\xreg1_reg_n_0_[2] ));
  FDCE \xreg1_reg[3] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(\inputvals_reg[119] [3]),
        .Q(\xreg1_reg_n_0_[3] ));
  FDCE \xreg1_reg[4] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(\inputvals_reg[119] [4]),
        .Q(\xreg1_reg_n_0_[4] ));
  FDCE \xreg1_reg[5] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(\inputvals_reg[119] [5]),
        .Q(\xreg1_reg_n_0_[5] ));
  FDCE \xreg1_reg[6] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(\inputvals_reg[119] [6]),
        .Q(\xreg1_reg_n_0_[6] ));
  FDCE \xreg1_reg[7] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(\inputvals_reg[119] [7]),
        .Q(\xreg1_reg_n_0_[7] ));
  FDCE \xreg1_reg[8] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(\inputvals_reg[119] [8]),
        .Q(\xreg1_reg_n_0_[8] ));
  FDCE \xreg1_reg[9] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(\inputvals_reg[119] [9]),
        .Q(\xreg1_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \xreg2[0]_i_1__1 
       (.I0(Q[16]),
        .I1(Q[0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \xreg2[1]_i_1__1 
       (.I0(Q[1]),
        .I1(Q[17]),
        .I2(Q[8]),
        .I3(Q[0]),
        .I4(Q[16]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \xreg2[2]_i_1__1 
       (.I0(Q[2]),
        .I1(Q[9]),
        .I2(Q[18]),
        .I3(Q[8]),
        .I4(Q[17]),
        .I5(\c5/x12/C_1 ),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \xreg2[2]_i_2__1 
       (.I0(Q[16]),
        .I1(Q[0]),
        .I2(Q[8]),
        .I3(Q[17]),
        .I4(Q[1]),
        .O(\c5/x12/C_1 ));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \xreg2[3]_i_1__1 
       (.I0(Q[3]),
        .I1(\c5/outx11 [3]),
        .I2(\c5/x12/C_2 ),
        .O(D[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \xreg2[3]_i_2__1 
       (.I0(Q[10]),
        .I1(Q[19]),
        .I2(Q[9]),
        .I3(Q[18]),
        .I4(Q[8]),
        .I5(Q[17]),
        .O(\c5/outx11 [3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \xreg2[3]_i_3__1 
       (.I0(\c5/x12/C_1 ),
        .I1(Q[17]),
        .I2(Q[8]),
        .I3(Q[18]),
        .I4(Q[9]),
        .I5(Q[2]),
        .O(\c5/x12/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg2[4]_i_1__1 
       (.I0(Q[4]),
        .I1(Q[11]),
        .I2(Q[20]),
        .I3(\c5/x11/C_3 ),
        .I4(\c5/x12/C_3 ),
        .O(D[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \xreg2[5]_i_1__1 
       (.I0(\c5/x12/C_3 ),
        .I1(\c5/x11/C_3 ),
        .I2(Q[20]),
        .I3(Q[11]),
        .I4(Q[4]),
        .I5(\c5/x12/f6/S ),
        .O(D[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[5]_i_2__1 
       (.I0(\c5/x12/C_1 ),
        .I1(\c5/outx11 [2]),
        .I2(Q[2]),
        .I3(\c5/outx11 [3]),
        .I4(Q[3]),
        .O(\c5/x12/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \xreg2[5]_i_3__1 
       (.I0(Q[17]),
        .I1(Q[8]),
        .I2(Q[18]),
        .I3(Q[9]),
        .I4(Q[19]),
        .I5(Q[10]),
        .O(\c5/x11/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg2[5]_i_4__1 
       (.I0(\c5/x11/C_3 ),
        .I1(Q[20]),
        .I2(Q[11]),
        .I3(Q[21]),
        .I4(Q[12]),
        .I5(Q[5]),
        .O(\c5/x12/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \xreg2[5]_i_5__1 
       (.I0(Q[9]),
        .I1(Q[18]),
        .I2(Q[8]),
        .I3(Q[17]),
        .O(\c5/outx11 [2]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg2[6]_i_1__1 
       (.I0(Q[6]),
        .I1(Q[13]),
        .I2(Q[22]),
        .I3(\c5/x11/C_5 ),
        .I4(\c5/x12/C_5 ),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[6]_i_2__1 
       (.I0(\c5/x11/C_3 ),
        .I1(Q[20]),
        .I2(Q[11]),
        .I3(Q[21]),
        .I4(Q[12]),
        .O(\c5/x11/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[6]_i_3__1 
       (.I0(\c5/x12/C_3 ),
        .I1(\c5/outx11 [4]),
        .I2(Q[4]),
        .I3(\c5/outx11 [5]),
        .I4(Q[5]),
        .O(\c5/x12/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \xreg2[6]_i_4__1 
       (.I0(Q[11]),
        .I1(Q[20]),
        .I2(\c5/x11/C_3 ),
        .O(\c5/outx11 [4]));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \xreg2[6]_i_5__1 
       (.I0(Q[12]),
        .I1(Q[21]),
        .I2(Q[11]),
        .I3(Q[20]),
        .I4(\c5/x11/C_3 ),
        .O(\c5/outx11 [5]));
  LUT2 #(
    .INIT(4'h6)) 
    \xreg2[7]_i_1__1 
       (.I0(\c5/x12/f8/S ),
        .I1(\c5/x12/C_6 ),
        .O(D[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg2[7]_i_2__1 
       (.I0(\c5/x11/C_5 ),
        .I1(Q[22]),
        .I2(Q[13]),
        .I3(Q[23]),
        .I4(Q[14]),
        .I5(Q[7]),
        .O(\c5/x12/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \xreg2[8]_i_1__1 
       (.I0(Q[15]),
        .I1(Q[7]),
        .I2(Q[14]),
        .I3(Q[23]),
        .I4(\c5/x11/C_6 ),
        .I5(\c5/x12/C_6 ),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \xreg2[9]_i_1__1 
       (.I0(\c5/x12/C_6 ),
        .I1(\c5/x11/C_6 ),
        .I2(Q[23]),
        .I3(Q[14]),
        .I4(Q[7]),
        .I5(Q[15]),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \xreg2[9]_i_2__1 
       (.I0(\c5/x12/C_5 ),
        .I1(\c5/x11/C_5 ),
        .I2(Q[22]),
        .I3(Q[13]),
        .I4(Q[6]),
        .O(\c5/x12/C_6 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \xreg2[9]_i_3__1 
       (.I0(\c5/x11/C_5 ),
        .I1(Q[22]),
        .I2(Q[13]),
        .O(\c5/x11/C_6 ));
  FDCE \xreg2_reg[0] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(D[0]),
        .Q(xreg2[0]));
  FDCE \xreg2_reg[1] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(D[1]),
        .Q(xreg2[1]));
  FDCE \xreg2_reg[2] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(D[2]),
        .Q(xreg2[2]));
  FDCE \xreg2_reg[3] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(D[3]),
        .Q(xreg2[3]));
  FDCE \xreg2_reg[4] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(D[4]),
        .Q(xreg2[4]));
  FDCE \xreg2_reg[5] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(D[5]),
        .Q(xreg2[5]));
  FDCE \xreg2_reg[6] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(D[6]),
        .Q(xreg2[6]));
  FDCE \xreg2_reg[7] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(D[7]),
        .Q(xreg2[7]));
  FDCE \xreg2_reg[8] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(D[8]),
        .Q(xreg2[8]));
  FDCE \xreg2_reg[9] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\out_reg[0]_0 ),
        .D(D[9]),
        .Q(xreg2[9]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg1[0]_i_1__1 
       (.I0(Q[48]),
        .I1(Q[16]),
        .O(\y12/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \yreg1[1]_i_1__1 
       (.I0(Q[17]),
        .I1(Q[49]),
        .I2(Q[32]),
        .I3(Q[16]),
        .I4(Q[48]),
        .O(outy12[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \yreg1[2]_i_1__1 
       (.I0(Q[18]),
        .I1(Q[33]),
        .I2(Q[50]),
        .I3(Q[32]),
        .I4(Q[49]),
        .I5(\y12/C_1 ),
        .O(outy12[2]));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \yreg1[2]_i_2__1 
       (.I0(Q[48]),
        .I1(Q[16]),
        .I2(Q[32]),
        .I3(Q[49]),
        .I4(Q[17]),
        .O(\y12/C_1 ));
  LUT3 #(
    .INIT(8'h96)) 
    \yreg1[3]_i_1__1 
       (.I0(Q[19]),
        .I1(outy11[3]),
        .I2(\y12/C_2 ),
        .O(outy12[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \yreg1[3]_i_2__1 
       (.I0(Q[34]),
        .I1(Q[51]),
        .I2(Q[33]),
        .I3(Q[50]),
        .I4(Q[32]),
        .I5(Q[49]),
        .O(outy11[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \yreg1[3]_i_3__1 
       (.I0(\y12/C_1 ),
        .I1(Q[49]),
        .I2(Q[32]),
        .I3(Q[50]),
        .I4(Q[33]),
        .I5(Q[18]),
        .O(\y12/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg1[4]_i_1__1 
       (.I0(Q[20]),
        .I1(Q[35]),
        .I2(Q[52]),
        .I3(\y11/C_3 ),
        .I4(\y12/C_3 ),
        .O(outy12[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \yreg1[5]_i_1__1 
       (.I0(\y12/C_3 ),
        .I1(\y11/C_3 ),
        .I2(Q[52]),
        .I3(Q[35]),
        .I4(Q[20]),
        .I5(\y12/f6/S ),
        .O(outy12[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[5]_i_2__1 
       (.I0(\y12/C_1 ),
        .I1(outy11[2]),
        .I2(Q[18]),
        .I3(outy11[3]),
        .I4(Q[19]),
        .O(\y12/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \yreg1[5]_i_3__1 
       (.I0(Q[49]),
        .I1(Q[32]),
        .I2(Q[50]),
        .I3(Q[33]),
        .I4(Q[51]),
        .I5(Q[34]),
        .O(\y11/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg1[5]_i_4__1 
       (.I0(\y11/C_3 ),
        .I1(Q[52]),
        .I2(Q[35]),
        .I3(Q[53]),
        .I4(Q[36]),
        .I5(Q[21]),
        .O(\y12/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \yreg1[5]_i_5__1 
       (.I0(Q[33]),
        .I1(Q[50]),
        .I2(Q[32]),
        .I3(Q[49]),
        .O(outy11[2]));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg1[6]_i_1__1 
       (.I0(Q[22]),
        .I1(Q[37]),
        .I2(Q[54]),
        .I3(\y11/C_5 ),
        .I4(\y12/C_5 ),
        .O(outy12[6]));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[6]_i_2__1 
       (.I0(\y11/C_3 ),
        .I1(Q[52]),
        .I2(Q[35]),
        .I3(Q[53]),
        .I4(Q[36]),
        .O(\y11/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[6]_i_3__1 
       (.I0(\y12/C_3 ),
        .I1(outy11[4]),
        .I2(Q[20]),
        .I3(outy11[5]),
        .I4(Q[21]),
        .O(\y12/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg1[6]_i_4__1 
       (.I0(Q[35]),
        .I1(Q[52]),
        .I2(\y11/C_3 ),
        .O(outy11[4]));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \yreg1[6]_i_5__1 
       (.I0(Q[36]),
        .I1(Q[53]),
        .I2(Q[35]),
        .I3(Q[52]),
        .I4(\y11/C_3 ),
        .O(outy11[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg1[7]_i_1__1 
       (.I0(\y12/f8/S ),
        .I1(\y12/C_6 ),
        .O(outy12[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg1[7]_i_2__1 
       (.I0(\y11/C_5 ),
        .I1(Q[54]),
        .I2(Q[37]),
        .I3(Q[55]),
        .I4(Q[38]),
        .I5(Q[23]),
        .O(\y12/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \yreg1[8]_i_1__1 
       (.I0(Q[39]),
        .I1(Q[23]),
        .I2(Q[38]),
        .I3(Q[55]),
        .I4(\y11/C_6 ),
        .I5(\y12/C_6 ),
        .O(outy12[8]));
  LUT6 #(
    .INIT(64'hF7FF000000000000)) 
    \yreg1[9]_i_1__1 
       (.I0(\full_reg_n_0_[0] ),
        .I1(\m_axis_tkeep[2] ),
        .I2(m_axis_tready),
        .I3(\full_reg_n_0_[1] ),
        .I4(\valid_reg[2] ),
        .I5(hold_reg),
        .O(yreg1));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \yreg1[9]_i_2__1 
       (.I0(\y12/C_6 ),
        .I1(\y11/C_6 ),
        .I2(Q[55]),
        .I3(Q[38]),
        .I4(Q[23]),
        .I5(Q[39]),
        .O(outy12[9]));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \yreg1[9]_i_3__0 
       (.I0(\y12/C_5 ),
        .I1(\y11/C_5 ),
        .I2(Q[54]),
        .I3(Q[37]),
        .I4(Q[22]),
        .O(\y12/C_6 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \yreg1[9]_i_4__1 
       (.I0(\y11/C_5 ),
        .I1(Q[54]),
        .I2(Q[37]),
        .O(\y11/C_6 ));
  FDRE \yreg1_reg[0] 
       (.C(clock),
        .CE(yreg1),
        .D(\y12/f1/S ),
        .Q(\yreg1_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \yreg1_reg[1] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[1]),
        .Q(\yreg1_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \yreg1_reg[2] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[2]),
        .Q(\yreg1_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \yreg1_reg[3] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[3]),
        .Q(\yreg1_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \yreg1_reg[4] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[4]),
        .Q(\yreg1_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \yreg1_reg[5] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[5]),
        .Q(\yreg1_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \yreg1_reg[6] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[6]),
        .Q(\yreg1_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \yreg1_reg[7] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[7]),
        .Q(\yreg1_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \yreg1_reg[8] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[8]),
        .Q(\yreg1_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \yreg1_reg[9] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[9]),
        .Q(\yreg1_reg_n_0_[9] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \yreg2[0]_i_1__1 
       (.I0(Q[40]),
        .I1(Q[0]),
        .O(\y22/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \yreg2[1]_i_1__1 
       (.I0(Q[1]),
        .I1(Q[41]),
        .I2(Q[24]),
        .I3(Q[0]),
        .I4(Q[40]),
        .O(outy22[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \yreg2[2]_i_1__1 
       (.I0(Q[2]),
        .I1(Q[25]),
        .I2(Q[42]),
        .I3(Q[24]),
        .I4(Q[41]),
        .I5(\y22/C_1 ),
        .O(outy22[2]));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \yreg2[2]_i_2__1 
       (.I0(Q[40]),
        .I1(Q[0]),
        .I2(Q[24]),
        .I3(Q[41]),
        .I4(Q[1]),
        .O(\y22/C_1 ));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg2[3]_i_1__1 
       (.I0(Q[3]),
        .I1(outy21[3]),
        .I2(\y22/C_2 ),
        .O(outy22[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \yreg2[3]_i_2__1 
       (.I0(Q[26]),
        .I1(Q[43]),
        .I2(Q[25]),
        .I3(Q[42]),
        .I4(Q[24]),
        .I5(Q[41]),
        .O(outy21[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \yreg2[3]_i_3__1 
       (.I0(\y22/C_1 ),
        .I1(Q[41]),
        .I2(Q[24]),
        .I3(Q[42]),
        .I4(Q[25]),
        .I5(Q[2]),
        .O(\y22/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg2[4]_i_1__1 
       (.I0(Q[4]),
        .I1(Q[27]),
        .I2(Q[44]),
        .I3(\y21/C_3 ),
        .I4(\y22/C_3 ),
        .O(outy22[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \yreg2[5]_i_1__1 
       (.I0(\y22/C_3 ),
        .I1(\y21/C_3 ),
        .I2(Q[44]),
        .I3(Q[27]),
        .I4(Q[4]),
        .I5(\y22/f6/S ),
        .O(outy22[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[5]_i_2__1 
       (.I0(\y22/C_1 ),
        .I1(outy21[2]),
        .I2(Q[2]),
        .I3(outy21[3]),
        .I4(Q[3]),
        .O(\y22/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \yreg2[5]_i_3__1 
       (.I0(Q[41]),
        .I1(Q[24]),
        .I2(Q[42]),
        .I3(Q[25]),
        .I4(Q[43]),
        .I5(Q[26]),
        .O(\y21/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg2[5]_i_4__1 
       (.I0(\y21/C_3 ),
        .I1(Q[44]),
        .I2(Q[27]),
        .I3(Q[45]),
        .I4(Q[28]),
        .I5(Q[5]),
        .O(\y22/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \yreg2[5]_i_5__1 
       (.I0(Q[25]),
        .I1(Q[42]),
        .I2(Q[24]),
        .I3(Q[41]),
        .O(outy21[2]));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg2[6]_i_1__1 
       (.I0(Q[6]),
        .I1(Q[29]),
        .I2(Q[46]),
        .I3(\y21/C_5 ),
        .I4(\y22/C_5 ),
        .O(outy22[6]));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[6]_i_2__1 
       (.I0(\y21/C_3 ),
        .I1(Q[44]),
        .I2(Q[27]),
        .I3(Q[45]),
        .I4(Q[28]),
        .O(\y21/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[6]_i_3__1 
       (.I0(\y22/C_3 ),
        .I1(outy21[4]),
        .I2(Q[4]),
        .I3(outy21[5]),
        .I4(Q[5]),
        .O(\y22/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg2[6]_i_4__1 
       (.I0(Q[27]),
        .I1(Q[44]),
        .I2(\y21/C_3 ),
        .O(outy21[4]));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \yreg2[6]_i_5__1 
       (.I0(Q[28]),
        .I1(Q[45]),
        .I2(Q[27]),
        .I3(Q[44]),
        .I4(\y21/C_3 ),
        .O(outy21[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg2[7]_i_1__1 
       (.I0(\y22/f8/S ),
        .I1(\y22/C_6 ),
        .O(outy22[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg2[7]_i_2__1 
       (.I0(\y21/C_5 ),
        .I1(Q[46]),
        .I2(Q[29]),
        .I3(Q[47]),
        .I4(Q[30]),
        .I5(Q[7]),
        .O(\y22/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \yreg2[8]_i_1__1 
       (.I0(Q[31]),
        .I1(Q[7]),
        .I2(Q[30]),
        .I3(Q[47]),
        .I4(\y21/C_6 ),
        .I5(\y22/C_6 ),
        .O(outy22[8]));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \yreg2[9]_i_1__1 
       (.I0(\y22/C_6 ),
        .I1(\y21/C_6 ),
        .I2(Q[47]),
        .I3(Q[30]),
        .I4(Q[7]),
        .I5(Q[31]),
        .O(outy22[9]));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \yreg2[9]_i_2__1 
       (.I0(\y22/C_5 ),
        .I1(\y21/C_5 ),
        .I2(Q[46]),
        .I3(Q[29]),
        .I4(Q[6]),
        .O(\y22/C_6 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \yreg2[9]_i_3__1 
       (.I0(\y21/C_5 ),
        .I1(Q[46]),
        .I2(Q[29]),
        .O(\y21/C_6 ));
  FDRE \yreg2_reg[0] 
       (.C(clock),
        .CE(yreg1),
        .D(\y22/f1/S ),
        .Q(yreg2[0]),
        .R(1'b0));
  FDRE \yreg2_reg[1] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[1]),
        .Q(yreg2[1]),
        .R(1'b0));
  FDRE \yreg2_reg[2] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[2]),
        .Q(yreg2[2]),
        .R(1'b0));
  FDRE \yreg2_reg[3] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[3]),
        .Q(yreg2[3]),
        .R(1'b0));
  FDRE \yreg2_reg[4] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[4]),
        .Q(yreg2[4]),
        .R(1'b0));
  FDRE \yreg2_reg[5] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[5]),
        .Q(yreg2[5]),
        .R(1'b0));
  FDRE \yreg2_reg[6] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[6]),
        .Q(yreg2[6]),
        .R(1'b0));
  FDRE \yreg2_reg[7] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[7]),
        .Q(yreg2[7]),
        .R(1'b0));
  FDRE \yreg2_reg[8] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[8]),
        .Q(yreg2[8]),
        .R(1'b0));
  FDRE \yreg2_reg[9] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[9]),
        .Q(yreg2[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "convolution" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convolution_2
   (\full_reg[2]_0 ,
    \m_axis_tkeep[3] ,
    m_axis_tdata,
    Q,
    reset,
    D,
    clock,
    hold,
    m_axis_tready,
    \valid_reg[3] ,
    hold_reg);
  output \full_reg[2]_0 ;
  output \m_axis_tkeep[3] ;
  output [7:0]m_axis_tdata;
  input [55:0]Q;
  input reset;
  input [9:0]D;
  input clock;
  input hold;
  input m_axis_tready;
  input [0:0]\valid_reg[3] ;
  input hold_reg;

  wire [9:0]D;
  wire [55:0]Q;
  wire [7:1]ans1;
  wire [9:1]ans2;
  wire clock;
  wire \final/C_1 ;
  wire \final/C_2 ;
  wire \final/C_4 ;
  wire \final/C_6 ;
  wire \final/f1/Ca ;
  wire \final/f2/S ;
  wire \final/f4/S ;
  wire \final/f5/S ;
  wire \final/f6/S ;
  wire \full[0]_i_1__2_n_0 ;
  wire \full[1]_i_1__2_n_0 ;
  wire \full[2]_i_1__2_n_0 ;
  wire \full_reg[2]_0 ;
  wire \full_reg_n_0_[0] ;
  wire \full_reg_n_0_[1] ;
  wire hold;
  wire hold_reg;
  wire [7:0]m_axis_tdata;
  wire \m_axis_tkeep[3] ;
  wire m_axis_tready;
  wire out;
  wire \out[0]_i_1__2_n_0 ;
  wire \out[0]_i_2__2_n_0 ;
  wire \out[1]_i_1__2_n_0 ;
  wire \out[1]_i_2__2_n_0 ;
  wire \out[2]_i_1__2_n_0 ;
  wire \out[2]_i_2__2_n_0 ;
  wire \out[3]_i_1__2_n_0 ;
  wire \out[3]_i_2__2_n_0 ;
  wire \out[4]_i_1__2_n_0 ;
  wire \out[4]_i_2__2_n_0 ;
  wire \out[5]_i_1__2_n_0 ;
  wire \out[5]_i_2__2_n_0 ;
  wire \out[6]_i_1__2_n_0 ;
  wire \out[6]_i_2__2_n_0 ;
  wire \out[7]_i_26__2_n_0 ;
  wire \out[7]_i_27__2_n_0 ;
  wire \out[7]_i_28__2_n_0 ;
  wire \out[7]_i_29__2_n_0 ;
  wire \out[7]_i_2__2_n_0 ;
  wire \out[7]_i_8__2_n_0 ;
  wire [5:2]outx21;
  wire [9:1]outx22;
  wire [5:2]outy11;
  wire [9:1]outy12;
  wire [5:2]outy21;
  wire [9:1]outy22;
  wire reset;
  wire \s1/C2 ;
  wire \s1/F6/S ;
  wire \s1/s1/C_2 ;
  wire \s1/s2/C_0 ;
  wire \s1/s2/C_2 ;
  wire \s1/s2/f2/S ;
  wire \s11[0]_i_1__2_n_0 ;
  wire \s11[1]_i_1__2_n_0 ;
  wire \s11[2]_i_1__2_n_0 ;
  wire \s11[3]_i_1__2_n_0 ;
  wire \s11[4]_i_1__2_n_0 ;
  wire \s11[5]_i_1__2_n_0 ;
  wire \s11[6]_i_1__2_n_0 ;
  wire \s11[7]_i_1__2_n_0 ;
  wire \s11[8]_i_1__2_n_0 ;
  wire \s11[9]_i_1__2_n_0 ;
  wire \s11_reg_n_0_[0] ;
  wire \s11_reg_n_0_[1] ;
  wire \s11_reg_n_0_[2] ;
  wire \s11_reg_n_0_[3] ;
  wire \s11_reg_n_0_[4] ;
  wire \s11_reg_n_0_[5] ;
  wire \s11_reg_n_0_[6] ;
  wire \s11_reg_n_0_[7] ;
  wire \s11_reg_n_0_[8] ;
  wire \s11_reg_n_0_[9] ;
  wire [9:0]s12;
  wire \s12[0]_i_1__2_n_0 ;
  wire \s12[1]_i_1__2_n_0 ;
  wire \s12[2]_i_1__2_n_0 ;
  wire \s12[3]_i_1__2_n_0 ;
  wire \s12[4]_i_1__2_n_0 ;
  wire \s12[5]_i_1__2_n_0 ;
  wire \s12[6]_i_1__2_n_0 ;
  wire \s12[7]_i_1__2_n_0 ;
  wire \s12[8]_i_1__2_n_0 ;
  wire \s12[8]_i_2__2_n_0 ;
  wire \s12[8]_i_3__2_n_0 ;
  wire \s12[8]_i_4__2_n_0 ;
  wire \s12[8]_i_5__2_n_0 ;
  wire \s12[8]_i_6__2_n_0 ;
  wire \s12[8]_i_7__2_n_0 ;
  wire \s12[8]_i_8__2_n_0 ;
  wire \s12[8]_i_9__2_n_0 ;
  wire \s12[9]_i_2__2_n_0 ;
  wire \s2/C2 ;
  wire \s2/F6/S ;
  wire \s2/f5/S ;
  wire \s2/s1/C_2 ;
  wire \s2/s1/f1/S ;
  wire \s2/s2/C_0 ;
  wire \s2/s2/C_2 ;
  wire \s2/s2/f2/S ;
  wire s21;
  wire \s21[0]_i_1__2_n_0 ;
  wire \s21[1]_i_1__2_n_0 ;
  wire \s21[2]_i_1__2_n_0 ;
  wire \s21[3]_i_1__2_n_0 ;
  wire \s21[4]_i_1__2_n_0 ;
  wire \s21[5]_i_1__2_n_0 ;
  wire \s21[6]_i_1__2_n_0 ;
  wire \s21[7]_i_1__2_n_0 ;
  wire \s21[8]_i_1__2_n_0 ;
  wire \s21[9]_i_1__2_n_0 ;
  wire \s21_reg_n_0_[0] ;
  wire \s21_reg_n_0_[1] ;
  wire \s21_reg_n_0_[2] ;
  wire \s21_reg_n_0_[3] ;
  wire \s21_reg_n_0_[4] ;
  wire \s21_reg_n_0_[5] ;
  wire \s21_reg_n_0_[6] ;
  wire \s21_reg_n_0_[7] ;
  wire \s21_reg_n_0_[8] ;
  wire \s21_reg_n_0_[9] ;
  wire [9:0]s22;
  wire \s22[0]_i_1__2_n_0 ;
  wire \s22[1]_i_1__2_n_0 ;
  wire \s22[2]_i_1__2_n_0 ;
  wire \s22[3]_i_1__2_n_0 ;
  wire \s22[4]_i_1__2_n_0 ;
  wire \s22[5]_i_1__2_n_0 ;
  wire \s22[6]_i_1__2_n_0 ;
  wire \s22[7]_i_1__2_n_0 ;
  wire \s22[8]_i_1__2_n_0 ;
  wire \s22[8]_i_2__2_n_0 ;
  wire \s22[8]_i_3__2_n_0 ;
  wire \s22[8]_i_4__2_n_0 ;
  wire \s22[8]_i_5__2_n_0 ;
  wire \s22[8]_i_6__2_n_0 ;
  wire \s22[8]_i_7__2_n_0 ;
  wire \s22[8]_i_8__2_n_0 ;
  wire \s22[8]_i_9__2_n_0 ;
  wire \s22[9]_i_1__2_n_0 ;
  wire [0:0]\valid_reg[3] ;
  wire \x21/C_3 ;
  wire \x21/C_5 ;
  wire \x21/C_6 ;
  wire \x22/C_1 ;
  wire \x22/C_2 ;
  wire \x22/C_3 ;
  wire \x22/C_5 ;
  wire \x22/C_6 ;
  wire \x22/f1/S ;
  wire \x22/f6/S ;
  wire \x22/f8/S ;
  wire xreg1;
  wire \xreg1_reg_n_0_[0] ;
  wire \xreg1_reg_n_0_[1] ;
  wire \xreg1_reg_n_0_[2] ;
  wire \xreg1_reg_n_0_[3] ;
  wire \xreg1_reg_n_0_[4] ;
  wire \xreg1_reg_n_0_[5] ;
  wire \xreg1_reg_n_0_[6] ;
  wire \xreg1_reg_n_0_[7] ;
  wire \xreg1_reg_n_0_[8] ;
  wire \xreg1_reg_n_0_[9] ;
  wire [9:0]xreg2;
  wire \y11/C_3 ;
  wire \y11/C_5 ;
  wire \y11/C_6 ;
  wire \y12/C_1 ;
  wire \y12/C_2 ;
  wire \y12/C_3 ;
  wire \y12/C_5 ;
  wire \y12/C_6 ;
  wire \y12/f1/S ;
  wire \y12/f6/S ;
  wire \y12/f8/S ;
  wire \y21/C_3 ;
  wire \y21/C_5 ;
  wire \y21/C_6 ;
  wire \y22/C_1 ;
  wire \y22/C_2 ;
  wire \y22/C_3 ;
  wire \y22/C_5 ;
  wire \y22/C_6 ;
  wire \y22/f1/S ;
  wire \y22/f6/S ;
  wire \y22/f8/S ;
  wire yreg1;
  wire \yreg1_reg_n_0_[0] ;
  wire \yreg1_reg_n_0_[1] ;
  wire \yreg1_reg_n_0_[2] ;
  wire \yreg1_reg_n_0_[3] ;
  wire \yreg1_reg_n_0_[4] ;
  wire \yreg1_reg_n_0_[5] ;
  wire \yreg1_reg_n_0_[6] ;
  wire \yreg1_reg_n_0_[7] ;
  wire \yreg1_reg_n_0_[8] ;
  wire \yreg1_reg_n_0_[9] ;
  wire [9:0]yreg2;

  LUT6 #(
    .INIT(64'hEEFEEEEE44444444)) 
    \full[0]_i_1__2 
       (.I0(hold),
        .I1(\valid_reg[3] ),
        .I2(\full_reg_n_0_[1] ),
        .I3(m_axis_tready),
        .I4(\m_axis_tkeep[3] ),
        .I5(\full_reg_n_0_[0] ),
        .O(\full[0]_i_1__2_n_0 ));
  LUT5 #(
    .INIT(32'hEEFE4444)) 
    \full[1]_i_1__2 
       (.I0(hold),
        .I1(\full_reg_n_0_[0] ),
        .I2(\m_axis_tkeep[3] ),
        .I3(m_axis_tready),
        .I4(\full_reg_n_0_[1] ),
        .O(\full[1]_i_1__2_n_0 ));
  LUT5 #(
    .INIT(32'hFBFF4040)) 
    \full[2]_i_1__2 
       (.I0(hold),
        .I1(reset),
        .I2(\full_reg_n_0_[1] ),
        .I3(m_axis_tready),
        .I4(\m_axis_tkeep[3] ),
        .O(\full[2]_i_1__2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[0] 
       (.C(clock),
        .CE(1'b1),
        .CLR(\full_reg[2]_0 ),
        .D(\full[0]_i_1__2_n_0 ),
        .Q(\full_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[1] 
       (.C(clock),
        .CE(1'b1),
        .CLR(\full_reg[2]_0 ),
        .D(\full[1]_i_1__2_n_0 ),
        .Q(\full_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[2] 
       (.C(clock),
        .CE(1'b1),
        .CLR(\full_reg[2]_0 ),
        .D(\full[2]_i_1__2_n_0 ),
        .Q(\m_axis_tkeep[3] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[0]_i_1__2 
       (.I0(\out[0]_i_2__2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__2_n_0 ),
        .O(\out[0]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    \out[0]_i_2__2 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s11_reg_n_0_[0] ),
        .O(\out[0]_i_2__2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[1]_i_1__2 
       (.I0(\out[1]_i_2__2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__2_n_0 ),
        .O(\out[1]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT5 #(
    .INIT(32'hAA9696AA)) 
    \out[1]_i_2__2 
       (.I0(\final/f2/S ),
        .I1(\s11_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s21_reg_n_0_[0] ),
        .I4(s22[0]),
        .O(\out[1]_i_2__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair177" *) 
  LUT5 #(
    .INIT(32'hD22D2DD2)) 
    \out[1]_i_3__2 
       (.I0(s12[0]),
        .I1(\s11_reg_n_0_[0] ),
        .I2(\s11_reg_n_0_[1] ),
        .I3(s12[1]),
        .I4(ans2[1]),
        .O(\final/f2/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[2]_i_1__2 
       (.I0(\out[2]_i_2__2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__2_n_0 ),
        .O(\out[2]_i_1__2_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \out[2]_i_2__2 
       (.I0(ans1[2]),
        .I1(ans2[2]),
        .I2(\final/C_1 ),
        .O(\out[2]_i_2__2_n_0 ));
  LUT6 #(
    .INIT(64'h6966996969666966)) 
    \out[2]_i_3__2 
       (.I0(s12[2]),
        .I1(\s11_reg_n_0_[2] ),
        .I2(\s11_reg_n_0_[1] ),
        .I3(s12[1]),
        .I4(\s11_reg_n_0_[0] ),
        .I5(s12[0]),
        .O(ans1[2]));
  LUT6 #(
    .INIT(64'h6966996969666966)) 
    \out[2]_i_4__2 
       (.I0(s22[2]),
        .I1(\s21_reg_n_0_[2] ),
        .I2(\s21_reg_n_0_[1] ),
        .I3(s22[1]),
        .I4(\s21_reg_n_0_[0] ),
        .I5(s22[0]),
        .O(ans2[2]));
  LUT6 #(
    .INIT(64'h0CC0D44D4DD40CC0)) 
    \out[2]_i_5__2 
       (.I0(\s2/s1/f1/S ),
        .I1(ans2[1]),
        .I2(s12[1]),
        .I3(\s11_reg_n_0_[1] ),
        .I4(\s11_reg_n_0_[0] ),
        .I5(s12[0]),
        .O(\final/C_1 ));
  (* SOFT_HLUTNM = "soft_lutpair181" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \out[2]_i_6__2 
       (.I0(\s21_reg_n_0_[0] ),
        .I1(s22[0]),
        .O(\s2/s1/f1/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[3]_i_1__2 
       (.I0(\out[3]_i_2__2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__2_n_0 ),
        .O(\out[3]_i_1__2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out[3]_i_2__2 
       (.I0(\final/f4/S ),
        .I1(\final/C_2 ),
        .O(\out[3]_i_2__2_n_0 ));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    \out[3]_i_3__2 
       (.I0(\s1/s1/C_2 ),
        .I1(\s11_reg_n_0_[3] ),
        .I2(s12[3]),
        .I3(\s2/s1/C_2 ),
        .I4(\s21_reg_n_0_[3] ),
        .I5(s22[3]),
        .O(\final/f4/S ));
  LUT6 #(
    .INIT(64'hDF0DFFFF0000DF0D)) 
    \out[4]_i_10__2 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s22[1]),
        .I3(\s21_reg_n_0_[1] ),
        .I4(s22[2]),
        .I5(\s21_reg_n_0_[2] ),
        .O(\s2/s1/C_2 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[4]_i_1__2 
       (.I0(\out[4]_i_2__2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__2_n_0 ),
        .O(\out[4]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'h711717718EE8E88E)) 
    \out[4]_i_2__2 
       (.I0(\final/C_2 ),
        .I1(ans2[3]),
        .I2(s12[3]),
        .I3(\s11_reg_n_0_[3] ),
        .I4(\s1/s1/C_2 ),
        .I5(\final/f5/S ),
        .O(\out[4]_i_2__2_n_0 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[4]_i_3__2 
       (.I0(\final/f1/Ca ),
        .I1(ans2[1]),
        .I2(ans1[1]),
        .I3(ans2[2]),
        .I4(ans1[2]),
        .O(\final/C_2 ));
  LUT3 #(
    .INIT(8'h69)) 
    \out[4]_i_4__2 
       (.I0(s22[3]),
        .I1(\s21_reg_n_0_[3] ),
        .I2(\s2/s1/C_2 ),
        .O(ans2[3]));
  LUT6 #(
    .INIT(64'hDF0DFFFF0000DF0D)) 
    \out[4]_i_5__2 
       (.I0(s12[0]),
        .I1(\s11_reg_n_0_[0] ),
        .I2(s12[1]),
        .I3(\s11_reg_n_0_[1] ),
        .I4(s12[2]),
        .I5(\s11_reg_n_0_[2] ),
        .O(\s1/s1/C_2 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \out[4]_i_6__2 
       (.I0(\s1/s1/C_2 ),
        .I1(s12[3]),
        .I2(\s11_reg_n_0_[3] ),
        .I3(\s11_reg_n_0_[4] ),
        .I4(s12[4]),
        .I5(ans2[4]),
        .O(\final/f5/S ));
  (* SOFT_HLUTNM = "soft_lutpair181" *) 
  LUT4 #(
    .INIT(16'h0660)) 
    \out[4]_i_7__2 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s11_reg_n_0_[0] ),
        .O(\final/f1/Ca ));
  LUT4 #(
    .INIT(16'h6966)) 
    \out[4]_i_8__2 
       (.I0(s22[1]),
        .I1(\s21_reg_n_0_[1] ),
        .I2(\s21_reg_n_0_[0] ),
        .I3(s22[0]),
        .O(ans2[1]));
  (* SOFT_HLUTNM = "soft_lutpair177" *) 
  LUT4 #(
    .INIT(16'h6966)) 
    \out[4]_i_9__2 
       (.I0(s12[1]),
        .I1(\s11_reg_n_0_[1] ),
        .I2(\s11_reg_n_0_[0] ),
        .I3(s12[0]),
        .O(ans1[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[5]_i_1__2 
       (.I0(\out[5]_i_2__2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__2_n_0 ),
        .O(\out[5]_i_1__2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out[5]_i_2__2 
       (.I0(\final/f6/S ),
        .I1(\final/C_4 ),
        .O(\out[5]_i_2__2_n_0 ));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    \out[5]_i_3__2 
       (.I0(\s1/s2/C_0 ),
        .I1(\s11_reg_n_0_[5] ),
        .I2(s12[5]),
        .I3(\s2/s2/C_0 ),
        .I4(\s21_reg_n_0_[5] ),
        .I5(s22[5]),
        .O(\final/f6/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[6]_i_1__2 
       (.I0(\out[6]_i_2__2_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__2_n_0 ),
        .O(\out[6]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \out[6]_i_2__2 
       (.I0(ans2[6]),
        .I1(ans1[6]),
        .I2(ans1[5]),
        .I3(ans2[5]),
        .I4(\final/C_4 ),
        .O(\out[6]_i_2__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_10__2 
       (.I0(\s2/s2/C_0 ),
        .I1(s22[5]),
        .I2(\s21_reg_n_0_[5] ),
        .I3(s22[6]),
        .I4(\s21_reg_n_0_[6] ),
        .O(\s2/s2/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_11__2 
       (.I0(\s1/s2/C_0 ),
        .I1(s12[5]),
        .I2(\s11_reg_n_0_[5] ),
        .I3(s12[6]),
        .I4(\s11_reg_n_0_[6] ),
        .O(\s1/s2/C_2 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[7]_i_12__2 
       (.I0(\final/C_2 ),
        .I1(ans2[3]),
        .I2(ans1[3]),
        .I3(ans2[4]),
        .I4(ans1[4]),
        .O(\final/C_4 ));
  (* SOFT_HLUTNM = "soft_lutpair201" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_13__2 
       (.I0(s22[5]),
        .I1(\s21_reg_n_0_[5] ),
        .I2(\s2/s2/C_0 ),
        .O(ans2[5]));
  (* SOFT_HLUTNM = "soft_lutpair202" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_14__2 
       (.I0(s12[5]),
        .I1(\s11_reg_n_0_[5] ),
        .I2(\s1/s2/C_0 ),
        .O(ans1[5]));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_15__2 
       (.I0(s22[6]),
        .I1(\s21_reg_n_0_[6] ),
        .I2(\s21_reg_n_0_[5] ),
        .I3(s22[5]),
        .I4(\s2/s2/C_0 ),
        .O(ans2[6]));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_16__2 
       (.I0(s12[6]),
        .I1(\s11_reg_n_0_[6] ),
        .I2(\s11_reg_n_0_[5] ),
        .I3(s12[5]),
        .I4(\s1/s2/C_0 ),
        .O(ans1[6]));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_17__2 
       (.I0(\s11_reg_n_0_[9] ),
        .I1(s12[9]),
        .O(\s1/F6/S ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_18__2 
       (.I0(\s21_reg_n_0_[8] ),
        .I1(s22[8]),
        .O(\s2/f5/S ));
  LUT6 #(
    .INIT(64'hEF0EFFFF0000EF0E)) 
    \out[7]_i_19__2 
       (.I0(\out[7]_i_26__2_n_0 ),
        .I1(\out[7]_i_27__2_n_0 ),
        .I2(s22[6]),
        .I3(\s21_reg_n_0_[6] ),
        .I4(s22[7]),
        .I5(\s21_reg_n_0_[7] ),
        .O(\s2/C2 ));
  LUT4 #(
    .INIT(16'h00D0)) 
    \out[7]_i_1__2 
       (.I0(\m_axis_tkeep[3] ),
        .I1(m_axis_tready),
        .I2(\full_reg_n_0_[1] ),
        .I3(hold),
        .O(out));
  LUT6 #(
    .INIT(64'hEF0EFFFF0000EF0E)) 
    \out[7]_i_20__2 
       (.I0(\out[7]_i_28__2_n_0 ),
        .I1(\out[7]_i_29__2_n_0 ),
        .I2(s12[6]),
        .I3(\s11_reg_n_0_[6] ),
        .I4(s12[7]),
        .I5(\s11_reg_n_0_[7] ),
        .O(\s1/C2 ));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_21__2 
       (.I0(\s2/s1/C_2 ),
        .I1(s22[3]),
        .I2(\s21_reg_n_0_[3] ),
        .I3(s22[4]),
        .I4(\s21_reg_n_0_[4] ),
        .O(\s2/s2/C_0 ));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_22__2 
       (.I0(\s1/s1/C_2 ),
        .I1(s12[3]),
        .I2(\s11_reg_n_0_[3] ),
        .I3(s12[4]),
        .I4(\s11_reg_n_0_[4] ),
        .O(\s1/s2/C_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_23__2 
       (.I0(s12[3]),
        .I1(\s11_reg_n_0_[3] ),
        .I2(\s1/s1/C_2 ),
        .O(ans1[3]));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_24__2 
       (.I0(s22[4]),
        .I1(\s21_reg_n_0_[4] ),
        .I2(\s21_reg_n_0_[3] ),
        .I3(s22[3]),
        .I4(\s2/s1/C_2 ),
        .O(ans2[4]));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_25__2 
       (.I0(s12[4]),
        .I1(\s11_reg_n_0_[4] ),
        .I2(\s11_reg_n_0_[3] ),
        .I3(s12[3]),
        .I4(\s1/s1/C_2 ),
        .O(ans1[4]));
  LUT6 #(
    .INIT(64'h8A088A8A08088A08)) 
    \out[7]_i_26__2 
       (.I0(\s2/s2/f2/S ),
        .I1(\s21_reg_n_0_[4] ),
        .I2(s22[4]),
        .I3(\s21_reg_n_0_[3] ),
        .I4(s22[3]),
        .I5(\s2/s1/C_2 ),
        .O(\out[7]_i_26__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair201" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \out[7]_i_27__2 
       (.I0(\s21_reg_n_0_[5] ),
        .I1(s22[5]),
        .O(\out[7]_i_27__2_n_0 ));
  LUT6 #(
    .INIT(64'h8A088A8A08088A08)) 
    \out[7]_i_28__2 
       (.I0(\s1/s2/f2/S ),
        .I1(\s11_reg_n_0_[4] ),
        .I2(s12[4]),
        .I3(\s11_reg_n_0_[3] ),
        .I4(s12[3]),
        .I5(\s1/s1/C_2 ),
        .O(\out[7]_i_28__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair202" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \out[7]_i_29__2 
       (.I0(\s11_reg_n_0_[5] ),
        .I1(s12[5]),
        .O(\out[7]_i_29__2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \out[7]_i_2__2 
       (.I0(ans2[9]),
        .I1(ans1[7]),
        .I2(ans2[7]),
        .I3(\final/C_6 ),
        .I4(\out[7]_i_8__2_n_0 ),
        .O(\out[7]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_30__2 
       (.I0(\s21_reg_n_0_[5] ),
        .I1(s22[5]),
        .O(\s2/s2/f2/S ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_31__0 
       (.I0(\s11_reg_n_0_[5] ),
        .I1(s12[5]),
        .O(\s1/s2/f2/S ));
  LUT1 #(
    .INIT(2'h1)) 
    \out[7]_i_3__0 
       (.I0(reset),
        .O(\full_reg[2]_0 ));
  LUT6 #(
    .INIT(64'h65A66565A6A665A6)) 
    \out[7]_i_4__2 
       (.I0(\s2/F6/S ),
        .I1(\s21_reg_n_0_[8] ),
        .I2(s22[8]),
        .I3(\s21_reg_n_0_[7] ),
        .I4(s22[7]),
        .I5(\s2/s2/C_2 ),
        .O(ans2[9]));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_5__2 
       (.I0(s12[7]),
        .I1(\s11_reg_n_0_[7] ),
        .I2(\s1/s2/C_2 ),
        .O(ans1[7]));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_6__2 
       (.I0(s22[7]),
        .I1(\s21_reg_n_0_[7] ),
        .I2(\s2/s2/C_2 ),
        .O(ans2[7]));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[7]_i_7__2 
       (.I0(\final/C_4 ),
        .I1(ans2[5]),
        .I2(ans1[5]),
        .I3(ans2[6]),
        .I4(ans1[6]),
        .O(\final/C_6 ));
  LUT6 #(
    .INIT(64'h7DFFFF7DFF7DBEFF)) 
    \out[7]_i_8__2 
       (.I0(\s1/F6/S ),
        .I1(\s2/f5/S ),
        .I2(\s2/C2 ),
        .I3(s12[8]),
        .I4(\s11_reg_n_0_[8] ),
        .I5(\s1/C2 ),
        .O(\out[7]_i_8__2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_9__2 
       (.I0(\s21_reg_n_0_[9] ),
        .I1(s22[9]),
        .O(\s2/F6/S ));
  FDCE \out_reg[0] 
       (.C(clock),
        .CE(out),
        .CLR(\full_reg[2]_0 ),
        .D(\out[0]_i_1__2_n_0 ),
        .Q(m_axis_tdata[0]));
  FDCE \out_reg[1] 
       (.C(clock),
        .CE(out),
        .CLR(\full_reg[2]_0 ),
        .D(\out[1]_i_1__2_n_0 ),
        .Q(m_axis_tdata[1]));
  FDCE \out_reg[2] 
       (.C(clock),
        .CE(out),
        .CLR(\full_reg[2]_0 ),
        .D(\out[2]_i_1__2_n_0 ),
        .Q(m_axis_tdata[2]));
  FDCE \out_reg[3] 
       (.C(clock),
        .CE(out),
        .CLR(\full_reg[2]_0 ),
        .D(\out[3]_i_1__2_n_0 ),
        .Q(m_axis_tdata[3]));
  FDCE \out_reg[4] 
       (.C(clock),
        .CE(out),
        .CLR(\full_reg[2]_0 ),
        .D(\out[4]_i_1__2_n_0 ),
        .Q(m_axis_tdata[4]));
  FDCE \out_reg[5] 
       (.C(clock),
        .CE(out),
        .CLR(\full_reg[2]_0 ),
        .D(\out[5]_i_1__2_n_0 ),
        .Q(m_axis_tdata[5]));
  FDCE \out_reg[6] 
       (.C(clock),
        .CE(out),
        .CLR(\full_reg[2]_0 ),
        .D(\out[6]_i_1__2_n_0 ),
        .Q(m_axis_tdata[6]));
  FDCE \out_reg[7] 
       (.C(clock),
        .CE(out),
        .CLR(\full_reg[2]_0 ),
        .D(\out[7]_i_2__2_n_0 ),
        .Q(m_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair198" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[0]_i_1__2 
       (.I0(xreg2[0]),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(\xreg1_reg_n_0_[0] ),
        .O(\s11[0]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair197" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[1]_i_1__2 
       (.I0(xreg2[1]),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(\xreg1_reg_n_0_[1] ),
        .O(\s11[1]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair196" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[2]_i_1__2 
       (.I0(xreg2[2]),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(\xreg1_reg_n_0_[2] ),
        .O(\s11[2]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair200" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[3]_i_1__2 
       (.I0(xreg2[3]),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(\xreg1_reg_n_0_[3] ),
        .O(\s11[3]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair195" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[4]_i_1__2 
       (.I0(xreg2[4]),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(\xreg1_reg_n_0_[4] ),
        .O(\s11[4]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair194" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[5]_i_1__2 
       (.I0(xreg2[5]),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(\xreg1_reg_n_0_[5] ),
        .O(\s11[5]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair193" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[6]_i_1__2 
       (.I0(xreg2[6]),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(\xreg1_reg_n_0_[6] ),
        .O(\s11[6]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair199" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[7]_i_1__2 
       (.I0(xreg2[7]),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(\xreg1_reg_n_0_[7] ),
        .O(\s11[7]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair192" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[8]_i_1__2 
       (.I0(xreg2[8]),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(\xreg1_reg_n_0_[8] ),
        .O(\s11[8]_i_1__2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \s11[9]_i_1__2 
       (.I0(\xreg1_reg_n_0_[9] ),
        .I1(xreg2[9]),
        .O(\s11[9]_i_1__2_n_0 ));
  FDCE \s11_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s11[0]_i_1__2_n_0 ),
        .Q(\s11_reg_n_0_[0] ));
  FDCE \s11_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s11[1]_i_1__2_n_0 ),
        .Q(\s11_reg_n_0_[1] ));
  FDCE \s11_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s11[2]_i_1__2_n_0 ),
        .Q(\s11_reg_n_0_[2] ));
  FDCE \s11_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s11[3]_i_1__2_n_0 ),
        .Q(\s11_reg_n_0_[3] ));
  FDCE \s11_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s11[4]_i_1__2_n_0 ),
        .Q(\s11_reg_n_0_[4] ));
  FDCE \s11_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s11[5]_i_1__2_n_0 ),
        .Q(\s11_reg_n_0_[5] ));
  FDCE \s11_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s11[6]_i_1__2_n_0 ),
        .Q(\s11_reg_n_0_[6] ));
  FDCE \s11_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s11[7]_i_1__2_n_0 ),
        .Q(\s11_reg_n_0_[7] ));
  FDCE \s11_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s11[8]_i_1__2_n_0 ),
        .Q(\s11_reg_n_0_[8] ));
  FDCE \s11_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s11[9]_i_1__2_n_0 ),
        .Q(\s11_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair198" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[0]_i_1__2 
       (.I0(\xreg1_reg_n_0_[0] ),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(xreg2[0]),
        .O(\s12[0]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair197" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[1]_i_1__2 
       (.I0(\xreg1_reg_n_0_[1] ),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(xreg2[1]),
        .O(\s12[1]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair196" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[2]_i_1__2 
       (.I0(\xreg1_reg_n_0_[2] ),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(xreg2[2]),
        .O(\s12[2]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair200" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[3]_i_1__2 
       (.I0(\xreg1_reg_n_0_[3] ),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(xreg2[3]),
        .O(\s12[3]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair195" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[4]_i_1__2 
       (.I0(\xreg1_reg_n_0_[4] ),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(xreg2[4]),
        .O(\s12[4]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair194" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[5]_i_1__2 
       (.I0(\xreg1_reg_n_0_[5] ),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(xreg2[5]),
        .O(\s12[5]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair193" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[6]_i_1__2 
       (.I0(\xreg1_reg_n_0_[6] ),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(xreg2[6]),
        .O(\s12[6]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair199" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[7]_i_1__2 
       (.I0(\xreg1_reg_n_0_[7] ),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(xreg2[7]),
        .O(\s12[7]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair192" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[8]_i_1__2 
       (.I0(\xreg1_reg_n_0_[8] ),
        .I1(\s12[8]_i_2__2_n_0 ),
        .I2(xreg2[8]),
        .O(\s12[8]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair176" *) 
  LUT5 #(
    .INIT(32'hB200FFB2)) 
    \s12[8]_i_2__2 
       (.I0(\s12[8]_i_3__2_n_0 ),
        .I1(\xreg1_reg_n_0_[8] ),
        .I2(xreg2[8]),
        .I3(xreg2[9]),
        .I4(\xreg1_reg_n_0_[9] ),
        .O(\s12[8]_i_2__2_n_0 ));
  LUT5 #(
    .INIT(32'h7050FFFF)) 
    \s12[8]_i_3__2 
       (.I0(\s12[8]_i_4__2_n_0 ),
        .I1(\s12[8]_i_5__2_n_0 ),
        .I2(\s12[8]_i_6__2_n_0 ),
        .I3(\s12[8]_i_7__2_n_0 ),
        .I4(\s12[8]_i_8__2_n_0 ),
        .O(\s12[8]_i_3__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair178" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s12[8]_i_4__2 
       (.I0(\xreg1_reg_n_0_[4] ),
        .I1(xreg2[4]),
        .I2(xreg2[5]),
        .I3(\xreg1_reg_n_0_[5] ),
        .O(\s12[8]_i_4__2_n_0 ));
  LUT5 #(
    .INIT(32'h2F22BF2F)) 
    \s12[8]_i_5__2 
       (.I0(\xreg1_reg_n_0_[3] ),
        .I1(xreg2[3]),
        .I2(\s12[8]_i_9__2_n_0 ),
        .I3(\xreg1_reg_n_0_[2] ),
        .I4(xreg2[2]),
        .O(\s12[8]_i_5__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair179" *) 
  LUT4 #(
    .INIT(16'hBB2B)) 
    \s12[8]_i_6__2 
       (.I0(xreg2[7]),
        .I1(\xreg1_reg_n_0_[7] ),
        .I2(\xreg1_reg_n_0_[6] ),
        .I3(xreg2[6]),
        .O(\s12[8]_i_6__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair178" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s12[8]_i_7__2 
       (.I0(xreg2[4]),
        .I1(\xreg1_reg_n_0_[4] ),
        .I2(\xreg1_reg_n_0_[5] ),
        .I3(xreg2[5]),
        .O(\s12[8]_i_7__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair179" *) 
  LUT4 #(
    .INIT(16'hB0FB)) 
    \s12[8]_i_8__2 
       (.I0(\xreg1_reg_n_0_[6] ),
        .I1(xreg2[6]),
        .I2(\xreg1_reg_n_0_[7] ),
        .I3(xreg2[7]),
        .O(\s12[8]_i_8__2_n_0 ));
  LUT6 #(
    .INIT(64'h40F440F4FFFF40F4)) 
    \s12[8]_i_9__2 
       (.I0(\xreg1_reg_n_0_[0] ),
        .I1(xreg2[0]),
        .I2(xreg2[1]),
        .I3(\xreg1_reg_n_0_[1] ),
        .I4(xreg2[3]),
        .I5(\xreg1_reg_n_0_[3] ),
        .O(\s12[8]_i_9__2_n_0 ));
  LUT5 #(
    .INIT(32'h0000DF00)) 
    \s12[9]_i_1__2 
       (.I0(\full_reg_n_0_[1] ),
        .I1(m_axis_tready),
        .I2(\m_axis_tkeep[3] ),
        .I3(\full_reg_n_0_[0] ),
        .I4(hold),
        .O(s21));
  (* SOFT_HLUTNM = "soft_lutpair176" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s12[9]_i_2__2 
       (.I0(\xreg1_reg_n_0_[9] ),
        .I1(xreg2[9]),
        .O(\s12[9]_i_2__2_n_0 ));
  FDCE \s12_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s12[0]_i_1__2_n_0 ),
        .Q(s12[0]));
  FDCE \s12_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s12[1]_i_1__2_n_0 ),
        .Q(s12[1]));
  FDCE \s12_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s12[2]_i_1__2_n_0 ),
        .Q(s12[2]));
  FDCE \s12_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s12[3]_i_1__2_n_0 ),
        .Q(s12[3]));
  FDCE \s12_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s12[4]_i_1__2_n_0 ),
        .Q(s12[4]));
  FDCE \s12_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s12[5]_i_1__2_n_0 ),
        .Q(s12[5]));
  FDCE \s12_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s12[6]_i_1__2_n_0 ),
        .Q(s12[6]));
  FDCE \s12_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s12[7]_i_1__2_n_0 ),
        .Q(s12[7]));
  FDCE \s12_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s12[8]_i_1__2_n_0 ),
        .Q(s12[8]));
  FDCE \s12_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s12[9]_i_2__2_n_0 ),
        .Q(s12[9]));
  (* SOFT_HLUTNM = "soft_lutpair191" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[0]_i_1__2 
       (.I0(yreg2[0]),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(\yreg1_reg_n_0_[0] ),
        .O(\s21[0]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair190" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[1]_i_1__2 
       (.I0(yreg2[1]),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(\yreg1_reg_n_0_[1] ),
        .O(\s21[1]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair183" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[2]_i_1__2 
       (.I0(yreg2[2]),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(\yreg1_reg_n_0_[2] ),
        .O(\s21[2]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair184" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[3]_i_1__2 
       (.I0(yreg2[3]),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(\yreg1_reg_n_0_[3] ),
        .O(\s21[3]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair185" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[4]_i_1__2 
       (.I0(yreg2[4]),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(\yreg1_reg_n_0_[4] ),
        .O(\s21[4]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair187" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[5]_i_1__2 
       (.I0(yreg2[5]),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(\yreg1_reg_n_0_[5] ),
        .O(\s21[5]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair186" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[6]_i_1__2 
       (.I0(yreg2[6]),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(\yreg1_reg_n_0_[6] ),
        .O(\s21[6]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair189" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[7]_i_1__2 
       (.I0(yreg2[7]),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(\yreg1_reg_n_0_[7] ),
        .O(\s21[7]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair188" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[8]_i_1__2 
       (.I0(yreg2[8]),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(\yreg1_reg_n_0_[8] ),
        .O(\s21[8]_i_1__2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \s21[9]_i_1__2 
       (.I0(\yreg1_reg_n_0_[9] ),
        .I1(yreg2[9]),
        .O(\s21[9]_i_1__2_n_0 ));
  FDCE \s21_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s21[0]_i_1__2_n_0 ),
        .Q(\s21_reg_n_0_[0] ));
  FDCE \s21_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s21[1]_i_1__2_n_0 ),
        .Q(\s21_reg_n_0_[1] ));
  FDCE \s21_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s21[2]_i_1__2_n_0 ),
        .Q(\s21_reg_n_0_[2] ));
  FDCE \s21_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s21[3]_i_1__2_n_0 ),
        .Q(\s21_reg_n_0_[3] ));
  FDCE \s21_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s21[4]_i_1__2_n_0 ),
        .Q(\s21_reg_n_0_[4] ));
  FDCE \s21_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s21[5]_i_1__2_n_0 ),
        .Q(\s21_reg_n_0_[5] ));
  FDCE \s21_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s21[6]_i_1__2_n_0 ),
        .Q(\s21_reg_n_0_[6] ));
  FDCE \s21_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s21[7]_i_1__2_n_0 ),
        .Q(\s21_reg_n_0_[7] ));
  FDCE \s21_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s21[8]_i_1__2_n_0 ),
        .Q(\s21_reg_n_0_[8] ));
  FDCE \s21_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s21[9]_i_1__2_n_0 ),
        .Q(\s21_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair191" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[0]_i_1__2 
       (.I0(\yreg1_reg_n_0_[0] ),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(yreg2[0]),
        .O(\s22[0]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair190" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[1]_i_1__2 
       (.I0(\yreg1_reg_n_0_[1] ),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(yreg2[1]),
        .O(\s22[1]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair183" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[2]_i_1__2 
       (.I0(\yreg1_reg_n_0_[2] ),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(yreg2[2]),
        .O(\s22[2]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair184" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[3]_i_1__2 
       (.I0(\yreg1_reg_n_0_[3] ),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(yreg2[3]),
        .O(\s22[3]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair185" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[4]_i_1__2 
       (.I0(\yreg1_reg_n_0_[4] ),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(yreg2[4]),
        .O(\s22[4]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair187" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[5]_i_1__2 
       (.I0(\yreg1_reg_n_0_[5] ),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(yreg2[5]),
        .O(\s22[5]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair186" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[6]_i_1__2 
       (.I0(\yreg1_reg_n_0_[6] ),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(yreg2[6]),
        .O(\s22[6]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair189" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[7]_i_1__2 
       (.I0(\yreg1_reg_n_0_[7] ),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(yreg2[7]),
        .O(\s22[7]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair188" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[8]_i_1__2 
       (.I0(\yreg1_reg_n_0_[8] ),
        .I1(\s22[8]_i_2__2_n_0 ),
        .I2(yreg2[8]),
        .O(\s22[8]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair175" *) 
  LUT5 #(
    .INIT(32'hB200FFB2)) 
    \s22[8]_i_2__2 
       (.I0(\s22[8]_i_3__2_n_0 ),
        .I1(\yreg1_reg_n_0_[8] ),
        .I2(yreg2[8]),
        .I3(yreg2[9]),
        .I4(\yreg1_reg_n_0_[9] ),
        .O(\s22[8]_i_2__2_n_0 ));
  LUT5 #(
    .INIT(32'h7050FFFF)) 
    \s22[8]_i_3__2 
       (.I0(\s22[8]_i_4__2_n_0 ),
        .I1(\s22[8]_i_5__2_n_0 ),
        .I2(\s22[8]_i_6__2_n_0 ),
        .I3(\s22[8]_i_7__2_n_0 ),
        .I4(\s22[8]_i_8__2_n_0 ),
        .O(\s22[8]_i_3__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair182" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s22[8]_i_4__2 
       (.I0(\yreg1_reg_n_0_[4] ),
        .I1(yreg2[4]),
        .I2(yreg2[5]),
        .I3(\yreg1_reg_n_0_[5] ),
        .O(\s22[8]_i_4__2_n_0 ));
  LUT5 #(
    .INIT(32'h4D44DDDD)) 
    \s22[8]_i_5__2 
       (.I0(yreg2[3]),
        .I1(\yreg1_reg_n_0_[3] ),
        .I2(yreg2[2]),
        .I3(\yreg1_reg_n_0_[2] ),
        .I4(\s22[8]_i_9__2_n_0 ),
        .O(\s22[8]_i_5__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair180" *) 
  LUT4 #(
    .INIT(16'hBB2B)) 
    \s22[8]_i_6__2 
       (.I0(yreg2[7]),
        .I1(\yreg1_reg_n_0_[7] ),
        .I2(\yreg1_reg_n_0_[6] ),
        .I3(yreg2[6]),
        .O(\s22[8]_i_6__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair182" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s22[8]_i_7__2 
       (.I0(yreg2[4]),
        .I1(\yreg1_reg_n_0_[4] ),
        .I2(\yreg1_reg_n_0_[5] ),
        .I3(yreg2[5]),
        .O(\s22[8]_i_7__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair180" *) 
  LUT4 #(
    .INIT(16'hB0FB)) 
    \s22[8]_i_8__2 
       (.I0(\yreg1_reg_n_0_[6] ),
        .I1(yreg2[6]),
        .I2(\yreg1_reg_n_0_[7] ),
        .I3(yreg2[7]),
        .O(\s22[8]_i_8__2_n_0 ));
  LUT6 #(
    .INIT(64'h40F440F4FFFF40F4)) 
    \s22[8]_i_9__2 
       (.I0(\yreg1_reg_n_0_[0] ),
        .I1(yreg2[0]),
        .I2(yreg2[1]),
        .I3(\yreg1_reg_n_0_[1] ),
        .I4(yreg2[2]),
        .I5(\yreg1_reg_n_0_[2] ),
        .O(\s22[8]_i_9__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair175" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s22[9]_i_1__2 
       (.I0(\yreg1_reg_n_0_[9] ),
        .I1(yreg2[9]),
        .O(\s22[9]_i_1__2_n_0 ));
  FDCE \s22_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s22[0]_i_1__2_n_0 ),
        .Q(s22[0]));
  FDCE \s22_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s22[1]_i_1__2_n_0 ),
        .Q(s22[1]));
  FDCE \s22_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s22[2]_i_1__2_n_0 ),
        .Q(s22[2]));
  FDCE \s22_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s22[3]_i_1__2_n_0 ),
        .Q(s22[3]));
  FDCE \s22_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s22[4]_i_1__2_n_0 ),
        .Q(s22[4]));
  FDCE \s22_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s22[5]_i_1__2_n_0 ),
        .Q(s22[5]));
  FDCE \s22_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s22[6]_i_1__2_n_0 ),
        .Q(s22[6]));
  FDCE \s22_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s22[7]_i_1__2_n_0 ),
        .Q(s22[7]));
  FDCE \s22_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s22[8]_i_1__2_n_0 ),
        .Q(s22[8]));
  FDCE \s22_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(\full_reg[2]_0 ),
        .D(\s22[9]_i_1__2_n_0 ),
        .Q(s22[9]));
  LUT6 #(
    .INIT(64'h00000000F7FF0000)) 
    \xreg1[9]_i_1__2 
       (.I0(\full_reg_n_0_[0] ),
        .I1(\m_axis_tkeep[3] ),
        .I2(m_axis_tready),
        .I3(\full_reg_n_0_[1] ),
        .I4(\valid_reg[3] ),
        .I5(hold),
        .O(xreg1));
  FDCE \xreg1_reg[0] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(D[0]),
        .Q(\xreg1_reg_n_0_[0] ));
  FDCE \xreg1_reg[1] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(D[1]),
        .Q(\xreg1_reg_n_0_[1] ));
  FDCE \xreg1_reg[2] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(D[2]),
        .Q(\xreg1_reg_n_0_[2] ));
  FDCE \xreg1_reg[3] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(D[3]),
        .Q(\xreg1_reg_n_0_[3] ));
  FDCE \xreg1_reg[4] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(D[4]),
        .Q(\xreg1_reg_n_0_[4] ));
  FDCE \xreg1_reg[5] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(D[5]),
        .Q(\xreg1_reg_n_0_[5] ));
  FDCE \xreg1_reg[6] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(D[6]),
        .Q(\xreg1_reg_n_0_[6] ));
  FDCE \xreg1_reg[7] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(D[7]),
        .Q(\xreg1_reg_n_0_[7] ));
  FDCE \xreg1_reg[8] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(D[8]),
        .Q(\xreg1_reg_n_0_[8] ));
  FDCE \xreg1_reg[9] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(D[9]),
        .Q(\xreg1_reg_n_0_[9] ));
  LUT2 #(
    .INIT(4'h6)) 
    \xreg2[0]_i_1 
       (.I0(Q[16]),
        .I1(Q[0]),
        .O(\x22/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \xreg2[1]_i_1 
       (.I0(Q[1]),
        .I1(Q[17]),
        .I2(Q[8]),
        .I3(Q[0]),
        .I4(Q[16]),
        .O(outx22[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \xreg2[2]_i_1 
       (.I0(Q[2]),
        .I1(Q[9]),
        .I2(Q[18]),
        .I3(Q[8]),
        .I4(Q[17]),
        .I5(\x22/C_1 ),
        .O(outx22[2]));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \xreg2[2]_i_2 
       (.I0(Q[16]),
        .I1(Q[0]),
        .I2(Q[8]),
        .I3(Q[17]),
        .I4(Q[1]),
        .O(\x22/C_1 ));
  LUT3 #(
    .INIT(8'h96)) 
    \xreg2[3]_i_1 
       (.I0(Q[3]),
        .I1(outx21[3]),
        .I2(\x22/C_2 ),
        .O(outx22[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \xreg2[3]_i_2 
       (.I0(Q[10]),
        .I1(Q[19]),
        .I2(Q[9]),
        .I3(Q[18]),
        .I4(Q[8]),
        .I5(Q[17]),
        .O(outx21[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \xreg2[3]_i_3 
       (.I0(\x22/C_1 ),
        .I1(Q[17]),
        .I2(Q[8]),
        .I3(Q[18]),
        .I4(Q[9]),
        .I5(Q[2]),
        .O(\x22/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair174" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg2[4]_i_1 
       (.I0(Q[4]),
        .I1(Q[11]),
        .I2(Q[20]),
        .I3(\x21/C_3 ),
        .I4(\x22/C_3 ),
        .O(outx22[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \xreg2[5]_i_1 
       (.I0(\x22/C_3 ),
        .I1(\x21/C_3 ),
        .I2(Q[20]),
        .I3(Q[11]),
        .I4(Q[4]),
        .I5(\x22/f6/S ),
        .O(outx22[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[5]_i_2 
       (.I0(\x22/C_1 ),
        .I1(outx21[2]),
        .I2(Q[2]),
        .I3(outx21[3]),
        .I4(Q[3]),
        .O(\x22/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \xreg2[5]_i_3 
       (.I0(Q[17]),
        .I1(Q[8]),
        .I2(Q[18]),
        .I3(Q[9]),
        .I4(Q[19]),
        .I5(Q[10]),
        .O(\x21/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg2[5]_i_4 
       (.I0(\x21/C_3 ),
        .I1(Q[20]),
        .I2(Q[11]),
        .I3(Q[21]),
        .I4(Q[12]),
        .I5(Q[5]),
        .O(\x22/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \xreg2[5]_i_5 
       (.I0(Q[9]),
        .I1(Q[18]),
        .I2(Q[8]),
        .I3(Q[17]),
        .O(outx21[2]));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg2[6]_i_1 
       (.I0(Q[6]),
        .I1(Q[13]),
        .I2(Q[22]),
        .I3(\x21/C_5 ),
        .I4(\x22/C_5 ),
        .O(outx22[6]));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[6]_i_2 
       (.I0(\x21/C_3 ),
        .I1(Q[20]),
        .I2(Q[11]),
        .I3(Q[21]),
        .I4(Q[12]),
        .O(\x21/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[6]_i_3 
       (.I0(\x22/C_3 ),
        .I1(outx21[4]),
        .I2(Q[4]),
        .I3(outx21[5]),
        .I4(Q[5]),
        .O(\x22/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair174" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \xreg2[6]_i_4 
       (.I0(Q[11]),
        .I1(Q[20]),
        .I2(\x21/C_3 ),
        .O(outx21[4]));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \xreg2[6]_i_5 
       (.I0(Q[12]),
        .I1(Q[21]),
        .I2(Q[11]),
        .I3(Q[20]),
        .I4(\x21/C_3 ),
        .O(outx21[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \xreg2[7]_i_1 
       (.I0(\x22/f8/S ),
        .I1(\x22/C_6 ),
        .O(outx22[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg2[7]_i_2 
       (.I0(\x21/C_5 ),
        .I1(Q[22]),
        .I2(Q[13]),
        .I3(Q[23]),
        .I4(Q[14]),
        .I5(Q[7]),
        .O(\x22/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \xreg2[8]_i_1 
       (.I0(Q[15]),
        .I1(Q[7]),
        .I2(Q[14]),
        .I3(Q[23]),
        .I4(\x21/C_6 ),
        .I5(\x22/C_6 ),
        .O(outx22[8]));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \xreg2[9]_i_1 
       (.I0(\x22/C_6 ),
        .I1(\x21/C_6 ),
        .I2(Q[23]),
        .I3(Q[14]),
        .I4(Q[7]),
        .I5(Q[15]),
        .O(outx22[9]));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \xreg2[9]_i_2 
       (.I0(\x22/C_5 ),
        .I1(\x21/C_5 ),
        .I2(Q[22]),
        .I3(Q[13]),
        .I4(Q[6]),
        .O(\x22/C_6 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \xreg2[9]_i_3 
       (.I0(\x21/C_5 ),
        .I1(Q[22]),
        .I2(Q[13]),
        .O(\x21/C_6 ));
  FDCE \xreg2_reg[0] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(\x22/f1/S ),
        .Q(xreg2[0]));
  FDCE \xreg2_reg[1] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(outx22[1]),
        .Q(xreg2[1]));
  FDCE \xreg2_reg[2] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(outx22[2]),
        .Q(xreg2[2]));
  FDCE \xreg2_reg[3] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(outx22[3]),
        .Q(xreg2[3]));
  FDCE \xreg2_reg[4] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(outx22[4]),
        .Q(xreg2[4]));
  FDCE \xreg2_reg[5] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(outx22[5]),
        .Q(xreg2[5]));
  FDCE \xreg2_reg[6] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(outx22[6]),
        .Q(xreg2[6]));
  FDCE \xreg2_reg[7] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(outx22[7]),
        .Q(xreg2[7]));
  FDCE \xreg2_reg[8] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(outx22[8]),
        .Q(xreg2[8]));
  FDCE \xreg2_reg[9] 
       (.C(clock),
        .CE(xreg1),
        .CLR(\full_reg[2]_0 ),
        .D(outx22[9]),
        .Q(xreg2[9]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg1[0]_i_1__2 
       (.I0(Q[48]),
        .I1(Q[16]),
        .O(\y12/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \yreg1[1]_i_1__2 
       (.I0(Q[17]),
        .I1(Q[49]),
        .I2(Q[32]),
        .I3(Q[16]),
        .I4(Q[48]),
        .O(outy12[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \yreg1[2]_i_1__2 
       (.I0(Q[18]),
        .I1(Q[33]),
        .I2(Q[50]),
        .I3(Q[32]),
        .I4(Q[49]),
        .I5(\y12/C_1 ),
        .O(outy12[2]));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \yreg1[2]_i_2__2 
       (.I0(Q[48]),
        .I1(Q[16]),
        .I2(Q[32]),
        .I3(Q[49]),
        .I4(Q[17]),
        .O(\y12/C_1 ));
  LUT3 #(
    .INIT(8'h96)) 
    \yreg1[3]_i_1__2 
       (.I0(Q[19]),
        .I1(outy11[3]),
        .I2(\y12/C_2 ),
        .O(outy12[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \yreg1[3]_i_2__2 
       (.I0(Q[34]),
        .I1(Q[51]),
        .I2(Q[33]),
        .I3(Q[50]),
        .I4(Q[32]),
        .I5(Q[49]),
        .O(outy11[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \yreg1[3]_i_3__2 
       (.I0(\y12/C_1 ),
        .I1(Q[49]),
        .I2(Q[32]),
        .I3(Q[50]),
        .I4(Q[33]),
        .I5(Q[18]),
        .O(\y12/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg1[4]_i_1__2 
       (.I0(Q[20]),
        .I1(Q[35]),
        .I2(Q[52]),
        .I3(\y11/C_3 ),
        .I4(\y12/C_3 ),
        .O(outy12[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \yreg1[5]_i_1__2 
       (.I0(\y12/C_3 ),
        .I1(\y11/C_3 ),
        .I2(Q[52]),
        .I3(Q[35]),
        .I4(Q[20]),
        .I5(\y12/f6/S ),
        .O(outy12[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[5]_i_2__2 
       (.I0(\y12/C_1 ),
        .I1(outy11[2]),
        .I2(Q[18]),
        .I3(outy11[3]),
        .I4(Q[19]),
        .O(\y12/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \yreg1[5]_i_3__2 
       (.I0(Q[49]),
        .I1(Q[32]),
        .I2(Q[50]),
        .I3(Q[33]),
        .I4(Q[51]),
        .I5(Q[34]),
        .O(\y11/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg1[5]_i_4__2 
       (.I0(\y11/C_3 ),
        .I1(Q[52]),
        .I2(Q[35]),
        .I3(Q[53]),
        .I4(Q[36]),
        .I5(Q[21]),
        .O(\y12/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \yreg1[5]_i_5__2 
       (.I0(Q[33]),
        .I1(Q[50]),
        .I2(Q[32]),
        .I3(Q[49]),
        .O(outy11[2]));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg1[6]_i_1__2 
       (.I0(Q[22]),
        .I1(Q[37]),
        .I2(Q[54]),
        .I3(\y11/C_5 ),
        .I4(\y12/C_5 ),
        .O(outy12[6]));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[6]_i_2__2 
       (.I0(\y11/C_3 ),
        .I1(Q[52]),
        .I2(Q[35]),
        .I3(Q[53]),
        .I4(Q[36]),
        .O(\y11/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[6]_i_3__2 
       (.I0(\y12/C_3 ),
        .I1(outy11[4]),
        .I2(Q[20]),
        .I3(outy11[5]),
        .I4(Q[21]),
        .O(\y12/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg1[6]_i_4__2 
       (.I0(Q[35]),
        .I1(Q[52]),
        .I2(\y11/C_3 ),
        .O(outy11[4]));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \yreg1[6]_i_5__2 
       (.I0(Q[36]),
        .I1(Q[53]),
        .I2(Q[35]),
        .I3(Q[52]),
        .I4(\y11/C_3 ),
        .O(outy11[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg1[7]_i_1__2 
       (.I0(\y12/f8/S ),
        .I1(\y12/C_6 ),
        .O(outy12[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg1[7]_i_2__2 
       (.I0(\y11/C_5 ),
        .I1(Q[54]),
        .I2(Q[37]),
        .I3(Q[55]),
        .I4(Q[38]),
        .I5(Q[23]),
        .O(\y12/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \yreg1[8]_i_1__2 
       (.I0(Q[39]),
        .I1(Q[23]),
        .I2(Q[38]),
        .I3(Q[55]),
        .I4(\y11/C_6 ),
        .I5(\y12/C_6 ),
        .O(outy12[8]));
  LUT6 #(
    .INIT(64'hF7FF000000000000)) 
    \yreg1[9]_i_1__2 
       (.I0(\full_reg_n_0_[0] ),
        .I1(\m_axis_tkeep[3] ),
        .I2(m_axis_tready),
        .I3(\full_reg_n_0_[1] ),
        .I4(\valid_reg[3] ),
        .I5(hold_reg),
        .O(yreg1));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \yreg1[9]_i_2__2 
       (.I0(\y12/C_6 ),
        .I1(\y11/C_6 ),
        .I2(Q[55]),
        .I3(Q[38]),
        .I4(Q[23]),
        .I5(Q[39]),
        .O(outy12[9]));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \yreg1[9]_i_3__1 
       (.I0(\y12/C_5 ),
        .I1(\y11/C_5 ),
        .I2(Q[54]),
        .I3(Q[37]),
        .I4(Q[22]),
        .O(\y12/C_6 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \yreg1[9]_i_4__2 
       (.I0(\y11/C_5 ),
        .I1(Q[54]),
        .I2(Q[37]),
        .O(\y11/C_6 ));
  FDRE \yreg1_reg[0] 
       (.C(clock),
        .CE(yreg1),
        .D(\y12/f1/S ),
        .Q(\yreg1_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \yreg1_reg[1] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[1]),
        .Q(\yreg1_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \yreg1_reg[2] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[2]),
        .Q(\yreg1_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \yreg1_reg[3] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[3]),
        .Q(\yreg1_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \yreg1_reg[4] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[4]),
        .Q(\yreg1_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \yreg1_reg[5] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[5]),
        .Q(\yreg1_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \yreg1_reg[6] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[6]),
        .Q(\yreg1_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \yreg1_reg[7] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[7]),
        .Q(\yreg1_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \yreg1_reg[8] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[8]),
        .Q(\yreg1_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \yreg1_reg[9] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[9]),
        .Q(\yreg1_reg_n_0_[9] ),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg2[0]_i_1__2 
       (.I0(Q[40]),
        .I1(Q[0]),
        .O(\y22/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \yreg2[1]_i_1__2 
       (.I0(Q[1]),
        .I1(Q[41]),
        .I2(Q[24]),
        .I3(Q[0]),
        .I4(Q[40]),
        .O(outy22[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \yreg2[2]_i_1__2 
       (.I0(Q[2]),
        .I1(Q[25]),
        .I2(Q[42]),
        .I3(Q[24]),
        .I4(Q[41]),
        .I5(\y22/C_1 ),
        .O(outy22[2]));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \yreg2[2]_i_2__2 
       (.I0(Q[40]),
        .I1(Q[0]),
        .I2(Q[24]),
        .I3(Q[41]),
        .I4(Q[1]),
        .O(\y22/C_1 ));
  LUT3 #(
    .INIT(8'h96)) 
    \yreg2[3]_i_1__2 
       (.I0(Q[3]),
        .I1(outy21[3]),
        .I2(\y22/C_2 ),
        .O(outy22[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \yreg2[3]_i_2__2 
       (.I0(Q[26]),
        .I1(Q[43]),
        .I2(Q[25]),
        .I3(Q[42]),
        .I4(Q[24]),
        .I5(Q[41]),
        .O(outy21[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \yreg2[3]_i_3__2 
       (.I0(\y22/C_1 ),
        .I1(Q[41]),
        .I2(Q[24]),
        .I3(Q[42]),
        .I4(Q[25]),
        .I5(Q[2]),
        .O(\y22/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg2[4]_i_1__2 
       (.I0(Q[4]),
        .I1(Q[27]),
        .I2(Q[44]),
        .I3(\y21/C_3 ),
        .I4(\y22/C_3 ),
        .O(outy22[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \yreg2[5]_i_1__2 
       (.I0(\y22/C_3 ),
        .I1(\y21/C_3 ),
        .I2(Q[44]),
        .I3(Q[27]),
        .I4(Q[4]),
        .I5(\y22/f6/S ),
        .O(outy22[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[5]_i_2__2 
       (.I0(\y22/C_1 ),
        .I1(outy21[2]),
        .I2(Q[2]),
        .I3(outy21[3]),
        .I4(Q[3]),
        .O(\y22/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \yreg2[5]_i_3__2 
       (.I0(Q[41]),
        .I1(Q[24]),
        .I2(Q[42]),
        .I3(Q[25]),
        .I4(Q[43]),
        .I5(Q[26]),
        .O(\y21/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg2[5]_i_4__2 
       (.I0(\y21/C_3 ),
        .I1(Q[44]),
        .I2(Q[27]),
        .I3(Q[45]),
        .I4(Q[28]),
        .I5(Q[5]),
        .O(\y22/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \yreg2[5]_i_5__2 
       (.I0(Q[25]),
        .I1(Q[42]),
        .I2(Q[24]),
        .I3(Q[41]),
        .O(outy21[2]));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg2[6]_i_1__2 
       (.I0(Q[6]),
        .I1(Q[29]),
        .I2(Q[46]),
        .I3(\y21/C_5 ),
        .I4(\y22/C_5 ),
        .O(outy22[6]));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[6]_i_2__2 
       (.I0(\y21/C_3 ),
        .I1(Q[44]),
        .I2(Q[27]),
        .I3(Q[45]),
        .I4(Q[28]),
        .O(\y21/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[6]_i_3__2 
       (.I0(\y22/C_3 ),
        .I1(outy21[4]),
        .I2(Q[4]),
        .I3(outy21[5]),
        .I4(Q[5]),
        .O(\y22/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg2[6]_i_4__2 
       (.I0(Q[27]),
        .I1(Q[44]),
        .I2(\y21/C_3 ),
        .O(outy21[4]));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \yreg2[6]_i_5__2 
       (.I0(Q[28]),
        .I1(Q[45]),
        .I2(Q[27]),
        .I3(Q[44]),
        .I4(\y21/C_3 ),
        .O(outy21[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg2[7]_i_1__2 
       (.I0(\y22/f8/S ),
        .I1(\y22/C_6 ),
        .O(outy22[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg2[7]_i_2__2 
       (.I0(\y21/C_5 ),
        .I1(Q[46]),
        .I2(Q[29]),
        .I3(Q[47]),
        .I4(Q[30]),
        .I5(Q[7]),
        .O(\y22/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \yreg2[8]_i_1__2 
       (.I0(Q[31]),
        .I1(Q[7]),
        .I2(Q[30]),
        .I3(Q[47]),
        .I4(\y21/C_6 ),
        .I5(\y22/C_6 ),
        .O(outy22[8]));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \yreg2[9]_i_1__2 
       (.I0(\y22/C_6 ),
        .I1(\y21/C_6 ),
        .I2(Q[47]),
        .I3(Q[30]),
        .I4(Q[7]),
        .I5(Q[31]),
        .O(outy22[9]));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \yreg2[9]_i_2__2 
       (.I0(\y22/C_5 ),
        .I1(\y21/C_5 ),
        .I2(Q[46]),
        .I3(Q[29]),
        .I4(Q[6]),
        .O(\y22/C_6 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \yreg2[9]_i_3__2 
       (.I0(\y21/C_5 ),
        .I1(Q[46]),
        .I2(Q[29]),
        .O(\y21/C_6 ));
  FDRE \yreg2_reg[0] 
       (.C(clock),
        .CE(yreg1),
        .D(\y22/f1/S ),
        .Q(yreg2[0]),
        .R(1'b0));
  FDRE \yreg2_reg[1] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[1]),
        .Q(yreg2[1]),
        .R(1'b0));
  FDRE \yreg2_reg[2] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[2]),
        .Q(yreg2[2]),
        .R(1'b0));
  FDRE \yreg2_reg[3] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[3]),
        .Q(yreg2[3]),
        .R(1'b0));
  FDRE \yreg2_reg[4] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[4]),
        .Q(yreg2[4]),
        .R(1'b0));
  FDRE \yreg2_reg[5] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[5]),
        .Q(yreg2[5]),
        .R(1'b0));
  FDRE \yreg2_reg[6] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[6]),
        .Q(yreg2[6]),
        .R(1'b0));
  FDRE \yreg2_reg[7] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[7]),
        .Q(yreg2[7]),
        .R(1'b0));
  FDRE \yreg2_reg[8] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[8]),
        .Q(yreg2[8]),
        .R(1'b0));
  FDRE \yreg2_reg[9] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[9]),
        .Q(yreg2[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "convolution" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convolution_3
   (\yreg2_reg[0]_0 ,
    \m_axis_tkeep[4] ,
    m_axis_tdata,
    Q,
    reset,
    hold,
    D,
    clock,
    reset_0,
    m_axis_tready,
    \valid_reg[4] ,
    reset_1,
    reset_2);
  output \yreg2_reg[0]_0 ;
  output \m_axis_tkeep[4] ;
  output [7:0]m_axis_tdata;
  input [55:0]Q;
  input reset;
  input hold;
  input [9:0]D;
  input clock;
  input reset_0;
  input m_axis_tready;
  input [0:0]\valid_reg[4] ;
  input reset_1;
  input reset_2;

  wire [9:0]D;
  wire [55:0]Q;
  wire [7:1]ans1;
  wire [9:1]ans2;
  wire clock;
  wire \final/C_1 ;
  wire \final/C_2 ;
  wire \final/C_4 ;
  wire \final/C_6 ;
  wire \final/f1/Ca ;
  wire \final/f2/S ;
  wire \final/f4/S ;
  wire \final/f5/S ;
  wire \final/f6/S ;
  wire \full[0]_i_1__3_n_0 ;
  wire \full[1]_i_1__3_n_0 ;
  wire \full[2]_i_1__3_n_0 ;
  wire \full_reg_n_0_[0] ;
  wire \full_reg_n_0_[1] ;
  wire hold;
  wire [7:0]m_axis_tdata;
  wire \m_axis_tkeep[4] ;
  wire m_axis_tready;
  wire out;
  wire \out[0]_i_1__3_n_0 ;
  wire \out[0]_i_2__3_n_0 ;
  wire \out[1]_i_1__3_n_0 ;
  wire \out[1]_i_2__3_n_0 ;
  wire \out[2]_i_1__3_n_0 ;
  wire \out[2]_i_2__3_n_0 ;
  wire \out[3]_i_1__3_n_0 ;
  wire \out[3]_i_2__3_n_0 ;
  wire \out[4]_i_1__3_n_0 ;
  wire \out[4]_i_2__3_n_0 ;
  wire \out[5]_i_1__3_n_0 ;
  wire \out[5]_i_2__3_n_0 ;
  wire \out[6]_i_1__3_n_0 ;
  wire \out[6]_i_2__3_n_0 ;
  wire \out[7]_i_26__3_n_0 ;
  wire \out[7]_i_27__3_n_0 ;
  wire \out[7]_i_28__3_n_0 ;
  wire \out[7]_i_29__3_n_0 ;
  wire \out[7]_i_2__3_n_0 ;
  wire \out[7]_i_8__3_n_0 ;
  wire [5:2]outx21;
  wire [9:1]outx22;
  wire [5:2]outy11;
  wire [9:1]outy12;
  wire [5:2]outy21;
  wire [9:1]outy22;
  wire reset;
  wire reset_0;
  wire reset_1;
  wire reset_2;
  wire \s1/C2 ;
  wire \s1/F6/S ;
  wire \s1/s1/C_2 ;
  wire \s1/s2/C_0 ;
  wire \s1/s2/C_2 ;
  wire \s1/s2/f2/S ;
  wire \s11[0]_i_1__3_n_0 ;
  wire \s11[1]_i_1__3_n_0 ;
  wire \s11[2]_i_1__3_n_0 ;
  wire \s11[3]_i_1__3_n_0 ;
  wire \s11[4]_i_1__3_n_0 ;
  wire \s11[5]_i_1__3_n_0 ;
  wire \s11[6]_i_1__3_n_0 ;
  wire \s11[7]_i_1__3_n_0 ;
  wire \s11[8]_i_1__3_n_0 ;
  wire \s11[9]_i_1__3_n_0 ;
  wire \s11_reg_n_0_[0] ;
  wire \s11_reg_n_0_[1] ;
  wire \s11_reg_n_0_[2] ;
  wire \s11_reg_n_0_[3] ;
  wire \s11_reg_n_0_[4] ;
  wire \s11_reg_n_0_[5] ;
  wire \s11_reg_n_0_[6] ;
  wire \s11_reg_n_0_[7] ;
  wire \s11_reg_n_0_[8] ;
  wire \s11_reg_n_0_[9] ;
  wire [9:0]s12;
  wire \s12[0]_i_1__3_n_0 ;
  wire \s12[1]_i_1__3_n_0 ;
  wire \s12[2]_i_1__3_n_0 ;
  wire \s12[3]_i_1__3_n_0 ;
  wire \s12[4]_i_1__3_n_0 ;
  wire \s12[5]_i_1__3_n_0 ;
  wire \s12[6]_i_1__3_n_0 ;
  wire \s12[7]_i_1__3_n_0 ;
  wire \s12[8]_i_1__3_n_0 ;
  wire \s12[8]_i_2__3_n_0 ;
  wire \s12[8]_i_3__3_n_0 ;
  wire \s12[8]_i_4__3_n_0 ;
  wire \s12[8]_i_5__3_n_0 ;
  wire \s12[8]_i_6__3_n_0 ;
  wire \s12[8]_i_7__3_n_0 ;
  wire \s12[8]_i_8__3_n_0 ;
  wire \s12[8]_i_9__3_n_0 ;
  wire \s12[9]_i_2__3_n_0 ;
  wire \s2/C2 ;
  wire \s2/F6/S ;
  wire \s2/f5/S ;
  wire \s2/s1/C_2 ;
  wire \s2/s1/f1/S ;
  wire \s2/s2/C_0 ;
  wire \s2/s2/C_2 ;
  wire \s2/s2/f2/S ;
  wire s21;
  wire \s21[0]_i_1__3_n_0 ;
  wire \s21[1]_i_1__3_n_0 ;
  wire \s21[2]_i_1__3_n_0 ;
  wire \s21[3]_i_1__3_n_0 ;
  wire \s21[4]_i_1__3_n_0 ;
  wire \s21[5]_i_1__3_n_0 ;
  wire \s21[6]_i_1__3_n_0 ;
  wire \s21[7]_i_1__3_n_0 ;
  wire \s21[8]_i_1__3_n_0 ;
  wire \s21[9]_i_1__3_n_0 ;
  wire \s21_reg_n_0_[0] ;
  wire \s21_reg_n_0_[1] ;
  wire \s21_reg_n_0_[2] ;
  wire \s21_reg_n_0_[3] ;
  wire \s21_reg_n_0_[4] ;
  wire \s21_reg_n_0_[5] ;
  wire \s21_reg_n_0_[6] ;
  wire \s21_reg_n_0_[7] ;
  wire \s21_reg_n_0_[8] ;
  wire \s21_reg_n_0_[9] ;
  wire [9:0]s22;
  wire \s22[0]_i_1__3_n_0 ;
  wire \s22[1]_i_1__3_n_0 ;
  wire \s22[2]_i_1__3_n_0 ;
  wire \s22[3]_i_1__3_n_0 ;
  wire \s22[4]_i_1__3_n_0 ;
  wire \s22[5]_i_1__3_n_0 ;
  wire \s22[6]_i_1__3_n_0 ;
  wire \s22[7]_i_1__3_n_0 ;
  wire \s22[8]_i_1__3_n_0 ;
  wire \s22[8]_i_2__3_n_0 ;
  wire \s22[8]_i_3__3_n_0 ;
  wire \s22[8]_i_4__3_n_0 ;
  wire \s22[8]_i_5__3_n_0 ;
  wire \s22[8]_i_6__3_n_0 ;
  wire \s22[8]_i_7__3_n_0 ;
  wire \s22[8]_i_8__3_n_0 ;
  wire \s22[8]_i_9__3_n_0 ;
  wire \s22[9]_i_1__3_n_0 ;
  wire [0:0]\valid_reg[4] ;
  wire \x21/C_3 ;
  wire \x21/C_5 ;
  wire \x21/C_6 ;
  wire \x22/C_1 ;
  wire \x22/C_2 ;
  wire \x22/C_3 ;
  wire \x22/C_5 ;
  wire \x22/C_6 ;
  wire \x22/f1/S ;
  wire \x22/f6/S ;
  wire \x22/f8/S ;
  wire xreg1;
  wire \xreg1_reg_n_0_[0] ;
  wire \xreg1_reg_n_0_[1] ;
  wire \xreg1_reg_n_0_[2] ;
  wire \xreg1_reg_n_0_[3] ;
  wire \xreg1_reg_n_0_[4] ;
  wire \xreg1_reg_n_0_[5] ;
  wire \xreg1_reg_n_0_[6] ;
  wire \xreg1_reg_n_0_[7] ;
  wire \xreg1_reg_n_0_[8] ;
  wire \xreg1_reg_n_0_[9] ;
  wire [9:0]xreg2;
  wire \y11/C_3 ;
  wire \y11/C_5 ;
  wire \y11/C_6 ;
  wire \y12/C_1 ;
  wire \y12/C_2 ;
  wire \y12/C_3 ;
  wire \y12/C_5 ;
  wire \y12/C_6 ;
  wire \y12/f1/S ;
  wire \y12/f6/S ;
  wire \y12/f8/S ;
  wire \y21/C_3 ;
  wire \y21/C_5 ;
  wire \y21/C_6 ;
  wire \y22/C_1 ;
  wire \y22/C_2 ;
  wire \y22/C_3 ;
  wire \y22/C_5 ;
  wire \y22/C_6 ;
  wire \y22/f1/S ;
  wire \y22/f6/S ;
  wire \y22/f8/S ;
  wire yreg1;
  wire \yreg1_reg_n_0_[0] ;
  wire \yreg1_reg_n_0_[1] ;
  wire \yreg1_reg_n_0_[2] ;
  wire \yreg1_reg_n_0_[3] ;
  wire \yreg1_reg_n_0_[4] ;
  wire \yreg1_reg_n_0_[5] ;
  wire \yreg1_reg_n_0_[6] ;
  wire \yreg1_reg_n_0_[7] ;
  wire \yreg1_reg_n_0_[8] ;
  wire \yreg1_reg_n_0_[9] ;
  wire [9:0]yreg2;
  wire \yreg2_reg[0]_0 ;

  LUT6 #(
    .INIT(64'hEEFEEEEE44444444)) 
    \full[0]_i_1__3 
       (.I0(hold),
        .I1(\valid_reg[4] ),
        .I2(\full_reg_n_0_[1] ),
        .I3(m_axis_tready),
        .I4(\m_axis_tkeep[4] ),
        .I5(\full_reg_n_0_[0] ),
        .O(\full[0]_i_1__3_n_0 ));
  LUT5 #(
    .INIT(32'hEEFE4444)) 
    \full[1]_i_1__3 
       (.I0(hold),
        .I1(\full_reg_n_0_[0] ),
        .I2(\m_axis_tkeep[4] ),
        .I3(m_axis_tready),
        .I4(\full_reg_n_0_[1] ),
        .O(\full[1]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair207" *) 
  LUT5 #(
    .INIT(32'hFBFF4040)) 
    \full[2]_i_1__3 
       (.I0(hold),
        .I1(reset),
        .I2(\full_reg_n_0_[1] ),
        .I3(m_axis_tready),
        .I4(\m_axis_tkeep[4] ),
        .O(\full[2]_i_1__3_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[0] 
       (.C(clock),
        .CE(1'b1),
        .CLR(reset_1),
        .D(\full[0]_i_1__3_n_0 ),
        .Q(\full_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[1] 
       (.C(clock),
        .CE(1'b1),
        .CLR(reset_1),
        .D(\full[1]_i_1__3_n_0 ),
        .Q(\full_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \full_reg[2] 
       (.C(clock),
        .CE(1'b1),
        .CLR(reset_1),
        .D(\full[2]_i_1__3_n_0 ),
        .Q(\m_axis_tkeep[4] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[0]_i_1__3 
       (.I0(\out[0]_i_2__3_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__3_n_0 ),
        .O(\out[0]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair220" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    \out[0]_i_2__3 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s11_reg_n_0_[0] ),
        .O(\out[0]_i_2__3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[1]_i_1__3 
       (.I0(\out[1]_i_2__3_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__3_n_0 ),
        .O(\out[1]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair220" *) 
  LUT5 #(
    .INIT(32'hAA9696AA)) 
    \out[1]_i_2__3 
       (.I0(\final/f2/S ),
        .I1(\s11_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s21_reg_n_0_[0] ),
        .I4(s22[0]),
        .O(\out[1]_i_2__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair224" *) 
  LUT5 #(
    .INIT(32'hD22D2DD2)) 
    \out[1]_i_3__3 
       (.I0(s12[0]),
        .I1(\s11_reg_n_0_[0] ),
        .I2(\s11_reg_n_0_[1] ),
        .I3(s12[1]),
        .I4(ans2[1]),
        .O(\final/f2/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[2]_i_1__3 
       (.I0(\out[2]_i_2__3_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__3_n_0 ),
        .O(\out[2]_i_1__3_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \out[2]_i_2__3 
       (.I0(ans1[2]),
        .I1(ans2[2]),
        .I2(\final/C_1 ),
        .O(\out[2]_i_2__3_n_0 ));
  LUT6 #(
    .INIT(64'h6966996969666966)) 
    \out[2]_i_3__3 
       (.I0(s12[2]),
        .I1(\s11_reg_n_0_[2] ),
        .I2(\s11_reg_n_0_[1] ),
        .I3(s12[1]),
        .I4(\s11_reg_n_0_[0] ),
        .I5(s12[0]),
        .O(ans1[2]));
  LUT6 #(
    .INIT(64'h6966996969666966)) 
    \out[2]_i_4__3 
       (.I0(s22[2]),
        .I1(\s21_reg_n_0_[2] ),
        .I2(\s21_reg_n_0_[1] ),
        .I3(s22[1]),
        .I4(\s21_reg_n_0_[0] ),
        .I5(s22[0]),
        .O(ans2[2]));
  LUT6 #(
    .INIT(64'h0CC0D44D4DD40CC0)) 
    \out[2]_i_5__3 
       (.I0(\s2/s1/f1/S ),
        .I1(ans2[1]),
        .I2(s12[1]),
        .I3(\s11_reg_n_0_[1] ),
        .I4(\s11_reg_n_0_[0] ),
        .I5(s12[0]),
        .O(\final/C_1 ));
  (* SOFT_HLUTNM = "soft_lutpair227" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \out[2]_i_6__3 
       (.I0(\s21_reg_n_0_[0] ),
        .I1(s22[0]),
        .O(\s2/s1/f1/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[3]_i_1__3 
       (.I0(\out[3]_i_2__3_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__3_n_0 ),
        .O(\out[3]_i_1__3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out[3]_i_2__3 
       (.I0(\final/f4/S ),
        .I1(\final/C_2 ),
        .O(\out[3]_i_2__3_n_0 ));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    \out[3]_i_3__3 
       (.I0(\s1/s1/C_2 ),
        .I1(\s11_reg_n_0_[3] ),
        .I2(s12[3]),
        .I3(\s2/s1/C_2 ),
        .I4(\s21_reg_n_0_[3] ),
        .I5(s22[3]),
        .O(\final/f4/S ));
  LUT6 #(
    .INIT(64'hDF0DFFFF0000DF0D)) 
    \out[4]_i_10__3 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s22[1]),
        .I3(\s21_reg_n_0_[1] ),
        .I4(s22[2]),
        .I5(\s21_reg_n_0_[2] ),
        .O(\s2/s1/C_2 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[4]_i_1__3 
       (.I0(\out[4]_i_2__3_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__3_n_0 ),
        .O(\out[4]_i_1__3_n_0 ));
  LUT6 #(
    .INIT(64'h711717718EE8E88E)) 
    \out[4]_i_2__3 
       (.I0(\final/C_2 ),
        .I1(ans2[3]),
        .I2(s12[3]),
        .I3(\s11_reg_n_0_[3] ),
        .I4(\s1/s1/C_2 ),
        .I5(\final/f5/S ),
        .O(\out[4]_i_2__3_n_0 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[4]_i_3__3 
       (.I0(\final/f1/Ca ),
        .I1(ans2[1]),
        .I2(ans1[1]),
        .I3(ans2[2]),
        .I4(ans1[2]),
        .O(\final/C_2 ));
  LUT3 #(
    .INIT(8'h69)) 
    \out[4]_i_4__3 
       (.I0(s22[3]),
        .I1(\s21_reg_n_0_[3] ),
        .I2(\s2/s1/C_2 ),
        .O(ans2[3]));
  LUT6 #(
    .INIT(64'hDF0DFFFF0000DF0D)) 
    \out[4]_i_5__3 
       (.I0(s12[0]),
        .I1(\s11_reg_n_0_[0] ),
        .I2(s12[1]),
        .I3(\s11_reg_n_0_[1] ),
        .I4(s12[2]),
        .I5(\s11_reg_n_0_[2] ),
        .O(\s1/s1/C_2 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \out[4]_i_6__3 
       (.I0(\s1/s1/C_2 ),
        .I1(s12[3]),
        .I2(\s11_reg_n_0_[3] ),
        .I3(\s11_reg_n_0_[4] ),
        .I4(s12[4]),
        .I5(ans2[4]),
        .O(\final/f5/S ));
  LUT4 #(
    .INIT(16'h0660)) 
    \out[4]_i_7__3 
       (.I0(s22[0]),
        .I1(\s21_reg_n_0_[0] ),
        .I2(s12[0]),
        .I3(\s11_reg_n_0_[0] ),
        .O(\final/f1/Ca ));
  (* SOFT_HLUTNM = "soft_lutpair227" *) 
  LUT4 #(
    .INIT(16'h6966)) 
    \out[4]_i_8__3 
       (.I0(s22[1]),
        .I1(\s21_reg_n_0_[1] ),
        .I2(\s21_reg_n_0_[0] ),
        .I3(s22[0]),
        .O(ans2[1]));
  (* SOFT_HLUTNM = "soft_lutpair224" *) 
  LUT4 #(
    .INIT(16'h6966)) 
    \out[4]_i_9__3 
       (.I0(s12[1]),
        .I1(\s11_reg_n_0_[1] ),
        .I2(\s11_reg_n_0_[0] ),
        .I3(s12[0]),
        .O(ans1[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[5]_i_1__3 
       (.I0(\out[5]_i_2__3_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__3_n_0 ),
        .O(\out[5]_i_1__3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \out[5]_i_2__3 
       (.I0(\final/f6/S ),
        .I1(\final/C_4 ),
        .O(\out[5]_i_2__3_n_0 ));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    \out[5]_i_3__3 
       (.I0(\s1/s2/C_0 ),
        .I1(\s11_reg_n_0_[5] ),
        .I2(s12[5]),
        .I3(\s2/s2/C_0 ),
        .I4(\s21_reg_n_0_[5] ),
        .I5(s22[5]),
        .O(\final/f6/S ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEEE)) 
    \out[6]_i_1__3 
       (.I0(\out[6]_i_2__3_n_0 ),
        .I1(ans2[9]),
        .I2(ans1[7]),
        .I3(ans2[7]),
        .I4(\final/C_6 ),
        .I5(\out[7]_i_8__3_n_0 ),
        .O(\out[6]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair222" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \out[6]_i_2__3 
       (.I0(ans2[6]),
        .I1(ans1[6]),
        .I2(ans1[5]),
        .I3(ans2[5]),
        .I4(\final/C_4 ),
        .O(\out[6]_i_2__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair221" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_10__3 
       (.I0(\s2/s2/C_0 ),
        .I1(s22[5]),
        .I2(\s21_reg_n_0_[5] ),
        .I3(s22[6]),
        .I4(\s21_reg_n_0_[6] ),
        .O(\s2/s2/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair216" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_11__3 
       (.I0(\s1/s2/C_0 ),
        .I1(s12[5]),
        .I2(\s11_reg_n_0_[5] ),
        .I3(s12[6]),
        .I4(\s11_reg_n_0_[6] ),
        .O(\s1/s2/C_2 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[7]_i_12__3 
       (.I0(\final/C_2 ),
        .I1(ans2[3]),
        .I2(ans1[3]),
        .I3(ans2[4]),
        .I4(ans1[4]),
        .O(\final/C_4 ));
  (* SOFT_HLUTNM = "soft_lutpair246" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_13__3 
       (.I0(s22[5]),
        .I1(\s21_reg_n_0_[5] ),
        .I2(\s2/s2/C_0 ),
        .O(ans2[5]));
  (* SOFT_HLUTNM = "soft_lutpair248" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_14__3 
       (.I0(s12[5]),
        .I1(\s11_reg_n_0_[5] ),
        .I2(\s1/s2/C_0 ),
        .O(ans1[5]));
  (* SOFT_HLUTNM = "soft_lutpair221" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_15__3 
       (.I0(s22[6]),
        .I1(\s21_reg_n_0_[6] ),
        .I2(\s21_reg_n_0_[5] ),
        .I3(s22[5]),
        .I4(\s2/s2/C_0 ),
        .O(ans2[6]));
  (* SOFT_HLUTNM = "soft_lutpair216" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_16__3 
       (.I0(s12[6]),
        .I1(\s11_reg_n_0_[6] ),
        .I2(\s11_reg_n_0_[5] ),
        .I3(s12[5]),
        .I4(\s1/s2/C_0 ),
        .O(ans1[6]));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_17__3 
       (.I0(\s11_reg_n_0_[9] ),
        .I1(s12[9]),
        .O(\s1/F6/S ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_18__3 
       (.I0(\s21_reg_n_0_[8] ),
        .I1(s22[8]),
        .O(\s2/f5/S ));
  LUT6 #(
    .INIT(64'hEF0EFFFF0000EF0E)) 
    \out[7]_i_19__3 
       (.I0(\out[7]_i_26__3_n_0 ),
        .I1(\out[7]_i_27__3_n_0 ),
        .I2(s22[6]),
        .I3(\s21_reg_n_0_[6] ),
        .I4(s22[7]),
        .I5(\s21_reg_n_0_[7] ),
        .O(\s2/C2 ));
  LUT4 #(
    .INIT(16'h00D0)) 
    \out[7]_i_1__3 
       (.I0(\m_axis_tkeep[4] ),
        .I1(m_axis_tready),
        .I2(\full_reg_n_0_[1] ),
        .I3(hold),
        .O(out));
  LUT6 #(
    .INIT(64'hEF0EFFFF0000EF0E)) 
    \out[7]_i_20__3 
       (.I0(\out[7]_i_28__3_n_0 ),
        .I1(\out[7]_i_29__3_n_0 ),
        .I2(s12[6]),
        .I3(\s11_reg_n_0_[6] ),
        .I4(s12[7]),
        .I5(\s11_reg_n_0_[7] ),
        .O(\s1/C2 ));
  (* SOFT_HLUTNM = "soft_lutpair219" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_21__3 
       (.I0(\s2/s1/C_2 ),
        .I1(s22[3]),
        .I2(\s21_reg_n_0_[3] ),
        .I3(s22[4]),
        .I4(\s21_reg_n_0_[4] ),
        .O(\s2/s2/C_0 ));
  (* SOFT_HLUTNM = "soft_lutpair204" *) 
  LUT5 #(
    .INIT(32'hB2FF00B2)) 
    \out[7]_i_22__3 
       (.I0(\s1/s1/C_2 ),
        .I1(s12[3]),
        .I2(\s11_reg_n_0_[3] ),
        .I3(s12[4]),
        .I4(\s11_reg_n_0_[4] ),
        .O(\s1/s2/C_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_23__3 
       (.I0(s12[3]),
        .I1(\s11_reg_n_0_[3] ),
        .I2(\s1/s1/C_2 ),
        .O(ans1[3]));
  (* SOFT_HLUTNM = "soft_lutpair219" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_24__3 
       (.I0(s22[4]),
        .I1(\s21_reg_n_0_[4] ),
        .I2(\s21_reg_n_0_[3] ),
        .I3(s22[3]),
        .I4(\s2/s1/C_2 ),
        .O(ans2[4]));
  (* SOFT_HLUTNM = "soft_lutpair204" *) 
  LUT5 #(
    .INIT(32'h69669969)) 
    \out[7]_i_25__3 
       (.I0(s12[4]),
        .I1(\s11_reg_n_0_[4] ),
        .I2(\s11_reg_n_0_[3] ),
        .I3(s12[3]),
        .I4(\s1/s1/C_2 ),
        .O(ans1[4]));
  LUT6 #(
    .INIT(64'h8A088A8A08088A08)) 
    \out[7]_i_26__3 
       (.I0(\s2/s2/f2/S ),
        .I1(\s21_reg_n_0_[4] ),
        .I2(s22[4]),
        .I3(\s21_reg_n_0_[3] ),
        .I4(s22[3]),
        .I5(\s2/s1/C_2 ),
        .O(\out[7]_i_26__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair246" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \out[7]_i_27__3 
       (.I0(\s21_reg_n_0_[5] ),
        .I1(s22[5]),
        .O(\out[7]_i_27__3_n_0 ));
  LUT6 #(
    .INIT(64'h8A088A8A08088A08)) 
    \out[7]_i_28__3 
       (.I0(\s1/s2/f2/S ),
        .I1(\s11_reg_n_0_[4] ),
        .I2(s12[4]),
        .I3(\s11_reg_n_0_[3] ),
        .I4(s12[3]),
        .I5(\s1/s1/C_2 ),
        .O(\out[7]_i_28__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair248" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \out[7]_i_29__3 
       (.I0(\s11_reg_n_0_[5] ),
        .I1(s12[5]),
        .O(\out[7]_i_29__3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \out[7]_i_2__3 
       (.I0(ans2[9]),
        .I1(ans1[7]),
        .I2(ans2[7]),
        .I3(\final/C_6 ),
        .I4(\out[7]_i_8__3_n_0 ),
        .O(\out[7]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_30__3 
       (.I0(\s21_reg_n_0_[5] ),
        .I1(s22[5]),
        .O(\s2/s2/f2/S ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_31__1 
       (.I0(\s11_reg_n_0_[5] ),
        .I1(s12[5]),
        .O(\s1/s2/f2/S ));
  LUT6 #(
    .INIT(64'h65A66565A6A665A6)) 
    \out[7]_i_4__3 
       (.I0(\s2/F6/S ),
        .I1(\s21_reg_n_0_[8] ),
        .I2(s22[8]),
        .I3(\s21_reg_n_0_[7] ),
        .I4(s22[7]),
        .I5(\s2/s2/C_2 ),
        .O(ans2[9]));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_5__3 
       (.I0(s12[7]),
        .I1(\s11_reg_n_0_[7] ),
        .I2(\s1/s2/C_2 ),
        .O(ans1[7]));
  LUT3 #(
    .INIT(8'h69)) 
    \out[7]_i_6__3 
       (.I0(s22[7]),
        .I1(\s21_reg_n_0_[7] ),
        .I2(\s2/s2/C_2 ),
        .O(ans2[7]));
  (* SOFT_HLUTNM = "soft_lutpair222" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \out[7]_i_7__3 
       (.I0(\final/C_4 ),
        .I1(ans2[5]),
        .I2(ans1[5]),
        .I3(ans2[6]),
        .I4(ans1[6]),
        .O(\final/C_6 ));
  LUT6 #(
    .INIT(64'h7DFFFF7DFF7DBEFF)) 
    \out[7]_i_8__3 
       (.I0(\s1/F6/S ),
        .I1(\s2/f5/S ),
        .I2(\s2/C2 ),
        .I3(s12[8]),
        .I4(\s11_reg_n_0_[8] ),
        .I5(\s1/C2 ),
        .O(\out[7]_i_8__3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \out[7]_i_9__3 
       (.I0(\s21_reg_n_0_[9] ),
        .I1(s22[9]),
        .O(\s2/F6/S ));
  FDCE \out_reg[0] 
       (.C(clock),
        .CE(out),
        .CLR(reset_2),
        .D(\out[0]_i_1__3_n_0 ),
        .Q(m_axis_tdata[0]));
  FDCE \out_reg[1] 
       (.C(clock),
        .CE(out),
        .CLR(reset_2),
        .D(\out[1]_i_1__3_n_0 ),
        .Q(m_axis_tdata[1]));
  FDCE \out_reg[2] 
       (.C(clock),
        .CE(out),
        .CLR(reset_2),
        .D(\out[2]_i_1__3_n_0 ),
        .Q(m_axis_tdata[2]));
  FDCE \out_reg[3] 
       (.C(clock),
        .CE(out),
        .CLR(reset_2),
        .D(\out[3]_i_1__3_n_0 ),
        .Q(m_axis_tdata[3]));
  FDCE \out_reg[4] 
       (.C(clock),
        .CE(out),
        .CLR(reset_2),
        .D(\out[4]_i_1__3_n_0 ),
        .Q(m_axis_tdata[4]));
  FDCE \out_reg[5] 
       (.C(clock),
        .CE(out),
        .CLR(reset_2),
        .D(\out[5]_i_1__3_n_0 ),
        .Q(m_axis_tdata[5]));
  FDCE \out_reg[6] 
       (.C(clock),
        .CE(out),
        .CLR(reset_2),
        .D(\out[6]_i_1__3_n_0 ),
        .Q(m_axis_tdata[6]));
  FDCE \out_reg[7] 
       (.C(clock),
        .CE(out),
        .CLR(reset_2),
        .D(\out[7]_i_2__3_n_0 ),
        .Q(m_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair243" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[0]_i_1__3 
       (.I0(xreg2[0]),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(\xreg1_reg_n_0_[0] ),
        .O(\s11[0]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair242" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[1]_i_1__3 
       (.I0(xreg2[1]),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(\xreg1_reg_n_0_[1] ),
        .O(\s11[1]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair241" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[2]_i_1__3 
       (.I0(xreg2[2]),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(\xreg1_reg_n_0_[2] ),
        .O(\s11[2]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair238" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[3]_i_1__3 
       (.I0(xreg2[3]),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(\xreg1_reg_n_0_[3] ),
        .O(\s11[3]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair239" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[4]_i_1__3 
       (.I0(xreg2[4]),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(\xreg1_reg_n_0_[4] ),
        .O(\s11[4]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair245" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[5]_i_1__3 
       (.I0(xreg2[5]),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(\xreg1_reg_n_0_[5] ),
        .O(\s11[5]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair244" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[6]_i_1__3 
       (.I0(xreg2[6]),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(\xreg1_reg_n_0_[6] ),
        .O(\s11[6]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair247" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[7]_i_1__3 
       (.I0(xreg2[7]),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(\xreg1_reg_n_0_[7] ),
        .O(\s11[7]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair237" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s11[8]_i_1__3 
       (.I0(xreg2[8]),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(\xreg1_reg_n_0_[8] ),
        .O(\s11[8]_i_1__3_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \s11[9]_i_1__3 
       (.I0(\xreg1_reg_n_0_[9] ),
        .I1(xreg2[9]),
        .O(\s11[9]_i_1__3_n_0 ));
  FDCE \s11_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[0]_i_1__3_n_0 ),
        .Q(\s11_reg_n_0_[0] ));
  FDCE \s11_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[1]_i_1__3_n_0 ),
        .Q(\s11_reg_n_0_[1] ));
  FDCE \s11_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[2]_i_1__3_n_0 ),
        .Q(\s11_reg_n_0_[2] ));
  FDCE \s11_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[3]_i_1__3_n_0 ),
        .Q(\s11_reg_n_0_[3] ));
  FDCE \s11_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[4]_i_1__3_n_0 ),
        .Q(\s11_reg_n_0_[4] ));
  FDCE \s11_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[5]_i_1__3_n_0 ),
        .Q(\s11_reg_n_0_[5] ));
  FDCE \s11_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[6]_i_1__3_n_0 ),
        .Q(\s11_reg_n_0_[6] ));
  FDCE \s11_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[7]_i_1__3_n_0 ),
        .Q(\s11_reg_n_0_[7] ));
  FDCE \s11_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[8]_i_1__3_n_0 ),
        .Q(\s11_reg_n_0_[8] ));
  FDCE \s11_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s11[9]_i_1__3_n_0 ),
        .Q(\s11_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair243" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[0]_i_1__3 
       (.I0(\xreg1_reg_n_0_[0] ),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(xreg2[0]),
        .O(\s12[0]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair242" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[1]_i_1__3 
       (.I0(\xreg1_reg_n_0_[1] ),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(xreg2[1]),
        .O(\s12[1]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair241" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[2]_i_1__3 
       (.I0(\xreg1_reg_n_0_[2] ),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(xreg2[2]),
        .O(\s12[2]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair238" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[3]_i_1__3 
       (.I0(\xreg1_reg_n_0_[3] ),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(xreg2[3]),
        .O(\s12[3]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair239" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[4]_i_1__3 
       (.I0(\xreg1_reg_n_0_[4] ),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(xreg2[4]),
        .O(\s12[4]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair245" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[5]_i_1__3 
       (.I0(\xreg1_reg_n_0_[5] ),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(xreg2[5]),
        .O(\s12[5]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair244" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[6]_i_1__3 
       (.I0(\xreg1_reg_n_0_[6] ),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(xreg2[6]),
        .O(\s12[6]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair247" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[7]_i_1__3 
       (.I0(\xreg1_reg_n_0_[7] ),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(xreg2[7]),
        .O(\s12[7]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair237" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s12[8]_i_1__3 
       (.I0(\xreg1_reg_n_0_[8] ),
        .I1(\s12[8]_i_2__3_n_0 ),
        .I2(xreg2[8]),
        .O(\s12[8]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair223" *) 
  LUT5 #(
    .INIT(32'hB200FFB2)) 
    \s12[8]_i_2__3 
       (.I0(\s12[8]_i_3__3_n_0 ),
        .I1(\xreg1_reg_n_0_[8] ),
        .I2(xreg2[8]),
        .I3(xreg2[9]),
        .I4(\xreg1_reg_n_0_[9] ),
        .O(\s12[8]_i_2__3_n_0 ));
  LUT5 #(
    .INIT(32'h7050FFFF)) 
    \s12[8]_i_3__3 
       (.I0(\s12[8]_i_4__3_n_0 ),
        .I1(\s12[8]_i_5__3_n_0 ),
        .I2(\s12[8]_i_6__3_n_0 ),
        .I3(\s12[8]_i_7__3_n_0 ),
        .I4(\s12[8]_i_8__3_n_0 ),
        .O(\s12[8]_i_3__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair226" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s12[8]_i_4__3 
       (.I0(\xreg1_reg_n_0_[4] ),
        .I1(xreg2[4]),
        .I2(xreg2[5]),
        .I3(\xreg1_reg_n_0_[5] ),
        .O(\s12[8]_i_4__3_n_0 ));
  LUT5 #(
    .INIT(32'h2F22BF2F)) 
    \s12[8]_i_5__3 
       (.I0(\xreg1_reg_n_0_[3] ),
        .I1(xreg2[3]),
        .I2(\s12[8]_i_9__3_n_0 ),
        .I3(\xreg1_reg_n_0_[2] ),
        .I4(xreg2[2]),
        .O(\s12[8]_i_5__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair225" *) 
  LUT4 #(
    .INIT(16'hBB2B)) 
    \s12[8]_i_6__3 
       (.I0(xreg2[7]),
        .I1(\xreg1_reg_n_0_[7] ),
        .I2(\xreg1_reg_n_0_[6] ),
        .I3(xreg2[6]),
        .O(\s12[8]_i_6__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair226" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s12[8]_i_7__3 
       (.I0(xreg2[4]),
        .I1(\xreg1_reg_n_0_[4] ),
        .I2(\xreg1_reg_n_0_[5] ),
        .I3(xreg2[5]),
        .O(\s12[8]_i_7__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair225" *) 
  LUT4 #(
    .INIT(16'hB0FB)) 
    \s12[8]_i_8__3 
       (.I0(\xreg1_reg_n_0_[6] ),
        .I1(xreg2[6]),
        .I2(\xreg1_reg_n_0_[7] ),
        .I3(xreg2[7]),
        .O(\s12[8]_i_8__3_n_0 ));
  LUT6 #(
    .INIT(64'h40F440F4FFFF40F4)) 
    \s12[8]_i_9__3 
       (.I0(\xreg1_reg_n_0_[0] ),
        .I1(xreg2[0]),
        .I2(xreg2[1]),
        .I3(\xreg1_reg_n_0_[1] ),
        .I4(xreg2[3]),
        .I5(\xreg1_reg_n_0_[3] ),
        .O(\s12[8]_i_9__3_n_0 ));
  LUT5 #(
    .INIT(32'h0000DF00)) 
    \s12[9]_i_1__3 
       (.I0(\full_reg_n_0_[1] ),
        .I1(m_axis_tready),
        .I2(\m_axis_tkeep[4] ),
        .I3(\full_reg_n_0_[0] ),
        .I4(hold),
        .O(s21));
  (* SOFT_HLUTNM = "soft_lutpair223" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s12[9]_i_2__3 
       (.I0(\xreg1_reg_n_0_[9] ),
        .I1(xreg2[9]),
        .O(\s12[9]_i_2__3_n_0 ));
  FDCE \s12_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[0]_i_1__3_n_0 ),
        .Q(s12[0]));
  FDCE \s12_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[1]_i_1__3_n_0 ),
        .Q(s12[1]));
  FDCE \s12_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[2]_i_1__3_n_0 ),
        .Q(s12[2]));
  FDCE \s12_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[3]_i_1__3_n_0 ),
        .Q(s12[3]));
  FDCE \s12_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[4]_i_1__3_n_0 ),
        .Q(s12[4]));
  FDCE \s12_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[5]_i_1__3_n_0 ),
        .Q(s12[5]));
  FDCE \s12_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[6]_i_1__3_n_0 ),
        .Q(s12[6]));
  FDCE \s12_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[7]_i_1__3_n_0 ),
        .Q(s12[7]));
  FDCE \s12_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[8]_i_1__3_n_0 ),
        .Q(s12[8]));
  FDCE \s12_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s12[9]_i_2__3_n_0 ),
        .Q(s12[9]));
  (* SOFT_HLUTNM = "soft_lutpair240" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[0]_i_1__3 
       (.I0(yreg2[0]),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(\yreg1_reg_n_0_[0] ),
        .O(\s21[0]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair240" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[1]_i_1__3 
       (.I0(yreg2[1]),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(\yreg1_reg_n_0_[1] ),
        .O(\s21[1]_i_1__3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[2]_i_1__3 
       (.I0(yreg2[2]),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(\yreg1_reg_n_0_[2] ),
        .O(\s21[2]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair230" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[3]_i_1__3 
       (.I0(yreg2[3]),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(\yreg1_reg_n_0_[3] ),
        .O(\s21[3]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair231" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[4]_i_1__3 
       (.I0(yreg2[4]),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(\yreg1_reg_n_0_[4] ),
        .O(\s21[4]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair232" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[5]_i_1__3 
       (.I0(yreg2[5]),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(\yreg1_reg_n_0_[5] ),
        .O(\s21[5]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair234" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[6]_i_1__3 
       (.I0(yreg2[6]),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(\yreg1_reg_n_0_[6] ),
        .O(\s21[6]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair235" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[7]_i_1__3 
       (.I0(yreg2[7]),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(\yreg1_reg_n_0_[7] ),
        .O(\s21[7]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair233" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s21[8]_i_1__3 
       (.I0(yreg2[8]),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(\yreg1_reg_n_0_[8] ),
        .O(\s21[8]_i_1__3_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \s21[9]_i_1__3 
       (.I0(\yreg1_reg_n_0_[9] ),
        .I1(yreg2[9]),
        .O(\s21[9]_i_1__3_n_0 ));
  FDCE \s21_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_2),
        .D(\s21[0]_i_1__3_n_0 ),
        .Q(\s21_reg_n_0_[0] ));
  FDCE \s21_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_2),
        .D(\s21[1]_i_1__3_n_0 ),
        .Q(\s21_reg_n_0_[1] ));
  FDCE \s21_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_2),
        .D(\s21[2]_i_1__3_n_0 ),
        .Q(\s21_reg_n_0_[2] ));
  FDCE \s21_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s21[3]_i_1__3_n_0 ),
        .Q(\s21_reg_n_0_[3] ));
  FDCE \s21_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s21[4]_i_1__3_n_0 ),
        .Q(\s21_reg_n_0_[4] ));
  FDCE \s21_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s21[5]_i_1__3_n_0 ),
        .Q(\s21_reg_n_0_[5] ));
  FDCE \s21_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s21[6]_i_1__3_n_0 ),
        .Q(\s21_reg_n_0_[6] ));
  FDCE \s21_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s21[7]_i_1__3_n_0 ),
        .Q(\s21_reg_n_0_[7] ));
  FDCE \s21_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s21[8]_i_1__3_n_0 ),
        .Q(\s21_reg_n_0_[8] ));
  FDCE \s21_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s21[9]_i_1__3_n_0 ),
        .Q(\s21_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair231" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[0]_i_1__3 
       (.I0(\yreg1_reg_n_0_[0] ),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(yreg2[0]),
        .O(\s22[0]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair230" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[1]_i_1__3 
       (.I0(\yreg1_reg_n_0_[1] ),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(yreg2[1]),
        .O(\s22[1]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair232" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[2]_i_1__3 
       (.I0(\yreg1_reg_n_0_[2] ),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(yreg2[2]),
        .O(\s22[2]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair234" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[3]_i_1__3 
       (.I0(\yreg1_reg_n_0_[3] ),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(yreg2[3]),
        .O(\s22[3]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair236" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[4]_i_1__3 
       (.I0(\yreg1_reg_n_0_[4] ),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(yreg2[4]),
        .O(\s22[4]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair236" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[5]_i_1__3 
       (.I0(\yreg1_reg_n_0_[5] ),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(yreg2[5]),
        .O(\s22[5]_i_1__3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[6]_i_1__3 
       (.I0(\yreg1_reg_n_0_[6] ),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(yreg2[6]),
        .O(\s22[6]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair235" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[7]_i_1__3 
       (.I0(\yreg1_reg_n_0_[7] ),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(yreg2[7]),
        .O(\s22[7]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair233" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \s22[8]_i_1__3 
       (.I0(\yreg1_reg_n_0_[8] ),
        .I1(\s22[8]_i_2__3_n_0 ),
        .I2(yreg2[8]),
        .O(\s22[8]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair218" *) 
  LUT5 #(
    .INIT(32'hB200FFB2)) 
    \s22[8]_i_2__3 
       (.I0(\s22[8]_i_3__3_n_0 ),
        .I1(\yreg1_reg_n_0_[8] ),
        .I2(yreg2[8]),
        .I3(yreg2[9]),
        .I4(\yreg1_reg_n_0_[9] ),
        .O(\s22[8]_i_2__3_n_0 ));
  LUT5 #(
    .INIT(32'h7050FFFF)) 
    \s22[8]_i_3__3 
       (.I0(\s22[8]_i_4__3_n_0 ),
        .I1(\s22[8]_i_5__3_n_0 ),
        .I2(\s22[8]_i_6__3_n_0 ),
        .I3(\s22[8]_i_7__3_n_0 ),
        .I4(\s22[8]_i_8__3_n_0 ),
        .O(\s22[8]_i_3__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair229" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s22[8]_i_4__3 
       (.I0(\yreg1_reg_n_0_[4] ),
        .I1(yreg2[4]),
        .I2(yreg2[5]),
        .I3(\yreg1_reg_n_0_[5] ),
        .O(\s22[8]_i_4__3_n_0 ));
  LUT5 #(
    .INIT(32'h4D44DDDD)) 
    \s22[8]_i_5__3 
       (.I0(yreg2[3]),
        .I1(\yreg1_reg_n_0_[3] ),
        .I2(yreg2[2]),
        .I3(\yreg1_reg_n_0_[2] ),
        .I4(\s22[8]_i_9__3_n_0 ),
        .O(\s22[8]_i_5__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair228" *) 
  LUT4 #(
    .INIT(16'hBB2B)) 
    \s22[8]_i_6__3 
       (.I0(yreg2[7]),
        .I1(\yreg1_reg_n_0_[7] ),
        .I2(\yreg1_reg_n_0_[6] ),
        .I3(yreg2[6]),
        .O(\s22[8]_i_6__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair229" *) 
  LUT4 #(
    .INIT(16'hBF0B)) 
    \s22[8]_i_7__3 
       (.I0(yreg2[4]),
        .I1(\yreg1_reg_n_0_[4] ),
        .I2(\yreg1_reg_n_0_[5] ),
        .I3(yreg2[5]),
        .O(\s22[8]_i_7__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair228" *) 
  LUT4 #(
    .INIT(16'hB0FB)) 
    \s22[8]_i_8__3 
       (.I0(\yreg1_reg_n_0_[6] ),
        .I1(yreg2[6]),
        .I2(\yreg1_reg_n_0_[7] ),
        .I3(yreg2[7]),
        .O(\s22[8]_i_8__3_n_0 ));
  LUT6 #(
    .INIT(64'h40F440F4FFFF40F4)) 
    \s22[8]_i_9__3 
       (.I0(\yreg1_reg_n_0_[0] ),
        .I1(yreg2[0]),
        .I2(yreg2[1]),
        .I3(\yreg1_reg_n_0_[1] ),
        .I4(yreg2[2]),
        .I5(\yreg1_reg_n_0_[2] ),
        .O(\s22[8]_i_9__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair218" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s22[9]_i_1__3 
       (.I0(\yreg1_reg_n_0_[9] ),
        .I1(yreg2[9]),
        .O(\s22[9]_i_1__3_n_0 ));
  FDCE \s22_reg[0] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[0]_i_1__3_n_0 ),
        .Q(s22[0]));
  FDCE \s22_reg[1] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[1]_i_1__3_n_0 ),
        .Q(s22[1]));
  FDCE \s22_reg[2] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[2]_i_1__3_n_0 ),
        .Q(s22[2]));
  FDCE \s22_reg[3] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[3]_i_1__3_n_0 ),
        .Q(s22[3]));
  FDCE \s22_reg[4] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[4]_i_1__3_n_0 ),
        .Q(s22[4]));
  FDCE \s22_reg[5] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[5]_i_1__3_n_0 ),
        .Q(s22[5]));
  FDCE \s22_reg[6] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[6]_i_1__3_n_0 ),
        .Q(s22[6]));
  FDCE \s22_reg[7] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[7]_i_1__3_n_0 ),
        .Q(s22[7]));
  FDCE \s22_reg[8] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[8]_i_1__3_n_0 ),
        .Q(s22[8]));
  FDCE \s22_reg[9] 
       (.C(clock),
        .CE(s21),
        .CLR(reset_0),
        .D(\s22[9]_i_1__3_n_0 ),
        .Q(s22[9]));
  LUT6 #(
    .INIT(64'h00000000F7FF0000)) 
    \xreg1[9]_i_1__3 
       (.I0(\full_reg_n_0_[0] ),
        .I1(\m_axis_tkeep[4] ),
        .I2(m_axis_tready),
        .I3(\full_reg_n_0_[1] ),
        .I4(\valid_reg[4] ),
        .I5(hold),
        .O(xreg1));
  FDCE \xreg1_reg[0] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(D[0]),
        .Q(\xreg1_reg_n_0_[0] ));
  FDCE \xreg1_reg[1] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(D[1]),
        .Q(\xreg1_reg_n_0_[1] ));
  FDCE \xreg1_reg[2] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(D[2]),
        .Q(\xreg1_reg_n_0_[2] ));
  FDCE \xreg1_reg[3] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(D[3]),
        .Q(\xreg1_reg_n_0_[3] ));
  FDCE \xreg1_reg[4] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(D[4]),
        .Q(\xreg1_reg_n_0_[4] ));
  FDCE \xreg1_reg[5] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(D[5]),
        .Q(\xreg1_reg_n_0_[5] ));
  FDCE \xreg1_reg[6] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(D[6]),
        .Q(\xreg1_reg_n_0_[6] ));
  FDCE \xreg1_reg[7] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(D[7]),
        .Q(\xreg1_reg_n_0_[7] ));
  FDCE \xreg1_reg[8] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(D[8]),
        .Q(\xreg1_reg_n_0_[8] ));
  FDCE \xreg1_reg[9] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(D[9]),
        .Q(\xreg1_reg_n_0_[9] ));
  LUT2 #(
    .INIT(4'h6)) 
    \xreg2[0]_i_1__0 
       (.I0(Q[16]),
        .I1(Q[0]),
        .O(\x22/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair206" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \xreg2[1]_i_1__0 
       (.I0(Q[1]),
        .I1(Q[17]),
        .I2(Q[8]),
        .I3(Q[0]),
        .I4(Q[16]),
        .O(outx22[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \xreg2[2]_i_1__0 
       (.I0(Q[2]),
        .I1(Q[9]),
        .I2(Q[18]),
        .I3(Q[8]),
        .I4(Q[17]),
        .I5(\x22/C_1 ),
        .O(outx22[2]));
  (* SOFT_HLUTNM = "soft_lutpair206" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \xreg2[2]_i_2__0 
       (.I0(Q[16]),
        .I1(Q[0]),
        .I2(Q[8]),
        .I3(Q[17]),
        .I4(Q[1]),
        .O(\x22/C_1 ));
  LUT3 #(
    .INIT(8'h96)) 
    \xreg2[3]_i_1__0 
       (.I0(Q[3]),
        .I1(outx21[3]),
        .I2(\x22/C_2 ),
        .O(outx22[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \xreg2[3]_i_2__0 
       (.I0(Q[10]),
        .I1(Q[19]),
        .I2(Q[9]),
        .I3(Q[18]),
        .I4(Q[8]),
        .I5(Q[17]),
        .O(outx21[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \xreg2[3]_i_3__0 
       (.I0(\x22/C_1 ),
        .I1(Q[17]),
        .I2(Q[8]),
        .I3(Q[18]),
        .I4(Q[9]),
        .I5(Q[2]),
        .O(\x22/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair217" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg2[4]_i_1__0 
       (.I0(Q[4]),
        .I1(Q[11]),
        .I2(Q[20]),
        .I3(\x21/C_3 ),
        .I4(\x22/C_3 ),
        .O(outx22[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \xreg2[5]_i_1__0 
       (.I0(\x22/C_3 ),
        .I1(\x21/C_3 ),
        .I2(Q[20]),
        .I3(Q[11]),
        .I4(Q[4]),
        .I5(\x22/f6/S ),
        .O(outx22[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[5]_i_2__0 
       (.I0(\x22/C_1 ),
        .I1(outx21[2]),
        .I2(Q[2]),
        .I3(outx21[3]),
        .I4(Q[3]),
        .O(\x22/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \xreg2[5]_i_3__0 
       (.I0(Q[17]),
        .I1(Q[8]),
        .I2(Q[18]),
        .I3(Q[9]),
        .I4(Q[19]),
        .I5(Q[10]),
        .O(\x21/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg2[5]_i_4__0 
       (.I0(\x21/C_3 ),
        .I1(Q[20]),
        .I2(Q[11]),
        .I3(Q[21]),
        .I4(Q[12]),
        .I5(Q[5]),
        .O(\x22/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \xreg2[5]_i_5__0 
       (.I0(Q[9]),
        .I1(Q[18]),
        .I2(Q[8]),
        .I3(Q[17]),
        .O(outx21[2]));
  (* SOFT_HLUTNM = "soft_lutpair212" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \xreg2[6]_i_1__0 
       (.I0(Q[6]),
        .I1(Q[13]),
        .I2(Q[22]),
        .I3(\x21/C_5 ),
        .I4(\x22/C_5 ),
        .O(outx22[6]));
  (* SOFT_HLUTNM = "soft_lutpair203" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[6]_i_2__0 
       (.I0(\x21/C_3 ),
        .I1(Q[20]),
        .I2(Q[11]),
        .I3(Q[21]),
        .I4(Q[12]),
        .O(\x21/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \xreg2[6]_i_3__0 
       (.I0(\x22/C_3 ),
        .I1(outx21[4]),
        .I2(Q[4]),
        .I3(outx21[5]),
        .I4(Q[5]),
        .O(\x22/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair217" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \xreg2[6]_i_4__0 
       (.I0(Q[11]),
        .I1(Q[20]),
        .I2(\x21/C_3 ),
        .O(outx21[4]));
  (* SOFT_HLUTNM = "soft_lutpair203" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \xreg2[6]_i_5__0 
       (.I0(Q[12]),
        .I1(Q[21]),
        .I2(Q[11]),
        .I3(Q[20]),
        .I4(\x21/C_3 ),
        .O(outx21[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \xreg2[7]_i_1__0 
       (.I0(\x22/f8/S ),
        .I1(\x22/C_6 ),
        .O(outx22[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \xreg2[7]_i_2__0 
       (.I0(\x21/C_5 ),
        .I1(Q[22]),
        .I2(Q[13]),
        .I3(Q[23]),
        .I4(Q[14]),
        .I5(Q[7]),
        .O(\x22/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \xreg2[8]_i_1__0 
       (.I0(Q[15]),
        .I1(Q[7]),
        .I2(Q[14]),
        .I3(Q[23]),
        .I4(\x21/C_6 ),
        .I5(\x22/C_6 ),
        .O(outx22[8]));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \xreg2[9]_i_1__0 
       (.I0(\x22/C_6 ),
        .I1(\x21/C_6 ),
        .I2(Q[23]),
        .I3(Q[14]),
        .I4(Q[7]),
        .I5(Q[15]),
        .O(outx22[9]));
  (* SOFT_HLUTNM = "soft_lutpair212" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \xreg2[9]_i_2__0 
       (.I0(\x22/C_5 ),
        .I1(\x21/C_5 ),
        .I2(Q[22]),
        .I3(Q[13]),
        .I4(Q[6]),
        .O(\x22/C_6 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \xreg2[9]_i_3__0 
       (.I0(\x21/C_5 ),
        .I1(Q[22]),
        .I2(Q[13]),
        .O(\x21/C_6 ));
  FDCE \xreg2_reg[0] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(\x22/f1/S ),
        .Q(xreg2[0]));
  FDCE \xreg2_reg[1] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(outx22[1]),
        .Q(xreg2[1]));
  FDCE \xreg2_reg[2] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(outx22[2]),
        .Q(xreg2[2]));
  FDCE \xreg2_reg[3] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(outx22[3]),
        .Q(xreg2[3]));
  FDCE \xreg2_reg[4] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(outx22[4]),
        .Q(xreg2[4]));
  FDCE \xreg2_reg[5] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(outx22[5]),
        .Q(xreg2[5]));
  FDCE \xreg2_reg[6] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(outx22[6]),
        .Q(xreg2[6]));
  FDCE \xreg2_reg[7] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(outx22[7]),
        .Q(xreg2[7]));
  FDCE \xreg2_reg[8] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(outx22[8]),
        .Q(xreg2[8]));
  FDCE \xreg2_reg[9] 
       (.C(clock),
        .CE(xreg1),
        .CLR(reset_0),
        .D(outx22[9]),
        .Q(xreg2[9]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg1[0]_i_1__3 
       (.I0(Q[48]),
        .I1(Q[16]),
        .O(\y12/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair205" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \yreg1[1]_i_1__3 
       (.I0(Q[17]),
        .I1(Q[49]),
        .I2(Q[32]),
        .I3(Q[16]),
        .I4(Q[48]),
        .O(outy12[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \yreg1[2]_i_1__3 
       (.I0(Q[18]),
        .I1(Q[33]),
        .I2(Q[50]),
        .I3(Q[32]),
        .I4(Q[49]),
        .I5(\y12/C_1 ),
        .O(outy12[2]));
  (* SOFT_HLUTNM = "soft_lutpair205" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \yreg1[2]_i_2__3 
       (.I0(Q[48]),
        .I1(Q[16]),
        .I2(Q[32]),
        .I3(Q[49]),
        .I4(Q[17]),
        .O(\y12/C_1 ));
  LUT3 #(
    .INIT(8'h96)) 
    \yreg1[3]_i_1__3 
       (.I0(Q[19]),
        .I1(outy11[3]),
        .I2(\y12/C_2 ),
        .O(outy12[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \yreg1[3]_i_2__3 
       (.I0(Q[34]),
        .I1(Q[51]),
        .I2(Q[33]),
        .I3(Q[50]),
        .I4(Q[32]),
        .I5(Q[49]),
        .O(outy11[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \yreg1[3]_i_3__3 
       (.I0(\y12/C_1 ),
        .I1(Q[49]),
        .I2(Q[32]),
        .I3(Q[50]),
        .I4(Q[33]),
        .I5(Q[18]),
        .O(\y12/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair214" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg1[4]_i_1__3 
       (.I0(Q[20]),
        .I1(Q[35]),
        .I2(Q[52]),
        .I3(\y11/C_3 ),
        .I4(\y12/C_3 ),
        .O(outy12[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \yreg1[5]_i_1__3 
       (.I0(\y12/C_3 ),
        .I1(\y11/C_3 ),
        .I2(Q[52]),
        .I3(Q[35]),
        .I4(Q[20]),
        .I5(\y12/f6/S ),
        .O(outy12[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[5]_i_2__3 
       (.I0(\y12/C_1 ),
        .I1(outy11[2]),
        .I2(Q[18]),
        .I3(outy11[3]),
        .I4(Q[19]),
        .O(\y12/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \yreg1[5]_i_3__3 
       (.I0(Q[49]),
        .I1(Q[32]),
        .I2(Q[50]),
        .I3(Q[33]),
        .I4(Q[51]),
        .I5(Q[34]),
        .O(\y11/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg1[5]_i_4__3 
       (.I0(\y11/C_3 ),
        .I1(Q[52]),
        .I2(Q[35]),
        .I3(Q[53]),
        .I4(Q[36]),
        .I5(Q[21]),
        .O(\y12/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \yreg1[5]_i_5__3 
       (.I0(Q[33]),
        .I1(Q[50]),
        .I2(Q[32]),
        .I3(Q[49]),
        .O(outy11[2]));
  (* SOFT_HLUTNM = "soft_lutpair215" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg1[6]_i_1__3 
       (.I0(Q[22]),
        .I1(Q[37]),
        .I2(Q[54]),
        .I3(\y11/C_5 ),
        .I4(\y12/C_5 ),
        .O(outy12[6]));
  (* SOFT_HLUTNM = "soft_lutpair210" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[6]_i_2__3 
       (.I0(\y11/C_3 ),
        .I1(Q[52]),
        .I2(Q[35]),
        .I3(Q[53]),
        .I4(Q[36]),
        .O(\y11/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg1[6]_i_3__3 
       (.I0(\y12/C_3 ),
        .I1(outy11[4]),
        .I2(Q[20]),
        .I3(outy11[5]),
        .I4(Q[21]),
        .O(\y12/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair214" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg1[6]_i_4__3 
       (.I0(Q[35]),
        .I1(Q[52]),
        .I2(\y11/C_3 ),
        .O(outy11[4]));
  (* SOFT_HLUTNM = "soft_lutpair210" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \yreg1[6]_i_5__3 
       (.I0(Q[36]),
        .I1(Q[53]),
        .I2(Q[35]),
        .I3(Q[52]),
        .I4(\y11/C_3 ),
        .O(outy11[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg1[7]_i_1__3 
       (.I0(\y12/f8/S ),
        .I1(\y12/C_6 ),
        .O(outy12[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg1[7]_i_2__3 
       (.I0(\y11/C_5 ),
        .I1(Q[54]),
        .I2(Q[37]),
        .I3(Q[55]),
        .I4(Q[38]),
        .I5(Q[23]),
        .O(\y12/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \yreg1[8]_i_1__3 
       (.I0(Q[39]),
        .I1(Q[23]),
        .I2(Q[38]),
        .I3(Q[55]),
        .I4(\y11/C_6 ),
        .I5(\y12/C_6 ),
        .O(outy12[8]));
  LUT6 #(
    .INIT(64'hF7FF000000000000)) 
    \yreg1[9]_i_1__3 
       (.I0(\full_reg_n_0_[0] ),
        .I1(\m_axis_tkeep[4] ),
        .I2(m_axis_tready),
        .I3(\full_reg_n_0_[1] ),
        .I4(\valid_reg[4] ),
        .I5(\yreg2_reg[0]_0 ),
        .O(yreg1));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \yreg1[9]_i_2__3 
       (.I0(\y12/C_6 ),
        .I1(\y11/C_6 ),
        .I2(Q[55]),
        .I3(Q[38]),
        .I4(Q[23]),
        .I5(Q[39]),
        .O(outy12[9]));
  (* SOFT_HLUTNM = "soft_lutpair215" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \yreg1[9]_i_3__2 
       (.I0(\y12/C_5 ),
        .I1(\y11/C_5 ),
        .I2(Q[54]),
        .I3(Q[37]),
        .I4(Q[22]),
        .O(\y12/C_6 ));
  (* SOFT_HLUTNM = "soft_lutpair207" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \yreg1[9]_i_3__3 
       (.I0(reset),
        .I1(hold),
        .O(\yreg2_reg[0]_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \yreg1[9]_i_4__3 
       (.I0(\y11/C_5 ),
        .I1(Q[54]),
        .I2(Q[37]),
        .O(\y11/C_6 ));
  FDRE \yreg1_reg[0] 
       (.C(clock),
        .CE(yreg1),
        .D(\y12/f1/S ),
        .Q(\yreg1_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \yreg1_reg[1] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[1]),
        .Q(\yreg1_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \yreg1_reg[2] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[2]),
        .Q(\yreg1_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \yreg1_reg[3] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[3]),
        .Q(\yreg1_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \yreg1_reg[4] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[4]),
        .Q(\yreg1_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \yreg1_reg[5] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[5]),
        .Q(\yreg1_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \yreg1_reg[6] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[6]),
        .Q(\yreg1_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \yreg1_reg[7] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[7]),
        .Q(\yreg1_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \yreg1_reg[8] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[8]),
        .Q(\yreg1_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \yreg1_reg[9] 
       (.C(clock),
        .CE(yreg1),
        .D(outy12[9]),
        .Q(\yreg1_reg_n_0_[9] ),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg2[0]_i_1__3 
       (.I0(Q[40]),
        .I1(Q[0]),
        .O(\y22/f1/S ));
  (* SOFT_HLUTNM = "soft_lutpair211" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    \yreg2[1]_i_1__3 
       (.I0(Q[1]),
        .I1(Q[41]),
        .I2(Q[24]),
        .I3(Q[0]),
        .I4(Q[40]),
        .O(outy22[1]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    \yreg2[2]_i_1__3 
       (.I0(Q[2]),
        .I1(Q[25]),
        .I2(Q[42]),
        .I3(Q[24]),
        .I4(Q[41]),
        .I5(\y22/C_1 ),
        .O(outy22[2]));
  (* SOFT_HLUTNM = "soft_lutpair211" *) 
  LUT5 #(
    .INIT(32'h8FF80880)) 
    \yreg2[2]_i_2__3 
       (.I0(Q[40]),
        .I1(Q[0]),
        .I2(Q[24]),
        .I3(Q[41]),
        .I4(Q[1]),
        .O(\y22/C_1 ));
  LUT3 #(
    .INIT(8'h96)) 
    \yreg2[3]_i_1__3 
       (.I0(Q[3]),
        .I1(outy21[3]),
        .I2(\y22/C_2 ),
        .O(outy22[3]));
  LUT6 #(
    .INIT(64'h9996966696669666)) 
    \yreg2[3]_i_2__3 
       (.I0(Q[26]),
        .I1(Q[43]),
        .I2(Q[25]),
        .I3(Q[42]),
        .I4(Q[24]),
        .I5(Q[41]),
        .O(outy21[3]));
  LUT6 #(
    .INIT(64'hEABFBFEA802A2A80)) 
    \yreg2[3]_i_3__3 
       (.I0(\y22/C_1 ),
        .I1(Q[41]),
        .I2(Q[24]),
        .I3(Q[42]),
        .I4(Q[25]),
        .I5(Q[2]),
        .O(\y22/C_2 ));
  (* SOFT_HLUTNM = "soft_lutpair209" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg2[4]_i_1__3 
       (.I0(Q[4]),
        .I1(Q[27]),
        .I2(Q[44]),
        .I3(\y21/C_3 ),
        .I4(\y22/C_3 ),
        .O(outy22[4]));
  LUT6 #(
    .INIT(64'h14417DD7EBBE8228)) 
    \yreg2[5]_i_1__3 
       (.I0(\y22/C_3 ),
        .I1(\y21/C_3 ),
        .I2(Q[44]),
        .I3(Q[27]),
        .I4(Q[4]),
        .I5(\y22/f6/S ),
        .O(outy22[5]));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[5]_i_2__3 
       (.I0(\y22/C_1 ),
        .I1(outy21[2]),
        .I2(Q[2]),
        .I3(outy21[3]),
        .I4(Q[3]),
        .O(\y22/C_3 ));
  LUT6 #(
    .INIT(64'hFFFFF880F8800000)) 
    \yreg2[5]_i_3__3 
       (.I0(Q[41]),
        .I1(Q[24]),
        .I2(Q[42]),
        .I3(Q[25]),
        .I4(Q[43]),
        .I5(Q[26]),
        .O(\y21/C_3 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg2[5]_i_4__3 
       (.I0(\y21/C_3 ),
        .I1(Q[44]),
        .I2(Q[27]),
        .I3(Q[45]),
        .I4(Q[28]),
        .I5(Q[5]),
        .O(\y22/f6/S ));
  LUT4 #(
    .INIT(16'h9666)) 
    \yreg2[5]_i_5__3 
       (.I0(Q[25]),
        .I1(Q[42]),
        .I2(Q[24]),
        .I3(Q[41]),
        .O(outy21[2]));
  (* SOFT_HLUTNM = "soft_lutpair208" *) 
  LUT5 #(
    .INIT(32'h96696996)) 
    \yreg2[6]_i_1__3 
       (.I0(Q[6]),
        .I1(Q[29]),
        .I2(Q[46]),
        .I3(\y21/C_5 ),
        .I4(\y22/C_5 ),
        .O(outy22[6]));
  (* SOFT_HLUTNM = "soft_lutpair213" *) 
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[6]_i_2__3 
       (.I0(\y21/C_3 ),
        .I1(Q[44]),
        .I2(Q[27]),
        .I3(Q[45]),
        .I4(Q[28]),
        .O(\y21/C_5 ));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    \yreg2[6]_i_3__3 
       (.I0(\y22/C_3 ),
        .I1(outy21[4]),
        .I2(Q[4]),
        .I3(outy21[5]),
        .I4(Q[5]),
        .O(\y22/C_5 ));
  (* SOFT_HLUTNM = "soft_lutpair209" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \yreg2[6]_i_4__3 
       (.I0(Q[27]),
        .I1(Q[44]),
        .I2(\y21/C_3 ),
        .O(outy21[4]));
  (* SOFT_HLUTNM = "soft_lutpair213" *) 
  LUT5 #(
    .INIT(32'h99969666)) 
    \yreg2[6]_i_5__3 
       (.I0(Q[28]),
        .I1(Q[45]),
        .I2(Q[27]),
        .I3(Q[44]),
        .I4(\y21/C_3 ),
        .O(outy21[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \yreg2[7]_i_1__3 
       (.I0(\y22/f8/S ),
        .I1(\y22/C_6 ),
        .O(outy22[7]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \yreg2[7]_i_2__3 
       (.I0(\y21/C_5 ),
        .I1(Q[46]),
        .I2(Q[29]),
        .I3(Q[47]),
        .I4(Q[30]),
        .I5(Q[7]),
        .O(\y22/f8/S ));
  LUT6 #(
    .INIT(64'hA99595569556566A)) 
    \yreg2[8]_i_1__3 
       (.I0(Q[31]),
        .I1(Q[7]),
        .I2(Q[30]),
        .I3(Q[47]),
        .I4(\y21/C_6 ),
        .I5(\y22/C_6 ),
        .O(outy22[8]));
  LUT6 #(
    .INIT(64'hFFFEFEE8E8808000)) 
    \yreg2[9]_i_1__3 
       (.I0(\y22/C_6 ),
        .I1(\y21/C_6 ),
        .I2(Q[47]),
        .I3(Q[30]),
        .I4(Q[7]),
        .I5(Q[31]),
        .O(outy22[9]));
  (* SOFT_HLUTNM = "soft_lutpair208" *) 
  LUT5 #(
    .INIT(32'hEBBE8228)) 
    \yreg2[9]_i_2__3 
       (.I0(\y22/C_5 ),
        .I1(\y21/C_5 ),
        .I2(Q[46]),
        .I3(Q[29]),
        .I4(Q[6]),
        .O(\y22/C_6 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \yreg2[9]_i_3__3 
       (.I0(\y21/C_5 ),
        .I1(Q[46]),
        .I2(Q[29]),
        .O(\y21/C_6 ));
  FDRE \yreg2_reg[0] 
       (.C(clock),
        .CE(yreg1),
        .D(\y22/f1/S ),
        .Q(yreg2[0]),
        .R(1'b0));
  FDRE \yreg2_reg[1] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[1]),
        .Q(yreg2[1]),
        .R(1'b0));
  FDRE \yreg2_reg[2] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[2]),
        .Q(yreg2[2]),
        .R(1'b0));
  FDRE \yreg2_reg[3] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[3]),
        .Q(yreg2[3]),
        .R(1'b0));
  FDRE \yreg2_reg[4] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[4]),
        .Q(yreg2[4]),
        .R(1'b0));
  FDRE \yreg2_reg[5] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[5]),
        .Q(yreg2[5]),
        .R(1'b0));
  FDRE \yreg2_reg[6] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[6]),
        .Q(yreg2[6]),
        .R(1'b0));
  FDRE \yreg2_reg[7] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[7]),
        .Q(yreg2[7]),
        .R(1'b0));
  FDRE \yreg2_reg[8] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[8]),
        .Q(yreg2[8]),
        .R(1'b0));
  FDRE \yreg2_reg[9] 
       (.C(clock),
        .CE(yreg1),
        .D(outy22[9]),
        .Q(yreg2[9]),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "finalproj_sobelfilteraxistream_0_0,sobelfilteraxistream,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "sobelfilteraxistream,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clock,
    reset,
    s_axis_tlast,
    s_axis_tvalid,
    s_axis_tready,
    s_axis_tdata,
    s_axis_tstrb,
    m_axis_tlast,
    m_axis_tvalid,
    m_axis_tready,
    m_axis_tdata,
    m_axis_tkeep);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clock CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clock, ASSOCIATED_BUSIF m_axis:s_axis, ASSOCIATED_RESET reset, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN finalproj_processing_system7_0_0_FCLK_CLK0" *) input clock;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 reset RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW" *) input reset;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TLAST" *) input s_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TVALID" *) input s_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TREADY" *) output s_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TDATA" *) input [127:0]s_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TSTRB" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axis, TDATA_NUM_BYTES 16, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN finalproj_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) input [15:0]s_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TLAST" *) output m_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TVALID" *) output m_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TREADY" *) input m_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TDATA" *) output [39:0]m_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TKEEP" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_axis, TDATA_NUM_BYTES 5, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN finalproj_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) output [4:0]m_axis_tkeep;

  wire clock;
  wire [39:0]m_axis_tdata;
  wire [4:0]m_axis_tkeep;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire m_axis_tvalid;
  wire reset;
  wire [127:0]s_axis_tdata;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire [15:0]s_axis_tstrb;
  wire s_axis_tvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sobelfilteraxistream inst
       (.clock(clock),
        .m_axis_tdata(m_axis_tdata),
        .\m_axis_tkeep[0] (m_axis_tkeep[0]),
        .\m_axis_tkeep[1] (m_axis_tkeep[1]),
        .\m_axis_tkeep[2] (m_axis_tkeep[2]),
        .\m_axis_tkeep[3] (m_axis_tkeep[3]),
        .\m_axis_tkeep[4] (m_axis_tkeep[4]),
        .m_axis_tlast(m_axis_tlast),
        .m_axis_tready(m_axis_tready),
        .m_axis_tvalid(m_axis_tvalid),
        .reset(reset),
        .s_axis_tdata(s_axis_tdata),
        .s_axis_tlast(s_axis_tlast),
        .s_axis_tready(s_axis_tready),
        .s_axis_tstrb(s_axis_tstrb[14:0]),
        .s_axis_tvalid(s_axis_tvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sobelfilteraxistream
   (\m_axis_tkeep[0] ,
    m_axis_tdata,
    \m_axis_tkeep[1] ,
    \m_axis_tkeep[2] ,
    \m_axis_tkeep[3] ,
    \m_axis_tkeep[4] ,
    m_axis_tlast,
    s_axis_tready,
    m_axis_tvalid,
    s_axis_tstrb,
    clock,
    reset,
    m_axis_tready,
    s_axis_tdata,
    s_axis_tlast,
    s_axis_tvalid);
  output \m_axis_tkeep[0] ;
  output [39:0]m_axis_tdata;
  output \m_axis_tkeep[1] ;
  output \m_axis_tkeep[2] ;
  output \m_axis_tkeep[3] ;
  output \m_axis_tkeep[4] ;
  output m_axis_tlast;
  output s_axis_tready;
  output m_axis_tvalid;
  input [14:0]s_axis_tstrb;
  input clock;
  input reset;
  input m_axis_tready;
  input [127:0]s_axis_tdata;
  input s_axis_tlast;
  input s_axis_tvalid;

  wire c1_n_10;
  wire c1_n_11;
  wire c1_n_15;
  wire c1_n_16;
  wire c1_n_18;
  wire c3_n_10;
  wire c4_n_0;
  wire c5_n_0;
  wire clock;
  wire f;
  wire f_i_2_n_0;
  wire f_i_3_n_0;
  wire hold;
  wire hold0;
  wire [167:0]inputvals;
  wire [39:0]m_axis_tdata;
  wire \m_axis_tkeep[0] ;
  wire \m_axis_tkeep[1] ;
  wire \m_axis_tkeep[2] ;
  wire \m_axis_tkeep[3] ;
  wire \m_axis_tkeep[4] ;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire m_axis_tvalid;
  wire [9:1]outx12;
  wire [9:1]outx12_1;
  wire [9:1]outx12_3;
  wire [167:48]p_0_in;
  wire [4:0]p_1_out;
  wire reset;
  wire [127:0]s_axis_tdata;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire [14:0]s_axis_tstrb;
  wire s_axis_tvalid;
  wire temp0;
  wire \temp[47]_i_2_n_0 ;
  wire \temp_reg_n_0_[0] ;
  wire \temp_reg_n_0_[10] ;
  wire \temp_reg_n_0_[11] ;
  wire \temp_reg_n_0_[12] ;
  wire \temp_reg_n_0_[13] ;
  wire \temp_reg_n_0_[14] ;
  wire \temp_reg_n_0_[15] ;
  wire \temp_reg_n_0_[16] ;
  wire \temp_reg_n_0_[17] ;
  wire \temp_reg_n_0_[18] ;
  wire \temp_reg_n_0_[19] ;
  wire \temp_reg_n_0_[1] ;
  wire \temp_reg_n_0_[20] ;
  wire \temp_reg_n_0_[21] ;
  wire \temp_reg_n_0_[22] ;
  wire \temp_reg_n_0_[23] ;
  wire \temp_reg_n_0_[24] ;
  wire \temp_reg_n_0_[25] ;
  wire \temp_reg_n_0_[26] ;
  wire \temp_reg_n_0_[27] ;
  wire \temp_reg_n_0_[28] ;
  wire \temp_reg_n_0_[29] ;
  wire \temp_reg_n_0_[2] ;
  wire \temp_reg_n_0_[30] ;
  wire \temp_reg_n_0_[31] ;
  wire \temp_reg_n_0_[32] ;
  wire \temp_reg_n_0_[33] ;
  wire \temp_reg_n_0_[34] ;
  wire \temp_reg_n_0_[35] ;
  wire \temp_reg_n_0_[36] ;
  wire \temp_reg_n_0_[37] ;
  wire \temp_reg_n_0_[38] ;
  wire \temp_reg_n_0_[39] ;
  wire \temp_reg_n_0_[3] ;
  wire \temp_reg_n_0_[40] ;
  wire \temp_reg_n_0_[41] ;
  wire \temp_reg_n_0_[42] ;
  wire \temp_reg_n_0_[43] ;
  wire \temp_reg_n_0_[44] ;
  wire \temp_reg_n_0_[45] ;
  wire \temp_reg_n_0_[46] ;
  wire \temp_reg_n_0_[47] ;
  wire \temp_reg_n_0_[4] ;
  wire \temp_reg_n_0_[5] ;
  wire \temp_reg_n_0_[6] ;
  wire \temp_reg_n_0_[7] ;
  wire \temp_reg_n_0_[8] ;
  wire \temp_reg_n_0_[9] ;
  wire valid;
  wire \valid[1]_i_2_n_0 ;
  wire \valid[1]_i_3_n_0 ;
  wire \valid[2]_i_1_n_0 ;
  wire \valid[2]_i_2_n_0 ;
  wire \valid_reg_n_0_[0] ;
  wire \valid_reg_n_0_[1] ;
  wire \valid_reg_n_0_[2] ;
  wire \valid_reg_n_0_[3] ;
  wire \valid_reg_n_0_[4] ;
  wire \x12/f1/S ;
  wire \x12/f1/S_0 ;
  wire \x12/f1/S_2 ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convolution c1
       (.D({outx12,\x12/f1/S }),
        .E(c1_n_16),
        .Q({inputvals[167:136],inputvals[127:96]}),
        .clock(clock),
        .f(f),
        .f_reg(c1_n_18),
        .hold(hold),
        .hold0(hold0),
        .hold_reg(c5_n_0),
        .lastpck_reg(c1_n_15),
        .m_axis_tdata(m_axis_tdata[7:0]),
        .\m_axis_tkeep[0] (\m_axis_tkeep[0] ),
        .m_axis_tlast(m_axis_tlast),
        .m_axis_tready(m_axis_tready),
        .reset(reset),
        .reset_0(c4_n_0),
        .s_axis_tlast(s_axis_tlast),
        .s_axis_tready(s_axis_tready),
        .s_axis_tstrb({s_axis_tstrb[14:12],s_axis_tstrb[2:0]}),
        .\s_axis_tstrb[6] (f_i_2_n_0),
        .\s_axis_tstrb[9] (\temp[47]_i_2_n_0 ),
        .s_axis_tvalid(s_axis_tvalid),
        .temp0(temp0),
        .\temp_reg[0] (c1_n_11),
        .\valid_reg[0] (\valid_reg_n_0_[0] ),
        .\xreg1_reg[8]_0 (c1_n_10));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convolution_0 c2
       (.D({outx12_1,\x12/f1/S_0 }),
        .Q({inputvals[143:112],inputvals[103:72]}),
        .clock(clock),
        .\full_reg[2]_0 (\m_axis_tkeep[4] ),
        .\full_reg[2]_1 (\m_axis_tkeep[0] ),
        .\full_reg[2]_2 (\m_axis_tkeep[3] ),
        .\full_reg[2]_3 (\m_axis_tkeep[2] ),
        .hold(hold),
        .hold_reg(c5_n_0),
        .m_axis_tdata(m_axis_tdata[15:8]),
        .\m_axis_tkeep[1] (\m_axis_tkeep[1] ),
        .m_axis_tready(m_axis_tready),
        .m_axis_tvalid(m_axis_tvalid),
        .reset(reset),
        .reset_0(c4_n_0),
        .reset_1(c3_n_10),
        .\valid_reg[1] (\valid_reg_n_0_[1] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convolution_1 c3
       (.D({outx12_3,\x12/f1/S_2 }),
        .Q({inputvals[119:112],inputvals[103:88],inputvals[79:48]}),
        .clock(clock),
        .hold(hold),
        .hold_reg(c5_n_0),
        .\inputvals_reg[119] ({outx12,\x12/f1/S }),
        .m_axis_tdata(m_axis_tdata[23:16]),
        .\m_axis_tkeep[2] (\m_axis_tkeep[2] ),
        .m_axis_tready(m_axis_tready),
        .\out_reg[0]_0 (c3_n_10),
        .reset(reset),
        .reset_0(c4_n_0),
        .\valid_reg[2] (\valid_reg_n_0_[2] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convolution_2 c4
       (.D({outx12_1,\x12/f1/S_0 }),
        .Q({inputvals[95:88],inputvals[79:64],inputvals[55:24]}),
        .clock(clock),
        .\full_reg[2]_0 (c4_n_0),
        .hold(hold),
        .hold_reg(c5_n_0),
        .m_axis_tdata(m_axis_tdata[31:24]),
        .\m_axis_tkeep[3] (\m_axis_tkeep[3] ),
        .m_axis_tready(m_axis_tready),
        .reset(reset),
        .\valid_reg[3] (\valid_reg_n_0_[3] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_convolution_3 c5
       (.D({outx12_3,\x12/f1/S_2 }),
        .Q({inputvals[71:64],inputvals[55:40],inputvals[31:0]}),
        .clock(clock),
        .hold(hold),
        .m_axis_tdata(m_axis_tdata[39:32]),
        .\m_axis_tkeep[4] (\m_axis_tkeep[4] ),
        .m_axis_tready(m_axis_tready),
        .reset(reset),
        .reset_0(c1_n_10),
        .reset_1(c4_n_0),
        .reset_2(c3_n_10),
        .\valid_reg[4] (\valid_reg_n_0_[4] ),
        .\yreg2_reg[0]_0 (c5_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEAAA)) 
    f_i_2
       (.I0(\valid[1]_i_3_n_0 ),
        .I1(s_axis_tdata[121]),
        .I2(s_axis_tdata[120]),
        .I3(f_i_3_n_0),
        .I4(\valid[2]_i_2_n_0 ),
        .I5(\valid[1]_i_2_n_0 ),
        .O(f_i_2_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    f_i_3
       (.I0(s_axis_tdata[124]),
        .I1(s_axis_tdata[125]),
        .I2(s_axis_tdata[122]),
        .I3(s_axis_tdata[123]),
        .I4(s_axis_tdata[127]),
        .I5(s_axis_tdata[126]),
        .O(f_i_3_n_0));
  FDRE #(
    .INIT(1'b1)) 
    f_reg
       (.C(clock),
        .CE(1'b1),
        .D(c1_n_18),
        .Q(f),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    hold_reg
       (.C(clock),
        .CE(temp0),
        .D(hold0),
        .Q(hold),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair270" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[100]_i_1 
       (.I0(s_axis_tdata[68]),
        .I1(f),
        .I2(s_axis_tdata[20]),
        .O(p_0_in[100]));
  (* SOFT_HLUTNM = "soft_lutpair269" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[101]_i_1 
       (.I0(s_axis_tdata[69]),
        .I1(f),
        .I2(s_axis_tdata[21]),
        .O(p_0_in[101]));
  (* SOFT_HLUTNM = "soft_lutpair268" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[102]_i_1 
       (.I0(s_axis_tdata[70]),
        .I1(f),
        .I2(s_axis_tdata[22]),
        .O(p_0_in[102]));
  (* SOFT_HLUTNM = "soft_lutpair267" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[103]_i_1 
       (.I0(s_axis_tdata[71]),
        .I1(f),
        .I2(s_axis_tdata[23]),
        .O(p_0_in[103]));
  (* SOFT_HLUTNM = "soft_lutpair266" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[104]_i_1 
       (.I0(s_axis_tdata[56]),
        .I1(f),
        .I2(s_axis_tdata[8]),
        .O(p_0_in[104]));
  (* SOFT_HLUTNM = "soft_lutpair265" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[105]_i_1 
       (.I0(s_axis_tdata[57]),
        .I1(f),
        .I2(s_axis_tdata[9]),
        .O(p_0_in[105]));
  (* SOFT_HLUTNM = "soft_lutpair264" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[106]_i_1 
       (.I0(s_axis_tdata[58]),
        .I1(f),
        .I2(s_axis_tdata[10]),
        .O(p_0_in[106]));
  (* SOFT_HLUTNM = "soft_lutpair263" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[107]_i_1 
       (.I0(s_axis_tdata[59]),
        .I1(f),
        .I2(s_axis_tdata[11]),
        .O(p_0_in[107]));
  (* SOFT_HLUTNM = "soft_lutpair262" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[108]_i_1 
       (.I0(s_axis_tdata[60]),
        .I1(f),
        .I2(s_axis_tdata[12]),
        .O(p_0_in[108]));
  (* SOFT_HLUTNM = "soft_lutpair261" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[109]_i_1 
       (.I0(s_axis_tdata[61]),
        .I1(f),
        .I2(s_axis_tdata[13]),
        .O(p_0_in[109]));
  (* SOFT_HLUTNM = "soft_lutpair260" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[110]_i_1 
       (.I0(s_axis_tdata[62]),
        .I1(f),
        .I2(s_axis_tdata[14]),
        .O(p_0_in[110]));
  (* SOFT_HLUTNM = "soft_lutpair259" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[111]_i_1 
       (.I0(s_axis_tdata[63]),
        .I1(f),
        .I2(s_axis_tdata[15]),
        .O(p_0_in[111]));
  (* SOFT_HLUTNM = "soft_lutpair258" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[112]_i_1 
       (.I0(s_axis_tdata[48]),
        .I1(f),
        .I2(s_axis_tdata[0]),
        .O(p_0_in[112]));
  (* SOFT_HLUTNM = "soft_lutpair257" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[113]_i_1 
       (.I0(s_axis_tdata[49]),
        .I1(f),
        .I2(s_axis_tdata[1]),
        .O(p_0_in[113]));
  (* SOFT_HLUTNM = "soft_lutpair256" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[114]_i_1 
       (.I0(s_axis_tdata[50]),
        .I1(f),
        .I2(s_axis_tdata[2]),
        .O(p_0_in[114]));
  (* SOFT_HLUTNM = "soft_lutpair255" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[115]_i_1 
       (.I0(s_axis_tdata[51]),
        .I1(f),
        .I2(s_axis_tdata[3]),
        .O(p_0_in[115]));
  (* SOFT_HLUTNM = "soft_lutpair254" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[116]_i_1 
       (.I0(s_axis_tdata[52]),
        .I1(f),
        .I2(s_axis_tdata[4]),
        .O(p_0_in[116]));
  (* SOFT_HLUTNM = "soft_lutpair253" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[117]_i_1 
       (.I0(s_axis_tdata[53]),
        .I1(f),
        .I2(s_axis_tdata[5]),
        .O(p_0_in[117]));
  (* SOFT_HLUTNM = "soft_lutpair252" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[118]_i_1 
       (.I0(s_axis_tdata[54]),
        .I1(f),
        .I2(s_axis_tdata[6]),
        .O(p_0_in[118]));
  (* SOFT_HLUTNM = "soft_lutpair251" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[119]_i_1 
       (.I0(s_axis_tdata[55]),
        .I1(f),
        .I2(s_axis_tdata[7]),
        .O(p_0_in[119]));
  (* SOFT_HLUTNM = "soft_lutpair298" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[120]_i_1 
       (.I0(s_axis_tdata[40]),
        .I1(f),
        .I2(\temp_reg_n_0_[0] ),
        .O(p_0_in[120]));
  (* SOFT_HLUTNM = "soft_lutpair297" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[121]_i_1 
       (.I0(s_axis_tdata[41]),
        .I1(f),
        .I2(\temp_reg_n_0_[1] ),
        .O(p_0_in[121]));
  (* SOFT_HLUTNM = "soft_lutpair296" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[122]_i_1 
       (.I0(s_axis_tdata[42]),
        .I1(f),
        .I2(\temp_reg_n_0_[2] ),
        .O(p_0_in[122]));
  (* SOFT_HLUTNM = "soft_lutpair295" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[123]_i_1 
       (.I0(s_axis_tdata[43]),
        .I1(f),
        .I2(\temp_reg_n_0_[3] ),
        .O(p_0_in[123]));
  (* SOFT_HLUTNM = "soft_lutpair294" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[124]_i_1 
       (.I0(s_axis_tdata[44]),
        .I1(f),
        .I2(\temp_reg_n_0_[4] ),
        .O(p_0_in[124]));
  (* SOFT_HLUTNM = "soft_lutpair293" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[125]_i_1 
       (.I0(s_axis_tdata[45]),
        .I1(f),
        .I2(\temp_reg_n_0_[5] ),
        .O(p_0_in[125]));
  (* SOFT_HLUTNM = "soft_lutpair292" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[126]_i_1 
       (.I0(s_axis_tdata[46]),
        .I1(f),
        .I2(\temp_reg_n_0_[6] ),
        .O(p_0_in[126]));
  (* SOFT_HLUTNM = "soft_lutpair291" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[127]_i_1 
       (.I0(s_axis_tdata[47]),
        .I1(f),
        .I2(\temp_reg_n_0_[7] ),
        .O(p_0_in[127]));
  (* SOFT_HLUTNM = "soft_lutpair290" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[128]_i_1 
       (.I0(s_axis_tdata[32]),
        .I1(f),
        .I2(\temp_reg_n_0_[8] ),
        .O(p_0_in[128]));
  (* SOFT_HLUTNM = "soft_lutpair289" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[129]_i_1 
       (.I0(s_axis_tdata[33]),
        .I1(f),
        .I2(\temp_reg_n_0_[9] ),
        .O(p_0_in[129]));
  (* SOFT_HLUTNM = "soft_lutpair288" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[130]_i_1 
       (.I0(s_axis_tdata[34]),
        .I1(f),
        .I2(\temp_reg_n_0_[10] ),
        .O(p_0_in[130]));
  (* SOFT_HLUTNM = "soft_lutpair287" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[131]_i_1 
       (.I0(s_axis_tdata[35]),
        .I1(f),
        .I2(\temp_reg_n_0_[11] ),
        .O(p_0_in[131]));
  (* SOFT_HLUTNM = "soft_lutpair286" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[132]_i_1 
       (.I0(s_axis_tdata[36]),
        .I1(f),
        .I2(\temp_reg_n_0_[12] ),
        .O(p_0_in[132]));
  (* SOFT_HLUTNM = "soft_lutpair285" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[133]_i_1 
       (.I0(s_axis_tdata[37]),
        .I1(f),
        .I2(\temp_reg_n_0_[13] ),
        .O(p_0_in[133]));
  (* SOFT_HLUTNM = "soft_lutpair284" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[134]_i_1 
       (.I0(s_axis_tdata[38]),
        .I1(f),
        .I2(\temp_reg_n_0_[14] ),
        .O(p_0_in[134]));
  (* SOFT_HLUTNM = "soft_lutpair283" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[135]_i_1 
       (.I0(s_axis_tdata[39]),
        .I1(f),
        .I2(\temp_reg_n_0_[15] ),
        .O(p_0_in[135]));
  (* SOFT_HLUTNM = "soft_lutpair282" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[136]_i_1 
       (.I0(s_axis_tdata[24]),
        .I1(f),
        .I2(\temp_reg_n_0_[16] ),
        .O(p_0_in[136]));
  (* SOFT_HLUTNM = "soft_lutpair281" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[137]_i_1 
       (.I0(s_axis_tdata[25]),
        .I1(f),
        .I2(\temp_reg_n_0_[17] ),
        .O(p_0_in[137]));
  (* SOFT_HLUTNM = "soft_lutpair280" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[138]_i_1 
       (.I0(s_axis_tdata[26]),
        .I1(f),
        .I2(\temp_reg_n_0_[18] ),
        .O(p_0_in[138]));
  (* SOFT_HLUTNM = "soft_lutpair279" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[139]_i_1 
       (.I0(s_axis_tdata[27]),
        .I1(f),
        .I2(\temp_reg_n_0_[19] ),
        .O(p_0_in[139]));
  (* SOFT_HLUTNM = "soft_lutpair278" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[140]_i_1 
       (.I0(s_axis_tdata[28]),
        .I1(f),
        .I2(\temp_reg_n_0_[20] ),
        .O(p_0_in[140]));
  (* SOFT_HLUTNM = "soft_lutpair277" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[141]_i_1 
       (.I0(s_axis_tdata[29]),
        .I1(f),
        .I2(\temp_reg_n_0_[21] ),
        .O(p_0_in[141]));
  (* SOFT_HLUTNM = "soft_lutpair276" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[142]_i_1 
       (.I0(s_axis_tdata[30]),
        .I1(f),
        .I2(\temp_reg_n_0_[22] ),
        .O(p_0_in[142]));
  (* SOFT_HLUTNM = "soft_lutpair275" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[143]_i_1 
       (.I0(s_axis_tdata[31]),
        .I1(f),
        .I2(\temp_reg_n_0_[23] ),
        .O(p_0_in[143]));
  (* SOFT_HLUTNM = "soft_lutpair274" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[144]_i_1 
       (.I0(s_axis_tdata[16]),
        .I1(f),
        .I2(\temp_reg_n_0_[24] ),
        .O(p_0_in[144]));
  (* SOFT_HLUTNM = "soft_lutpair273" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[145]_i_1 
       (.I0(s_axis_tdata[17]),
        .I1(f),
        .I2(\temp_reg_n_0_[25] ),
        .O(p_0_in[145]));
  (* SOFT_HLUTNM = "soft_lutpair272" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[146]_i_1 
       (.I0(s_axis_tdata[18]),
        .I1(f),
        .I2(\temp_reg_n_0_[26] ),
        .O(p_0_in[146]));
  (* SOFT_HLUTNM = "soft_lutpair271" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[147]_i_1 
       (.I0(s_axis_tdata[19]),
        .I1(f),
        .I2(\temp_reg_n_0_[27] ),
        .O(p_0_in[147]));
  (* SOFT_HLUTNM = "soft_lutpair270" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[148]_i_1 
       (.I0(s_axis_tdata[20]),
        .I1(f),
        .I2(\temp_reg_n_0_[28] ),
        .O(p_0_in[148]));
  (* SOFT_HLUTNM = "soft_lutpair269" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[149]_i_1 
       (.I0(s_axis_tdata[21]),
        .I1(f),
        .I2(\temp_reg_n_0_[29] ),
        .O(p_0_in[149]));
  (* SOFT_HLUTNM = "soft_lutpair268" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[150]_i_1 
       (.I0(s_axis_tdata[22]),
        .I1(f),
        .I2(\temp_reg_n_0_[30] ),
        .O(p_0_in[150]));
  (* SOFT_HLUTNM = "soft_lutpair267" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[151]_i_1 
       (.I0(s_axis_tdata[23]),
        .I1(f),
        .I2(\temp_reg_n_0_[31] ),
        .O(p_0_in[151]));
  (* SOFT_HLUTNM = "soft_lutpair266" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[152]_i_1 
       (.I0(s_axis_tdata[8]),
        .I1(f),
        .I2(\temp_reg_n_0_[32] ),
        .O(p_0_in[152]));
  (* SOFT_HLUTNM = "soft_lutpair265" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[153]_i_1 
       (.I0(s_axis_tdata[9]),
        .I1(f),
        .I2(\temp_reg_n_0_[33] ),
        .O(p_0_in[153]));
  (* SOFT_HLUTNM = "soft_lutpair264" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[154]_i_1 
       (.I0(s_axis_tdata[10]),
        .I1(f),
        .I2(\temp_reg_n_0_[34] ),
        .O(p_0_in[154]));
  (* SOFT_HLUTNM = "soft_lutpair263" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[155]_i_1 
       (.I0(s_axis_tdata[11]),
        .I1(f),
        .I2(\temp_reg_n_0_[35] ),
        .O(p_0_in[155]));
  (* SOFT_HLUTNM = "soft_lutpair262" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[156]_i_1 
       (.I0(s_axis_tdata[12]),
        .I1(f),
        .I2(\temp_reg_n_0_[36] ),
        .O(p_0_in[156]));
  (* SOFT_HLUTNM = "soft_lutpair261" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[157]_i_1 
       (.I0(s_axis_tdata[13]),
        .I1(f),
        .I2(\temp_reg_n_0_[37] ),
        .O(p_0_in[157]));
  (* SOFT_HLUTNM = "soft_lutpair260" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[158]_i_1 
       (.I0(s_axis_tdata[14]),
        .I1(f),
        .I2(\temp_reg_n_0_[38] ),
        .O(p_0_in[158]));
  (* SOFT_HLUTNM = "soft_lutpair259" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[159]_i_1 
       (.I0(s_axis_tdata[15]),
        .I1(f),
        .I2(\temp_reg_n_0_[39] ),
        .O(p_0_in[159]));
  (* SOFT_HLUTNM = "soft_lutpair258" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[160]_i_1 
       (.I0(s_axis_tdata[0]),
        .I1(f),
        .I2(\temp_reg_n_0_[40] ),
        .O(p_0_in[160]));
  (* SOFT_HLUTNM = "soft_lutpair257" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[161]_i_1 
       (.I0(s_axis_tdata[1]),
        .I1(f),
        .I2(\temp_reg_n_0_[41] ),
        .O(p_0_in[161]));
  (* SOFT_HLUTNM = "soft_lutpair256" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[162]_i_1 
       (.I0(s_axis_tdata[2]),
        .I1(f),
        .I2(\temp_reg_n_0_[42] ),
        .O(p_0_in[162]));
  (* SOFT_HLUTNM = "soft_lutpair255" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[163]_i_1 
       (.I0(s_axis_tdata[3]),
        .I1(f),
        .I2(\temp_reg_n_0_[43] ),
        .O(p_0_in[163]));
  (* SOFT_HLUTNM = "soft_lutpair254" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[164]_i_1 
       (.I0(s_axis_tdata[4]),
        .I1(f),
        .I2(\temp_reg_n_0_[44] ),
        .O(p_0_in[164]));
  (* SOFT_HLUTNM = "soft_lutpair253" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[165]_i_1 
       (.I0(s_axis_tdata[5]),
        .I1(f),
        .I2(\temp_reg_n_0_[45] ),
        .O(p_0_in[165]));
  (* SOFT_HLUTNM = "soft_lutpair252" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[166]_i_1 
       (.I0(s_axis_tdata[6]),
        .I1(f),
        .I2(\temp_reg_n_0_[46] ),
        .O(p_0_in[166]));
  (* SOFT_HLUTNM = "soft_lutpair251" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[167]_i_1 
       (.I0(s_axis_tdata[7]),
        .I1(f),
        .I2(\temp_reg_n_0_[47] ),
        .O(p_0_in[167]));
  (* SOFT_HLUTNM = "soft_lutpair310" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[48]_i_1 
       (.I0(s_axis_tdata[112]),
        .I1(f),
        .I2(s_axis_tdata[64]),
        .O(p_0_in[48]));
  (* SOFT_HLUTNM = "soft_lutpair310" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[49]_i_1 
       (.I0(s_axis_tdata[113]),
        .I1(f),
        .I2(s_axis_tdata[65]),
        .O(p_0_in[49]));
  (* SOFT_HLUTNM = "soft_lutpair309" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[50]_i_1 
       (.I0(s_axis_tdata[114]),
        .I1(f),
        .I2(s_axis_tdata[66]),
        .O(p_0_in[50]));
  (* SOFT_HLUTNM = "soft_lutpair309" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[51]_i_1 
       (.I0(s_axis_tdata[115]),
        .I1(f),
        .I2(s_axis_tdata[67]),
        .O(p_0_in[51]));
  (* SOFT_HLUTNM = "soft_lutpair308" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[52]_i_1 
       (.I0(s_axis_tdata[116]),
        .I1(f),
        .I2(s_axis_tdata[68]),
        .O(p_0_in[52]));
  (* SOFT_HLUTNM = "soft_lutpair308" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[53]_i_1 
       (.I0(s_axis_tdata[117]),
        .I1(f),
        .I2(s_axis_tdata[69]),
        .O(p_0_in[53]));
  (* SOFT_HLUTNM = "soft_lutpair307" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[54]_i_1 
       (.I0(s_axis_tdata[118]),
        .I1(f),
        .I2(s_axis_tdata[70]),
        .O(p_0_in[54]));
  (* SOFT_HLUTNM = "soft_lutpair307" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[55]_i_1 
       (.I0(s_axis_tdata[119]),
        .I1(f),
        .I2(s_axis_tdata[71]),
        .O(p_0_in[55]));
  (* SOFT_HLUTNM = "soft_lutpair306" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[56]_i_1 
       (.I0(s_axis_tdata[104]),
        .I1(f),
        .I2(s_axis_tdata[56]),
        .O(p_0_in[56]));
  (* SOFT_HLUTNM = "soft_lutpair306" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[57]_i_1 
       (.I0(s_axis_tdata[105]),
        .I1(f),
        .I2(s_axis_tdata[57]),
        .O(p_0_in[57]));
  (* SOFT_HLUTNM = "soft_lutpair305" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[58]_i_1 
       (.I0(s_axis_tdata[106]),
        .I1(f),
        .I2(s_axis_tdata[58]),
        .O(p_0_in[58]));
  (* SOFT_HLUTNM = "soft_lutpair305" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[59]_i_1 
       (.I0(s_axis_tdata[107]),
        .I1(f),
        .I2(s_axis_tdata[59]),
        .O(p_0_in[59]));
  (* SOFT_HLUTNM = "soft_lutpair304" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[60]_i_1 
       (.I0(s_axis_tdata[108]),
        .I1(f),
        .I2(s_axis_tdata[60]),
        .O(p_0_in[60]));
  (* SOFT_HLUTNM = "soft_lutpair304" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[61]_i_1 
       (.I0(s_axis_tdata[109]),
        .I1(f),
        .I2(s_axis_tdata[61]),
        .O(p_0_in[61]));
  (* SOFT_HLUTNM = "soft_lutpair303" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[62]_i_1 
       (.I0(s_axis_tdata[110]),
        .I1(f),
        .I2(s_axis_tdata[62]),
        .O(p_0_in[62]));
  (* SOFT_HLUTNM = "soft_lutpair303" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[63]_i_1 
       (.I0(s_axis_tdata[111]),
        .I1(f),
        .I2(s_axis_tdata[63]),
        .O(p_0_in[63]));
  (* SOFT_HLUTNM = "soft_lutpair302" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[64]_i_1 
       (.I0(s_axis_tdata[96]),
        .I1(f),
        .I2(s_axis_tdata[48]),
        .O(p_0_in[64]));
  (* SOFT_HLUTNM = "soft_lutpair302" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[65]_i_1 
       (.I0(s_axis_tdata[97]),
        .I1(f),
        .I2(s_axis_tdata[49]),
        .O(p_0_in[65]));
  (* SOFT_HLUTNM = "soft_lutpair301" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[66]_i_1 
       (.I0(s_axis_tdata[98]),
        .I1(f),
        .I2(s_axis_tdata[50]),
        .O(p_0_in[66]));
  (* SOFT_HLUTNM = "soft_lutpair301" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[67]_i_1 
       (.I0(s_axis_tdata[99]),
        .I1(f),
        .I2(s_axis_tdata[51]),
        .O(p_0_in[67]));
  (* SOFT_HLUTNM = "soft_lutpair300" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[68]_i_1 
       (.I0(s_axis_tdata[100]),
        .I1(f),
        .I2(s_axis_tdata[52]),
        .O(p_0_in[68]));
  (* SOFT_HLUTNM = "soft_lutpair300" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[69]_i_1 
       (.I0(s_axis_tdata[101]),
        .I1(f),
        .I2(s_axis_tdata[53]),
        .O(p_0_in[69]));
  (* SOFT_HLUTNM = "soft_lutpair299" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[70]_i_1 
       (.I0(s_axis_tdata[102]),
        .I1(f),
        .I2(s_axis_tdata[54]),
        .O(p_0_in[70]));
  (* SOFT_HLUTNM = "soft_lutpair299" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[71]_i_1 
       (.I0(s_axis_tdata[103]),
        .I1(f),
        .I2(s_axis_tdata[55]),
        .O(p_0_in[71]));
  (* SOFT_HLUTNM = "soft_lutpair298" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[72]_i_1 
       (.I0(s_axis_tdata[88]),
        .I1(f),
        .I2(s_axis_tdata[40]),
        .O(p_0_in[72]));
  (* SOFT_HLUTNM = "soft_lutpair297" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[73]_i_1 
       (.I0(s_axis_tdata[89]),
        .I1(f),
        .I2(s_axis_tdata[41]),
        .O(p_0_in[73]));
  (* SOFT_HLUTNM = "soft_lutpair296" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[74]_i_1 
       (.I0(s_axis_tdata[90]),
        .I1(f),
        .I2(s_axis_tdata[42]),
        .O(p_0_in[74]));
  (* SOFT_HLUTNM = "soft_lutpair295" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[75]_i_1 
       (.I0(s_axis_tdata[91]),
        .I1(f),
        .I2(s_axis_tdata[43]),
        .O(p_0_in[75]));
  (* SOFT_HLUTNM = "soft_lutpair294" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[76]_i_1 
       (.I0(s_axis_tdata[92]),
        .I1(f),
        .I2(s_axis_tdata[44]),
        .O(p_0_in[76]));
  (* SOFT_HLUTNM = "soft_lutpair293" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[77]_i_1 
       (.I0(s_axis_tdata[93]),
        .I1(f),
        .I2(s_axis_tdata[45]),
        .O(p_0_in[77]));
  (* SOFT_HLUTNM = "soft_lutpair292" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[78]_i_1 
       (.I0(s_axis_tdata[94]),
        .I1(f),
        .I2(s_axis_tdata[46]),
        .O(p_0_in[78]));
  (* SOFT_HLUTNM = "soft_lutpair291" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[79]_i_1 
       (.I0(s_axis_tdata[95]),
        .I1(f),
        .I2(s_axis_tdata[47]),
        .O(p_0_in[79]));
  (* SOFT_HLUTNM = "soft_lutpair290" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[80]_i_1 
       (.I0(s_axis_tdata[80]),
        .I1(f),
        .I2(s_axis_tdata[32]),
        .O(p_0_in[80]));
  (* SOFT_HLUTNM = "soft_lutpair289" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[81]_i_1 
       (.I0(s_axis_tdata[81]),
        .I1(f),
        .I2(s_axis_tdata[33]),
        .O(p_0_in[81]));
  (* SOFT_HLUTNM = "soft_lutpair288" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[82]_i_1 
       (.I0(s_axis_tdata[82]),
        .I1(f),
        .I2(s_axis_tdata[34]),
        .O(p_0_in[82]));
  (* SOFT_HLUTNM = "soft_lutpair287" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[83]_i_1 
       (.I0(s_axis_tdata[83]),
        .I1(f),
        .I2(s_axis_tdata[35]),
        .O(p_0_in[83]));
  (* SOFT_HLUTNM = "soft_lutpair286" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[84]_i_1 
       (.I0(s_axis_tdata[84]),
        .I1(f),
        .I2(s_axis_tdata[36]),
        .O(p_0_in[84]));
  (* SOFT_HLUTNM = "soft_lutpair285" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[85]_i_1 
       (.I0(s_axis_tdata[85]),
        .I1(f),
        .I2(s_axis_tdata[37]),
        .O(p_0_in[85]));
  (* SOFT_HLUTNM = "soft_lutpair284" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[86]_i_1 
       (.I0(s_axis_tdata[86]),
        .I1(f),
        .I2(s_axis_tdata[38]),
        .O(p_0_in[86]));
  (* SOFT_HLUTNM = "soft_lutpair283" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[87]_i_1 
       (.I0(s_axis_tdata[87]),
        .I1(f),
        .I2(s_axis_tdata[39]),
        .O(p_0_in[87]));
  (* SOFT_HLUTNM = "soft_lutpair282" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[88]_i_1 
       (.I0(s_axis_tdata[72]),
        .I1(f),
        .I2(s_axis_tdata[24]),
        .O(p_0_in[88]));
  (* SOFT_HLUTNM = "soft_lutpair281" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[89]_i_1 
       (.I0(s_axis_tdata[73]),
        .I1(f),
        .I2(s_axis_tdata[25]),
        .O(p_0_in[89]));
  (* SOFT_HLUTNM = "soft_lutpair280" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[90]_i_1 
       (.I0(s_axis_tdata[74]),
        .I1(f),
        .I2(s_axis_tdata[26]),
        .O(p_0_in[90]));
  (* SOFT_HLUTNM = "soft_lutpair279" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[91]_i_1 
       (.I0(s_axis_tdata[75]),
        .I1(f),
        .I2(s_axis_tdata[27]),
        .O(p_0_in[91]));
  (* SOFT_HLUTNM = "soft_lutpair278" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[92]_i_1 
       (.I0(s_axis_tdata[76]),
        .I1(f),
        .I2(s_axis_tdata[28]),
        .O(p_0_in[92]));
  (* SOFT_HLUTNM = "soft_lutpair277" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[93]_i_1 
       (.I0(s_axis_tdata[77]),
        .I1(f),
        .I2(s_axis_tdata[29]),
        .O(p_0_in[93]));
  (* SOFT_HLUTNM = "soft_lutpair276" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[94]_i_1 
       (.I0(s_axis_tdata[78]),
        .I1(f),
        .I2(s_axis_tdata[30]),
        .O(p_0_in[94]));
  (* SOFT_HLUTNM = "soft_lutpair275" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[95]_i_1 
       (.I0(s_axis_tdata[79]),
        .I1(f),
        .I2(s_axis_tdata[31]),
        .O(p_0_in[95]));
  (* SOFT_HLUTNM = "soft_lutpair274" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[96]_i_1 
       (.I0(s_axis_tdata[64]),
        .I1(f),
        .I2(s_axis_tdata[16]),
        .O(p_0_in[96]));
  (* SOFT_HLUTNM = "soft_lutpair273" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[97]_i_1 
       (.I0(s_axis_tdata[65]),
        .I1(f),
        .I2(s_axis_tdata[17]),
        .O(p_0_in[97]));
  (* SOFT_HLUTNM = "soft_lutpair272" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[98]_i_1 
       (.I0(s_axis_tdata[66]),
        .I1(f),
        .I2(s_axis_tdata[18]),
        .O(p_0_in[98]));
  (* SOFT_HLUTNM = "soft_lutpair271" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \inputvals[99]_i_1 
       (.I0(s_axis_tdata[67]),
        .I1(f),
        .I2(s_axis_tdata[19]),
        .O(p_0_in[99]));
  FDRE \inputvals_reg[0] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[112]),
        .Q(inputvals[0]),
        .R(1'b0));
  FDRE \inputvals_reg[100] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[100]),
        .Q(inputvals[100]),
        .R(1'b0));
  FDRE \inputvals_reg[101] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[101]),
        .Q(inputvals[101]),
        .R(1'b0));
  FDRE \inputvals_reg[102] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[102]),
        .Q(inputvals[102]),
        .R(1'b0));
  FDRE \inputvals_reg[103] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[103]),
        .Q(inputvals[103]),
        .R(1'b0));
  FDRE \inputvals_reg[104] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[104]),
        .Q(inputvals[104]),
        .R(1'b0));
  FDRE \inputvals_reg[105] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[105]),
        .Q(inputvals[105]),
        .R(1'b0));
  FDRE \inputvals_reg[106] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[106]),
        .Q(inputvals[106]),
        .R(1'b0));
  FDRE \inputvals_reg[107] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[107]),
        .Q(inputvals[107]),
        .R(1'b0));
  FDRE \inputvals_reg[108] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[108]),
        .Q(inputvals[108]),
        .R(1'b0));
  FDRE \inputvals_reg[109] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[109]),
        .Q(inputvals[109]),
        .R(1'b0));
  FDRE \inputvals_reg[10] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[106]),
        .Q(inputvals[10]),
        .R(1'b0));
  FDRE \inputvals_reg[110] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[110]),
        .Q(inputvals[110]),
        .R(1'b0));
  FDRE \inputvals_reg[111] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[111]),
        .Q(inputvals[111]),
        .R(1'b0));
  FDRE \inputvals_reg[112] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[112]),
        .Q(inputvals[112]),
        .R(1'b0));
  FDRE \inputvals_reg[113] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[113]),
        .Q(inputvals[113]),
        .R(1'b0));
  FDRE \inputvals_reg[114] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[114]),
        .Q(inputvals[114]),
        .R(1'b0));
  FDRE \inputvals_reg[115] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[115]),
        .Q(inputvals[115]),
        .R(1'b0));
  FDRE \inputvals_reg[116] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[116]),
        .Q(inputvals[116]),
        .R(1'b0));
  FDRE \inputvals_reg[117] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[117]),
        .Q(inputvals[117]),
        .R(1'b0));
  FDRE \inputvals_reg[118] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[118]),
        .Q(inputvals[118]),
        .R(1'b0));
  FDRE \inputvals_reg[119] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[119]),
        .Q(inputvals[119]),
        .R(1'b0));
  FDRE \inputvals_reg[11] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[107]),
        .Q(inputvals[11]),
        .R(1'b0));
  FDRE \inputvals_reg[120] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[120]),
        .Q(inputvals[120]),
        .R(1'b0));
  FDRE \inputvals_reg[121] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[121]),
        .Q(inputvals[121]),
        .R(1'b0));
  FDRE \inputvals_reg[122] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[122]),
        .Q(inputvals[122]),
        .R(1'b0));
  FDRE \inputvals_reg[123] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[123]),
        .Q(inputvals[123]),
        .R(1'b0));
  FDRE \inputvals_reg[124] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[124]),
        .Q(inputvals[124]),
        .R(1'b0));
  FDRE \inputvals_reg[125] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[125]),
        .Q(inputvals[125]),
        .R(1'b0));
  FDRE \inputvals_reg[126] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[126]),
        .Q(inputvals[126]),
        .R(1'b0));
  FDRE \inputvals_reg[127] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[127]),
        .Q(inputvals[127]),
        .R(1'b0));
  FDRE \inputvals_reg[128] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[128]),
        .Q(inputvals[128]),
        .R(1'b0));
  FDRE \inputvals_reg[129] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[129]),
        .Q(inputvals[129]),
        .R(1'b0));
  FDRE \inputvals_reg[12] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[108]),
        .Q(inputvals[12]),
        .R(1'b0));
  FDRE \inputvals_reg[130] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[130]),
        .Q(inputvals[130]),
        .R(1'b0));
  FDRE \inputvals_reg[131] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[131]),
        .Q(inputvals[131]),
        .R(1'b0));
  FDRE \inputvals_reg[132] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[132]),
        .Q(inputvals[132]),
        .R(1'b0));
  FDRE \inputvals_reg[133] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[133]),
        .Q(inputvals[133]),
        .R(1'b0));
  FDRE \inputvals_reg[134] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[134]),
        .Q(inputvals[134]),
        .R(1'b0));
  FDRE \inputvals_reg[135] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[135]),
        .Q(inputvals[135]),
        .R(1'b0));
  FDRE \inputvals_reg[136] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[136]),
        .Q(inputvals[136]),
        .R(1'b0));
  FDRE \inputvals_reg[137] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[137]),
        .Q(inputvals[137]),
        .R(1'b0));
  FDRE \inputvals_reg[138] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[138]),
        .Q(inputvals[138]),
        .R(1'b0));
  FDRE \inputvals_reg[139] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[139]),
        .Q(inputvals[139]),
        .R(1'b0));
  FDRE \inputvals_reg[13] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[109]),
        .Q(inputvals[13]),
        .R(1'b0));
  FDRE \inputvals_reg[140] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[140]),
        .Q(inputvals[140]),
        .R(1'b0));
  FDRE \inputvals_reg[141] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[141]),
        .Q(inputvals[141]),
        .R(1'b0));
  FDRE \inputvals_reg[142] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[142]),
        .Q(inputvals[142]),
        .R(1'b0));
  FDRE \inputvals_reg[143] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[143]),
        .Q(inputvals[143]),
        .R(1'b0));
  FDRE \inputvals_reg[144] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[144]),
        .Q(inputvals[144]),
        .R(1'b0));
  FDRE \inputvals_reg[145] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[145]),
        .Q(inputvals[145]),
        .R(1'b0));
  FDRE \inputvals_reg[146] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[146]),
        .Q(inputvals[146]),
        .R(1'b0));
  FDRE \inputvals_reg[147] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[147]),
        .Q(inputvals[147]),
        .R(1'b0));
  FDRE \inputvals_reg[148] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[148]),
        .Q(inputvals[148]),
        .R(1'b0));
  FDRE \inputvals_reg[149] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[149]),
        .Q(inputvals[149]),
        .R(1'b0));
  FDRE \inputvals_reg[14] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[110]),
        .Q(inputvals[14]),
        .R(1'b0));
  FDRE \inputvals_reg[150] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[150]),
        .Q(inputvals[150]),
        .R(1'b0));
  FDRE \inputvals_reg[151] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[151]),
        .Q(inputvals[151]),
        .R(1'b0));
  FDRE \inputvals_reg[152] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[152]),
        .Q(inputvals[152]),
        .R(1'b0));
  FDRE \inputvals_reg[153] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[153]),
        .Q(inputvals[153]),
        .R(1'b0));
  FDRE \inputvals_reg[154] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[154]),
        .Q(inputvals[154]),
        .R(1'b0));
  FDRE \inputvals_reg[155] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[155]),
        .Q(inputvals[155]),
        .R(1'b0));
  FDRE \inputvals_reg[156] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[156]),
        .Q(inputvals[156]),
        .R(1'b0));
  FDRE \inputvals_reg[157] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[157]),
        .Q(inputvals[157]),
        .R(1'b0));
  FDRE \inputvals_reg[158] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[158]),
        .Q(inputvals[158]),
        .R(1'b0));
  FDRE \inputvals_reg[159] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[159]),
        .Q(inputvals[159]),
        .R(1'b0));
  FDRE \inputvals_reg[15] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[111]),
        .Q(inputvals[15]),
        .R(1'b0));
  FDRE \inputvals_reg[160] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[160]),
        .Q(inputvals[160]),
        .R(1'b0));
  FDRE \inputvals_reg[161] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[161]),
        .Q(inputvals[161]),
        .R(1'b0));
  FDRE \inputvals_reg[162] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[162]),
        .Q(inputvals[162]),
        .R(1'b0));
  FDRE \inputvals_reg[163] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[163]),
        .Q(inputvals[163]),
        .R(1'b0));
  FDRE \inputvals_reg[164] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[164]),
        .Q(inputvals[164]),
        .R(1'b0));
  FDRE \inputvals_reg[165] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[165]),
        .Q(inputvals[165]),
        .R(1'b0));
  FDRE \inputvals_reg[166] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[166]),
        .Q(inputvals[166]),
        .R(1'b0));
  FDRE \inputvals_reg[167] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[167]),
        .Q(inputvals[167]),
        .R(1'b0));
  FDRE \inputvals_reg[16] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[96]),
        .Q(inputvals[16]),
        .R(1'b0));
  FDRE \inputvals_reg[17] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[97]),
        .Q(inputvals[17]),
        .R(1'b0));
  FDRE \inputvals_reg[18] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[98]),
        .Q(inputvals[18]),
        .R(1'b0));
  FDRE \inputvals_reg[19] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[99]),
        .Q(inputvals[19]),
        .R(1'b0));
  FDRE \inputvals_reg[1] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[113]),
        .Q(inputvals[1]),
        .R(1'b0));
  FDRE \inputvals_reg[20] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[100]),
        .Q(inputvals[20]),
        .R(1'b0));
  FDRE \inputvals_reg[21] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[101]),
        .Q(inputvals[21]),
        .R(1'b0));
  FDRE \inputvals_reg[22] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[102]),
        .Q(inputvals[22]),
        .R(1'b0));
  FDRE \inputvals_reg[23] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[103]),
        .Q(inputvals[23]),
        .R(1'b0));
  FDRE \inputvals_reg[24] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[88]),
        .Q(inputvals[24]),
        .R(1'b0));
  FDRE \inputvals_reg[25] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[89]),
        .Q(inputvals[25]),
        .R(1'b0));
  FDRE \inputvals_reg[26] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[90]),
        .Q(inputvals[26]),
        .R(1'b0));
  FDRE \inputvals_reg[27] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[91]),
        .Q(inputvals[27]),
        .R(1'b0));
  FDRE \inputvals_reg[28] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[92]),
        .Q(inputvals[28]),
        .R(1'b0));
  FDRE \inputvals_reg[29] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[93]),
        .Q(inputvals[29]),
        .R(1'b0));
  FDRE \inputvals_reg[2] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[114]),
        .Q(inputvals[2]),
        .R(1'b0));
  FDRE \inputvals_reg[30] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[94]),
        .Q(inputvals[30]),
        .R(1'b0));
  FDRE \inputvals_reg[31] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[95]),
        .Q(inputvals[31]),
        .R(1'b0));
  FDRE \inputvals_reg[32] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[80]),
        .Q(inputvals[32]),
        .R(1'b0));
  FDRE \inputvals_reg[33] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[81]),
        .Q(inputvals[33]),
        .R(1'b0));
  FDRE \inputvals_reg[34] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[82]),
        .Q(inputvals[34]),
        .R(1'b0));
  FDRE \inputvals_reg[35] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[83]),
        .Q(inputvals[35]),
        .R(1'b0));
  FDRE \inputvals_reg[36] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[84]),
        .Q(inputvals[36]),
        .R(1'b0));
  FDRE \inputvals_reg[37] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[85]),
        .Q(inputvals[37]),
        .R(1'b0));
  FDRE \inputvals_reg[38] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[86]),
        .Q(inputvals[38]),
        .R(1'b0));
  FDRE \inputvals_reg[39] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[87]),
        .Q(inputvals[39]),
        .R(1'b0));
  FDRE \inputvals_reg[3] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[115]),
        .Q(inputvals[3]),
        .R(1'b0));
  FDRE \inputvals_reg[40] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[72]),
        .Q(inputvals[40]),
        .R(1'b0));
  FDRE \inputvals_reg[41] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[73]),
        .Q(inputvals[41]),
        .R(1'b0));
  FDRE \inputvals_reg[42] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[74]),
        .Q(inputvals[42]),
        .R(1'b0));
  FDRE \inputvals_reg[43] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[75]),
        .Q(inputvals[43]),
        .R(1'b0));
  FDRE \inputvals_reg[44] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[76]),
        .Q(inputvals[44]),
        .R(1'b0));
  FDRE \inputvals_reg[45] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[77]),
        .Q(inputvals[45]),
        .R(1'b0));
  FDRE \inputvals_reg[46] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[78]),
        .Q(inputvals[46]),
        .R(1'b0));
  FDRE \inputvals_reg[47] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[79]),
        .Q(inputvals[47]),
        .R(1'b0));
  FDRE \inputvals_reg[48] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[48]),
        .Q(inputvals[48]),
        .R(1'b0));
  FDRE \inputvals_reg[49] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[49]),
        .Q(inputvals[49]),
        .R(1'b0));
  FDRE \inputvals_reg[4] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[116]),
        .Q(inputvals[4]),
        .R(1'b0));
  FDRE \inputvals_reg[50] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[50]),
        .Q(inputvals[50]),
        .R(1'b0));
  FDRE \inputvals_reg[51] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[51]),
        .Q(inputvals[51]),
        .R(1'b0));
  FDRE \inputvals_reg[52] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[52]),
        .Q(inputvals[52]),
        .R(1'b0));
  FDRE \inputvals_reg[53] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[53]),
        .Q(inputvals[53]),
        .R(1'b0));
  FDRE \inputvals_reg[54] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[54]),
        .Q(inputvals[54]),
        .R(1'b0));
  FDRE \inputvals_reg[55] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[55]),
        .Q(inputvals[55]),
        .R(1'b0));
  FDRE \inputvals_reg[56] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[56]),
        .Q(inputvals[56]),
        .R(1'b0));
  FDRE \inputvals_reg[57] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[57]),
        .Q(inputvals[57]),
        .R(1'b0));
  FDRE \inputvals_reg[58] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[58]),
        .Q(inputvals[58]),
        .R(1'b0));
  FDRE \inputvals_reg[59] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[59]),
        .Q(inputvals[59]),
        .R(1'b0));
  FDRE \inputvals_reg[5] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[117]),
        .Q(inputvals[5]),
        .R(1'b0));
  FDRE \inputvals_reg[60] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[60]),
        .Q(inputvals[60]),
        .R(1'b0));
  FDRE \inputvals_reg[61] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[61]),
        .Q(inputvals[61]),
        .R(1'b0));
  FDRE \inputvals_reg[62] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[62]),
        .Q(inputvals[62]),
        .R(1'b0));
  FDRE \inputvals_reg[63] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[63]),
        .Q(inputvals[63]),
        .R(1'b0));
  FDRE \inputvals_reg[64] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[64]),
        .Q(inputvals[64]),
        .R(1'b0));
  FDRE \inputvals_reg[65] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[65]),
        .Q(inputvals[65]),
        .R(1'b0));
  FDRE \inputvals_reg[66] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[66]),
        .Q(inputvals[66]),
        .R(1'b0));
  FDRE \inputvals_reg[67] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[67]),
        .Q(inputvals[67]),
        .R(1'b0));
  FDRE \inputvals_reg[68] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[68]),
        .Q(inputvals[68]),
        .R(1'b0));
  FDRE \inputvals_reg[69] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[69]),
        .Q(inputvals[69]),
        .R(1'b0));
  FDRE \inputvals_reg[6] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[118]),
        .Q(inputvals[6]),
        .R(1'b0));
  FDRE \inputvals_reg[70] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[70]),
        .Q(inputvals[70]),
        .R(1'b0));
  FDRE \inputvals_reg[71] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[71]),
        .Q(inputvals[71]),
        .R(1'b0));
  FDRE \inputvals_reg[72] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[72]),
        .Q(inputvals[72]),
        .R(1'b0));
  FDRE \inputvals_reg[73] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[73]),
        .Q(inputvals[73]),
        .R(1'b0));
  FDRE \inputvals_reg[74] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[74]),
        .Q(inputvals[74]),
        .R(1'b0));
  FDRE \inputvals_reg[75] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[75]),
        .Q(inputvals[75]),
        .R(1'b0));
  FDRE \inputvals_reg[76] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[76]),
        .Q(inputvals[76]),
        .R(1'b0));
  FDRE \inputvals_reg[77] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[77]),
        .Q(inputvals[77]),
        .R(1'b0));
  FDRE \inputvals_reg[78] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[78]),
        .Q(inputvals[78]),
        .R(1'b0));
  FDRE \inputvals_reg[79] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[79]),
        .Q(inputvals[79]),
        .R(1'b0));
  FDRE \inputvals_reg[7] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[119]),
        .Q(inputvals[7]),
        .R(1'b0));
  FDRE \inputvals_reg[80] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[80]),
        .Q(inputvals[80]),
        .R(1'b0));
  FDRE \inputvals_reg[81] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[81]),
        .Q(inputvals[81]),
        .R(1'b0));
  FDRE \inputvals_reg[82] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[82]),
        .Q(inputvals[82]),
        .R(1'b0));
  FDRE \inputvals_reg[83] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[83]),
        .Q(inputvals[83]),
        .R(1'b0));
  FDRE \inputvals_reg[84] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[84]),
        .Q(inputvals[84]),
        .R(1'b0));
  FDRE \inputvals_reg[85] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[85]),
        .Q(inputvals[85]),
        .R(1'b0));
  FDRE \inputvals_reg[86] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[86]),
        .Q(inputvals[86]),
        .R(1'b0));
  FDRE \inputvals_reg[87] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[87]),
        .Q(inputvals[87]),
        .R(1'b0));
  FDRE \inputvals_reg[88] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[88]),
        .Q(inputvals[88]),
        .R(1'b0));
  FDRE \inputvals_reg[89] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[89]),
        .Q(inputvals[89]),
        .R(1'b0));
  FDRE \inputvals_reg[8] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[104]),
        .Q(inputvals[8]),
        .R(1'b0));
  FDRE \inputvals_reg[90] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[90]),
        .Q(inputvals[90]),
        .R(1'b0));
  FDRE \inputvals_reg[91] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[91]),
        .Q(inputvals[91]),
        .R(1'b0));
  FDRE \inputvals_reg[92] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[92]),
        .Q(inputvals[92]),
        .R(1'b0));
  FDRE \inputvals_reg[93] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[93]),
        .Q(inputvals[93]),
        .R(1'b0));
  FDRE \inputvals_reg[94] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[94]),
        .Q(inputvals[94]),
        .R(1'b0));
  FDRE \inputvals_reg[95] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[95]),
        .Q(inputvals[95]),
        .R(1'b0));
  FDRE \inputvals_reg[96] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[96]),
        .Q(inputvals[96]),
        .R(1'b0));
  FDRE \inputvals_reg[97] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[97]),
        .Q(inputvals[97]),
        .R(1'b0));
  FDRE \inputvals_reg[98] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[98]),
        .Q(inputvals[98]),
        .R(1'b0));
  FDRE \inputvals_reg[99] 
       (.C(clock),
        .CE(temp0),
        .D(p_0_in[99]),
        .Q(inputvals[99]),
        .R(1'b0));
  FDRE \inputvals_reg[9] 
       (.C(clock),
        .CE(c1_n_16),
        .D(s_axis_tdata[105]),
        .Q(inputvals[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    lastpck_reg
       (.C(clock),
        .CE(1'b1),
        .D(c1_n_15),
        .Q(m_axis_tlast),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair250" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \temp[47]_i_2 
       (.I0(s_axis_tstrb[11]),
        .I1(s_axis_tstrb[10]),
        .I2(s_axis_tstrb[9]),
        .O(\temp[47]_i_2_n_0 ));
  FDRE \temp_reg[0] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[112]),
        .Q(\temp_reg_n_0_[0] ),
        .R(c1_n_11));
  FDRE \temp_reg[10] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[106]),
        .Q(\temp_reg_n_0_[10] ),
        .R(c1_n_11));
  FDRE \temp_reg[11] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[107]),
        .Q(\temp_reg_n_0_[11] ),
        .R(c1_n_11));
  FDRE \temp_reg[12] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[108]),
        .Q(\temp_reg_n_0_[12] ),
        .R(c1_n_11));
  FDRE \temp_reg[13] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[109]),
        .Q(\temp_reg_n_0_[13] ),
        .R(c1_n_11));
  FDRE \temp_reg[14] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[110]),
        .Q(\temp_reg_n_0_[14] ),
        .R(c1_n_11));
  FDRE \temp_reg[15] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[111]),
        .Q(\temp_reg_n_0_[15] ),
        .R(c1_n_11));
  FDRE \temp_reg[16] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[96]),
        .Q(\temp_reg_n_0_[16] ),
        .R(c1_n_11));
  FDRE \temp_reg[17] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[97]),
        .Q(\temp_reg_n_0_[17] ),
        .R(c1_n_11));
  FDRE \temp_reg[18] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[98]),
        .Q(\temp_reg_n_0_[18] ),
        .R(c1_n_11));
  FDRE \temp_reg[19] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[99]),
        .Q(\temp_reg_n_0_[19] ),
        .R(c1_n_11));
  FDRE \temp_reg[1] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[113]),
        .Q(\temp_reg_n_0_[1] ),
        .R(c1_n_11));
  FDRE \temp_reg[20] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[100]),
        .Q(\temp_reg_n_0_[20] ),
        .R(c1_n_11));
  FDRE \temp_reg[21] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[101]),
        .Q(\temp_reg_n_0_[21] ),
        .R(c1_n_11));
  FDRE \temp_reg[22] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[102]),
        .Q(\temp_reg_n_0_[22] ),
        .R(c1_n_11));
  FDRE \temp_reg[23] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[103]),
        .Q(\temp_reg_n_0_[23] ),
        .R(c1_n_11));
  FDRE \temp_reg[24] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[88]),
        .Q(\temp_reg_n_0_[24] ),
        .R(c1_n_11));
  FDRE \temp_reg[25] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[89]),
        .Q(\temp_reg_n_0_[25] ),
        .R(c1_n_11));
  FDRE \temp_reg[26] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[90]),
        .Q(\temp_reg_n_0_[26] ),
        .R(c1_n_11));
  FDRE \temp_reg[27] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[91]),
        .Q(\temp_reg_n_0_[27] ),
        .R(c1_n_11));
  FDRE \temp_reg[28] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[92]),
        .Q(\temp_reg_n_0_[28] ),
        .R(c1_n_11));
  FDRE \temp_reg[29] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[93]),
        .Q(\temp_reg_n_0_[29] ),
        .R(c1_n_11));
  FDRE \temp_reg[2] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[114]),
        .Q(\temp_reg_n_0_[2] ),
        .R(c1_n_11));
  FDRE \temp_reg[30] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[94]),
        .Q(\temp_reg_n_0_[30] ),
        .R(c1_n_11));
  FDRE \temp_reg[31] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[95]),
        .Q(\temp_reg_n_0_[31] ),
        .R(c1_n_11));
  FDRE \temp_reg[32] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[80]),
        .Q(\temp_reg_n_0_[32] ),
        .R(c1_n_11));
  FDRE \temp_reg[33] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[81]),
        .Q(\temp_reg_n_0_[33] ),
        .R(c1_n_11));
  FDRE \temp_reg[34] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[82]),
        .Q(\temp_reg_n_0_[34] ),
        .R(c1_n_11));
  FDRE \temp_reg[35] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[83]),
        .Q(\temp_reg_n_0_[35] ),
        .R(c1_n_11));
  FDRE \temp_reg[36] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[84]),
        .Q(\temp_reg_n_0_[36] ),
        .R(c1_n_11));
  FDRE \temp_reg[37] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[85]),
        .Q(\temp_reg_n_0_[37] ),
        .R(c1_n_11));
  FDRE \temp_reg[38] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[86]),
        .Q(\temp_reg_n_0_[38] ),
        .R(c1_n_11));
  FDRE \temp_reg[39] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[87]),
        .Q(\temp_reg_n_0_[39] ),
        .R(c1_n_11));
  FDRE \temp_reg[3] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[115]),
        .Q(\temp_reg_n_0_[3] ),
        .R(c1_n_11));
  FDRE \temp_reg[40] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[72]),
        .Q(\temp_reg_n_0_[40] ),
        .R(c1_n_11));
  FDRE \temp_reg[41] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[73]),
        .Q(\temp_reg_n_0_[41] ),
        .R(c1_n_11));
  FDRE \temp_reg[42] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[74]),
        .Q(\temp_reg_n_0_[42] ),
        .R(c1_n_11));
  FDRE \temp_reg[43] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[75]),
        .Q(\temp_reg_n_0_[43] ),
        .R(c1_n_11));
  FDRE \temp_reg[44] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[76]),
        .Q(\temp_reg_n_0_[44] ),
        .R(c1_n_11));
  FDRE \temp_reg[45] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[77]),
        .Q(\temp_reg_n_0_[45] ),
        .R(c1_n_11));
  FDRE \temp_reg[46] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[78]),
        .Q(\temp_reg_n_0_[46] ),
        .R(c1_n_11));
  FDRE \temp_reg[47] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[79]),
        .Q(\temp_reg_n_0_[47] ),
        .R(c1_n_11));
  FDRE \temp_reg[4] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[116]),
        .Q(\temp_reg_n_0_[4] ),
        .R(c1_n_11));
  FDRE \temp_reg[5] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[117]),
        .Q(\temp_reg_n_0_[5] ),
        .R(c1_n_11));
  FDRE \temp_reg[6] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[118]),
        .Q(\temp_reg_n_0_[6] ),
        .R(c1_n_11));
  FDRE \temp_reg[7] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[119]),
        .Q(\temp_reg_n_0_[7] ),
        .R(c1_n_11));
  FDRE \temp_reg[8] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[104]),
        .Q(\temp_reg_n_0_[8] ),
        .R(c1_n_11));
  FDRE \temp_reg[9] 
       (.C(clock),
        .CE(temp0),
        .D(s_axis_tdata[105]),
        .Q(\temp_reg_n_0_[9] ),
        .R(c1_n_11));
  LUT6 #(
    .INIT(64'h0080008000808080)) 
    \valid[0]_i_1 
       (.I0(s_axis_tstrb[0]),
        .I1(s_axis_tstrb[1]),
        .I2(s_axis_tstrb[2]),
        .I3(f),
        .I4(\valid[1]_i_3_n_0 ),
        .I5(\valid[1]_i_2_n_0 ),
        .O(p_1_out[0]));
  LUT6 #(
    .INIT(64'h000000004000FFFF)) 
    \valid[1]_i_1 
       (.I0(\valid[1]_i_2_n_0 ),
        .I1(s_axis_tstrb[9]),
        .I2(s_axis_tstrb[10]),
        .I3(s_axis_tstrb[11]),
        .I4(f),
        .I5(\valid[1]_i_3_n_0 ),
        .O(p_1_out[1]));
  (* SOFT_HLUTNM = "soft_lutpair249" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \valid[1]_i_2 
       (.I0(s_axis_tstrb[8]),
        .I1(s_axis_tstrb[7]),
        .I2(s_axis_tstrb[6]),
        .O(\valid[1]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h7F)) 
    \valid[1]_i_3 
       (.I0(s_axis_tstrb[5]),
        .I1(s_axis_tstrb[4]),
        .I2(s_axis_tstrb[3]),
        .O(\valid[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair249" *) 
  LUT5 #(
    .INIT(32'h70000000)) 
    \valid[2]_i_1 
       (.I0(f),
        .I1(\valid[2]_i_2_n_0 ),
        .I2(s_axis_tstrb[8]),
        .I3(s_axis_tstrb[7]),
        .I4(s_axis_tstrb[6]),
        .O(\valid[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \valid[2]_i_2 
       (.I0(s_axis_tstrb[12]),
        .I1(s_axis_tstrb[13]),
        .I2(s_axis_tstrb[14]),
        .I3(s_axis_tstrb[9]),
        .I4(s_axis_tstrb[10]),
        .I5(s_axis_tstrb[11]),
        .O(\valid[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair250" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \valid[3]_i_1 
       (.I0(s_axis_tstrb[9]),
        .I1(s_axis_tstrb[10]),
        .I2(s_axis_tstrb[11]),
        .I3(f),
        .O(p_1_out[3]));
  LUT1 #(
    .INIT(2'h1)) 
    \valid[4]_i_1 
       (.I0(s_axis_tvalid),
        .O(valid));
  LUT4 #(
    .INIT(16'h0080)) 
    \valid[4]_i_2 
       (.I0(s_axis_tstrb[12]),
        .I1(s_axis_tstrb[13]),
        .I2(s_axis_tstrb[14]),
        .I3(f),
        .O(p_1_out[4]));
  FDRE \valid_reg[0] 
       (.C(clock),
        .CE(temp0),
        .D(p_1_out[0]),
        .Q(\valid_reg_n_0_[0] ),
        .R(valid));
  FDRE \valid_reg[1] 
       (.C(clock),
        .CE(temp0),
        .D(p_1_out[1]),
        .Q(\valid_reg_n_0_[1] ),
        .R(valid));
  FDRE \valid_reg[2] 
       (.C(clock),
        .CE(temp0),
        .D(\valid[2]_i_1_n_0 ),
        .Q(\valid_reg_n_0_[2] ),
        .R(valid));
  FDRE \valid_reg[3] 
       (.C(clock),
        .CE(temp0),
        .D(p_1_out[3]),
        .Q(\valid_reg_n_0_[3] ),
        .R(valid));
  FDRE \valid_reg[4] 
       (.C(clock),
        .CE(temp0),
        .D(p_1_out[4]),
        .Q(\valid_reg_n_0_[4] ),
        .R(valid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
