// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Thu Oct 25 18:25:05 2018
// Host        : dell-inspiron-3567 running 64-bit Ubuntu 16.04.5 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ finalproj_sobelfilteraxistream_0_0_stub.v
// Design      : finalproj_sobelfilteraxistream_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "sobelfilteraxistream,Vivado 2017.4" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clock, reset, s_axis_tlast, s_axis_tvalid, 
  s_axis_tready, s_axis_tdata, s_axis_tstrb, m_axis_tlast, m_axis_tvalid, m_axis_tready, 
  m_axis_tdata, m_axis_tkeep)
/* synthesis syn_black_box black_box_pad_pin="clock,reset,s_axis_tlast,s_axis_tvalid,s_axis_tready,s_axis_tdata[127:0],s_axis_tstrb[15:0],m_axis_tlast,m_axis_tvalid,m_axis_tready,m_axis_tdata[39:0],m_axis_tkeep[4:0]" */;
  input clock;
  input reset;
  input s_axis_tlast;
  input s_axis_tvalid;
  output s_axis_tready;
  input [127:0]s_axis_tdata;
  input [15:0]s_axis_tstrb;
  output m_axis_tlast;
  output m_axis_tvalid;
  input m_axis_tready;
  output [39:0]m_axis_tdata;
  output [4:0]m_axis_tkeep;
endmodule
