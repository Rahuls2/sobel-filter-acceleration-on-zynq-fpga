
module convolution(
  input [7:0] a1,b1,c1,a2,b2,c2,a3,b3,c3,
  input clk,
  output [7:0] ans,
  output [2:0] ful,
  input valid,
  output validout,
  input reset,
  input ready,
  input hold
  );

  reg [2:0] full = 3'b0;
  wire [9:0] ans1,ans2,finalans;
  reg [9:0] xreg1,xreg2,yreg1,yreg2;
  reg [7:0] out;
  reg [9:0] s11,s12,s21,s22;
  wire [9:0] outx11,outx12,outx21,outx22,outy11,outy12,outy21,outy22;
  wire Cx11,Cx12,Cx21,Cx22,Cy11,Cy12,Cy21,Cy22,ca1,ca2,anscarry,agtx,eqx,bgtx,agty,eqy,bgty;

  adder128b x11({1'b0,1'b0,a1},{1'b0,b1,1'b0},1'b0,outx11,Cx11);
  adder128b x12(outx11,{1'b0,1'b0,c1},1'b0,outx12,Cx12);
  adder128b x21({1'b0,1'b0,a3},{1'b0,b3,1'b0},1'b0,outx21,Cx21);
  adder128b x22(outx21,{1'b0,1'b0,c3},1'b0,outx22,Cx22);

  adder128b y11({1'b0,1'b0,a1},{1'b0,a2,1'b0},1'b0,outy11,Cy11);
  adder128b y12(outy11,{1'b0,1'b0,a3},1'b0,outy12,Cy12);
  adder128b y21({1'b0,1'b0,c1},{1'b0,c2,1'b0},1'b0,outy21,Cy21);
  adder128b y22(outy21,{1'b0,1'b0,c3},1'b0,outy22,Cy22);

  comp10b xc1(xreg1,xreg2,agtx,eqx,bgtx);
  comp10b yc1(yreg1,yreg2,agty,eqy,bgty);

  sub10b s1(s11,s12,1'b1,ans1,ca1);
  sub10b s2(s21,s22,1'b1,ans2,ca2);

  adder128b final(ans1,ans2,1'b0,finalans,anscarry);

  always @ (posedge clk or negedge reset)
  begin
    if (!reset)
    begin
      xreg1 <= {9{1'b0}};
      xreg2 <= {9{1'b0}};
      s11 <= {9{1'b0}};
      full <= 3'b000;
      s12 <= {9{1'b0}};
      out <= {7{1'b0}};
      s21 <= {9{1'b0}};
      s22 <= {9{1'b0}};
    end

    else if (!hold)
    begin


    //stage1
    if ((valid == 1'b1) && ( full[0] == 1'b0 | full[1]==1'b0 | full[2]==1'b0 | ready == 1'b1))
      begin
        xreg1 <= outx12;
        xreg2 <= outx22;
        yreg1 <= outy12;
        yreg2 <= outy22;
        full[0] <= 1'b1;
      end
    else if (full[1] == 1'b0 | full[2] == 1'b0 | ready == 1'b1)
      full[0] <= 1'b0;

    //stage2
    if (full[0] == 1'b1 && (full[2] == 1'b0 | ready == 1'b1 | full[1] == 1'b0))
    begin
      if( agtx == 1'b1 | eqx == 1'b1 )
      begin
        s11 <= xreg1;
        s12 <= xreg2;
      end
      else if(bgtx == 1'b1)
      begin
        s11 <= xreg2;
        s12 <= xreg1;
      end

      if( agty == 1'b1 | eqy == 1'b1 )
      begin
        s21 <= yreg1;
        s22 <= yreg2;
      end
      else if(bgty == 1'b1)
      begin
        s21 <= yreg2;
        s22 <= yreg1;
      end
      full[1] <= 1'b1;
    end
    else if (full[2] == 1'b0 | ready == 1'b1)
    begin
      full[1] <= 1'b0;
    end


    //stage3
    if (full[1] == 1'b1 && (ready == 1'b1 | full[2] == 1'b0))
    begin
      if( (anscarry | finalans[9] | finalans[8]) == 1'b1 )
        out <= 8'b11111111;
      else
        out <= finalans[7:0];

      full[2] = 1'b1;
    end
    else if (ready == 1'b1)
      full[2] = 1'b0;
    end
end
assign ful = full;
assign ans = out;
assign validout = full[2];
endmodule
