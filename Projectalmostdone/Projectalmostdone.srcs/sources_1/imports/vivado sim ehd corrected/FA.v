`include "HA.v"
module FA(A,B,C,Sum,Carry);
    input A,B,C;
    output Sum,Carry;
    HA h1(A,B,S,Ca);
    HA h2(S,C,Sum,Ca2);
    assign Carry = Ca | Ca2;
endmodule
