-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
-- Date        : Thu Oct 25 18:25:07 2018
-- Host        : dell-inspiron-3567 running 64-bit Ubuntu 16.04.5 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_sobelfilteraxistream_0_0/finalproj_sobelfilteraxistream_0_0_stub.vhdl
-- Design      : finalproj_sobelfilteraxistream_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity finalproj_sobelfilteraxistream_0_0 is
  Port ( 
    clock : in STD_LOGIC;
    reset : in STD_LOGIC;
    s_axis_tlast : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    s_axis_tstrb : in STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_tlast : out STD_LOGIC;
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 39 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );

end finalproj_sobelfilteraxistream_0_0;

architecture stub of finalproj_sobelfilteraxistream_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clock,reset,s_axis_tlast,s_axis_tvalid,s_axis_tready,s_axis_tdata[127:0],s_axis_tstrb[15:0],m_axis_tlast,m_axis_tvalid,m_axis_tready,m_axis_tdata[39:0],m_axis_tkeep[4:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "sobelfilteraxistream,Vivado 2017.4";
begin
end;
