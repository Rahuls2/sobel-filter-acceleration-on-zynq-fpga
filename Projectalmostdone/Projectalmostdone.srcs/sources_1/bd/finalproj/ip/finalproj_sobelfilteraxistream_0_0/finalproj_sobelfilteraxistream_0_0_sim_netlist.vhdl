-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
-- Date        : Thu Oct 25 18:25:07 2018
-- Host        : dell-inspiron-3567 running 64-bit Ubuntu 16.04.5 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/rahul/Vivadoproject/Projectalmostdone/Projectalmostdone.srcs/sources_1/bd/finalproj/ip/finalproj_sobelfilteraxistream_0_0/finalproj_sobelfilteraxistream_0_0_sim_netlist.vhdl
-- Design      : finalproj_sobelfilteraxistream_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity finalproj_sobelfilteraxistream_0_0_convolution is
  port (
    D : out STD_LOGIC_VECTOR ( 9 downto 0 );
    \xreg1_reg[8]_0\ : out STD_LOGIC;
    \temp_reg[0]\ : out STD_LOGIC;
    temp0 : out STD_LOGIC;
    hold0 : out STD_LOGIC;
    \m_axis_tkeep[0]\ : out STD_LOGIC;
    lastpck_reg : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_tready : out STD_LOGIC;
    f_reg : out STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 63 downto 0 );
    reset : in STD_LOGIC;
    \s_axis_tstrb[9]\ : in STD_LOGIC;
    s_axis_tstrb : in STD_LOGIC_VECTOR ( 5 downto 0 );
    m_axis_tready : in STD_LOGIC;
    s_axis_tlast : in STD_LOGIC;
    m_axis_tlast : in STD_LOGIC;
    f : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    \s_axis_tstrb[6]\ : in STD_LOGIC;
    clock : in STD_LOGIC;
    hold : in STD_LOGIC;
    \valid_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_0 : in STD_LOGIC;
    hold_reg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of finalproj_sobelfilteraxistream_0_0_convolution : entity is "convolution";
end finalproj_sobelfilteraxistream_0_0_convolution;

architecture STRUCTURE of finalproj_sobelfilteraxistream_0_0_convolution is
  signal \^d\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal ans1 : STD_LOGIC_VECTOR ( 7 downto 1 );
  signal ans2 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal \c3/outx11\ : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal \c3/x11/C_3\ : STD_LOGIC;
  signal \c3/x11/C_5\ : STD_LOGIC;
  signal \c3/x11/C_6\ : STD_LOGIC;
  signal \c3/x12/C_1\ : STD_LOGIC;
  signal \c3/x12/C_2\ : STD_LOGIC;
  signal \c3/x12/C_3\ : STD_LOGIC;
  signal \c3/x12/C_5\ : STD_LOGIC;
  signal \c3/x12/C_6\ : STD_LOGIC;
  signal \c3/x12/f6/S\ : STD_LOGIC;
  signal \c3/x12/f8/S\ : STD_LOGIC;
  signal \final/C_1\ : STD_LOGIC;
  signal \final/C_2\ : STD_LOGIC;
  signal \final/C_4\ : STD_LOGIC;
  signal \final/C_6\ : STD_LOGIC;
  signal \final/f1/Ca\ : STD_LOGIC;
  signal \final/f2/S\ : STD_LOGIC;
  signal \final/f4/S\ : STD_LOGIC;
  signal \final/f5/S\ : STD_LOGIC;
  signal \final/f6/S\ : STD_LOGIC;
  signal \full[0]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \full[0]_i_1_n_0\ : STD_LOGIC;
  signal \full[1]_i_1_n_0\ : STD_LOGIC;
  signal \full[2]_i_1_n_0\ : STD_LOGIC;
  signal \^m_axis_tkeep[0]\ : STD_LOGIC;
  signal \out\ : STD_LOGIC;
  signal \out[0]_i_1_n_0\ : STD_LOGIC;
  signal \out[0]_i_2_n_0\ : STD_LOGIC;
  signal \out[1]_i_1_n_0\ : STD_LOGIC;
  signal \out[1]_i_2_n_0\ : STD_LOGIC;
  signal \out[2]_i_1_n_0\ : STD_LOGIC;
  signal \out[2]_i_2_n_0\ : STD_LOGIC;
  signal \out[3]_i_1_n_0\ : STD_LOGIC;
  signal \out[3]_i_2_n_0\ : STD_LOGIC;
  signal \out[4]_i_1_n_0\ : STD_LOGIC;
  signal \out[4]_i_2_n_0\ : STD_LOGIC;
  signal \out[5]_i_1_n_0\ : STD_LOGIC;
  signal \out[5]_i_2_n_0\ : STD_LOGIC;
  signal \out[6]_i_1_n_0\ : STD_LOGIC;
  signal \out[6]_i_2_n_0\ : STD_LOGIC;
  signal \out[7]_i_26_n_0\ : STD_LOGIC;
  signal \out[7]_i_27_n_0\ : STD_LOGIC;
  signal \out[7]_i_28_n_0\ : STD_LOGIC;
  signal \out[7]_i_29_n_0\ : STD_LOGIC;
  signal \out[7]_i_2_n_0\ : STD_LOGIC;
  signal \out[7]_i_3_n_0\ : STD_LOGIC;
  signal \out[7]_i_8_n_0\ : STD_LOGIC;
  signal outx11 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outx12 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal outy11 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outy12 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal outy21 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outy22 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal \s1/C2\ : STD_LOGIC;
  signal \s1/F6/S\ : STD_LOGIC;
  signal \s1/s1/C_2\ : STD_LOGIC;
  signal \s1/s2/C_0\ : STD_LOGIC;
  signal \s1/s2/C_2\ : STD_LOGIC;
  signal \s1/s2/f2/S\ : STD_LOGIC;
  signal \s11[0]_i_1_n_0\ : STD_LOGIC;
  signal \s11[1]_i_1_n_0\ : STD_LOGIC;
  signal \s11[2]_i_1_n_0\ : STD_LOGIC;
  signal \s11[3]_i_1_n_0\ : STD_LOGIC;
  signal \s11[4]_i_1_n_0\ : STD_LOGIC;
  signal \s11[5]_i_1_n_0\ : STD_LOGIC;
  signal \s11[6]_i_1_n_0\ : STD_LOGIC;
  signal \s11[7]_i_1_n_0\ : STD_LOGIC;
  signal \s11[8]_i_1_n_0\ : STD_LOGIC;
  signal \s11[9]_i_1_n_0\ : STD_LOGIC;
  signal \s11_reg_n_0_[0]\ : STD_LOGIC;
  signal \s11_reg_n_0_[1]\ : STD_LOGIC;
  signal \s11_reg_n_0_[2]\ : STD_LOGIC;
  signal \s11_reg_n_0_[3]\ : STD_LOGIC;
  signal \s11_reg_n_0_[4]\ : STD_LOGIC;
  signal \s11_reg_n_0_[5]\ : STD_LOGIC;
  signal \s11_reg_n_0_[6]\ : STD_LOGIC;
  signal \s11_reg_n_0_[7]\ : STD_LOGIC;
  signal \s11_reg_n_0_[8]\ : STD_LOGIC;
  signal \s11_reg_n_0_[9]\ : STD_LOGIC;
  signal s12 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \s12[0]_i_1_n_0\ : STD_LOGIC;
  signal \s12[1]_i_1_n_0\ : STD_LOGIC;
  signal \s12[2]_i_1_n_0\ : STD_LOGIC;
  signal \s12[3]_i_1_n_0\ : STD_LOGIC;
  signal \s12[4]_i_1_n_0\ : STD_LOGIC;
  signal \s12[5]_i_1_n_0\ : STD_LOGIC;
  signal \s12[6]_i_1_n_0\ : STD_LOGIC;
  signal \s12[7]_i_1_n_0\ : STD_LOGIC;
  signal \s12[8]_i_1_n_0\ : STD_LOGIC;
  signal \s12[8]_i_2_n_0\ : STD_LOGIC;
  signal \s12[8]_i_3_n_0\ : STD_LOGIC;
  signal \s12[8]_i_4_n_0\ : STD_LOGIC;
  signal \s12[8]_i_5_n_0\ : STD_LOGIC;
  signal \s12[8]_i_6_n_0\ : STD_LOGIC;
  signal \s12[8]_i_7_n_0\ : STD_LOGIC;
  signal \s12[8]_i_8_n_0\ : STD_LOGIC;
  signal \s12[8]_i_9_n_0\ : STD_LOGIC;
  signal \s12[9]_i_2_n_0\ : STD_LOGIC;
  signal \s2/C2\ : STD_LOGIC;
  signal \s2/F6/S\ : STD_LOGIC;
  signal \s2/f5/S\ : STD_LOGIC;
  signal \s2/s1/C_2\ : STD_LOGIC;
  signal \s2/s1/f1/S\ : STD_LOGIC;
  signal \s2/s2/C_0\ : STD_LOGIC;
  signal \s2/s2/C_2\ : STD_LOGIC;
  signal \s2/s2/f2/S\ : STD_LOGIC;
  signal s21 : STD_LOGIC;
  signal \s21[0]_i_1_n_0\ : STD_LOGIC;
  signal \s21[1]_i_1_n_0\ : STD_LOGIC;
  signal \s21[2]_i_1_n_0\ : STD_LOGIC;
  signal \s21[3]_i_1_n_0\ : STD_LOGIC;
  signal \s21[4]_i_1_n_0\ : STD_LOGIC;
  signal \s21[5]_i_1_n_0\ : STD_LOGIC;
  signal \s21[6]_i_1_n_0\ : STD_LOGIC;
  signal \s21[7]_i_1_n_0\ : STD_LOGIC;
  signal \s21[8]_i_1_n_0\ : STD_LOGIC;
  signal \s21[9]_i_1_n_0\ : STD_LOGIC;
  signal \s21_reg_n_0_[0]\ : STD_LOGIC;
  signal \s21_reg_n_0_[1]\ : STD_LOGIC;
  signal \s21_reg_n_0_[2]\ : STD_LOGIC;
  signal \s21_reg_n_0_[3]\ : STD_LOGIC;
  signal \s21_reg_n_0_[4]\ : STD_LOGIC;
  signal \s21_reg_n_0_[5]\ : STD_LOGIC;
  signal \s21_reg_n_0_[6]\ : STD_LOGIC;
  signal \s21_reg_n_0_[7]\ : STD_LOGIC;
  signal \s21_reg_n_0_[8]\ : STD_LOGIC;
  signal \s21_reg_n_0_[9]\ : STD_LOGIC;
  signal s22 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \s22[0]_i_1_n_0\ : STD_LOGIC;
  signal \s22[1]_i_1_n_0\ : STD_LOGIC;
  signal \s22[2]_i_1_n_0\ : STD_LOGIC;
  signal \s22[3]_i_1_n_0\ : STD_LOGIC;
  signal \s22[4]_i_1_n_0\ : STD_LOGIC;
  signal \s22[5]_i_1_n_0\ : STD_LOGIC;
  signal \s22[6]_i_1_n_0\ : STD_LOGIC;
  signal \s22[7]_i_1_n_0\ : STD_LOGIC;
  signal \s22[8]_i_1_n_0\ : STD_LOGIC;
  signal \s22[8]_i_2_n_0\ : STD_LOGIC;
  signal \s22[8]_i_3_n_0\ : STD_LOGIC;
  signal \s22[8]_i_4_n_0\ : STD_LOGIC;
  signal \s22[8]_i_5_n_0\ : STD_LOGIC;
  signal \s22[8]_i_6_n_0\ : STD_LOGIC;
  signal \s22[8]_i_7_n_0\ : STD_LOGIC;
  signal \s22[8]_i_8_n_0\ : STD_LOGIC;
  signal \s22[8]_i_9_n_0\ : STD_LOGIC;
  signal \s22[9]_i_1_n_0\ : STD_LOGIC;
  signal \^temp0\ : STD_LOGIC;
  signal \x11/C_3\ : STD_LOGIC;
  signal \x11/C_5\ : STD_LOGIC;
  signal \x11/C_6\ : STD_LOGIC;
  signal \x12/C_1\ : STD_LOGIC;
  signal \x12/C_2\ : STD_LOGIC;
  signal \x12/C_3\ : STD_LOGIC;
  signal \x12/C_5\ : STD_LOGIC;
  signal \x12/C_6\ : STD_LOGIC;
  signal \x12/f1/S\ : STD_LOGIC;
  signal \x12/f6/S\ : STD_LOGIC;
  signal \x12/f8/S\ : STD_LOGIC;
  signal xreg1 : STD_LOGIC;
  signal \^xreg1_reg[8]_0\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[0]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[1]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[2]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[3]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[4]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[5]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[6]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[7]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[8]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[9]\ : STD_LOGIC;
  signal xreg2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \y11/C_3\ : STD_LOGIC;
  signal \y11/C_5\ : STD_LOGIC;
  signal \y11/C_6\ : STD_LOGIC;
  signal \y12/C_1\ : STD_LOGIC;
  signal \y12/C_2\ : STD_LOGIC;
  signal \y12/C_3\ : STD_LOGIC;
  signal \y12/C_5\ : STD_LOGIC;
  signal \y12/C_6\ : STD_LOGIC;
  signal \y12/f1/S\ : STD_LOGIC;
  signal \y12/f6/S\ : STD_LOGIC;
  signal \y12/f8/S\ : STD_LOGIC;
  signal \y21/C_3\ : STD_LOGIC;
  signal \y21/C_5\ : STD_LOGIC;
  signal \y21/C_6\ : STD_LOGIC;
  signal \y22/C_1\ : STD_LOGIC;
  signal \y22/C_2\ : STD_LOGIC;
  signal \y22/C_3\ : STD_LOGIC;
  signal \y22/C_5\ : STD_LOGIC;
  signal \y22/C_6\ : STD_LOGIC;
  signal \y22/f1/S\ : STD_LOGIC;
  signal \y22/f6/S\ : STD_LOGIC;
  signal \y22/f8/S\ : STD_LOGIC;
  signal yreg1 : STD_LOGIC;
  signal \yreg1_reg_n_0_[0]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[1]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[2]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[3]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[4]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[5]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[6]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[7]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[8]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[9]\ : STD_LOGIC;
  signal yreg2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \full[1]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of hold_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of hold_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \inputvals[47]_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of lastpck_i_1 : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \out[0]_i_2\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \out[1]_i_2\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \out[1]_i_3\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \out[2]_i_6\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \out[4]_i_8\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \out[4]_i_9\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \out[6]_i_2\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \out[7]_i_10\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \out[7]_i_11\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \out[7]_i_13\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \out[7]_i_14\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \out[7]_i_15\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \out[7]_i_16\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \out[7]_i_21\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \out[7]_i_22\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \out[7]_i_24\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \out[7]_i_25\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \out[7]_i_27\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \out[7]_i_29\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \out[7]_i_7\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \s11[0]_i_1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \s11[1]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \s11[2]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \s11[3]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \s11[4]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \s11[5]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \s11[6]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \s11[7]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \s11[8]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \s12[0]_i_1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \s12[1]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \s12[2]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \s12[3]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \s12[4]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \s12[5]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \s12[6]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \s12[7]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \s12[8]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \s12[8]_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \s12[8]_i_4\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \s12[8]_i_6\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \s12[8]_i_7\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \s12[8]_i_8\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \s12[9]_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \s21[0]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \s21[1]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \s21[2]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \s21[3]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \s21[4]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \s21[5]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \s21[6]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \s21[7]_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \s21[8]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \s22[0]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \s22[1]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \s22[2]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \s22[3]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \s22[4]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \s22[5]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \s22[6]_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \s22[7]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \s22[8]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \s22[8]_i_2\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \s22[8]_i_4\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \s22[8]_i_6\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \s22[8]_i_7\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \s22[8]_i_8\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \s22[9]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of s_axis_tready_INST_0 : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \xreg1[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \xreg1[2]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \xreg1[4]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \xreg1[6]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \xreg1[6]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \xreg1[6]_i_4\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \xreg1[6]_i_5\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \xreg1[9]_i_4\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \xreg1[9]_i_5\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \xreg2[0]_i_1__3\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \xreg2[1]_i_1__3\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \xreg2[2]_i_2__3\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \xreg2[3]_i_1__3\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \xreg2[4]_i_1__3\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \xreg2[6]_i_1__3\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \xreg2[6]_i_2__3\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \xreg2[6]_i_4__3\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \xreg2[6]_i_5__3\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \xreg2[9]_i_2__3\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \yreg1[0]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \yreg1[1]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \yreg1[2]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \yreg1[4]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \yreg1[6]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \yreg1[6]_i_2\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \yreg1[6]_i_4\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \yreg1[6]_i_5\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \yreg1[9]_i_4\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \yreg1[9]_i_5\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \yreg2[1]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \yreg2[2]_i_2\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \yreg2[3]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \yreg2[4]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \yreg2[6]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \yreg2[6]_i_2\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \yreg2[6]_i_4\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \yreg2[6]_i_5\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \yreg2[9]_i_2\ : label is "soft_lutpair14";
begin
  D(9 downto 0) <= \^d\(9 downto 0);
  \m_axis_tkeep[0]\ <= \^m_axis_tkeep[0]\;
  temp0 <= \^temp0\;
  \xreg1_reg[8]_0\ <= \^xreg1_reg[8]_0\;
f_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF7FFFFF00000000"
    )
        port map (
      I0 => s_axis_tstrb(2),
      I1 => s_axis_tstrb(1),
      I2 => s_axis_tstrb(0),
      I3 => \s_axis_tstrb[6]\,
      I4 => \^temp0\,
      I5 => f,
      O => f_reg
    );
\full[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEFEEEEE44444444"
    )
        port map (
      I0 => hold,
      I1 => \valid_reg[0]\(0),
      I2 => \full[0]_0\(1),
      I3 => m_axis_tready,
      I4 => \^m_axis_tkeep[0]\,
      I5 => \full[0]_0\(0),
      O => \full[0]_i_1_n_0\
    );
\full[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEFE4444"
    )
        port map (
      I0 => hold,
      I1 => \full[0]_0\(0),
      I2 => \^m_axis_tkeep[0]\,
      I3 => m_axis_tready,
      I4 => \full[0]_0\(1),
      O => \full[1]_i_1_n_0\
    );
\full[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFF4040"
    )
        port map (
      I0 => hold,
      I1 => reset,
      I2 => \full[0]_0\(1),
      I3 => m_axis_tready,
      I4 => \^m_axis_tkeep[0]\,
      O => \full[2]_i_1_n_0\
    );
\full_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => reset_0,
      D => \full[0]_i_1_n_0\,
      Q => \full[0]_0\(0)
    );
\full_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => reset_0,
      D => \full[1]_i_1_n_0\,
      Q => \full[0]_0\(1)
    );
\full_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => reset_0,
      D => \full[2]_i_1_n_0\,
      Q => \^m_axis_tkeep[0]\
    );
hold_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F7FF0000"
    )
        port map (
      I0 => \^m_axis_tkeep[0]\,
      I1 => \full[0]_0\(1),
      I2 => m_axis_tready,
      I3 => \full[0]_0\(0),
      I4 => s_axis_tvalid,
      O => \^temp0\
    );
hold_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => \full[0]_0\(0),
      I1 => m_axis_tready,
      I2 => \full[0]_0\(1),
      I3 => \^m_axis_tkeep[0]\,
      O => hold0
    );
\inputvals[47]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^temp0\,
      I1 => f,
      O => E(0)
    );
lastpck_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => s_axis_tlast,
      I1 => \^temp0\,
      I2 => m_axis_tlast,
      O => lastpck_reg
    );
\out[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[0]_i_2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8_n_0\,
      O => \out[0]_i_1_n_0\
    );
\out[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s11_reg_n_0_[0]\,
      O => \out[0]_i_2_n_0\
    );
\out[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[1]_i_2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8_n_0\,
      O => \out[1]_i_1_n_0\
    );
\out[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA9696AA"
    )
        port map (
      I0 => \final/f2/S\,
      I1 => \s11_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s21_reg_n_0_[0]\,
      I4 => s22(0),
      O => \out[1]_i_2_n_0\
    );
\out[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D22D2DD2"
    )
        port map (
      I0 => s12(0),
      I1 => \s11_reg_n_0_[0]\,
      I2 => \s11_reg_n_0_[1]\,
      I3 => s12(1),
      I4 => ans2(1),
      O => \final/f2/S\
    );
\out[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[2]_i_2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8_n_0\,
      O => \out[2]_i_1_n_0\
    );
\out[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => ans1(2),
      I1 => ans2(2),
      I2 => \final/C_1\,
      O => \out[2]_i_2_n_0\
    );
\out[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966996969666966"
    )
        port map (
      I0 => s12(2),
      I1 => \s11_reg_n_0_[2]\,
      I2 => \s11_reg_n_0_[1]\,
      I3 => s12(1),
      I4 => \s11_reg_n_0_[0]\,
      I5 => s12(0),
      O => ans1(2)
    );
\out[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966996969666966"
    )
        port map (
      I0 => s22(2),
      I1 => \s21_reg_n_0_[2]\,
      I2 => \s21_reg_n_0_[1]\,
      I3 => s22(1),
      I4 => \s21_reg_n_0_[0]\,
      I5 => s22(0),
      O => ans2(2)
    );
\out[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0CC0D44D4DD40CC0"
    )
        port map (
      I0 => \s2/s1/f1/S\,
      I1 => ans2(1),
      I2 => s12(1),
      I3 => \s11_reg_n_0_[1]\,
      I4 => \s11_reg_n_0_[0]\,
      I5 => s12(0),
      O => \final/C_1\
    );
\out[2]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[0]\,
      I1 => s22(0),
      O => \s2/s1/f1/S\
    );
\out[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[3]_i_2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8_n_0\,
      O => \out[3]_i_1_n_0\
    );
\out[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \final/f4/S\,
      I1 => \final/C_2\,
      O => \out[3]_i_2_n_0\
    );
\out[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => \s11_reg_n_0_[3]\,
      I2 => s12(3),
      I3 => \s2/s1/C_2\,
      I4 => \s21_reg_n_0_[3]\,
      I5 => s22(3),
      O => \final/f4/S\
    );
\out[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[4]_i_2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8_n_0\,
      O => \out[4]_i_1_n_0\
    );
\out[4]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF0DFFFF0000DF0D"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s22(1),
      I3 => \s21_reg_n_0_[1]\,
      I4 => s22(2),
      I5 => \s21_reg_n_0_[2]\,
      O => \s2/s1/C_2\
    );
\out[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"711717718EE8E88E"
    )
        port map (
      I0 => \final/C_2\,
      I1 => ans2(3),
      I2 => s12(3),
      I3 => \s11_reg_n_0_[3]\,
      I4 => \s1/s1/C_2\,
      I5 => \final/f5/S\,
      O => \out[4]_i_2_n_0\
    );
\out[4]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/f1/Ca\,
      I1 => ans2(1),
      I2 => ans1(1),
      I3 => ans2(2),
      I4 => ans1(2),
      O => \final/C_2\
    );
\out[4]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(3),
      I1 => \s21_reg_n_0_[3]\,
      I2 => \s2/s1/C_2\,
      O => ans2(3)
    );
\out[4]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF0DFFFF0000DF0D"
    )
        port map (
      I0 => s12(0),
      I1 => \s11_reg_n_0_[0]\,
      I2 => s12(1),
      I3 => \s11_reg_n_0_[1]\,
      I4 => s12(2),
      I5 => \s11_reg_n_0_[2]\,
      O => \s1/s1/C_2\
    );
\out[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B24D4DB24DB2B24D"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => s12(3),
      I2 => \s11_reg_n_0_[3]\,
      I3 => \s11_reg_n_0_[4]\,
      I4 => s12(4),
      I5 => ans2(4),
      O => \final/f5/S\
    );
\out[4]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0660"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s11_reg_n_0_[0]\,
      O => \final/f1/Ca\
    );
\out[4]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => s22(1),
      I1 => \s21_reg_n_0_[1]\,
      I2 => \s21_reg_n_0_[0]\,
      I3 => s22(0),
      O => ans2(1)
    );
\out[4]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => s12(1),
      I1 => \s11_reg_n_0_[1]\,
      I2 => \s11_reg_n_0_[0]\,
      I3 => s12(0),
      O => ans1(1)
    );
\out[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[5]_i_2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8_n_0\,
      O => \out[5]_i_1_n_0\
    );
\out[5]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \final/f6/S\,
      I1 => \final/C_4\,
      O => \out[5]_i_2_n_0\
    );
\out[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \s1/s2/C_0\,
      I1 => \s11_reg_n_0_[5]\,
      I2 => s12(5),
      I3 => \s2/s2/C_0\,
      I4 => \s21_reg_n_0_[5]\,
      I5 => s22(5),
      O => \final/f6/S\
    );
\out[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[6]_i_2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8_n_0\,
      O => \out[6]_i_1_n_0\
    );
\out[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => ans2(6),
      I1 => ans1(6),
      I2 => ans1(5),
      I3 => ans2(5),
      I4 => \final/C_4\,
      O => \out[6]_i_2_n_0\
    );
\out[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00D0"
    )
        port map (
      I0 => \^m_axis_tkeep[0]\,
      I1 => m_axis_tready,
      I2 => \full[0]_0\(1),
      I3 => hold,
      O => \out\
    );
\out[7]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s2/s2/C_0\,
      I1 => s22(5),
      I2 => \s21_reg_n_0_[5]\,
      I3 => s22(6),
      I4 => \s21_reg_n_0_[6]\,
      O => \s2/s2/C_2\
    );
\out[7]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s1/s2/C_0\,
      I1 => s12(5),
      I2 => \s11_reg_n_0_[5]\,
      I3 => s12(6),
      I4 => \s11_reg_n_0_[6]\,
      O => \s1/s2/C_2\
    );
\out[7]_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/C_2\,
      I1 => ans2(3),
      I2 => ans1(3),
      I3 => ans2(4),
      I4 => ans1(4),
      O => \final/C_4\
    );
\out[7]_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(5),
      I1 => \s21_reg_n_0_[5]\,
      I2 => \s2/s2/C_0\,
      O => ans2(5)
    );
\out[7]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(5),
      I1 => \s11_reg_n_0_[5]\,
      I2 => \s1/s2/C_0\,
      O => ans1(5)
    );
\out[7]_i_15\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s22(6),
      I1 => \s21_reg_n_0_[6]\,
      I2 => \s21_reg_n_0_[5]\,
      I3 => s22(5),
      I4 => \s2/s2/C_0\,
      O => ans2(6)
    );
\out[7]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s12(6),
      I1 => \s11_reg_n_0_[6]\,
      I2 => \s11_reg_n_0_[5]\,
      I3 => s12(5),
      I4 => \s1/s2/C_0\,
      O => ans1(6)
    );
\out[7]_i_17\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s11_reg_n_0_[9]\,
      I1 => s12(9),
      O => \s1/F6/S\
    );
\out[7]_i_18\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[8]\,
      I1 => s22(8),
      O => \s2/f5/S\
    );
\out[7]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EF0EFFFF0000EF0E"
    )
        port map (
      I0 => \out[7]_i_26_n_0\,
      I1 => \out[7]_i_27_n_0\,
      I2 => s22(6),
      I3 => \s21_reg_n_0_[6]\,
      I4 => s22(7),
      I5 => \s21_reg_n_0_[7]\,
      O => \s2/C2\
    );
\out[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => ans2(9),
      I1 => ans1(7),
      I2 => ans2(7),
      I3 => \final/C_6\,
      I4 => \out[7]_i_8_n_0\,
      O => \out[7]_i_2_n_0\
    );
\out[7]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EF0EFFFF0000EF0E"
    )
        port map (
      I0 => \out[7]_i_28_n_0\,
      I1 => \out[7]_i_29_n_0\,
      I2 => s12(6),
      I3 => \s11_reg_n_0_[6]\,
      I4 => s12(7),
      I5 => \s11_reg_n_0_[7]\,
      O => \s1/C2\
    );
\out[7]_i_21\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s2/s1/C_2\,
      I1 => s22(3),
      I2 => \s21_reg_n_0_[3]\,
      I3 => s22(4),
      I4 => \s21_reg_n_0_[4]\,
      O => \s2/s2/C_0\
    );
\out[7]_i_22\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => s12(3),
      I2 => \s11_reg_n_0_[3]\,
      I3 => s12(4),
      I4 => \s11_reg_n_0_[4]\,
      O => \s1/s2/C_0\
    );
\out[7]_i_23\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(3),
      I1 => \s11_reg_n_0_[3]\,
      I2 => \s1/s1/C_2\,
      O => ans1(3)
    );
\out[7]_i_24\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s22(4),
      I1 => \s21_reg_n_0_[4]\,
      I2 => \s21_reg_n_0_[3]\,
      I3 => s22(3),
      I4 => \s2/s1/C_2\,
      O => ans2(4)
    );
\out[7]_i_25\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s12(4),
      I1 => \s11_reg_n_0_[4]\,
      I2 => \s11_reg_n_0_[3]\,
      I3 => s12(3),
      I4 => \s1/s1/C_2\,
      O => ans1(4)
    );
\out[7]_i_26\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A088A8A08088A08"
    )
        port map (
      I0 => \s2/s2/f2/S\,
      I1 => \s21_reg_n_0_[4]\,
      I2 => s22(4),
      I3 => \s21_reg_n_0_[3]\,
      I4 => s22(3),
      I5 => \s2/s1/C_2\,
      O => \out[7]_i_26_n_0\
    );
\out[7]_i_27\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \s21_reg_n_0_[5]\,
      I1 => s22(5),
      O => \out[7]_i_27_n_0\
    );
\out[7]_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A088A8A08088A08"
    )
        port map (
      I0 => \s1/s2/f2/S\,
      I1 => \s11_reg_n_0_[4]\,
      I2 => s12(4),
      I3 => \s11_reg_n_0_[3]\,
      I4 => s12(3),
      I5 => \s1/s1/C_2\,
      O => \out[7]_i_28_n_0\
    );
\out[7]_i_29\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \s11_reg_n_0_[5]\,
      I1 => s12(5),
      O => \out[7]_i_29_n_0\
    );
\out[7]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset,
      O => \out[7]_i_3_n_0\
    );
\out[7]_i_30\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[5]\,
      I1 => s22(5),
      O => \s2/s2/f2/S\
    );
\out[7]_i_31\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s11_reg_n_0_[5]\,
      I1 => s12(5),
      O => \s1/s2/f2/S\
    );
\out[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"65A66565A6A665A6"
    )
        port map (
      I0 => \s2/F6/S\,
      I1 => \s21_reg_n_0_[8]\,
      I2 => s22(8),
      I3 => \s21_reg_n_0_[7]\,
      I4 => s22(7),
      I5 => \s2/s2/C_2\,
      O => ans2(9)
    );
\out[7]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(7),
      I1 => \s11_reg_n_0_[7]\,
      I2 => \s1/s2/C_2\,
      O => ans1(7)
    );
\out[7]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(7),
      I1 => \s21_reg_n_0_[7]\,
      I2 => \s2/s2/C_2\,
      O => ans2(7)
    );
\out[7]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/C_4\,
      I1 => ans2(5),
      I2 => ans1(5),
      I3 => ans2(6),
      I4 => ans1(6),
      O => \final/C_6\
    );
\out[7]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DFFFF7DFF7DBEFF"
    )
        port map (
      I0 => \s1/F6/S\,
      I1 => \s2/f5/S\,
      I2 => \s2/C2\,
      I3 => s12(8),
      I4 => \s11_reg_n_0_[8]\,
      I5 => \s1/C2\,
      O => \out[7]_i_8_n_0\
    );
\out[7]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[9]\,
      I1 => s22(9),
      O => \s2/F6/S\
    );
\out_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \out[7]_i_3_n_0\,
      D => \out[0]_i_1_n_0\,
      Q => m_axis_tdata(0)
    );
\out_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \out[7]_i_3_n_0\,
      D => \out[1]_i_1_n_0\,
      Q => m_axis_tdata(1)
    );
\out_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \out[7]_i_3_n_0\,
      D => \out[2]_i_1_n_0\,
      Q => m_axis_tdata(2)
    );
\out_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \out[7]_i_3_n_0\,
      D => \out[3]_i_1_n_0\,
      Q => m_axis_tdata(3)
    );
\out_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \out[7]_i_3_n_0\,
      D => \out[4]_i_1_n_0\,
      Q => m_axis_tdata(4)
    );
\out_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \out[7]_i_3_n_0\,
      D => \out[5]_i_1_n_0\,
      Q => m_axis_tdata(5)
    );
\out_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \out[7]_i_3_n_0\,
      D => \out[6]_i_1_n_0\,
      Q => m_axis_tdata(6)
    );
\out_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \out[7]_i_3_n_0\,
      D => \out[7]_i_2_n_0\,
      Q => m_axis_tdata(7)
    );
\s11[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(0),
      I1 => \s12[8]_i_2_n_0\,
      I2 => \xreg1_reg_n_0_[0]\,
      O => \s11[0]_i_1_n_0\
    );
\s11[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(1),
      I1 => \s12[8]_i_2_n_0\,
      I2 => \xreg1_reg_n_0_[1]\,
      O => \s11[1]_i_1_n_0\
    );
\s11[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(2),
      I1 => \s12[8]_i_2_n_0\,
      I2 => \xreg1_reg_n_0_[2]\,
      O => \s11[2]_i_1_n_0\
    );
\s11[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(3),
      I1 => \s12[8]_i_2_n_0\,
      I2 => \xreg1_reg_n_0_[3]\,
      O => \s11[3]_i_1_n_0\
    );
\s11[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(4),
      I1 => \s12[8]_i_2_n_0\,
      I2 => \xreg1_reg_n_0_[4]\,
      O => \s11[4]_i_1_n_0\
    );
\s11[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(5),
      I1 => \s12[8]_i_2_n_0\,
      I2 => \xreg1_reg_n_0_[5]\,
      O => \s11[5]_i_1_n_0\
    );
\s11[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(6),
      I1 => \s12[8]_i_2_n_0\,
      I2 => \xreg1_reg_n_0_[6]\,
      O => \s11[6]_i_1_n_0\
    );
\s11[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(7),
      I1 => \s12[8]_i_2_n_0\,
      I2 => \xreg1_reg_n_0_[7]\,
      O => \s11[7]_i_1_n_0\
    );
\s11[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(8),
      I1 => \s12[8]_i_2_n_0\,
      I2 => \xreg1_reg_n_0_[8]\,
      O => \s11[8]_i_1_n_0\
    );
\s11[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \xreg1_reg_n_0_[9]\,
      I1 => xreg2(9),
      O => \s11[9]_i_1_n_0\
    );
\s11_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s11[0]_i_1_n_0\,
      Q => \s11_reg_n_0_[0]\
    );
\s11_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s11[1]_i_1_n_0\,
      Q => \s11_reg_n_0_[1]\
    );
\s11_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s11[2]_i_1_n_0\,
      Q => \s11_reg_n_0_[2]\
    );
\s11_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s11[3]_i_1_n_0\,
      Q => \s11_reg_n_0_[3]\
    );
\s11_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s11[4]_i_1_n_0\,
      Q => \s11_reg_n_0_[4]\
    );
\s11_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s11[5]_i_1_n_0\,
      Q => \s11_reg_n_0_[5]\
    );
\s11_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s11[6]_i_1_n_0\,
      Q => \s11_reg_n_0_[6]\
    );
\s11_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s11[7]_i_1_n_0\,
      Q => \s11_reg_n_0_[7]\
    );
\s11_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s11[8]_i_1_n_0\,
      Q => \s11_reg_n_0_[8]\
    );
\s11_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s11[9]_i_1_n_0\,
      Q => \s11_reg_n_0_[9]\
    );
\s12[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[0]\,
      I1 => \s12[8]_i_2_n_0\,
      I2 => xreg2(0),
      O => \s12[0]_i_1_n_0\
    );
\s12[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[1]\,
      I1 => \s12[8]_i_2_n_0\,
      I2 => xreg2(1),
      O => \s12[1]_i_1_n_0\
    );
\s12[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[2]\,
      I1 => \s12[8]_i_2_n_0\,
      I2 => xreg2(2),
      O => \s12[2]_i_1_n_0\
    );
\s12[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[3]\,
      I1 => \s12[8]_i_2_n_0\,
      I2 => xreg2(3),
      O => \s12[3]_i_1_n_0\
    );
\s12[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[4]\,
      I1 => \s12[8]_i_2_n_0\,
      I2 => xreg2(4),
      O => \s12[4]_i_1_n_0\
    );
\s12[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[5]\,
      I1 => \s12[8]_i_2_n_0\,
      I2 => xreg2(5),
      O => \s12[5]_i_1_n_0\
    );
\s12[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[6]\,
      I1 => \s12[8]_i_2_n_0\,
      I2 => xreg2(6),
      O => \s12[6]_i_1_n_0\
    );
\s12[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[7]\,
      I1 => \s12[8]_i_2_n_0\,
      I2 => xreg2(7),
      O => \s12[7]_i_1_n_0\
    );
\s12[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[8]\,
      I1 => \s12[8]_i_2_n_0\,
      I2 => xreg2(8),
      O => \s12[8]_i_1_n_0\
    );
\s12[8]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B200FFB2"
    )
        port map (
      I0 => \s12[8]_i_3_n_0\,
      I1 => \xreg1_reg_n_0_[8]\,
      I2 => xreg2(8),
      I3 => xreg2(9),
      I4 => \xreg1_reg_n_0_[9]\,
      O => \s12[8]_i_2_n_0\
    );
\s12[8]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7050FFFF"
    )
        port map (
      I0 => \s12[8]_i_4_n_0\,
      I1 => \s12[8]_i_5_n_0\,
      I2 => \s12[8]_i_6_n_0\,
      I3 => \s12[8]_i_7_n_0\,
      I4 => \s12[8]_i_8_n_0\,
      O => \s12[8]_i_3_n_0\
    );
\s12[8]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => \xreg1_reg_n_0_[4]\,
      I1 => xreg2(4),
      I2 => xreg2(5),
      I3 => \xreg1_reg_n_0_[5]\,
      O => \s12[8]_i_4_n_0\
    );
\s12[8]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2F22BF2F"
    )
        port map (
      I0 => \xreg1_reg_n_0_[3]\,
      I1 => xreg2(3),
      I2 => \s12[8]_i_9_n_0\,
      I3 => \xreg1_reg_n_0_[2]\,
      I4 => xreg2(2),
      O => \s12[8]_i_5_n_0\
    );
\s12[8]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BB2B"
    )
        port map (
      I0 => xreg2(7),
      I1 => \xreg1_reg_n_0_[7]\,
      I2 => \xreg1_reg_n_0_[6]\,
      I3 => xreg2(6),
      O => \s12[8]_i_6_n_0\
    );
\s12[8]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => xreg2(4),
      I1 => \xreg1_reg_n_0_[4]\,
      I2 => \xreg1_reg_n_0_[5]\,
      I3 => xreg2(5),
      O => \s12[8]_i_7_n_0\
    );
\s12[8]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B0FB"
    )
        port map (
      I0 => \xreg1_reg_n_0_[6]\,
      I1 => xreg2(6),
      I2 => \xreg1_reg_n_0_[7]\,
      I3 => xreg2(7),
      O => \s12[8]_i_8_n_0\
    );
\s12[8]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40F440F4FFFF40F4"
    )
        port map (
      I0 => \xreg1_reg_n_0_[0]\,
      I1 => xreg2(0),
      I2 => xreg2(1),
      I3 => \xreg1_reg_n_0_[1]\,
      I4 => xreg2(3),
      I5 => \xreg1_reg_n_0_[3]\,
      O => \s12[8]_i_9_n_0\
    );
\s12[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000DF00"
    )
        port map (
      I0 => \full[0]_0\(1),
      I1 => m_axis_tready,
      I2 => \^m_axis_tkeep[0]\,
      I3 => \full[0]_0\(0),
      I4 => hold,
      O => s21
    );
\s12[9]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[9]\,
      I1 => xreg2(9),
      O => \s12[9]_i_2_n_0\
    );
\s12_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s12[0]_i_1_n_0\,
      Q => s12(0)
    );
\s12_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s12[1]_i_1_n_0\,
      Q => s12(1)
    );
\s12_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s12[2]_i_1_n_0\,
      Q => s12(2)
    );
\s12_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s12[3]_i_1_n_0\,
      Q => s12(3)
    );
\s12_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s12[4]_i_1_n_0\,
      Q => s12(4)
    );
\s12_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s12[5]_i_1_n_0\,
      Q => s12(5)
    );
\s12_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s12[6]_i_1_n_0\,
      Q => s12(6)
    );
\s12_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s12[7]_i_1_n_0\,
      Q => s12(7)
    );
\s12_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s12[8]_i_1_n_0\,
      Q => s12(8)
    );
\s12_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s12[9]_i_2_n_0\,
      Q => s12(9)
    );
\s21[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(0),
      I1 => \s22[8]_i_2_n_0\,
      I2 => \yreg1_reg_n_0_[0]\,
      O => \s21[0]_i_1_n_0\
    );
\s21[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(1),
      I1 => \s22[8]_i_2_n_0\,
      I2 => \yreg1_reg_n_0_[1]\,
      O => \s21[1]_i_1_n_0\
    );
\s21[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(2),
      I1 => \s22[8]_i_2_n_0\,
      I2 => \yreg1_reg_n_0_[2]\,
      O => \s21[2]_i_1_n_0\
    );
\s21[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(3),
      I1 => \s22[8]_i_2_n_0\,
      I2 => \yreg1_reg_n_0_[3]\,
      O => \s21[3]_i_1_n_0\
    );
\s21[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(4),
      I1 => \s22[8]_i_2_n_0\,
      I2 => \yreg1_reg_n_0_[4]\,
      O => \s21[4]_i_1_n_0\
    );
\s21[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(5),
      I1 => \s22[8]_i_2_n_0\,
      I2 => \yreg1_reg_n_0_[5]\,
      O => \s21[5]_i_1_n_0\
    );
\s21[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(6),
      I1 => \s22[8]_i_2_n_0\,
      I2 => \yreg1_reg_n_0_[6]\,
      O => \s21[6]_i_1_n_0\
    );
\s21[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(7),
      I1 => \s22[8]_i_2_n_0\,
      I2 => \yreg1_reg_n_0_[7]\,
      O => \s21[7]_i_1_n_0\
    );
\s21[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(8),
      I1 => \s22[8]_i_2_n_0\,
      I2 => \yreg1_reg_n_0_[8]\,
      O => \s21[8]_i_1_n_0\
    );
\s21[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \yreg1_reg_n_0_[9]\,
      I1 => yreg2(9),
      O => \s21[9]_i_1_n_0\
    );
\s21_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \out[7]_i_3_n_0\,
      D => \s21[0]_i_1_n_0\,
      Q => \s21_reg_n_0_[0]\
    );
\s21_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \out[7]_i_3_n_0\,
      D => \s21[1]_i_1_n_0\,
      Q => \s21_reg_n_0_[1]\
    );
\s21_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s21[2]_i_1_n_0\,
      Q => \s21_reg_n_0_[2]\
    );
\s21_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s21[3]_i_1_n_0\,
      Q => \s21_reg_n_0_[3]\
    );
\s21_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s21[4]_i_1_n_0\,
      Q => \s21_reg_n_0_[4]\
    );
\s21_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s21[5]_i_1_n_0\,
      Q => \s21_reg_n_0_[5]\
    );
\s21_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s21[6]_i_1_n_0\,
      Q => \s21_reg_n_0_[6]\
    );
\s21_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s21[7]_i_1_n_0\,
      Q => \s21_reg_n_0_[7]\
    );
\s21_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s21[8]_i_1_n_0\,
      Q => \s21_reg_n_0_[8]\
    );
\s21_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s21[9]_i_1_n_0\,
      Q => \s21_reg_n_0_[9]\
    );
\s22[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[0]\,
      I1 => \s22[8]_i_2_n_0\,
      I2 => yreg2(0),
      O => \s22[0]_i_1_n_0\
    );
\s22[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[1]\,
      I1 => \s22[8]_i_2_n_0\,
      I2 => yreg2(1),
      O => \s22[1]_i_1_n_0\
    );
\s22[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[2]\,
      I1 => \s22[8]_i_2_n_0\,
      I2 => yreg2(2),
      O => \s22[2]_i_1_n_0\
    );
\s22[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[3]\,
      I1 => \s22[8]_i_2_n_0\,
      I2 => yreg2(3),
      O => \s22[3]_i_1_n_0\
    );
\s22[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[4]\,
      I1 => \s22[8]_i_2_n_0\,
      I2 => yreg2(4),
      O => \s22[4]_i_1_n_0\
    );
\s22[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[5]\,
      I1 => \s22[8]_i_2_n_0\,
      I2 => yreg2(5),
      O => \s22[5]_i_1_n_0\
    );
\s22[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[6]\,
      I1 => \s22[8]_i_2_n_0\,
      I2 => yreg2(6),
      O => \s22[6]_i_1_n_0\
    );
\s22[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[7]\,
      I1 => \s22[8]_i_2_n_0\,
      I2 => yreg2(7),
      O => \s22[7]_i_1_n_0\
    );
\s22[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[8]\,
      I1 => \s22[8]_i_2_n_0\,
      I2 => yreg2(8),
      O => \s22[8]_i_1_n_0\
    );
\s22[8]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B200FFB2"
    )
        port map (
      I0 => \s22[8]_i_3_n_0\,
      I1 => \yreg1_reg_n_0_[8]\,
      I2 => yreg2(8),
      I3 => yreg2(9),
      I4 => \yreg1_reg_n_0_[9]\,
      O => \s22[8]_i_2_n_0\
    );
\s22[8]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7050FFFF"
    )
        port map (
      I0 => \s22[8]_i_4_n_0\,
      I1 => \s22[8]_i_5_n_0\,
      I2 => \s22[8]_i_6_n_0\,
      I3 => \s22[8]_i_7_n_0\,
      I4 => \s22[8]_i_8_n_0\,
      O => \s22[8]_i_3_n_0\
    );
\s22[8]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => \yreg1_reg_n_0_[4]\,
      I1 => yreg2(4),
      I2 => yreg2(5),
      I3 => \yreg1_reg_n_0_[5]\,
      O => \s22[8]_i_4_n_0\
    );
\s22[8]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4D44DDDD"
    )
        port map (
      I0 => yreg2(3),
      I1 => \yreg1_reg_n_0_[3]\,
      I2 => yreg2(2),
      I3 => \yreg1_reg_n_0_[2]\,
      I4 => \s22[8]_i_9_n_0\,
      O => \s22[8]_i_5_n_0\
    );
\s22[8]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BB2B"
    )
        port map (
      I0 => yreg2(7),
      I1 => \yreg1_reg_n_0_[7]\,
      I2 => \yreg1_reg_n_0_[6]\,
      I3 => yreg2(6),
      O => \s22[8]_i_6_n_0\
    );
\s22[8]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => yreg2(4),
      I1 => \yreg1_reg_n_0_[4]\,
      I2 => \yreg1_reg_n_0_[5]\,
      I3 => yreg2(5),
      O => \s22[8]_i_7_n_0\
    );
\s22[8]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B0FB"
    )
        port map (
      I0 => \yreg1_reg_n_0_[6]\,
      I1 => yreg2(6),
      I2 => \yreg1_reg_n_0_[7]\,
      I3 => yreg2(7),
      O => \s22[8]_i_8_n_0\
    );
\s22[8]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40F440F4FFFF40F4"
    )
        port map (
      I0 => \yreg1_reg_n_0_[0]\,
      I1 => yreg2(0),
      I2 => yreg2(1),
      I3 => \yreg1_reg_n_0_[1]\,
      I4 => yreg2(2),
      I5 => \yreg1_reg_n_0_[2]\,
      O => \s22[8]_i_9_n_0\
    );
\s22[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[9]\,
      I1 => yreg2(9),
      O => \s22[9]_i_1_n_0\
    );
\s22_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s22[0]_i_1_n_0\,
      Q => s22(0)
    );
\s22_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s22[1]_i_1_n_0\,
      Q => s22(1)
    );
\s22_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s22[2]_i_1_n_0\,
      Q => s22(2)
    );
\s22_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s22[3]_i_1_n_0\,
      Q => s22(3)
    );
\s22_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s22[4]_i_1_n_0\,
      Q => s22(4)
    );
\s22_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s22[5]_i_1_n_0\,
      Q => s22(5)
    );
\s22_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s22[6]_i_1_n_0\,
      Q => s22(6)
    );
\s22_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s22[7]_i_1_n_0\,
      Q => s22(7)
    );
\s22_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s22[8]_i_1_n_0\,
      Q => s22(8)
    );
\s22_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^xreg1_reg[8]_0\,
      D => \s22[9]_i_1_n_0\,
      Q => s22(9)
    );
s_axis_tready_INST_0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => \full[0]_0\(0),
      I1 => m_axis_tready,
      I2 => \full[0]_0\(1),
      I3 => \^m_axis_tkeep[0]\,
      O => s_axis_tready
    );
\temp[47]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFF0000"
    )
        port map (
      I0 => \s_axis_tstrb[9]\,
      I1 => s_axis_tstrb(5),
      I2 => s_axis_tstrb(4),
      I3 => s_axis_tstrb(3),
      I4 => \^temp0\,
      O => \temp_reg[0]\
    );
\xreg1[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(56),
      I1 => Q(40),
      O => \x12/f1/S\
    );
\xreg1[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(41),
      I1 => Q(57),
      I2 => Q(48),
      I3 => Q(40),
      I4 => Q(56),
      O => outx12(1)
    );
\xreg1[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(42),
      I1 => Q(49),
      I2 => Q(58),
      I3 => Q(48),
      I4 => Q(57),
      I5 => \x12/C_1\,
      O => outx12(2)
    );
\xreg1[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(56),
      I1 => Q(40),
      I2 => Q(48),
      I3 => Q(57),
      I4 => Q(41),
      O => \x12/C_1\
    );
\xreg1[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(43),
      I1 => outx11(3),
      I2 => \x12/C_2\,
      O => outx12(3)
    );
\xreg1[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(50),
      I1 => Q(59),
      I2 => Q(49),
      I3 => Q(58),
      I4 => Q(48),
      I5 => Q(57),
      O => outx11(3)
    );
\xreg1[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \x12/C_1\,
      I1 => Q(57),
      I2 => Q(48),
      I3 => Q(58),
      I4 => Q(49),
      I5 => Q(42),
      O => \x12/C_2\
    );
\xreg1[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(44),
      I1 => Q(51),
      I2 => Q(60),
      I3 => \x11/C_3\,
      I4 => \x12/C_3\,
      O => outx12(4)
    );
\xreg1[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \x12/C_3\,
      I1 => \x11/C_3\,
      I2 => Q(60),
      I3 => Q(51),
      I4 => Q(44),
      I5 => \x12/f6/S\,
      O => outx12(5)
    );
\xreg1[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \x12/C_1\,
      I1 => outx11(2),
      I2 => Q(42),
      I3 => outx11(3),
      I4 => Q(43),
      O => \x12/C_3\
    );
\xreg1[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(57),
      I1 => Q(48),
      I2 => Q(58),
      I3 => Q(49),
      I4 => Q(59),
      I5 => Q(50),
      O => \x11/C_3\
    );
\xreg1[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \x11/C_3\,
      I1 => Q(60),
      I2 => Q(51),
      I3 => Q(61),
      I4 => Q(52),
      I5 => Q(45),
      O => \x12/f6/S\
    );
\xreg1[5]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(49),
      I1 => Q(58),
      I2 => Q(48),
      I3 => Q(57),
      O => outx11(2)
    );
\xreg1[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(46),
      I1 => Q(53),
      I2 => Q(62),
      I3 => \x11/C_5\,
      I4 => \x12/C_5\,
      O => outx12(6)
    );
\xreg1[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \x11/C_3\,
      I1 => Q(60),
      I2 => Q(51),
      I3 => Q(61),
      I4 => Q(52),
      O => \x11/C_5\
    );
\xreg1[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \x12/C_3\,
      I1 => outx11(4),
      I2 => Q(44),
      I3 => outx11(5),
      I4 => Q(45),
      O => \x12/C_5\
    );
\xreg1[6]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(51),
      I1 => Q(60),
      I2 => \x11/C_3\,
      O => outx11(4)
    );
\xreg1[6]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(52),
      I1 => Q(61),
      I2 => Q(51),
      I3 => Q(60),
      I4 => \x11/C_3\,
      O => outx11(5)
    );
\xreg1[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \x12/f8/S\,
      I1 => \x12/C_6\,
      O => outx12(7)
    );
\xreg1[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \x11/C_5\,
      I1 => Q(62),
      I2 => Q(53),
      I3 => Q(63),
      I4 => Q(54),
      I5 => Q(47),
      O => \x12/f8/S\
    );
\xreg1[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(55),
      I1 => Q(47),
      I2 => Q(54),
      I3 => Q(63),
      I4 => \x11/C_6\,
      I5 => \x12/C_6\,
      O => outx12(8)
    );
\xreg1[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F7FF0000"
    )
        port map (
      I0 => \full[0]_0\(0),
      I1 => \^m_axis_tkeep[0]\,
      I2 => m_axis_tready,
      I3 => \full[0]_0\(1),
      I4 => \valid_reg[0]\(0),
      I5 => hold,
      O => xreg1
    );
\xreg1[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \x12/C_6\,
      I1 => \x11/C_6\,
      I2 => Q(63),
      I3 => Q(54),
      I4 => Q(47),
      I5 => Q(55),
      O => outx12(9)
    );
\xreg1[9]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset,
      O => \^xreg1_reg[8]_0\
    );
\xreg1[9]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \x12/C_5\,
      I1 => \x11/C_5\,
      I2 => Q(62),
      I3 => Q(53),
      I4 => Q(46),
      O => \x12/C_6\
    );
\xreg1[9]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \x11/C_5\,
      I1 => Q(62),
      I2 => Q(53),
      O => \x11/C_6\
    );
\xreg1_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => \x12/f1/S\,
      Q => \xreg1_reg_n_0_[0]\
    );
\xreg1_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => outx12(1),
      Q => \xreg1_reg_n_0_[1]\
    );
\xreg1_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => outx12(2),
      Q => \xreg1_reg_n_0_[2]\
    );
\xreg1_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => outx12(3),
      Q => \xreg1_reg_n_0_[3]\
    );
\xreg1_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => outx12(4),
      Q => \xreg1_reg_n_0_[4]\
    );
\xreg1_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => outx12(5),
      Q => \xreg1_reg_n_0_[5]\
    );
\xreg1_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => outx12(6),
      Q => \xreg1_reg_n_0_[6]\
    );
\xreg1_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => outx12(7),
      Q => \xreg1_reg_n_0_[7]\
    );
\xreg1_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => outx12(8),
      Q => \xreg1_reg_n_0_[8]\
    );
\xreg1_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => outx12(9),
      Q => \xreg1_reg_n_0_[9]\
    );
\xreg2[0]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(16),
      I1 => Q(0),
      O => \^d\(0)
    );
\xreg2[1]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(1),
      I1 => Q(17),
      I2 => Q(8),
      I3 => Q(0),
      I4 => Q(16),
      O => \^d\(1)
    );
\xreg2[2]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(2),
      I1 => Q(9),
      I2 => Q(18),
      I3 => Q(8),
      I4 => Q(17),
      I5 => \c3/x12/C_1\,
      O => \^d\(2)
    );
\xreg2[2]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(16),
      I1 => Q(0),
      I2 => Q(8),
      I3 => Q(17),
      I4 => Q(1),
      O => \c3/x12/C_1\
    );
\xreg2[3]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(3),
      I1 => \c3/outx11\(3),
      I2 => \c3/x12/C_2\,
      O => \^d\(3)
    );
\xreg2[3]_i_2__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(10),
      I1 => Q(19),
      I2 => Q(9),
      I3 => Q(18),
      I4 => Q(8),
      I5 => Q(17),
      O => \c3/outx11\(3)
    );
\xreg2[3]_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \c3/x12/C_1\,
      I1 => Q(17),
      I2 => Q(8),
      I3 => Q(18),
      I4 => Q(9),
      I5 => Q(2),
      O => \c3/x12/C_2\
    );
\xreg2[4]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(4),
      I1 => Q(11),
      I2 => Q(20),
      I3 => \c3/x11/C_3\,
      I4 => \c3/x12/C_3\,
      O => \^d\(4)
    );
\xreg2[5]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \c3/x12/C_3\,
      I1 => \c3/x11/C_3\,
      I2 => Q(20),
      I3 => Q(11),
      I4 => Q(4),
      I5 => \c3/x12/f6/S\,
      O => \^d\(5)
    );
\xreg2[5]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \c3/x12/C_1\,
      I1 => \c3/outx11\(2),
      I2 => Q(2),
      I3 => \c3/outx11\(3),
      I4 => Q(3),
      O => \c3/x12/C_3\
    );
\xreg2[5]_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(17),
      I1 => Q(8),
      I2 => Q(18),
      I3 => Q(9),
      I4 => Q(19),
      I5 => Q(10),
      O => \c3/x11/C_3\
    );
\xreg2[5]_i_4__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \c3/x11/C_3\,
      I1 => Q(20),
      I2 => Q(11),
      I3 => Q(21),
      I4 => Q(12),
      I5 => Q(5),
      O => \c3/x12/f6/S\
    );
\xreg2[5]_i_5__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(9),
      I1 => Q(18),
      I2 => Q(8),
      I3 => Q(17),
      O => \c3/outx11\(2)
    );
\xreg2[6]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(13),
      I2 => Q(22),
      I3 => \c3/x11/C_5\,
      I4 => \c3/x12/C_5\,
      O => \^d\(6)
    );
\xreg2[6]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \c3/x11/C_3\,
      I1 => Q(20),
      I2 => Q(11),
      I3 => Q(21),
      I4 => Q(12),
      O => \c3/x11/C_5\
    );
\xreg2[6]_i_3__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \c3/x12/C_3\,
      I1 => \c3/outx11\(4),
      I2 => Q(4),
      I3 => \c3/outx11\(5),
      I4 => Q(5),
      O => \c3/x12/C_5\
    );
\xreg2[6]_i_4__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(11),
      I1 => Q(20),
      I2 => \c3/x11/C_3\,
      O => \c3/outx11\(4)
    );
\xreg2[6]_i_5__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(12),
      I1 => Q(21),
      I2 => Q(11),
      I3 => Q(20),
      I4 => \c3/x11/C_3\,
      O => \c3/outx11\(5)
    );
\xreg2[7]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \c3/x12/f8/S\,
      I1 => \c3/x12/C_6\,
      O => \^d\(7)
    );
\xreg2[7]_i_2__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \c3/x11/C_5\,
      I1 => Q(22),
      I2 => Q(13),
      I3 => Q(23),
      I4 => Q(14),
      I5 => Q(7),
      O => \c3/x12/f8/S\
    );
\xreg2[8]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(15),
      I1 => Q(7),
      I2 => Q(14),
      I3 => Q(23),
      I4 => \c3/x11/C_6\,
      I5 => \c3/x12/C_6\,
      O => \^d\(8)
    );
\xreg2[9]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \c3/x12/C_6\,
      I1 => \c3/x11/C_6\,
      I2 => Q(23),
      I3 => Q(14),
      I4 => Q(7),
      I5 => Q(15),
      O => \^d\(9)
    );
\xreg2[9]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \c3/x12/C_5\,
      I1 => \c3/x11/C_5\,
      I2 => Q(22),
      I3 => Q(13),
      I4 => Q(6),
      O => \c3/x12/C_6\
    );
\xreg2[9]_i_3__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \c3/x11/C_5\,
      I1 => Q(22),
      I2 => Q(13),
      O => \c3/x11/C_6\
    );
\xreg2_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => \^d\(0),
      Q => xreg2(0)
    );
\xreg2_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => \^d\(1),
      Q => xreg2(1)
    );
\xreg2_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => \^d\(2),
      Q => xreg2(2)
    );
\xreg2_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => \^d\(3),
      Q => xreg2(3)
    );
\xreg2_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => \^d\(4),
      Q => xreg2(4)
    );
\xreg2_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => \^d\(5),
      Q => xreg2(5)
    );
\xreg2_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => \^d\(6),
      Q => xreg2(6)
    );
\xreg2_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => \^d\(7),
      Q => xreg2(7)
    );
\xreg2_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => \^d\(8),
      Q => xreg2(8)
    );
\xreg2_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^xreg1_reg[8]_0\,
      D => \^d\(9),
      Q => xreg2(9)
    );
\yreg1[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(56),
      I1 => Q(16),
      O => \y12/f1/S\
    );
\yreg1[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(17),
      I1 => Q(57),
      I2 => Q(32),
      I3 => Q(16),
      I4 => Q(56),
      O => outy12(1)
    );
\yreg1[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(18),
      I1 => Q(33),
      I2 => Q(58),
      I3 => Q(32),
      I4 => Q(57),
      I5 => \y12/C_1\,
      O => outy12(2)
    );
\yreg1[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(56),
      I1 => Q(16),
      I2 => Q(32),
      I3 => Q(57),
      I4 => Q(17),
      O => \y12/C_1\
    );
\yreg1[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(19),
      I1 => outy11(3),
      I2 => \y12/C_2\,
      O => outy12(3)
    );
\yreg1[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(34),
      I1 => Q(59),
      I2 => Q(33),
      I3 => Q(58),
      I4 => Q(32),
      I5 => Q(57),
      O => outy11(3)
    );
\yreg1[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \y12/C_1\,
      I1 => Q(57),
      I2 => Q(32),
      I3 => Q(58),
      I4 => Q(33),
      I5 => Q(18),
      O => \y12/C_2\
    );
\yreg1[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(20),
      I1 => Q(35),
      I2 => Q(60),
      I3 => \y11/C_3\,
      I4 => \y12/C_3\,
      O => outy12(4)
    );
\yreg1[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \y12/C_3\,
      I1 => \y11/C_3\,
      I2 => Q(60),
      I3 => Q(35),
      I4 => Q(20),
      I5 => \y12/f6/S\,
      O => outy12(5)
    );
\yreg1[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y12/C_1\,
      I1 => outy11(2),
      I2 => Q(18),
      I3 => outy11(3),
      I4 => Q(19),
      O => \y12/C_3\
    );
\yreg1[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(57),
      I1 => Q(32),
      I2 => Q(58),
      I3 => Q(33),
      I4 => Q(59),
      I5 => Q(34),
      O => \y11/C_3\
    );
\yreg1[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y11/C_3\,
      I1 => Q(60),
      I2 => Q(35),
      I3 => Q(61),
      I4 => Q(36),
      I5 => Q(21),
      O => \y12/f6/S\
    );
\yreg1[5]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(33),
      I1 => Q(58),
      I2 => Q(32),
      I3 => Q(57),
      O => outy11(2)
    );
\yreg1[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(22),
      I1 => Q(37),
      I2 => Q(62),
      I3 => \y11/C_5\,
      I4 => \y12/C_5\,
      O => outy12(6)
    );
\yreg1[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y11/C_3\,
      I1 => Q(60),
      I2 => Q(35),
      I3 => Q(61),
      I4 => Q(36),
      O => \y11/C_5\
    );
\yreg1[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y12/C_3\,
      I1 => outy11(4),
      I2 => Q(20),
      I3 => outy11(5),
      I4 => Q(21),
      O => \y12/C_5\
    );
\yreg1[6]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(35),
      I1 => Q(60),
      I2 => \y11/C_3\,
      O => outy11(4)
    );
\yreg1[6]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(36),
      I1 => Q(61),
      I2 => Q(35),
      I3 => Q(60),
      I4 => \y11/C_3\,
      O => outy11(5)
    );
\yreg1[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \y12/f8/S\,
      I1 => \y12/C_6\,
      O => outy12(7)
    );
\yreg1[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y11/C_5\,
      I1 => Q(62),
      I2 => Q(37),
      I3 => Q(63),
      I4 => Q(38),
      I5 => Q(23),
      O => \y12/f8/S\
    );
\yreg1[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(39),
      I1 => Q(23),
      I2 => Q(38),
      I3 => Q(63),
      I4 => \y11/C_6\,
      I5 => \y12/C_6\,
      O => outy12(8)
    );
\yreg1[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FF000000000000"
    )
        port map (
      I0 => \full[0]_0\(0),
      I1 => \^m_axis_tkeep[0]\,
      I2 => m_axis_tready,
      I3 => \full[0]_0\(1),
      I4 => \valid_reg[0]\(0),
      I5 => hold_reg,
      O => yreg1
    );
\yreg1[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \y12/C_6\,
      I1 => \y11/C_6\,
      I2 => Q(63),
      I3 => Q(38),
      I4 => Q(23),
      I5 => Q(39),
      O => outy12(9)
    );
\yreg1[9]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \y12/C_5\,
      I1 => \y11/C_5\,
      I2 => Q(62),
      I3 => Q(37),
      I4 => Q(22),
      O => \y12/C_6\
    );
\yreg1[9]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \y11/C_5\,
      I1 => Q(62),
      I2 => Q(37),
      O => \y11/C_6\
    );
\yreg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => \y12/f1/S\,
      Q => \yreg1_reg_n_0_[0]\,
      R => '0'
    );
\yreg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(1),
      Q => \yreg1_reg_n_0_[1]\,
      R => '0'
    );
\yreg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(2),
      Q => \yreg1_reg_n_0_[2]\,
      R => '0'
    );
\yreg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(3),
      Q => \yreg1_reg_n_0_[3]\,
      R => '0'
    );
\yreg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(4),
      Q => \yreg1_reg_n_0_[4]\,
      R => '0'
    );
\yreg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(5),
      Q => \yreg1_reg_n_0_[5]\,
      R => '0'
    );
\yreg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(6),
      Q => \yreg1_reg_n_0_[6]\,
      R => '0'
    );
\yreg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(7),
      Q => \yreg1_reg_n_0_[7]\,
      R => '0'
    );
\yreg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(8),
      Q => \yreg1_reg_n_0_[8]\,
      R => '0'
    );
\yreg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(9),
      Q => \yreg1_reg_n_0_[9]\,
      R => '0'
    );
\yreg2[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(40),
      I1 => Q(0),
      O => \y22/f1/S\
    );
\yreg2[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(1),
      I1 => Q(41),
      I2 => Q(24),
      I3 => Q(0),
      I4 => Q(40),
      O => outy22(1)
    );
\yreg2[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(2),
      I1 => Q(25),
      I2 => Q(42),
      I3 => Q(24),
      I4 => Q(41),
      I5 => \y22/C_1\,
      O => outy22(2)
    );
\yreg2[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(40),
      I1 => Q(0),
      I2 => Q(24),
      I3 => Q(41),
      I4 => Q(1),
      O => \y22/C_1\
    );
\yreg2[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(3),
      I1 => outy21(3),
      I2 => \y22/C_2\,
      O => outy22(3)
    );
\yreg2[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(26),
      I1 => Q(43),
      I2 => Q(25),
      I3 => Q(42),
      I4 => Q(24),
      I5 => Q(41),
      O => outy21(3)
    );
\yreg2[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \y22/C_1\,
      I1 => Q(41),
      I2 => Q(24),
      I3 => Q(42),
      I4 => Q(25),
      I5 => Q(2),
      O => \y22/C_2\
    );
\yreg2[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(4),
      I1 => Q(27),
      I2 => Q(44),
      I3 => \y21/C_3\,
      I4 => \y22/C_3\,
      O => outy22(4)
    );
\yreg2[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \y22/C_3\,
      I1 => \y21/C_3\,
      I2 => Q(44),
      I3 => Q(27),
      I4 => Q(4),
      I5 => \y22/f6/S\,
      O => outy22(5)
    );
\yreg2[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y22/C_1\,
      I1 => outy21(2),
      I2 => Q(2),
      I3 => outy21(3),
      I4 => Q(3),
      O => \y22/C_3\
    );
\yreg2[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(41),
      I1 => Q(24),
      I2 => Q(42),
      I3 => Q(25),
      I4 => Q(43),
      I5 => Q(26),
      O => \y21/C_3\
    );
\yreg2[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y21/C_3\,
      I1 => Q(44),
      I2 => Q(27),
      I3 => Q(45),
      I4 => Q(28),
      I5 => Q(5),
      O => \y22/f6/S\
    );
\yreg2[5]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(25),
      I1 => Q(42),
      I2 => Q(24),
      I3 => Q(41),
      O => outy21(2)
    );
\yreg2[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(29),
      I2 => Q(46),
      I3 => \y21/C_5\,
      I4 => \y22/C_5\,
      O => outy22(6)
    );
\yreg2[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y21/C_3\,
      I1 => Q(44),
      I2 => Q(27),
      I3 => Q(45),
      I4 => Q(28),
      O => \y21/C_5\
    );
\yreg2[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y22/C_3\,
      I1 => outy21(4),
      I2 => Q(4),
      I3 => outy21(5),
      I4 => Q(5),
      O => \y22/C_5\
    );
\yreg2[6]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(27),
      I1 => Q(44),
      I2 => \y21/C_3\,
      O => outy21(4)
    );
\yreg2[6]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(28),
      I1 => Q(45),
      I2 => Q(27),
      I3 => Q(44),
      I4 => \y21/C_3\,
      O => outy21(5)
    );
\yreg2[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \y22/f8/S\,
      I1 => \y22/C_6\,
      O => outy22(7)
    );
\yreg2[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y21/C_5\,
      I1 => Q(46),
      I2 => Q(29),
      I3 => Q(47),
      I4 => Q(30),
      I5 => Q(7),
      O => \y22/f8/S\
    );
\yreg2[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(31),
      I1 => Q(7),
      I2 => Q(30),
      I3 => Q(47),
      I4 => \y21/C_6\,
      I5 => \y22/C_6\,
      O => outy22(8)
    );
\yreg2[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \y22/C_6\,
      I1 => \y21/C_6\,
      I2 => Q(47),
      I3 => Q(30),
      I4 => Q(7),
      I5 => Q(31),
      O => outy22(9)
    );
\yreg2[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \y22/C_5\,
      I1 => \y21/C_5\,
      I2 => Q(46),
      I3 => Q(29),
      I4 => Q(6),
      O => \y22/C_6\
    );
\yreg2[9]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \y21/C_5\,
      I1 => Q(46),
      I2 => Q(29),
      O => \y21/C_6\
    );
\yreg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => \y22/f1/S\,
      Q => yreg2(0),
      R => '0'
    );
\yreg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(1),
      Q => yreg2(1),
      R => '0'
    );
\yreg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(2),
      Q => yreg2(2),
      R => '0'
    );
\yreg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(3),
      Q => yreg2(3),
      R => '0'
    );
\yreg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(4),
      Q => yreg2(4),
      R => '0'
    );
\yreg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(5),
      Q => yreg2(5),
      R => '0'
    );
\yreg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(6),
      Q => yreg2(6),
      R => '0'
    );
\yreg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(7),
      Q => yreg2(7),
      R => '0'
    );
\yreg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(8),
      Q => yreg2(8),
      R => '0'
    );
\yreg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(9),
      Q => yreg2(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity finalproj_sobelfilteraxistream_0_0_convolution_0 is
  port (
    D : out STD_LOGIC_VECTOR ( 9 downto 0 );
    m_axis_tvalid : out STD_LOGIC;
    \m_axis_tkeep[1]\ : out STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 63 downto 0 );
    \full_reg[2]_0\ : in STD_LOGIC;
    \full_reg[2]_1\ : in STD_LOGIC;
    \full_reg[2]_2\ : in STD_LOGIC;
    hold : in STD_LOGIC;
    \full_reg[2]_3\ : in STD_LOGIC;
    clock : in STD_LOGIC;
    reset_0 : in STD_LOGIC;
    reset_1 : in STD_LOGIC;
    reset : in STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    \valid_reg[1]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    hold_reg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of finalproj_sobelfilteraxistream_0_0_convolution_0 : entity is "convolution";
end finalproj_sobelfilteraxistream_0_0_convolution_0;

architecture STRUCTURE of finalproj_sobelfilteraxistream_0_0_convolution_0 is
  signal \^d\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal ans1 : STD_LOGIC_VECTOR ( 7 downto 1 );
  signal ans2 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal \c4/outx11\ : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal \c4/x11/C_3\ : STD_LOGIC;
  signal \c4/x11/C_5\ : STD_LOGIC;
  signal \c4/x11/C_6\ : STD_LOGIC;
  signal \c4/x12/C_1\ : STD_LOGIC;
  signal \c4/x12/C_2\ : STD_LOGIC;
  signal \c4/x12/C_3\ : STD_LOGIC;
  signal \c4/x12/C_5\ : STD_LOGIC;
  signal \c4/x12/C_6\ : STD_LOGIC;
  signal \c4/x12/f6/S\ : STD_LOGIC;
  signal \c4/x12/f8/S\ : STD_LOGIC;
  signal \final/C_1\ : STD_LOGIC;
  signal \final/C_2\ : STD_LOGIC;
  signal \final/C_4\ : STD_LOGIC;
  signal \final/C_6\ : STD_LOGIC;
  signal \final/f1/Ca\ : STD_LOGIC;
  signal \final/f2/S\ : STD_LOGIC;
  signal \final/f4/S\ : STD_LOGIC;
  signal \final/f5/S\ : STD_LOGIC;
  signal \final/f6/S\ : STD_LOGIC;
  signal \full[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \full[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \full[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \full_reg_n_0_[0]\ : STD_LOGIC;
  signal \full_reg_n_0_[1]\ : STD_LOGIC;
  signal \^m_axis_tkeep[1]\ : STD_LOGIC;
  signal \out\ : STD_LOGIC;
  signal \out[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \out[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \out[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \out[1]_i_2__0_n_0\ : STD_LOGIC;
  signal \out[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \out[2]_i_2__0_n_0\ : STD_LOGIC;
  signal \out[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \out[3]_i_2__0_n_0\ : STD_LOGIC;
  signal \out[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \out[4]_i_2__0_n_0\ : STD_LOGIC;
  signal \out[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \out[5]_i_2__0_n_0\ : STD_LOGIC;
  signal \out[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \out[6]_i_2__0_n_0\ : STD_LOGIC;
  signal \out[7]_i_25__0_n_0\ : STD_LOGIC;
  signal \out[7]_i_26__0_n_0\ : STD_LOGIC;
  signal \out[7]_i_27__0_n_0\ : STD_LOGIC;
  signal \out[7]_i_28__0_n_0\ : STD_LOGIC;
  signal \out[7]_i_2__0_n_0\ : STD_LOGIC;
  signal \out[7]_i_7__0_n_0\ : STD_LOGIC;
  signal outx11 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outx12 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal outy11 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outy12 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal outy21 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outy22 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal \s1/C2\ : STD_LOGIC;
  signal \s1/F6/S\ : STD_LOGIC;
  signal \s1/s1/C_2\ : STD_LOGIC;
  signal \s1/s2/C_0\ : STD_LOGIC;
  signal \s1/s2/C_2\ : STD_LOGIC;
  signal \s1/s2/f2/S\ : STD_LOGIC;
  signal \s11[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \s11[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \s11[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \s11[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \s11[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \s11[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \s11[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \s11[7]_i_1__0_n_0\ : STD_LOGIC;
  signal \s11[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \s11[9]_i_1__0_n_0\ : STD_LOGIC;
  signal \s11_reg_n_0_[0]\ : STD_LOGIC;
  signal \s11_reg_n_0_[1]\ : STD_LOGIC;
  signal \s11_reg_n_0_[2]\ : STD_LOGIC;
  signal \s11_reg_n_0_[3]\ : STD_LOGIC;
  signal \s11_reg_n_0_[4]\ : STD_LOGIC;
  signal \s11_reg_n_0_[5]\ : STD_LOGIC;
  signal \s11_reg_n_0_[6]\ : STD_LOGIC;
  signal \s11_reg_n_0_[7]\ : STD_LOGIC;
  signal \s11_reg_n_0_[8]\ : STD_LOGIC;
  signal \s11_reg_n_0_[9]\ : STD_LOGIC;
  signal s12 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \s12[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \s12[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \s12[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \s12[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \s12[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \s12[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \s12[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \s12[7]_i_1__0_n_0\ : STD_LOGIC;
  signal \s12[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \s12[8]_i_2__0_n_0\ : STD_LOGIC;
  signal \s12[8]_i_3__0_n_0\ : STD_LOGIC;
  signal \s12[8]_i_4__0_n_0\ : STD_LOGIC;
  signal \s12[8]_i_5__0_n_0\ : STD_LOGIC;
  signal \s12[8]_i_6__0_n_0\ : STD_LOGIC;
  signal \s12[8]_i_7__0_n_0\ : STD_LOGIC;
  signal \s12[8]_i_8__0_n_0\ : STD_LOGIC;
  signal \s12[8]_i_9__0_n_0\ : STD_LOGIC;
  signal \s12[9]_i_2__0_n_0\ : STD_LOGIC;
  signal \s2/C2\ : STD_LOGIC;
  signal \s2/F6/S\ : STD_LOGIC;
  signal \s2/f5/S\ : STD_LOGIC;
  signal \s2/s1/C_2\ : STD_LOGIC;
  signal \s2/s1/f1/S\ : STD_LOGIC;
  signal \s2/s2/C_0\ : STD_LOGIC;
  signal \s2/s2/C_2\ : STD_LOGIC;
  signal \s2/s2/f2/S\ : STD_LOGIC;
  signal s21 : STD_LOGIC;
  signal \s21[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \s21[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \s21[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \s21[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \s21[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \s21[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \s21[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \s21[7]_i_1__0_n_0\ : STD_LOGIC;
  signal \s21[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \s21[9]_i_1__0_n_0\ : STD_LOGIC;
  signal \s21_reg_n_0_[0]\ : STD_LOGIC;
  signal \s21_reg_n_0_[1]\ : STD_LOGIC;
  signal \s21_reg_n_0_[2]\ : STD_LOGIC;
  signal \s21_reg_n_0_[3]\ : STD_LOGIC;
  signal \s21_reg_n_0_[4]\ : STD_LOGIC;
  signal \s21_reg_n_0_[5]\ : STD_LOGIC;
  signal \s21_reg_n_0_[6]\ : STD_LOGIC;
  signal \s21_reg_n_0_[7]\ : STD_LOGIC;
  signal \s21_reg_n_0_[8]\ : STD_LOGIC;
  signal \s21_reg_n_0_[9]\ : STD_LOGIC;
  signal s22 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \s22[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \s22[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \s22[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \s22[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \s22[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \s22[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \s22[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \s22[7]_i_1__0_n_0\ : STD_LOGIC;
  signal \s22[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \s22[8]_i_2__0_n_0\ : STD_LOGIC;
  signal \s22[8]_i_3__0_n_0\ : STD_LOGIC;
  signal \s22[8]_i_4__0_n_0\ : STD_LOGIC;
  signal \s22[8]_i_5__0_n_0\ : STD_LOGIC;
  signal \s22[8]_i_6__0_n_0\ : STD_LOGIC;
  signal \s22[8]_i_7__0_n_0\ : STD_LOGIC;
  signal \s22[8]_i_8__0_n_0\ : STD_LOGIC;
  signal \s22[8]_i_9__0_n_0\ : STD_LOGIC;
  signal \s22[9]_i_1__0_n_0\ : STD_LOGIC;
  signal \x11/C_3\ : STD_LOGIC;
  signal \x11/C_5\ : STD_LOGIC;
  signal \x11/C_6\ : STD_LOGIC;
  signal \x12/C_1\ : STD_LOGIC;
  signal \x12/C_2\ : STD_LOGIC;
  signal \x12/C_3\ : STD_LOGIC;
  signal \x12/C_5\ : STD_LOGIC;
  signal \x12/C_6\ : STD_LOGIC;
  signal \x12/f1/S\ : STD_LOGIC;
  signal \x12/f6/S\ : STD_LOGIC;
  signal \x12/f8/S\ : STD_LOGIC;
  signal xreg1 : STD_LOGIC;
  signal \xreg1_reg_n_0_[0]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[1]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[2]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[3]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[4]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[5]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[6]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[7]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[8]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[9]\ : STD_LOGIC;
  signal xreg2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \y11/C_3\ : STD_LOGIC;
  signal \y11/C_5\ : STD_LOGIC;
  signal \y11/C_6\ : STD_LOGIC;
  signal \y12/C_1\ : STD_LOGIC;
  signal \y12/C_2\ : STD_LOGIC;
  signal \y12/C_3\ : STD_LOGIC;
  signal \y12/C_5\ : STD_LOGIC;
  signal \y12/C_6\ : STD_LOGIC;
  signal \y12/f1/S\ : STD_LOGIC;
  signal \y12/f6/S\ : STD_LOGIC;
  signal \y12/f8/S\ : STD_LOGIC;
  signal \y21/C_3\ : STD_LOGIC;
  signal \y21/C_5\ : STD_LOGIC;
  signal \y21/C_6\ : STD_LOGIC;
  signal \y22/C_1\ : STD_LOGIC;
  signal \y22/C_2\ : STD_LOGIC;
  signal \y22/C_3\ : STD_LOGIC;
  signal \y22/C_5\ : STD_LOGIC;
  signal \y22/C_6\ : STD_LOGIC;
  signal \y22/f1/S\ : STD_LOGIC;
  signal \y22/f6/S\ : STD_LOGIC;
  signal \y22/f8/S\ : STD_LOGIC;
  signal yreg1 : STD_LOGIC;
  signal \yreg1_reg_n_0_[0]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[1]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[2]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[3]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[4]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[5]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[6]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[7]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[8]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[9]\ : STD_LOGIC;
  signal yreg2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \out[0]_i_2__0\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \out[1]_i_2__0\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \out[1]_i_3__0\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \out[2]_i_6__0\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \out[4]_i_8__0\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \out[4]_i_9__0\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \out[6]_i_2__0\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \out[7]_i_10__0\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \out[7]_i_12__0\ : label is "soft_lutpair106";
  attribute SOFT_HLUTNM of \out[7]_i_13__0\ : label is "soft_lutpair107";
  attribute SOFT_HLUTNM of \out[7]_i_14__0\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \out[7]_i_15__0\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \out[7]_i_20__0\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \out[7]_i_21__0\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \out[7]_i_23__0\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \out[7]_i_24__0\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \out[7]_i_26__0\ : label is "soft_lutpair106";
  attribute SOFT_HLUTNM of \out[7]_i_28__0\ : label is "soft_lutpair107";
  attribute SOFT_HLUTNM of \out[7]_i_6__0\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \out[7]_i_9__0\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \s11[0]_i_1__0\ : label is "soft_lutpair97";
  attribute SOFT_HLUTNM of \s11[1]_i_1__0\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \s11[2]_i_1__0\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \s11[3]_i_1__0\ : label is "soft_lutpair104";
  attribute SOFT_HLUTNM of \s11[4]_i_1__0\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \s11[5]_i_1__0\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \s11[6]_i_1__0\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \s11[7]_i_1__0\ : label is "soft_lutpair99";
  attribute SOFT_HLUTNM of \s11[8]_i_1__0\ : label is "soft_lutpair98";
  attribute SOFT_HLUTNM of \s12[0]_i_1__0\ : label is "soft_lutpair97";
  attribute SOFT_HLUTNM of \s12[1]_i_1__0\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \s12[2]_i_1__0\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \s12[3]_i_1__0\ : label is "soft_lutpair104";
  attribute SOFT_HLUTNM of \s12[4]_i_1__0\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \s12[5]_i_1__0\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \s12[6]_i_1__0\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \s12[7]_i_1__0\ : label is "soft_lutpair99";
  attribute SOFT_HLUTNM of \s12[8]_i_1__0\ : label is "soft_lutpair98";
  attribute SOFT_HLUTNM of \s12[8]_i_2__0\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \s12[8]_i_4__0\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \s12[8]_i_6__0\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \s12[8]_i_7__0\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \s12[8]_i_8__0\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \s12[9]_i_2__0\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \s21[0]_i_1__0\ : label is "soft_lutpair102";
  attribute SOFT_HLUTNM of \s21[1]_i_1__0\ : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \s21[2]_i_1__0\ : label is "soft_lutpair101";
  attribute SOFT_HLUTNM of \s21[3]_i_1__0\ : label is "soft_lutpair100";
  attribute SOFT_HLUTNM of \s21[4]_i_1__0\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \s21[5]_i_1__0\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \s21[6]_i_1__0\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \s21[7]_i_1__0\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \s21[8]_i_1__0\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \s22[0]_i_1__0\ : label is "soft_lutpair102";
  attribute SOFT_HLUTNM of \s22[1]_i_1__0\ : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \s22[2]_i_1__0\ : label is "soft_lutpair101";
  attribute SOFT_HLUTNM of \s22[3]_i_1__0\ : label is "soft_lutpair100";
  attribute SOFT_HLUTNM of \s22[4]_i_1__0\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \s22[5]_i_1__0\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \s22[6]_i_1__0\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \s22[7]_i_1__0\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \s22[8]_i_1__0\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \s22[8]_i_2__0\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \s22[8]_i_4__0\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \s22[8]_i_6__0\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \s22[8]_i_7__0\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \s22[8]_i_8__0\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \s22[9]_i_1__0\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \xreg1[1]_i_1__0\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \xreg1[2]_i_2__0\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \xreg1[4]_i_1__0\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \xreg1[6]_i_1__0\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \xreg1[6]_i_2__0\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \xreg1[6]_i_4__0\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \xreg1[6]_i_5__0\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \xreg1[9]_i_3__0\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \xreg1[9]_i_4__0\ : label is "soft_lutpair105";
  attribute SOFT_HLUTNM of \xreg2[0]_i_1__2\ : label is "soft_lutpair108";
  attribute SOFT_HLUTNM of \xreg2[1]_i_1__2\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \xreg2[2]_i_2__2\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \xreg2[3]_i_1__2\ : label is "soft_lutpair103";
  attribute SOFT_HLUTNM of \xreg2[4]_i_1__2\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \xreg2[6]_i_1__2\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \xreg2[6]_i_2__2\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \xreg2[6]_i_4__2\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \xreg2[6]_i_5__2\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \xreg2[9]_i_2__2\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \yreg1[1]_i_1__0\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \yreg1[2]_i_2__0\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \yreg1[4]_i_1__0\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \yreg1[6]_i_1__0\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \yreg1[6]_i_2__0\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \yreg1[6]_i_4__0\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \yreg1[6]_i_5__0\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \yreg1[9]_i_3\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \yreg1[9]_i_4__0\ : label is "soft_lutpair105";
  attribute SOFT_HLUTNM of \yreg2[0]_i_1__0\ : label is "soft_lutpair108";
  attribute SOFT_HLUTNM of \yreg2[1]_i_1__0\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \yreg2[2]_i_2__0\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \yreg2[3]_i_1__0\ : label is "soft_lutpair103";
  attribute SOFT_HLUTNM of \yreg2[4]_i_1__0\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \yreg2[6]_i_1__0\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \yreg2[6]_i_2__0\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \yreg2[6]_i_4__0\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \yreg2[6]_i_5__0\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \yreg2[9]_i_2__0\ : label is "soft_lutpair67";
begin
  D(9 downto 0) <= \^d\(9 downto 0);
  \m_axis_tkeep[1]\ <= \^m_axis_tkeep[1]\;
\full[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEFEEEEE44444444"
    )
        port map (
      I0 => hold,
      I1 => \valid_reg[1]\(0),
      I2 => \full_reg_n_0_[1]\,
      I3 => m_axis_tready,
      I4 => \^m_axis_tkeep[1]\,
      I5 => \full_reg_n_0_[0]\,
      O => \full[0]_i_1__0_n_0\
    );
\full[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEFE4444"
    )
        port map (
      I0 => hold,
      I1 => \full_reg_n_0_[0]\,
      I2 => \^m_axis_tkeep[1]\,
      I3 => m_axis_tready,
      I4 => \full_reg_n_0_[1]\,
      O => \full[1]_i_1__0_n_0\
    );
\full[2]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFF4040"
    )
        port map (
      I0 => hold,
      I1 => reset,
      I2 => \full_reg_n_0_[1]\,
      I3 => m_axis_tready,
      I4 => \^m_axis_tkeep[1]\,
      O => \full[2]_i_1__0_n_0\
    );
\full_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => reset_0,
      D => \full[0]_i_1__0_n_0\,
      Q => \full_reg_n_0_[0]\
    );
\full_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => reset_0,
      D => \full[1]_i_1__0_n_0\,
      Q => \full_reg_n_0_[1]\
    );
\full_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => reset_0,
      D => \full[2]_i_1__0_n_0\,
      Q => \^m_axis_tkeep[1]\
    );
m_axis_tvalid_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF0000FFFE"
    )
        port map (
      I0 => \^m_axis_tkeep[1]\,
      I1 => \full_reg[2]_0\,
      I2 => \full_reg[2]_1\,
      I3 => \full_reg[2]_2\,
      I4 => hold,
      I5 => \full_reg[2]_3\,
      O => m_axis_tvalid
    );
\out[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[0]_i_2__0_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__0_n_0\,
      O => \out[0]_i_1__0_n_0\
    );
\out[0]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s11_reg_n_0_[0]\,
      O => \out[0]_i_2__0_n_0\
    );
\out[1]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[1]_i_2__0_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__0_n_0\,
      O => \out[1]_i_1__0_n_0\
    );
\out[1]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA9696AA"
    )
        port map (
      I0 => \final/f2/S\,
      I1 => \s11_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s21_reg_n_0_[0]\,
      I4 => s22(0),
      O => \out[1]_i_2__0_n_0\
    );
\out[1]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D22D2DD2"
    )
        port map (
      I0 => s12(0),
      I1 => \s11_reg_n_0_[0]\,
      I2 => \s11_reg_n_0_[1]\,
      I3 => s12(1),
      I4 => ans2(1),
      O => \final/f2/S\
    );
\out[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[2]_i_2__0_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__0_n_0\,
      O => \out[2]_i_1__0_n_0\
    );
\out[2]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => ans1(2),
      I1 => ans2(2),
      I2 => \final/C_1\,
      O => \out[2]_i_2__0_n_0\
    );
\out[2]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966996969666966"
    )
        port map (
      I0 => s12(2),
      I1 => \s11_reg_n_0_[2]\,
      I2 => \s11_reg_n_0_[1]\,
      I3 => s12(1),
      I4 => \s11_reg_n_0_[0]\,
      I5 => s12(0),
      O => ans1(2)
    );
\out[2]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966996969666966"
    )
        port map (
      I0 => s22(2),
      I1 => \s21_reg_n_0_[2]\,
      I2 => \s21_reg_n_0_[1]\,
      I3 => s22(1),
      I4 => \s21_reg_n_0_[0]\,
      I5 => s22(0),
      O => ans2(2)
    );
\out[2]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0CC0D44D4DD40CC0"
    )
        port map (
      I0 => \s2/s1/f1/S\,
      I1 => ans2(1),
      I2 => s12(1),
      I3 => \s11_reg_n_0_[1]\,
      I4 => \s11_reg_n_0_[0]\,
      I5 => s12(0),
      O => \final/C_1\
    );
\out[2]_i_6__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[0]\,
      I1 => s22(0),
      O => \s2/s1/f1/S\
    );
\out[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[3]_i_2__0_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__0_n_0\,
      O => \out[3]_i_1__0_n_0\
    );
\out[3]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \final/f4/S\,
      I1 => \final/C_2\,
      O => \out[3]_i_2__0_n_0\
    );
\out[3]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => \s11_reg_n_0_[3]\,
      I2 => s12(3),
      I3 => \s2/s1/C_2\,
      I4 => \s21_reg_n_0_[3]\,
      I5 => s22(3),
      O => \final/f4/S\
    );
\out[4]_i_10__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF0DFFFF0000DF0D"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s22(1),
      I3 => \s21_reg_n_0_[1]\,
      I4 => s22(2),
      I5 => \s21_reg_n_0_[2]\,
      O => \s2/s1/C_2\
    );
\out[4]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[4]_i_2__0_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__0_n_0\,
      O => \out[4]_i_1__0_n_0\
    );
\out[4]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"711717718EE8E88E"
    )
        port map (
      I0 => \final/C_2\,
      I1 => ans2(3),
      I2 => s12(3),
      I3 => \s11_reg_n_0_[3]\,
      I4 => \s1/s1/C_2\,
      I5 => \final/f5/S\,
      O => \out[4]_i_2__0_n_0\
    );
\out[4]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/f1/Ca\,
      I1 => ans2(1),
      I2 => ans1(1),
      I3 => ans2(2),
      I4 => ans1(2),
      O => \final/C_2\
    );
\out[4]_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(3),
      I1 => \s21_reg_n_0_[3]\,
      I2 => \s2/s1/C_2\,
      O => ans2(3)
    );
\out[4]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF0DFFFF0000DF0D"
    )
        port map (
      I0 => s12(0),
      I1 => \s11_reg_n_0_[0]\,
      I2 => s12(1),
      I3 => \s11_reg_n_0_[1]\,
      I4 => s12(2),
      I5 => \s11_reg_n_0_[2]\,
      O => \s1/s1/C_2\
    );
\out[4]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B24D4DB24DB2B24D"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => s12(3),
      I2 => \s11_reg_n_0_[3]\,
      I3 => \s11_reg_n_0_[4]\,
      I4 => s12(4),
      I5 => ans2(4),
      O => \final/f5/S\
    );
\out[4]_i_7__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0660"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s11_reg_n_0_[0]\,
      O => \final/f1/Ca\
    );
\out[4]_i_8__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => s22(1),
      I1 => \s21_reg_n_0_[1]\,
      I2 => \s21_reg_n_0_[0]\,
      I3 => s22(0),
      O => ans2(1)
    );
\out[4]_i_9__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => s12(1),
      I1 => \s11_reg_n_0_[1]\,
      I2 => \s11_reg_n_0_[0]\,
      I3 => s12(0),
      O => ans1(1)
    );
\out[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[5]_i_2__0_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__0_n_0\,
      O => \out[5]_i_1__0_n_0\
    );
\out[5]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \final/f6/S\,
      I1 => \final/C_4\,
      O => \out[5]_i_2__0_n_0\
    );
\out[5]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \s1/s2/C_0\,
      I1 => \s11_reg_n_0_[5]\,
      I2 => s12(5),
      I3 => \s2/s2/C_0\,
      I4 => \s21_reg_n_0_[5]\,
      I5 => s22(5),
      O => \final/f6/S\
    );
\out[6]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[6]_i_2__0_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__0_n_0\,
      O => \out[6]_i_1__0_n_0\
    );
\out[6]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => ans2(6),
      I1 => ans1(6),
      I2 => ans1(5),
      I3 => ans2(5),
      I4 => \final/C_4\,
      O => \out[6]_i_2__0_n_0\
    );
\out[7]_i_10__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s1/s2/C_0\,
      I1 => s12(5),
      I2 => \s11_reg_n_0_[5]\,
      I3 => s12(6),
      I4 => \s11_reg_n_0_[6]\,
      O => \s1/s2/C_2\
    );
\out[7]_i_11__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/C_2\,
      I1 => ans2(3),
      I2 => ans1(3),
      I3 => ans2(4),
      I4 => ans1(4),
      O => \final/C_4\
    );
\out[7]_i_12__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(5),
      I1 => \s21_reg_n_0_[5]\,
      I2 => \s2/s2/C_0\,
      O => ans2(5)
    );
\out[7]_i_13__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(5),
      I1 => \s11_reg_n_0_[5]\,
      I2 => \s1/s2/C_0\,
      O => ans1(5)
    );
\out[7]_i_14__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s22(6),
      I1 => \s21_reg_n_0_[6]\,
      I2 => \s21_reg_n_0_[5]\,
      I3 => s22(5),
      I4 => \s2/s2/C_0\,
      O => ans2(6)
    );
\out[7]_i_15__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s12(6),
      I1 => \s11_reg_n_0_[6]\,
      I2 => \s11_reg_n_0_[5]\,
      I3 => s12(5),
      I4 => \s1/s2/C_0\,
      O => ans1(6)
    );
\out[7]_i_16__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s11_reg_n_0_[9]\,
      I1 => s12(9),
      O => \s1/F6/S\
    );
\out[7]_i_17__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[8]\,
      I1 => s22(8),
      O => \s2/f5/S\
    );
\out[7]_i_18__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EF0EFFFF0000EF0E"
    )
        port map (
      I0 => \out[7]_i_25__0_n_0\,
      I1 => \out[7]_i_26__0_n_0\,
      I2 => s22(6),
      I3 => \s21_reg_n_0_[6]\,
      I4 => s22(7),
      I5 => \s21_reg_n_0_[7]\,
      O => \s2/C2\
    );
\out[7]_i_19__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EF0EFFFF0000EF0E"
    )
        port map (
      I0 => \out[7]_i_27__0_n_0\,
      I1 => \out[7]_i_28__0_n_0\,
      I2 => s12(6),
      I3 => \s11_reg_n_0_[6]\,
      I4 => s12(7),
      I5 => \s11_reg_n_0_[7]\,
      O => \s1/C2\
    );
\out[7]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00D0"
    )
        port map (
      I0 => \^m_axis_tkeep[1]\,
      I1 => m_axis_tready,
      I2 => \full_reg_n_0_[1]\,
      I3 => hold,
      O => \out\
    );
\out[7]_i_20__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s2/s1/C_2\,
      I1 => s22(3),
      I2 => \s21_reg_n_0_[3]\,
      I3 => s22(4),
      I4 => \s21_reg_n_0_[4]\,
      O => \s2/s2/C_0\
    );
\out[7]_i_21__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => s12(3),
      I2 => \s11_reg_n_0_[3]\,
      I3 => s12(4),
      I4 => \s11_reg_n_0_[4]\,
      O => \s1/s2/C_0\
    );
\out[7]_i_22__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(3),
      I1 => \s11_reg_n_0_[3]\,
      I2 => \s1/s1/C_2\,
      O => ans1(3)
    );
\out[7]_i_23__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s22(4),
      I1 => \s21_reg_n_0_[4]\,
      I2 => \s21_reg_n_0_[3]\,
      I3 => s22(3),
      I4 => \s2/s1/C_2\,
      O => ans2(4)
    );
\out[7]_i_24__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s12(4),
      I1 => \s11_reg_n_0_[4]\,
      I2 => \s11_reg_n_0_[3]\,
      I3 => s12(3),
      I4 => \s1/s1/C_2\,
      O => ans1(4)
    );
\out[7]_i_25__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A088A8A08088A08"
    )
        port map (
      I0 => \s2/s2/f2/S\,
      I1 => \s21_reg_n_0_[4]\,
      I2 => s22(4),
      I3 => \s21_reg_n_0_[3]\,
      I4 => s22(3),
      I5 => \s2/s1/C_2\,
      O => \out[7]_i_25__0_n_0\
    );
\out[7]_i_26__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \s21_reg_n_0_[5]\,
      I1 => s22(5),
      O => \out[7]_i_26__0_n_0\
    );
\out[7]_i_27__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A088A8A08088A08"
    )
        port map (
      I0 => \s1/s2/f2/S\,
      I1 => \s11_reg_n_0_[4]\,
      I2 => s12(4),
      I3 => \s11_reg_n_0_[3]\,
      I4 => s12(3),
      I5 => \s1/s1/C_2\,
      O => \out[7]_i_27__0_n_0\
    );
\out[7]_i_28__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \s11_reg_n_0_[5]\,
      I1 => s12(5),
      O => \out[7]_i_28__0_n_0\
    );
\out[7]_i_29__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[5]\,
      I1 => s22(5),
      O => \s2/s2/f2/S\
    );
\out[7]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => ans2(9),
      I1 => ans1(7),
      I2 => ans2(7),
      I3 => \final/C_6\,
      I4 => \out[7]_i_7__0_n_0\,
      O => \out[7]_i_2__0_n_0\
    );
\out[7]_i_30__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s11_reg_n_0_[5]\,
      I1 => s12(5),
      O => \s1/s2/f2/S\
    );
\out[7]_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"65A66565A6A665A6"
    )
        port map (
      I0 => \s2/F6/S\,
      I1 => \s21_reg_n_0_[8]\,
      I2 => s22(8),
      I3 => \s21_reg_n_0_[7]\,
      I4 => s22(7),
      I5 => \s2/s2/C_2\,
      O => ans2(9)
    );
\out[7]_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(7),
      I1 => \s11_reg_n_0_[7]\,
      I2 => \s1/s2/C_2\,
      O => ans1(7)
    );
\out[7]_i_5__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(7),
      I1 => \s21_reg_n_0_[7]\,
      I2 => \s2/s2/C_2\,
      O => ans2(7)
    );
\out[7]_i_6__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/C_4\,
      I1 => ans2(5),
      I2 => ans1(5),
      I3 => ans2(6),
      I4 => ans1(6),
      O => \final/C_6\
    );
\out[7]_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DFFFF7DFF7DBEFF"
    )
        port map (
      I0 => \s1/F6/S\,
      I1 => \s2/f5/S\,
      I2 => \s2/C2\,
      I3 => s12(8),
      I4 => \s11_reg_n_0_[8]\,
      I5 => \s1/C2\,
      O => \out[7]_i_7__0_n_0\
    );
\out[7]_i_8__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[9]\,
      I1 => s22(9),
      O => \s2/F6/S\
    );
\out[7]_i_9__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s2/s2/C_0\,
      I1 => s22(5),
      I2 => \s21_reg_n_0_[5]\,
      I3 => s22(6),
      I4 => \s21_reg_n_0_[6]\,
      O => \s2/s2/C_2\
    );
\out_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_0,
      D => \out[0]_i_1__0_n_0\,
      Q => m_axis_tdata(0)
    );
\out_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_0,
      D => \out[1]_i_1__0_n_0\,
      Q => m_axis_tdata(1)
    );
\out_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_0,
      D => \out[2]_i_1__0_n_0\,
      Q => m_axis_tdata(2)
    );
\out_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_0,
      D => \out[3]_i_1__0_n_0\,
      Q => m_axis_tdata(3)
    );
\out_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_0,
      D => \out[4]_i_1__0_n_0\,
      Q => m_axis_tdata(4)
    );
\out_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_0,
      D => \out[5]_i_1__0_n_0\,
      Q => m_axis_tdata(5)
    );
\out_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_0,
      D => \out[6]_i_1__0_n_0\,
      Q => m_axis_tdata(6)
    );
\out_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_0,
      D => \out[7]_i_2__0_n_0\,
      Q => m_axis_tdata(7)
    );
\s11[0]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(0),
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => \xreg1_reg_n_0_[0]\,
      O => \s11[0]_i_1__0_n_0\
    );
\s11[1]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(1),
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => \xreg1_reg_n_0_[1]\,
      O => \s11[1]_i_1__0_n_0\
    );
\s11[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(2),
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => \xreg1_reg_n_0_[2]\,
      O => \s11[2]_i_1__0_n_0\
    );
\s11[3]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(3),
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => \xreg1_reg_n_0_[3]\,
      O => \s11[3]_i_1__0_n_0\
    );
\s11[4]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(4),
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => \xreg1_reg_n_0_[4]\,
      O => \s11[4]_i_1__0_n_0\
    );
\s11[5]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(5),
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => \xreg1_reg_n_0_[5]\,
      O => \s11[5]_i_1__0_n_0\
    );
\s11[6]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(6),
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => \xreg1_reg_n_0_[6]\,
      O => \s11[6]_i_1__0_n_0\
    );
\s11[7]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(7),
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => \xreg1_reg_n_0_[7]\,
      O => \s11[7]_i_1__0_n_0\
    );
\s11[8]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(8),
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => \xreg1_reg_n_0_[8]\,
      O => \s11[8]_i_1__0_n_0\
    );
\s11[9]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \xreg1_reg_n_0_[9]\,
      I1 => xreg2(9),
      O => \s11[9]_i_1__0_n_0\
    );
\s11_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[0]_i_1__0_n_0\,
      Q => \s11_reg_n_0_[0]\
    );
\s11_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[1]_i_1__0_n_0\,
      Q => \s11_reg_n_0_[1]\
    );
\s11_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[2]_i_1__0_n_0\,
      Q => \s11_reg_n_0_[2]\
    );
\s11_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[3]_i_1__0_n_0\,
      Q => \s11_reg_n_0_[3]\
    );
\s11_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[4]_i_1__0_n_0\,
      Q => \s11_reg_n_0_[4]\
    );
\s11_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s11[5]_i_1__0_n_0\,
      Q => \s11_reg_n_0_[5]\
    );
\s11_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s11[6]_i_1__0_n_0\,
      Q => \s11_reg_n_0_[6]\
    );
\s11_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s11[7]_i_1__0_n_0\,
      Q => \s11_reg_n_0_[7]\
    );
\s11_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s11[8]_i_1__0_n_0\,
      Q => \s11_reg_n_0_[8]\
    );
\s11_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s11[9]_i_1__0_n_0\,
      Q => \s11_reg_n_0_[9]\
    );
\s12[0]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[0]\,
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => xreg2(0),
      O => \s12[0]_i_1__0_n_0\
    );
\s12[1]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[1]\,
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => xreg2(1),
      O => \s12[1]_i_1__0_n_0\
    );
\s12[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[2]\,
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => xreg2(2),
      O => \s12[2]_i_1__0_n_0\
    );
\s12[3]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[3]\,
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => xreg2(3),
      O => \s12[3]_i_1__0_n_0\
    );
\s12[4]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[4]\,
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => xreg2(4),
      O => \s12[4]_i_1__0_n_0\
    );
\s12[5]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[5]\,
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => xreg2(5),
      O => \s12[5]_i_1__0_n_0\
    );
\s12[6]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[6]\,
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => xreg2(6),
      O => \s12[6]_i_1__0_n_0\
    );
\s12[7]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[7]\,
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => xreg2(7),
      O => \s12[7]_i_1__0_n_0\
    );
\s12[8]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[8]\,
      I1 => \s12[8]_i_2__0_n_0\,
      I2 => xreg2(8),
      O => \s12[8]_i_1__0_n_0\
    );
\s12[8]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B200FFB2"
    )
        port map (
      I0 => \s12[8]_i_3__0_n_0\,
      I1 => \xreg1_reg_n_0_[8]\,
      I2 => xreg2(8),
      I3 => xreg2(9),
      I4 => \xreg1_reg_n_0_[9]\,
      O => \s12[8]_i_2__0_n_0\
    );
\s12[8]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7050FFFF"
    )
        port map (
      I0 => \s12[8]_i_4__0_n_0\,
      I1 => \s12[8]_i_5__0_n_0\,
      I2 => \s12[8]_i_6__0_n_0\,
      I3 => \s12[8]_i_7__0_n_0\,
      I4 => \s12[8]_i_8__0_n_0\,
      O => \s12[8]_i_3__0_n_0\
    );
\s12[8]_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => \xreg1_reg_n_0_[4]\,
      I1 => xreg2(4),
      I2 => xreg2(5),
      I3 => \xreg1_reg_n_0_[5]\,
      O => \s12[8]_i_4__0_n_0\
    );
\s12[8]_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2F22BF2F"
    )
        port map (
      I0 => \xreg1_reg_n_0_[3]\,
      I1 => xreg2(3),
      I2 => \s12[8]_i_9__0_n_0\,
      I3 => \xreg1_reg_n_0_[2]\,
      I4 => xreg2(2),
      O => \s12[8]_i_5__0_n_0\
    );
\s12[8]_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BB2B"
    )
        port map (
      I0 => xreg2(7),
      I1 => \xreg1_reg_n_0_[7]\,
      I2 => \xreg1_reg_n_0_[6]\,
      I3 => xreg2(6),
      O => \s12[8]_i_6__0_n_0\
    );
\s12[8]_i_7__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => xreg2(4),
      I1 => \xreg1_reg_n_0_[4]\,
      I2 => \xreg1_reg_n_0_[5]\,
      I3 => xreg2(5),
      O => \s12[8]_i_7__0_n_0\
    );
\s12[8]_i_8__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B0FB"
    )
        port map (
      I0 => \xreg1_reg_n_0_[6]\,
      I1 => xreg2(6),
      I2 => \xreg1_reg_n_0_[7]\,
      I3 => xreg2(7),
      O => \s12[8]_i_8__0_n_0\
    );
\s12[8]_i_9__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40F440F4FFFF40F4"
    )
        port map (
      I0 => \xreg1_reg_n_0_[0]\,
      I1 => xreg2(0),
      I2 => xreg2(1),
      I3 => \xreg1_reg_n_0_[1]\,
      I4 => xreg2(3),
      I5 => \xreg1_reg_n_0_[3]\,
      O => \s12[8]_i_9__0_n_0\
    );
\s12[9]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000DF00"
    )
        port map (
      I0 => \full_reg_n_0_[1]\,
      I1 => m_axis_tready,
      I2 => \^m_axis_tkeep[1]\,
      I3 => \full_reg_n_0_[0]\,
      I4 => hold,
      O => s21
    );
\s12[9]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[9]\,
      I1 => xreg2(9),
      O => \s12[9]_i_2__0_n_0\
    );
\s12_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[0]_i_1__0_n_0\,
      Q => s12(0)
    );
\s12_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[1]_i_1__0_n_0\,
      Q => s12(1)
    );
\s12_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[2]_i_1__0_n_0\,
      Q => s12(2)
    );
\s12_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[3]_i_1__0_n_0\,
      Q => s12(3)
    );
\s12_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[4]_i_1__0_n_0\,
      Q => s12(4)
    );
\s12_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s12[5]_i_1__0_n_0\,
      Q => s12(5)
    );
\s12_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s12[6]_i_1__0_n_0\,
      Q => s12(6)
    );
\s12_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s12[7]_i_1__0_n_0\,
      Q => s12(7)
    );
\s12_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s12[8]_i_1__0_n_0\,
      Q => s12(8)
    );
\s12_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s12[9]_i_2__0_n_0\,
      Q => s12(9)
    );
\s21[0]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(0),
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => \yreg1_reg_n_0_[0]\,
      O => \s21[0]_i_1__0_n_0\
    );
\s21[1]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(1),
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => \yreg1_reg_n_0_[1]\,
      O => \s21[1]_i_1__0_n_0\
    );
\s21[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(2),
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => \yreg1_reg_n_0_[2]\,
      O => \s21[2]_i_1__0_n_0\
    );
\s21[3]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(3),
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => \yreg1_reg_n_0_[3]\,
      O => \s21[3]_i_1__0_n_0\
    );
\s21[4]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(4),
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => \yreg1_reg_n_0_[4]\,
      O => \s21[4]_i_1__0_n_0\
    );
\s21[5]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(5),
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => \yreg1_reg_n_0_[5]\,
      O => \s21[5]_i_1__0_n_0\
    );
\s21[6]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(6),
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => \yreg1_reg_n_0_[6]\,
      O => \s21[6]_i_1__0_n_0\
    );
\s21[7]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(7),
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => \yreg1_reg_n_0_[7]\,
      O => \s21[7]_i_1__0_n_0\
    );
\s21[8]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(8),
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => \yreg1_reg_n_0_[8]\,
      O => \s21[8]_i_1__0_n_0\
    );
\s21[9]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \yreg1_reg_n_0_[9]\,
      I1 => yreg2(9),
      O => \s21[9]_i_1__0_n_0\
    );
\s21_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s21[0]_i_1__0_n_0\,
      Q => \s21_reg_n_0_[0]\
    );
\s21_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s21[1]_i_1__0_n_0\,
      Q => \s21_reg_n_0_[1]\
    );
\s21_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s21[2]_i_1__0_n_0\,
      Q => \s21_reg_n_0_[2]\
    );
\s21_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s21[3]_i_1__0_n_0\,
      Q => \s21_reg_n_0_[3]\
    );
\s21_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s21[4]_i_1__0_n_0\,
      Q => \s21_reg_n_0_[4]\
    );
\s21_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s21[5]_i_1__0_n_0\,
      Q => \s21_reg_n_0_[5]\
    );
\s21_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s21[6]_i_1__0_n_0\,
      Q => \s21_reg_n_0_[6]\
    );
\s21_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s21[7]_i_1__0_n_0\,
      Q => \s21_reg_n_0_[7]\
    );
\s21_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s21[8]_i_1__0_n_0\,
      Q => \s21_reg_n_0_[8]\
    );
\s21_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s21[9]_i_1__0_n_0\,
      Q => \s21_reg_n_0_[9]\
    );
\s22[0]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[0]\,
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => yreg2(0),
      O => \s22[0]_i_1__0_n_0\
    );
\s22[1]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[1]\,
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => yreg2(1),
      O => \s22[1]_i_1__0_n_0\
    );
\s22[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[2]\,
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => yreg2(2),
      O => \s22[2]_i_1__0_n_0\
    );
\s22[3]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[3]\,
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => yreg2(3),
      O => \s22[3]_i_1__0_n_0\
    );
\s22[4]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[4]\,
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => yreg2(4),
      O => \s22[4]_i_1__0_n_0\
    );
\s22[5]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[5]\,
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => yreg2(5),
      O => \s22[5]_i_1__0_n_0\
    );
\s22[6]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[6]\,
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => yreg2(6),
      O => \s22[6]_i_1__0_n_0\
    );
\s22[7]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[7]\,
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => yreg2(7),
      O => \s22[7]_i_1__0_n_0\
    );
\s22[8]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[8]\,
      I1 => \s22[8]_i_2__0_n_0\,
      I2 => yreg2(8),
      O => \s22[8]_i_1__0_n_0\
    );
\s22[8]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B200FFB2"
    )
        port map (
      I0 => \s22[8]_i_3__0_n_0\,
      I1 => \yreg1_reg_n_0_[8]\,
      I2 => yreg2(8),
      I3 => yreg2(9),
      I4 => \yreg1_reg_n_0_[9]\,
      O => \s22[8]_i_2__0_n_0\
    );
\s22[8]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7050FFFF"
    )
        port map (
      I0 => \s22[8]_i_4__0_n_0\,
      I1 => \s22[8]_i_5__0_n_0\,
      I2 => \s22[8]_i_6__0_n_0\,
      I3 => \s22[8]_i_7__0_n_0\,
      I4 => \s22[8]_i_8__0_n_0\,
      O => \s22[8]_i_3__0_n_0\
    );
\s22[8]_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => \yreg1_reg_n_0_[4]\,
      I1 => yreg2(4),
      I2 => yreg2(5),
      I3 => \yreg1_reg_n_0_[5]\,
      O => \s22[8]_i_4__0_n_0\
    );
\s22[8]_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4D44DDDD"
    )
        port map (
      I0 => yreg2(3),
      I1 => \yreg1_reg_n_0_[3]\,
      I2 => yreg2(2),
      I3 => \yreg1_reg_n_0_[2]\,
      I4 => \s22[8]_i_9__0_n_0\,
      O => \s22[8]_i_5__0_n_0\
    );
\s22[8]_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BB2B"
    )
        port map (
      I0 => yreg2(7),
      I1 => \yreg1_reg_n_0_[7]\,
      I2 => \yreg1_reg_n_0_[6]\,
      I3 => yreg2(6),
      O => \s22[8]_i_6__0_n_0\
    );
\s22[8]_i_7__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => yreg2(4),
      I1 => \yreg1_reg_n_0_[4]\,
      I2 => \yreg1_reg_n_0_[5]\,
      I3 => yreg2(5),
      O => \s22[8]_i_7__0_n_0\
    );
\s22[8]_i_8__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B0FB"
    )
        port map (
      I0 => \yreg1_reg_n_0_[6]\,
      I1 => yreg2(6),
      I2 => \yreg1_reg_n_0_[7]\,
      I3 => yreg2(7),
      O => \s22[8]_i_8__0_n_0\
    );
\s22[8]_i_9__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40F440F4FFFF40F4"
    )
        port map (
      I0 => \yreg1_reg_n_0_[0]\,
      I1 => yreg2(0),
      I2 => yreg2(1),
      I3 => \yreg1_reg_n_0_[1]\,
      I4 => yreg2(2),
      I5 => \yreg1_reg_n_0_[2]\,
      O => \s22[8]_i_9__0_n_0\
    );
\s22[9]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[9]\,
      I1 => yreg2(9),
      O => \s22[9]_i_1__0_n_0\
    );
\s22_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[0]_i_1__0_n_0\,
      Q => s22(0)
    );
\s22_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[1]_i_1__0_n_0\,
      Q => s22(1)
    );
\s22_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[2]_i_1__0_n_0\,
      Q => s22(2)
    );
\s22_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[3]_i_1__0_n_0\,
      Q => s22(3)
    );
\s22_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[4]_i_1__0_n_0\,
      Q => s22(4)
    );
\s22_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s22[5]_i_1__0_n_0\,
      Q => s22(5)
    );
\s22_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s22[6]_i_1__0_n_0\,
      Q => s22(6)
    );
\s22_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s22[7]_i_1__0_n_0\,
      Q => s22(7)
    );
\s22_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s22[8]_i_1__0_n_0\,
      Q => s22(8)
    );
\s22_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_1,
      D => \s22[9]_i_1__0_n_0\,
      Q => s22(9)
    );
\xreg1[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(56),
      I1 => Q(40),
      O => \x12/f1/S\
    );
\xreg1[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(41),
      I1 => Q(57),
      I2 => Q(48),
      I3 => Q(40),
      I4 => Q(56),
      O => outx12(1)
    );
\xreg1[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(42),
      I1 => Q(49),
      I2 => Q(58),
      I3 => Q(48),
      I4 => Q(57),
      I5 => \x12/C_1\,
      O => outx12(2)
    );
\xreg1[2]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(56),
      I1 => Q(40),
      I2 => Q(48),
      I3 => Q(57),
      I4 => Q(41),
      O => \x12/C_1\
    );
\xreg1[3]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(43),
      I1 => outx11(3),
      I2 => \x12/C_2\,
      O => outx12(3)
    );
\xreg1[3]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(50),
      I1 => Q(59),
      I2 => Q(49),
      I3 => Q(58),
      I4 => Q(48),
      I5 => Q(57),
      O => outx11(3)
    );
\xreg1[3]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \x12/C_1\,
      I1 => Q(57),
      I2 => Q(48),
      I3 => Q(58),
      I4 => Q(49),
      I5 => Q(42),
      O => \x12/C_2\
    );
\xreg1[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(44),
      I1 => Q(51),
      I2 => Q(60),
      I3 => \x11/C_3\,
      I4 => \x12/C_3\,
      O => outx12(4)
    );
\xreg1[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \x12/C_3\,
      I1 => \x11/C_3\,
      I2 => Q(60),
      I3 => Q(51),
      I4 => Q(44),
      I5 => \x12/f6/S\,
      O => outx12(5)
    );
\xreg1[5]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \x12/C_1\,
      I1 => outx11(2),
      I2 => Q(42),
      I3 => outx11(3),
      I4 => Q(43),
      O => \x12/C_3\
    );
\xreg1[5]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(57),
      I1 => Q(48),
      I2 => Q(58),
      I3 => Q(49),
      I4 => Q(59),
      I5 => Q(50),
      O => \x11/C_3\
    );
\xreg1[5]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \x11/C_3\,
      I1 => Q(60),
      I2 => Q(51),
      I3 => Q(61),
      I4 => Q(52),
      I5 => Q(45),
      O => \x12/f6/S\
    );
\xreg1[5]_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(49),
      I1 => Q(58),
      I2 => Q(48),
      I3 => Q(57),
      O => outx11(2)
    );
\xreg1[6]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(46),
      I1 => Q(53),
      I2 => Q(62),
      I3 => \x11/C_5\,
      I4 => \x12/C_5\,
      O => outx12(6)
    );
\xreg1[6]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \x11/C_3\,
      I1 => Q(60),
      I2 => Q(51),
      I3 => Q(61),
      I4 => Q(52),
      O => \x11/C_5\
    );
\xreg1[6]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \x12/C_3\,
      I1 => outx11(4),
      I2 => Q(44),
      I3 => outx11(5),
      I4 => Q(45),
      O => \x12/C_5\
    );
\xreg1[6]_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(51),
      I1 => Q(60),
      I2 => \x11/C_3\,
      O => outx11(4)
    );
\xreg1[6]_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(52),
      I1 => Q(61),
      I2 => Q(51),
      I3 => Q(60),
      I4 => \x11/C_3\,
      O => outx11(5)
    );
\xreg1[7]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \x12/f8/S\,
      I1 => \x12/C_6\,
      O => outx12(7)
    );
\xreg1[7]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \x11/C_5\,
      I1 => Q(62),
      I2 => Q(53),
      I3 => Q(63),
      I4 => Q(54),
      I5 => Q(47),
      O => \x12/f8/S\
    );
\xreg1[8]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(55),
      I1 => Q(47),
      I2 => Q(54),
      I3 => Q(63),
      I4 => \x11/C_6\,
      I5 => \x12/C_6\,
      O => outx12(8)
    );
\xreg1[9]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F7FF0000"
    )
        port map (
      I0 => \full_reg_n_0_[0]\,
      I1 => \^m_axis_tkeep[1]\,
      I2 => m_axis_tready,
      I3 => \full_reg_n_0_[1]\,
      I4 => \valid_reg[1]\(0),
      I5 => hold,
      O => xreg1
    );
\xreg1[9]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \x12/C_6\,
      I1 => \x11/C_6\,
      I2 => Q(63),
      I3 => Q(54),
      I4 => Q(47),
      I5 => Q(55),
      O => outx12(9)
    );
\xreg1[9]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \x12/C_5\,
      I1 => \x11/C_5\,
      I2 => Q(62),
      I3 => Q(53),
      I4 => Q(46),
      O => \x12/C_6\
    );
\xreg1[9]_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \x11/C_5\,
      I1 => Q(62),
      I2 => Q(53),
      O => \x11/C_6\
    );
\xreg1_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => \x12/f1/S\,
      Q => \xreg1_reg_n_0_[0]\
    );
\xreg1_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => outx12(1),
      Q => \xreg1_reg_n_0_[1]\
    );
\xreg1_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => outx12(2),
      Q => \xreg1_reg_n_0_[2]\
    );
\xreg1_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => outx12(3),
      Q => \xreg1_reg_n_0_[3]\
    );
\xreg1_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => outx12(4),
      Q => \xreg1_reg_n_0_[4]\
    );
\xreg1_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => outx12(5),
      Q => \xreg1_reg_n_0_[5]\
    );
\xreg1_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => outx12(6),
      Q => \xreg1_reg_n_0_[6]\
    );
\xreg1_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => outx12(7),
      Q => \xreg1_reg_n_0_[7]\
    );
\xreg1_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => outx12(8),
      Q => \xreg1_reg_n_0_[8]\
    );
\xreg1_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => outx12(9),
      Q => \xreg1_reg_n_0_[9]\
    );
\xreg2[0]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(16),
      I1 => Q(0),
      O => \^d\(0)
    );
\xreg2[1]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(1),
      I1 => Q(17),
      I2 => Q(8),
      I3 => Q(0),
      I4 => Q(16),
      O => \^d\(1)
    );
\xreg2[2]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(2),
      I1 => Q(9),
      I2 => Q(18),
      I3 => Q(8),
      I4 => Q(17),
      I5 => \c4/x12/C_1\,
      O => \^d\(2)
    );
\xreg2[2]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(16),
      I1 => Q(0),
      I2 => Q(8),
      I3 => Q(17),
      I4 => Q(1),
      O => \c4/x12/C_1\
    );
\xreg2[3]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(3),
      I1 => \c4/outx11\(3),
      I2 => \c4/x12/C_2\,
      O => \^d\(3)
    );
\xreg2[3]_i_2__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(10),
      I1 => Q(19),
      I2 => Q(9),
      I3 => Q(18),
      I4 => Q(8),
      I5 => Q(17),
      O => \c4/outx11\(3)
    );
\xreg2[3]_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \c4/x12/C_1\,
      I1 => Q(17),
      I2 => Q(8),
      I3 => Q(18),
      I4 => Q(9),
      I5 => Q(2),
      O => \c4/x12/C_2\
    );
\xreg2[4]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(4),
      I1 => Q(11),
      I2 => Q(20),
      I3 => \c4/x11/C_3\,
      I4 => \c4/x12/C_3\,
      O => \^d\(4)
    );
\xreg2[5]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \c4/x12/C_3\,
      I1 => \c4/x11/C_3\,
      I2 => Q(20),
      I3 => Q(11),
      I4 => Q(4),
      I5 => \c4/x12/f6/S\,
      O => \^d\(5)
    );
\xreg2[5]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \c4/x12/C_1\,
      I1 => \c4/outx11\(2),
      I2 => Q(2),
      I3 => \c4/outx11\(3),
      I4 => Q(3),
      O => \c4/x12/C_3\
    );
\xreg2[5]_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(17),
      I1 => Q(8),
      I2 => Q(18),
      I3 => Q(9),
      I4 => Q(19),
      I5 => Q(10),
      O => \c4/x11/C_3\
    );
\xreg2[5]_i_4__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \c4/x11/C_3\,
      I1 => Q(20),
      I2 => Q(11),
      I3 => Q(21),
      I4 => Q(12),
      I5 => Q(5),
      O => \c4/x12/f6/S\
    );
\xreg2[5]_i_5__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(9),
      I1 => Q(18),
      I2 => Q(8),
      I3 => Q(17),
      O => \c4/outx11\(2)
    );
\xreg2[6]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(13),
      I2 => Q(22),
      I3 => \c4/x11/C_5\,
      I4 => \c4/x12/C_5\,
      O => \^d\(6)
    );
\xreg2[6]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \c4/x11/C_3\,
      I1 => Q(20),
      I2 => Q(11),
      I3 => Q(21),
      I4 => Q(12),
      O => \c4/x11/C_5\
    );
\xreg2[6]_i_3__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \c4/x12/C_3\,
      I1 => \c4/outx11\(4),
      I2 => Q(4),
      I3 => \c4/outx11\(5),
      I4 => Q(5),
      O => \c4/x12/C_5\
    );
\xreg2[6]_i_4__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(11),
      I1 => Q(20),
      I2 => \c4/x11/C_3\,
      O => \c4/outx11\(4)
    );
\xreg2[6]_i_5__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(12),
      I1 => Q(21),
      I2 => Q(11),
      I3 => Q(20),
      I4 => \c4/x11/C_3\,
      O => \c4/outx11\(5)
    );
\xreg2[7]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \c4/x12/f8/S\,
      I1 => \c4/x12/C_6\,
      O => \^d\(7)
    );
\xreg2[7]_i_2__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \c4/x11/C_5\,
      I1 => Q(22),
      I2 => Q(13),
      I3 => Q(23),
      I4 => Q(14),
      I5 => Q(7),
      O => \c4/x12/f8/S\
    );
\xreg2[8]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(15),
      I1 => Q(7),
      I2 => Q(14),
      I3 => Q(23),
      I4 => \c4/x11/C_6\,
      I5 => \c4/x12/C_6\,
      O => \^d\(8)
    );
\xreg2[9]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \c4/x12/C_6\,
      I1 => \c4/x11/C_6\,
      I2 => Q(23),
      I3 => Q(14),
      I4 => Q(7),
      I5 => Q(15),
      O => \^d\(9)
    );
\xreg2[9]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \c4/x12/C_5\,
      I1 => \c4/x11/C_5\,
      I2 => Q(22),
      I3 => Q(13),
      I4 => Q(6),
      O => \c4/x12/C_6\
    );
\xreg2[9]_i_3__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \c4/x11/C_5\,
      I1 => Q(22),
      I2 => Q(13),
      O => \c4/x11/C_6\
    );
\xreg2_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => \^d\(0),
      Q => xreg2(0)
    );
\xreg2_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => \^d\(1),
      Q => xreg2(1)
    );
\xreg2_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => \^d\(2),
      Q => xreg2(2)
    );
\xreg2_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => \^d\(3),
      Q => xreg2(3)
    );
\xreg2_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => \^d\(4),
      Q => xreg2(4)
    );
\xreg2_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => \^d\(5),
      Q => xreg2(5)
    );
\xreg2_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => \^d\(6),
      Q => xreg2(6)
    );
\xreg2_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_1,
      D => \^d\(7),
      Q => xreg2(7)
    );
\xreg2_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => \^d\(8),
      Q => xreg2(8)
    );
\xreg2_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => \^d\(9),
      Q => xreg2(9)
    );
\yreg1[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(56),
      I1 => Q(16),
      O => \y12/f1/S\
    );
\yreg1[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(17),
      I1 => Q(57),
      I2 => Q(32),
      I3 => Q(16),
      I4 => Q(56),
      O => outy12(1)
    );
\yreg1[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(18),
      I1 => Q(33),
      I2 => Q(58),
      I3 => Q(32),
      I4 => Q(57),
      I5 => \y12/C_1\,
      O => outy12(2)
    );
\yreg1[2]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(56),
      I1 => Q(16),
      I2 => Q(32),
      I3 => Q(57),
      I4 => Q(17),
      O => \y12/C_1\
    );
\yreg1[3]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(19),
      I1 => outy11(3),
      I2 => \y12/C_2\,
      O => outy12(3)
    );
\yreg1[3]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(34),
      I1 => Q(59),
      I2 => Q(33),
      I3 => Q(58),
      I4 => Q(32),
      I5 => Q(57),
      O => outy11(3)
    );
\yreg1[3]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \y12/C_1\,
      I1 => Q(57),
      I2 => Q(32),
      I3 => Q(58),
      I4 => Q(33),
      I5 => Q(18),
      O => \y12/C_2\
    );
\yreg1[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(20),
      I1 => Q(35),
      I2 => Q(60),
      I3 => \y11/C_3\,
      I4 => \y12/C_3\,
      O => outy12(4)
    );
\yreg1[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \y12/C_3\,
      I1 => \y11/C_3\,
      I2 => Q(60),
      I3 => Q(35),
      I4 => Q(20),
      I5 => \y12/f6/S\,
      O => outy12(5)
    );
\yreg1[5]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y12/C_1\,
      I1 => outy11(2),
      I2 => Q(18),
      I3 => outy11(3),
      I4 => Q(19),
      O => \y12/C_3\
    );
\yreg1[5]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(57),
      I1 => Q(32),
      I2 => Q(58),
      I3 => Q(33),
      I4 => Q(59),
      I5 => Q(34),
      O => \y11/C_3\
    );
\yreg1[5]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y11/C_3\,
      I1 => Q(60),
      I2 => Q(35),
      I3 => Q(61),
      I4 => Q(36),
      I5 => Q(21),
      O => \y12/f6/S\
    );
\yreg1[5]_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(33),
      I1 => Q(58),
      I2 => Q(32),
      I3 => Q(57),
      O => outy11(2)
    );
\yreg1[6]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(22),
      I1 => Q(37),
      I2 => Q(62),
      I3 => \y11/C_5\,
      I4 => \y12/C_5\,
      O => outy12(6)
    );
\yreg1[6]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y11/C_3\,
      I1 => Q(60),
      I2 => Q(35),
      I3 => Q(61),
      I4 => Q(36),
      O => \y11/C_5\
    );
\yreg1[6]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y12/C_3\,
      I1 => outy11(4),
      I2 => Q(20),
      I3 => outy11(5),
      I4 => Q(21),
      O => \y12/C_5\
    );
\yreg1[6]_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(35),
      I1 => Q(60),
      I2 => \y11/C_3\,
      O => outy11(4)
    );
\yreg1[6]_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(36),
      I1 => Q(61),
      I2 => Q(35),
      I3 => Q(60),
      I4 => \y11/C_3\,
      O => outy11(5)
    );
\yreg1[7]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \y12/f8/S\,
      I1 => \y12/C_6\,
      O => outy12(7)
    );
\yreg1[7]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y11/C_5\,
      I1 => Q(62),
      I2 => Q(37),
      I3 => Q(63),
      I4 => Q(38),
      I5 => Q(23),
      O => \y12/f8/S\
    );
\yreg1[8]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(39),
      I1 => Q(23),
      I2 => Q(38),
      I3 => Q(63),
      I4 => \y11/C_6\,
      I5 => \y12/C_6\,
      O => outy12(8)
    );
\yreg1[9]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FF000000000000"
    )
        port map (
      I0 => \full_reg_n_0_[0]\,
      I1 => \^m_axis_tkeep[1]\,
      I2 => m_axis_tready,
      I3 => \full_reg_n_0_[1]\,
      I4 => \valid_reg[1]\(0),
      I5 => hold_reg,
      O => yreg1
    );
\yreg1[9]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \y12/C_6\,
      I1 => \y11/C_6\,
      I2 => Q(63),
      I3 => Q(38),
      I4 => Q(23),
      I5 => Q(39),
      O => outy12(9)
    );
\yreg1[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \y12/C_5\,
      I1 => \y11/C_5\,
      I2 => Q(62),
      I3 => Q(37),
      I4 => Q(22),
      O => \y12/C_6\
    );
\yreg1[9]_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \y11/C_5\,
      I1 => Q(62),
      I2 => Q(37),
      O => \y11/C_6\
    );
\yreg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => \y12/f1/S\,
      Q => \yreg1_reg_n_0_[0]\,
      R => '0'
    );
\yreg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(1),
      Q => \yreg1_reg_n_0_[1]\,
      R => '0'
    );
\yreg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(2),
      Q => \yreg1_reg_n_0_[2]\,
      R => '0'
    );
\yreg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(3),
      Q => \yreg1_reg_n_0_[3]\,
      R => '0'
    );
\yreg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(4),
      Q => \yreg1_reg_n_0_[4]\,
      R => '0'
    );
\yreg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(5),
      Q => \yreg1_reg_n_0_[5]\,
      R => '0'
    );
\yreg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(6),
      Q => \yreg1_reg_n_0_[6]\,
      R => '0'
    );
\yreg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(7),
      Q => \yreg1_reg_n_0_[7]\,
      R => '0'
    );
\yreg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(8),
      Q => \yreg1_reg_n_0_[8]\,
      R => '0'
    );
\yreg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(9),
      Q => \yreg1_reg_n_0_[9]\,
      R => '0'
    );
\yreg2[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(40),
      I1 => Q(0),
      O => \y22/f1/S\
    );
\yreg2[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(1),
      I1 => Q(41),
      I2 => Q(24),
      I3 => Q(0),
      I4 => Q(40),
      O => outy22(1)
    );
\yreg2[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(2),
      I1 => Q(25),
      I2 => Q(42),
      I3 => Q(24),
      I4 => Q(41),
      I5 => \y22/C_1\,
      O => outy22(2)
    );
\yreg2[2]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(40),
      I1 => Q(0),
      I2 => Q(24),
      I3 => Q(41),
      I4 => Q(1),
      O => \y22/C_1\
    );
\yreg2[3]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(3),
      I1 => outy21(3),
      I2 => \y22/C_2\,
      O => outy22(3)
    );
\yreg2[3]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(26),
      I1 => Q(43),
      I2 => Q(25),
      I3 => Q(42),
      I4 => Q(24),
      I5 => Q(41),
      O => outy21(3)
    );
\yreg2[3]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \y22/C_1\,
      I1 => Q(41),
      I2 => Q(24),
      I3 => Q(42),
      I4 => Q(25),
      I5 => Q(2),
      O => \y22/C_2\
    );
\yreg2[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(4),
      I1 => Q(27),
      I2 => Q(44),
      I3 => \y21/C_3\,
      I4 => \y22/C_3\,
      O => outy22(4)
    );
\yreg2[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \y22/C_3\,
      I1 => \y21/C_3\,
      I2 => Q(44),
      I3 => Q(27),
      I4 => Q(4),
      I5 => \y22/f6/S\,
      O => outy22(5)
    );
\yreg2[5]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y22/C_1\,
      I1 => outy21(2),
      I2 => Q(2),
      I3 => outy21(3),
      I4 => Q(3),
      O => \y22/C_3\
    );
\yreg2[5]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(41),
      I1 => Q(24),
      I2 => Q(42),
      I3 => Q(25),
      I4 => Q(43),
      I5 => Q(26),
      O => \y21/C_3\
    );
\yreg2[5]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y21/C_3\,
      I1 => Q(44),
      I2 => Q(27),
      I3 => Q(45),
      I4 => Q(28),
      I5 => Q(5),
      O => \y22/f6/S\
    );
\yreg2[5]_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(25),
      I1 => Q(42),
      I2 => Q(24),
      I3 => Q(41),
      O => outy21(2)
    );
\yreg2[6]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(29),
      I2 => Q(46),
      I3 => \y21/C_5\,
      I4 => \y22/C_5\,
      O => outy22(6)
    );
\yreg2[6]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y21/C_3\,
      I1 => Q(44),
      I2 => Q(27),
      I3 => Q(45),
      I4 => Q(28),
      O => \y21/C_5\
    );
\yreg2[6]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y22/C_3\,
      I1 => outy21(4),
      I2 => Q(4),
      I3 => outy21(5),
      I4 => Q(5),
      O => \y22/C_5\
    );
\yreg2[6]_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(27),
      I1 => Q(44),
      I2 => \y21/C_3\,
      O => outy21(4)
    );
\yreg2[6]_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(28),
      I1 => Q(45),
      I2 => Q(27),
      I3 => Q(44),
      I4 => \y21/C_3\,
      O => outy21(5)
    );
\yreg2[7]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \y22/f8/S\,
      I1 => \y22/C_6\,
      O => outy22(7)
    );
\yreg2[7]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y21/C_5\,
      I1 => Q(46),
      I2 => Q(29),
      I3 => Q(47),
      I4 => Q(30),
      I5 => Q(7),
      O => \y22/f8/S\
    );
\yreg2[8]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(31),
      I1 => Q(7),
      I2 => Q(30),
      I3 => Q(47),
      I4 => \y21/C_6\,
      I5 => \y22/C_6\,
      O => outy22(8)
    );
\yreg2[9]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \y22/C_6\,
      I1 => \y21/C_6\,
      I2 => Q(47),
      I3 => Q(30),
      I4 => Q(7),
      I5 => Q(31),
      O => outy22(9)
    );
\yreg2[9]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \y22/C_5\,
      I1 => \y21/C_5\,
      I2 => Q(46),
      I3 => Q(29),
      I4 => Q(6),
      O => \y22/C_6\
    );
\yreg2[9]_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \y21/C_5\,
      I1 => Q(46),
      I2 => Q(29),
      O => \y21/C_6\
    );
\yreg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => \y22/f1/S\,
      Q => yreg2(0),
      R => '0'
    );
\yreg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(1),
      Q => yreg2(1),
      R => '0'
    );
\yreg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(2),
      Q => yreg2(2),
      R => '0'
    );
\yreg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(3),
      Q => yreg2(3),
      R => '0'
    );
\yreg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(4),
      Q => yreg2(4),
      R => '0'
    );
\yreg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(5),
      Q => yreg2(5),
      R => '0'
    );
\yreg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(6),
      Q => yreg2(6),
      R => '0'
    );
\yreg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(7),
      Q => yreg2(7),
      R => '0'
    );
\yreg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(8),
      Q => yreg2(8),
      R => '0'
    );
\yreg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(9),
      Q => yreg2(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity finalproj_sobelfilteraxistream_0_0_convolution_1 is
  port (
    D : out STD_LOGIC_VECTOR ( 9 downto 0 );
    \out_reg[0]_0\ : out STD_LOGIC;
    \m_axis_tkeep[2]\ : out STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 55 downto 0 );
    reset : in STD_LOGIC;
    \inputvals_reg[119]\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    clock : in STD_LOGIC;
    hold : in STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    \valid_reg[2]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_0 : in STD_LOGIC;
    hold_reg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of finalproj_sobelfilteraxistream_0_0_convolution_1 : entity is "convolution";
end finalproj_sobelfilteraxistream_0_0_convolution_1;

architecture STRUCTURE of finalproj_sobelfilteraxistream_0_0_convolution_1 is
  signal \^d\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal ans1 : STD_LOGIC_VECTOR ( 7 downto 1 );
  signal ans2 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal \c5/outx11\ : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal \c5/x11/C_3\ : STD_LOGIC;
  signal \c5/x11/C_5\ : STD_LOGIC;
  signal \c5/x11/C_6\ : STD_LOGIC;
  signal \c5/x12/C_1\ : STD_LOGIC;
  signal \c5/x12/C_2\ : STD_LOGIC;
  signal \c5/x12/C_3\ : STD_LOGIC;
  signal \c5/x12/C_5\ : STD_LOGIC;
  signal \c5/x12/C_6\ : STD_LOGIC;
  signal \c5/x12/f6/S\ : STD_LOGIC;
  signal \c5/x12/f8/S\ : STD_LOGIC;
  signal \final/C_1\ : STD_LOGIC;
  signal \final/C_2\ : STD_LOGIC;
  signal \final/C_4\ : STD_LOGIC;
  signal \final/C_6\ : STD_LOGIC;
  signal \final/f1/Ca\ : STD_LOGIC;
  signal \final/f2/S\ : STD_LOGIC;
  signal \final/f4/S\ : STD_LOGIC;
  signal \final/f5/S\ : STD_LOGIC;
  signal \final/f6/S\ : STD_LOGIC;
  signal \full[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \full[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \full[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \full_reg_n_0_[0]\ : STD_LOGIC;
  signal \full_reg_n_0_[1]\ : STD_LOGIC;
  signal \^m_axis_tkeep[2]\ : STD_LOGIC;
  signal \out\ : STD_LOGIC;
  signal \out[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \out[0]_i_2__1_n_0\ : STD_LOGIC;
  signal \out[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \out[1]_i_2__1_n_0\ : STD_LOGIC;
  signal \out[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \out[2]_i_2__1_n_0\ : STD_LOGIC;
  signal \out[3]_i_1__1_n_0\ : STD_LOGIC;
  signal \out[3]_i_2__1_n_0\ : STD_LOGIC;
  signal \out[4]_i_1__1_n_0\ : STD_LOGIC;
  signal \out[4]_i_2__1_n_0\ : STD_LOGIC;
  signal \out[5]_i_1__1_n_0\ : STD_LOGIC;
  signal \out[5]_i_2__1_n_0\ : STD_LOGIC;
  signal \out[6]_i_1__1_n_0\ : STD_LOGIC;
  signal \out[6]_i_2__1_n_0\ : STD_LOGIC;
  signal \out[7]_i_25__1_n_0\ : STD_LOGIC;
  signal \out[7]_i_26__1_n_0\ : STD_LOGIC;
  signal \out[7]_i_27__1_n_0\ : STD_LOGIC;
  signal \out[7]_i_28__1_n_0\ : STD_LOGIC;
  signal \out[7]_i_2__1_n_0\ : STD_LOGIC;
  signal \out[7]_i_7__1_n_0\ : STD_LOGIC;
  signal \^out_reg[0]_0\ : STD_LOGIC;
  signal outy11 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outy12 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal outy21 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outy22 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal \s1/C2\ : STD_LOGIC;
  signal \s1/F6/S\ : STD_LOGIC;
  signal \s1/s1/C_2\ : STD_LOGIC;
  signal \s1/s2/C_0\ : STD_LOGIC;
  signal \s1/s2/C_2\ : STD_LOGIC;
  signal \s1/s2/f2/S\ : STD_LOGIC;
  signal \s11[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \s11[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \s11[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \s11[3]_i_1__1_n_0\ : STD_LOGIC;
  signal \s11[4]_i_1__1_n_0\ : STD_LOGIC;
  signal \s11[5]_i_1__1_n_0\ : STD_LOGIC;
  signal \s11[6]_i_1__1_n_0\ : STD_LOGIC;
  signal \s11[7]_i_1__1_n_0\ : STD_LOGIC;
  signal \s11[8]_i_1__1_n_0\ : STD_LOGIC;
  signal \s11[9]_i_1__1_n_0\ : STD_LOGIC;
  signal \s11_reg_n_0_[0]\ : STD_LOGIC;
  signal \s11_reg_n_0_[1]\ : STD_LOGIC;
  signal \s11_reg_n_0_[2]\ : STD_LOGIC;
  signal \s11_reg_n_0_[3]\ : STD_LOGIC;
  signal \s11_reg_n_0_[4]\ : STD_LOGIC;
  signal \s11_reg_n_0_[5]\ : STD_LOGIC;
  signal \s11_reg_n_0_[6]\ : STD_LOGIC;
  signal \s11_reg_n_0_[7]\ : STD_LOGIC;
  signal \s11_reg_n_0_[8]\ : STD_LOGIC;
  signal \s11_reg_n_0_[9]\ : STD_LOGIC;
  signal s12 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \s12[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \s12[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \s12[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \s12[3]_i_1__1_n_0\ : STD_LOGIC;
  signal \s12[4]_i_1__1_n_0\ : STD_LOGIC;
  signal \s12[5]_i_1__1_n_0\ : STD_LOGIC;
  signal \s12[6]_i_1__1_n_0\ : STD_LOGIC;
  signal \s12[7]_i_1__1_n_0\ : STD_LOGIC;
  signal \s12[8]_i_1__1_n_0\ : STD_LOGIC;
  signal \s12[8]_i_2__1_n_0\ : STD_LOGIC;
  signal \s12[8]_i_3__1_n_0\ : STD_LOGIC;
  signal \s12[8]_i_4__1_n_0\ : STD_LOGIC;
  signal \s12[8]_i_5__1_n_0\ : STD_LOGIC;
  signal \s12[8]_i_6__1_n_0\ : STD_LOGIC;
  signal \s12[8]_i_7__1_n_0\ : STD_LOGIC;
  signal \s12[8]_i_8__1_n_0\ : STD_LOGIC;
  signal \s12[8]_i_9__1_n_0\ : STD_LOGIC;
  signal \s12[9]_i_2__1_n_0\ : STD_LOGIC;
  signal \s2/C2\ : STD_LOGIC;
  signal \s2/F6/S\ : STD_LOGIC;
  signal \s2/f5/S\ : STD_LOGIC;
  signal \s2/s1/C_2\ : STD_LOGIC;
  signal \s2/s1/f1/S\ : STD_LOGIC;
  signal \s2/s2/C_0\ : STD_LOGIC;
  signal \s2/s2/C_2\ : STD_LOGIC;
  signal \s2/s2/f2/S\ : STD_LOGIC;
  signal s21 : STD_LOGIC;
  signal \s21[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \s21[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \s21[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \s21[3]_i_1__1_n_0\ : STD_LOGIC;
  signal \s21[4]_i_1__1_n_0\ : STD_LOGIC;
  signal \s21[5]_i_1__1_n_0\ : STD_LOGIC;
  signal \s21[6]_i_1__1_n_0\ : STD_LOGIC;
  signal \s21[7]_i_1__1_n_0\ : STD_LOGIC;
  signal \s21[8]_i_1__1_n_0\ : STD_LOGIC;
  signal \s21[9]_i_1__1_n_0\ : STD_LOGIC;
  signal \s21_reg_n_0_[0]\ : STD_LOGIC;
  signal \s21_reg_n_0_[1]\ : STD_LOGIC;
  signal \s21_reg_n_0_[2]\ : STD_LOGIC;
  signal \s21_reg_n_0_[3]\ : STD_LOGIC;
  signal \s21_reg_n_0_[4]\ : STD_LOGIC;
  signal \s21_reg_n_0_[5]\ : STD_LOGIC;
  signal \s21_reg_n_0_[6]\ : STD_LOGIC;
  signal \s21_reg_n_0_[7]\ : STD_LOGIC;
  signal \s21_reg_n_0_[8]\ : STD_LOGIC;
  signal \s21_reg_n_0_[9]\ : STD_LOGIC;
  signal s22 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \s22[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \s22[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \s22[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \s22[3]_i_1__1_n_0\ : STD_LOGIC;
  signal \s22[4]_i_1__1_n_0\ : STD_LOGIC;
  signal \s22[5]_i_1__1_n_0\ : STD_LOGIC;
  signal \s22[6]_i_1__1_n_0\ : STD_LOGIC;
  signal \s22[7]_i_1__1_n_0\ : STD_LOGIC;
  signal \s22[8]_i_1__1_n_0\ : STD_LOGIC;
  signal \s22[8]_i_2__1_n_0\ : STD_LOGIC;
  signal \s22[8]_i_3__1_n_0\ : STD_LOGIC;
  signal \s22[8]_i_4__1_n_0\ : STD_LOGIC;
  signal \s22[8]_i_5__1_n_0\ : STD_LOGIC;
  signal \s22[8]_i_6__1_n_0\ : STD_LOGIC;
  signal \s22[8]_i_7__1_n_0\ : STD_LOGIC;
  signal \s22[8]_i_8__1_n_0\ : STD_LOGIC;
  signal \s22[8]_i_9__1_n_0\ : STD_LOGIC;
  signal \s22[9]_i_1__1_n_0\ : STD_LOGIC;
  signal xreg1 : STD_LOGIC;
  signal \xreg1_reg_n_0_[0]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[1]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[2]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[3]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[4]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[5]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[6]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[7]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[8]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[9]\ : STD_LOGIC;
  signal xreg2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \y11/C_3\ : STD_LOGIC;
  signal \y11/C_5\ : STD_LOGIC;
  signal \y11/C_6\ : STD_LOGIC;
  signal \y12/C_1\ : STD_LOGIC;
  signal \y12/C_2\ : STD_LOGIC;
  signal \y12/C_3\ : STD_LOGIC;
  signal \y12/C_5\ : STD_LOGIC;
  signal \y12/C_6\ : STD_LOGIC;
  signal \y12/f1/S\ : STD_LOGIC;
  signal \y12/f6/S\ : STD_LOGIC;
  signal \y12/f8/S\ : STD_LOGIC;
  signal \y21/C_3\ : STD_LOGIC;
  signal \y21/C_5\ : STD_LOGIC;
  signal \y21/C_6\ : STD_LOGIC;
  signal \y22/C_1\ : STD_LOGIC;
  signal \y22/C_2\ : STD_LOGIC;
  signal \y22/C_3\ : STD_LOGIC;
  signal \y22/C_5\ : STD_LOGIC;
  signal \y22/C_6\ : STD_LOGIC;
  signal \y22/f1/S\ : STD_LOGIC;
  signal \y22/f6/S\ : STD_LOGIC;
  signal \y22/f8/S\ : STD_LOGIC;
  signal yreg1 : STD_LOGIC;
  signal \yreg1_reg_n_0_[0]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[1]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[2]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[3]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[4]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[5]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[6]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[7]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[8]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[9]\ : STD_LOGIC;
  signal yreg2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \out[0]_i_2__1\ : label is "soft_lutpair114";
  attribute SOFT_HLUTNM of \out[1]_i_2__1\ : label is "soft_lutpair114";
  attribute SOFT_HLUTNM of \out[1]_i_3__1\ : label is "soft_lutpair129";
  attribute SOFT_HLUTNM of \out[2]_i_6__1\ : label is "soft_lutpair133";
  attribute SOFT_HLUTNM of \out[4]_i_7__1\ : label is "soft_lutpair133";
  attribute SOFT_HLUTNM of \out[4]_i_9__1\ : label is "soft_lutpair129";
  attribute SOFT_HLUTNM of \out[6]_i_2__1\ : label is "soft_lutpair124";
  attribute SOFT_HLUTNM of \out[7]_i_10__1\ : label is "soft_lutpair121";
  attribute SOFT_HLUTNM of \out[7]_i_12__1\ : label is "soft_lutpair153";
  attribute SOFT_HLUTNM of \out[7]_i_13__1\ : label is "soft_lutpair155";
  attribute SOFT_HLUTNM of \out[7]_i_14__1\ : label is "soft_lutpair123";
  attribute SOFT_HLUTNM of \out[7]_i_15__1\ : label is "soft_lutpair121";
  attribute SOFT_HLUTNM of \out[7]_i_20__1\ : label is "soft_lutpair116";
  attribute SOFT_HLUTNM of \out[7]_i_21__1\ : label is "soft_lutpair122";
  attribute SOFT_HLUTNM of \out[7]_i_23__1\ : label is "soft_lutpair116";
  attribute SOFT_HLUTNM of \out[7]_i_24__1\ : label is "soft_lutpair122";
  attribute SOFT_HLUTNM of \out[7]_i_26__1\ : label is "soft_lutpair153";
  attribute SOFT_HLUTNM of \out[7]_i_28__1\ : label is "soft_lutpair155";
  attribute SOFT_HLUTNM of \out[7]_i_6__1\ : label is "soft_lutpair124";
  attribute SOFT_HLUTNM of \out[7]_i_9__1\ : label is "soft_lutpair123";
  attribute SOFT_HLUTNM of \s11[0]_i_1__1\ : label is "soft_lutpair150";
  attribute SOFT_HLUTNM of \s11[1]_i_1__1\ : label is "soft_lutpair149";
  attribute SOFT_HLUTNM of \s11[2]_i_1__1\ : label is "soft_lutpair148";
  attribute SOFT_HLUTNM of \s11[3]_i_1__1\ : label is "soft_lutpair152";
  attribute SOFT_HLUTNM of \s11[4]_i_1__1\ : label is "soft_lutpair147";
  attribute SOFT_HLUTNM of \s11[5]_i_1__1\ : label is "soft_lutpair146";
  attribute SOFT_HLUTNM of \s11[6]_i_1__1\ : label is "soft_lutpair145";
  attribute SOFT_HLUTNM of \s11[7]_i_1__1\ : label is "soft_lutpair151";
  attribute SOFT_HLUTNM of \s11[8]_i_1__1\ : label is "soft_lutpair144";
  attribute SOFT_HLUTNM of \s12[0]_i_1__1\ : label is "soft_lutpair150";
  attribute SOFT_HLUTNM of \s12[1]_i_1__1\ : label is "soft_lutpair149";
  attribute SOFT_HLUTNM of \s12[2]_i_1__1\ : label is "soft_lutpair148";
  attribute SOFT_HLUTNM of \s12[3]_i_1__1\ : label is "soft_lutpair152";
  attribute SOFT_HLUTNM of \s12[4]_i_1__1\ : label is "soft_lutpair147";
  attribute SOFT_HLUTNM of \s12[5]_i_1__1\ : label is "soft_lutpair146";
  attribute SOFT_HLUTNM of \s12[6]_i_1__1\ : label is "soft_lutpair145";
  attribute SOFT_HLUTNM of \s12[7]_i_1__1\ : label is "soft_lutpair151";
  attribute SOFT_HLUTNM of \s12[8]_i_1__1\ : label is "soft_lutpair144";
  attribute SOFT_HLUTNM of \s12[8]_i_2__1\ : label is "soft_lutpair128";
  attribute SOFT_HLUTNM of \s12[8]_i_4__1\ : label is "soft_lutpair130";
  attribute SOFT_HLUTNM of \s12[8]_i_6__1\ : label is "soft_lutpair131";
  attribute SOFT_HLUTNM of \s12[8]_i_7__1\ : label is "soft_lutpair130";
  attribute SOFT_HLUTNM of \s12[8]_i_8__1\ : label is "soft_lutpair131";
  attribute SOFT_HLUTNM of \s12[9]_i_2__1\ : label is "soft_lutpair128";
  attribute SOFT_HLUTNM of \s21[0]_i_1__1\ : label is "soft_lutpair143";
  attribute SOFT_HLUTNM of \s21[1]_i_1__1\ : label is "soft_lutpair142";
  attribute SOFT_HLUTNM of \s21[2]_i_1__1\ : label is "soft_lutpair135";
  attribute SOFT_HLUTNM of \s21[3]_i_1__1\ : label is "soft_lutpair136";
  attribute SOFT_HLUTNM of \s21[4]_i_1__1\ : label is "soft_lutpair137";
  attribute SOFT_HLUTNM of \s21[5]_i_1__1\ : label is "soft_lutpair139";
  attribute SOFT_HLUTNM of \s21[6]_i_1__1\ : label is "soft_lutpair138";
  attribute SOFT_HLUTNM of \s21[7]_i_1__1\ : label is "soft_lutpair141";
  attribute SOFT_HLUTNM of \s21[8]_i_1__1\ : label is "soft_lutpair140";
  attribute SOFT_HLUTNM of \s22[0]_i_1__1\ : label is "soft_lutpair143";
  attribute SOFT_HLUTNM of \s22[1]_i_1__1\ : label is "soft_lutpair142";
  attribute SOFT_HLUTNM of \s22[2]_i_1__1\ : label is "soft_lutpair135";
  attribute SOFT_HLUTNM of \s22[3]_i_1__1\ : label is "soft_lutpair136";
  attribute SOFT_HLUTNM of \s22[4]_i_1__1\ : label is "soft_lutpair137";
  attribute SOFT_HLUTNM of \s22[5]_i_1__1\ : label is "soft_lutpair139";
  attribute SOFT_HLUTNM of \s22[6]_i_1__1\ : label is "soft_lutpair138";
  attribute SOFT_HLUTNM of \s22[7]_i_1__1\ : label is "soft_lutpair141";
  attribute SOFT_HLUTNM of \s22[8]_i_1__1\ : label is "soft_lutpair140";
  attribute SOFT_HLUTNM of \s22[8]_i_2__1\ : label is "soft_lutpair127";
  attribute SOFT_HLUTNM of \s22[8]_i_4__1\ : label is "soft_lutpair134";
  attribute SOFT_HLUTNM of \s22[8]_i_6__1\ : label is "soft_lutpair132";
  attribute SOFT_HLUTNM of \s22[8]_i_7__1\ : label is "soft_lutpair134";
  attribute SOFT_HLUTNM of \s22[8]_i_8__1\ : label is "soft_lutpair132";
  attribute SOFT_HLUTNM of \s22[9]_i_1__1\ : label is "soft_lutpair127";
  attribute SOFT_HLUTNM of \xreg2[0]_i_1__1\ : label is "soft_lutpair156";
  attribute SOFT_HLUTNM of \xreg2[1]_i_1__1\ : label is "soft_lutpair125";
  attribute SOFT_HLUTNM of \xreg2[2]_i_2__1\ : label is "soft_lutpair125";
  attribute SOFT_HLUTNM of \xreg2[3]_i_1__1\ : label is "soft_lutpair154";
  attribute SOFT_HLUTNM of \xreg2[4]_i_1__1\ : label is "soft_lutpair126";
  attribute SOFT_HLUTNM of \xreg2[6]_i_1__1\ : label is "soft_lutpair109";
  attribute SOFT_HLUTNM of \xreg2[6]_i_2__1\ : label is "soft_lutpair112";
  attribute SOFT_HLUTNM of \xreg2[6]_i_4__1\ : label is "soft_lutpair126";
  attribute SOFT_HLUTNM of \xreg2[6]_i_5__1\ : label is "soft_lutpair112";
  attribute SOFT_HLUTNM of \xreg2[9]_i_2__1\ : label is "soft_lutpair109";
  attribute SOFT_HLUTNM of \yreg1[1]_i_1__1\ : label is "soft_lutpair117";
  attribute SOFT_HLUTNM of \yreg1[2]_i_2__1\ : label is "soft_lutpair117";
  attribute SOFT_HLUTNM of \yreg1[4]_i_1__1\ : label is "soft_lutpair120";
  attribute SOFT_HLUTNM of \yreg1[6]_i_1__1\ : label is "soft_lutpair110";
  attribute SOFT_HLUTNM of \yreg1[6]_i_2__1\ : label is "soft_lutpair111";
  attribute SOFT_HLUTNM of \yreg1[6]_i_4__1\ : label is "soft_lutpair120";
  attribute SOFT_HLUTNM of \yreg1[6]_i_5__1\ : label is "soft_lutpair111";
  attribute SOFT_HLUTNM of \yreg1[9]_i_3__0\ : label is "soft_lutpair110";
  attribute SOFT_HLUTNM of \yreg2[0]_i_1__1\ : label is "soft_lutpair156";
  attribute SOFT_HLUTNM of \yreg2[1]_i_1__1\ : label is "soft_lutpair118";
  attribute SOFT_HLUTNM of \yreg2[2]_i_2__1\ : label is "soft_lutpair118";
  attribute SOFT_HLUTNM of \yreg2[3]_i_1__1\ : label is "soft_lutpair154";
  attribute SOFT_HLUTNM of \yreg2[4]_i_1__1\ : label is "soft_lutpair115";
  attribute SOFT_HLUTNM of \yreg2[6]_i_1__1\ : label is "soft_lutpair113";
  attribute SOFT_HLUTNM of \yreg2[6]_i_2__1\ : label is "soft_lutpair119";
  attribute SOFT_HLUTNM of \yreg2[6]_i_4__1\ : label is "soft_lutpair115";
  attribute SOFT_HLUTNM of \yreg2[6]_i_5__1\ : label is "soft_lutpair119";
  attribute SOFT_HLUTNM of \yreg2[9]_i_2__1\ : label is "soft_lutpair113";
begin
  D(9 downto 0) <= \^d\(9 downto 0);
  \m_axis_tkeep[2]\ <= \^m_axis_tkeep[2]\;
  \out_reg[0]_0\ <= \^out_reg[0]_0\;
\full[0]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEFEEEEE44444444"
    )
        port map (
      I0 => hold,
      I1 => \valid_reg[2]\(0),
      I2 => \full_reg_n_0_[1]\,
      I3 => m_axis_tready,
      I4 => \^m_axis_tkeep[2]\,
      I5 => \full_reg_n_0_[0]\,
      O => \full[0]_i_1__1_n_0\
    );
\full[1]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEFE4444"
    )
        port map (
      I0 => hold,
      I1 => \full_reg_n_0_[0]\,
      I2 => \^m_axis_tkeep[2]\,
      I3 => m_axis_tready,
      I4 => \full_reg_n_0_[1]\,
      O => \full[1]_i_1__1_n_0\
    );
\full[2]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFF4040"
    )
        port map (
      I0 => hold,
      I1 => reset,
      I2 => \full_reg_n_0_[1]\,
      I3 => m_axis_tready,
      I4 => \^m_axis_tkeep[2]\,
      O => \full[2]_i_1__1_n_0\
    );
\full_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => reset_0,
      D => \full[0]_i_1__1_n_0\,
      Q => \full_reg_n_0_[0]\
    );
\full_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => reset_0,
      D => \full[1]_i_1__1_n_0\,
      Q => \full_reg_n_0_[1]\
    );
\full_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => reset_0,
      D => \full[2]_i_1__1_n_0\,
      Q => \^m_axis_tkeep[2]\
    );
\out[0]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[0]_i_2__1_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__1_n_0\,
      O => \out[0]_i_1__1_n_0\
    );
\out[0]_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s11_reg_n_0_[0]\,
      O => \out[0]_i_2__1_n_0\
    );
\out[1]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[1]_i_2__1_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__1_n_0\,
      O => \out[1]_i_1__1_n_0\
    );
\out[1]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA9696AA"
    )
        port map (
      I0 => \final/f2/S\,
      I1 => \s11_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s21_reg_n_0_[0]\,
      I4 => s22(0),
      O => \out[1]_i_2__1_n_0\
    );
\out[1]_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D22D2DD2"
    )
        port map (
      I0 => s12(0),
      I1 => \s11_reg_n_0_[0]\,
      I2 => \s11_reg_n_0_[1]\,
      I3 => s12(1),
      I4 => ans2(1),
      O => \final/f2/S\
    );
\out[2]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[2]_i_2__1_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__1_n_0\,
      O => \out[2]_i_1__1_n_0\
    );
\out[2]_i_2__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => ans1(2),
      I1 => ans2(2),
      I2 => \final/C_1\,
      O => \out[2]_i_2__1_n_0\
    );
\out[2]_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966996969666966"
    )
        port map (
      I0 => s12(2),
      I1 => \s11_reg_n_0_[2]\,
      I2 => \s11_reg_n_0_[1]\,
      I3 => s12(1),
      I4 => \s11_reg_n_0_[0]\,
      I5 => s12(0),
      O => ans1(2)
    );
\out[2]_i_4__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966996969666966"
    )
        port map (
      I0 => s22(2),
      I1 => \s21_reg_n_0_[2]\,
      I2 => \s21_reg_n_0_[1]\,
      I3 => s22(1),
      I4 => \s21_reg_n_0_[0]\,
      I5 => s22(0),
      O => ans2(2)
    );
\out[2]_i_5__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0CC0D44D4DD40CC0"
    )
        port map (
      I0 => \s2/s1/f1/S\,
      I1 => ans2(1),
      I2 => s12(1),
      I3 => \s11_reg_n_0_[1]\,
      I4 => \s11_reg_n_0_[0]\,
      I5 => s12(0),
      O => \final/C_1\
    );
\out[2]_i_6__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[0]\,
      I1 => s22(0),
      O => \s2/s1/f1/S\
    );
\out[3]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[3]_i_2__1_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__1_n_0\,
      O => \out[3]_i_1__1_n_0\
    );
\out[3]_i_2__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \final/f4/S\,
      I1 => \final/C_2\,
      O => \out[3]_i_2__1_n_0\
    );
\out[3]_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => \s11_reg_n_0_[3]\,
      I2 => s12(3),
      I3 => \s2/s1/C_2\,
      I4 => \s21_reg_n_0_[3]\,
      I5 => s22(3),
      O => \final/f4/S\
    );
\out[4]_i_10__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF0DFFFF0000DF0D"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s22(1),
      I3 => \s21_reg_n_0_[1]\,
      I4 => s22(2),
      I5 => \s21_reg_n_0_[2]\,
      O => \s2/s1/C_2\
    );
\out[4]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[4]_i_2__1_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__1_n_0\,
      O => \out[4]_i_1__1_n_0\
    );
\out[4]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"711717718EE8E88E"
    )
        port map (
      I0 => \final/C_2\,
      I1 => ans2(3),
      I2 => s12(3),
      I3 => \s11_reg_n_0_[3]\,
      I4 => \s1/s1/C_2\,
      I5 => \final/f5/S\,
      O => \out[4]_i_2__1_n_0\
    );
\out[4]_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/f1/Ca\,
      I1 => ans2(1),
      I2 => ans1(1),
      I3 => ans2(2),
      I4 => ans1(2),
      O => \final/C_2\
    );
\out[4]_i_4__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(3),
      I1 => \s21_reg_n_0_[3]\,
      I2 => \s2/s1/C_2\,
      O => ans2(3)
    );
\out[4]_i_5__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF0DFFFF0000DF0D"
    )
        port map (
      I0 => s12(0),
      I1 => \s11_reg_n_0_[0]\,
      I2 => s12(1),
      I3 => \s11_reg_n_0_[1]\,
      I4 => s12(2),
      I5 => \s11_reg_n_0_[2]\,
      O => \s1/s1/C_2\
    );
\out[4]_i_6__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B24D4DB24DB2B24D"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => s12(3),
      I2 => \s11_reg_n_0_[3]\,
      I3 => \s11_reg_n_0_[4]\,
      I4 => s12(4),
      I5 => ans2(4),
      O => \final/f5/S\
    );
\out[4]_i_7__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0660"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s11_reg_n_0_[0]\,
      O => \final/f1/Ca\
    );
\out[4]_i_8__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => s22(1),
      I1 => \s21_reg_n_0_[1]\,
      I2 => \s21_reg_n_0_[0]\,
      I3 => s22(0),
      O => ans2(1)
    );
\out[4]_i_9__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => s12(1),
      I1 => \s11_reg_n_0_[1]\,
      I2 => \s11_reg_n_0_[0]\,
      I3 => s12(0),
      O => ans1(1)
    );
\out[5]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[5]_i_2__1_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__1_n_0\,
      O => \out[5]_i_1__1_n_0\
    );
\out[5]_i_2__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \final/f6/S\,
      I1 => \final/C_4\,
      O => \out[5]_i_2__1_n_0\
    );
\out[5]_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \s1/s2/C_0\,
      I1 => \s11_reg_n_0_[5]\,
      I2 => s12(5),
      I3 => \s2/s2/C_0\,
      I4 => \s21_reg_n_0_[5]\,
      I5 => s22(5),
      O => \final/f6/S\
    );
\out[6]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[6]_i_2__1_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_7__1_n_0\,
      O => \out[6]_i_1__1_n_0\
    );
\out[6]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => ans2(6),
      I1 => ans1(6),
      I2 => ans1(5),
      I3 => ans2(5),
      I4 => \final/C_4\,
      O => \out[6]_i_2__1_n_0\
    );
\out[7]_i_10__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s1/s2/C_0\,
      I1 => s12(5),
      I2 => \s11_reg_n_0_[5]\,
      I3 => s12(6),
      I4 => \s11_reg_n_0_[6]\,
      O => \s1/s2/C_2\
    );
\out[7]_i_11__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/C_2\,
      I1 => ans2(3),
      I2 => ans1(3),
      I3 => ans2(4),
      I4 => ans1(4),
      O => \final/C_4\
    );
\out[7]_i_12__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(5),
      I1 => \s21_reg_n_0_[5]\,
      I2 => \s2/s2/C_0\,
      O => ans2(5)
    );
\out[7]_i_13__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(5),
      I1 => \s11_reg_n_0_[5]\,
      I2 => \s1/s2/C_0\,
      O => ans1(5)
    );
\out[7]_i_14__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s22(6),
      I1 => \s21_reg_n_0_[6]\,
      I2 => \s21_reg_n_0_[5]\,
      I3 => s22(5),
      I4 => \s2/s2/C_0\,
      O => ans2(6)
    );
\out[7]_i_15__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s12(6),
      I1 => \s11_reg_n_0_[6]\,
      I2 => \s11_reg_n_0_[5]\,
      I3 => s12(5),
      I4 => \s1/s2/C_0\,
      O => ans1(6)
    );
\out[7]_i_16__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s11_reg_n_0_[9]\,
      I1 => s12(9),
      O => \s1/F6/S\
    );
\out[7]_i_17__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[8]\,
      I1 => s22(8),
      O => \s2/f5/S\
    );
\out[7]_i_18__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EF0EFFFF0000EF0E"
    )
        port map (
      I0 => \out[7]_i_25__1_n_0\,
      I1 => \out[7]_i_26__1_n_0\,
      I2 => s22(6),
      I3 => \s21_reg_n_0_[6]\,
      I4 => s22(7),
      I5 => \s21_reg_n_0_[7]\,
      O => \s2/C2\
    );
\out[7]_i_19__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EF0EFFFF0000EF0E"
    )
        port map (
      I0 => \out[7]_i_27__1_n_0\,
      I1 => \out[7]_i_28__1_n_0\,
      I2 => s12(6),
      I3 => \s11_reg_n_0_[6]\,
      I4 => s12(7),
      I5 => \s11_reg_n_0_[7]\,
      O => \s1/C2\
    );
\out[7]_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00D0"
    )
        port map (
      I0 => \^m_axis_tkeep[2]\,
      I1 => m_axis_tready,
      I2 => \full_reg_n_0_[1]\,
      I3 => hold,
      O => \out\
    );
\out[7]_i_20__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s2/s1/C_2\,
      I1 => s22(3),
      I2 => \s21_reg_n_0_[3]\,
      I3 => s22(4),
      I4 => \s21_reg_n_0_[4]\,
      O => \s2/s2/C_0\
    );
\out[7]_i_21__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => s12(3),
      I2 => \s11_reg_n_0_[3]\,
      I3 => s12(4),
      I4 => \s11_reg_n_0_[4]\,
      O => \s1/s2/C_0\
    );
\out[7]_i_22__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(3),
      I1 => \s11_reg_n_0_[3]\,
      I2 => \s1/s1/C_2\,
      O => ans1(3)
    );
\out[7]_i_23__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s22(4),
      I1 => \s21_reg_n_0_[4]\,
      I2 => \s21_reg_n_0_[3]\,
      I3 => s22(3),
      I4 => \s2/s1/C_2\,
      O => ans2(4)
    );
\out[7]_i_24__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s12(4),
      I1 => \s11_reg_n_0_[4]\,
      I2 => \s11_reg_n_0_[3]\,
      I3 => s12(3),
      I4 => \s1/s1/C_2\,
      O => ans1(4)
    );
\out[7]_i_25__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A088A8A08088A08"
    )
        port map (
      I0 => \s2/s2/f2/S\,
      I1 => \s21_reg_n_0_[4]\,
      I2 => s22(4),
      I3 => \s21_reg_n_0_[3]\,
      I4 => s22(3),
      I5 => \s2/s1/C_2\,
      O => \out[7]_i_25__1_n_0\
    );
\out[7]_i_26__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \s21_reg_n_0_[5]\,
      I1 => s22(5),
      O => \out[7]_i_26__1_n_0\
    );
\out[7]_i_27__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A088A8A08088A08"
    )
        port map (
      I0 => \s1/s2/f2/S\,
      I1 => \s11_reg_n_0_[4]\,
      I2 => s12(4),
      I3 => \s11_reg_n_0_[3]\,
      I4 => s12(3),
      I5 => \s1/s1/C_2\,
      O => \out[7]_i_27__1_n_0\
    );
\out[7]_i_28__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \s11_reg_n_0_[5]\,
      I1 => s12(5),
      O => \out[7]_i_28__1_n_0\
    );
\out[7]_i_29__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[5]\,
      I1 => s22(5),
      O => \s2/s2/f2/S\
    );
\out[7]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => ans2(9),
      I1 => ans1(7),
      I2 => ans2(7),
      I3 => \final/C_6\,
      I4 => \out[7]_i_7__1_n_0\,
      O => \out[7]_i_2__1_n_0\
    );
\out[7]_i_30__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s11_reg_n_0_[5]\,
      I1 => s12(5),
      O => \s1/s2/f2/S\
    );
\out[7]_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset,
      O => \^out_reg[0]_0\
    );
\out[7]_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"65A66565A6A665A6"
    )
        port map (
      I0 => \s2/F6/S\,
      I1 => \s21_reg_n_0_[8]\,
      I2 => s22(8),
      I3 => \s21_reg_n_0_[7]\,
      I4 => s22(7),
      I5 => \s2/s2/C_2\,
      O => ans2(9)
    );
\out[7]_i_4__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(7),
      I1 => \s11_reg_n_0_[7]\,
      I2 => \s1/s2/C_2\,
      O => ans1(7)
    );
\out[7]_i_5__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(7),
      I1 => \s21_reg_n_0_[7]\,
      I2 => \s2/s2/C_2\,
      O => ans2(7)
    );
\out[7]_i_6__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/C_4\,
      I1 => ans2(5),
      I2 => ans1(5),
      I3 => ans2(6),
      I4 => ans1(6),
      O => \final/C_6\
    );
\out[7]_i_7__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DFFFF7DFF7DBEFF"
    )
        port map (
      I0 => \s1/F6/S\,
      I1 => \s2/f5/S\,
      I2 => \s2/C2\,
      I3 => s12(8),
      I4 => \s11_reg_n_0_[8]\,
      I5 => \s1/C2\,
      O => \out[7]_i_7__1_n_0\
    );
\out[7]_i_8__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[9]\,
      I1 => s22(9),
      O => \s2/F6/S\
    );
\out[7]_i_9__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s2/s2/C_0\,
      I1 => s22(5),
      I2 => \s21_reg_n_0_[5]\,
      I3 => s22(6),
      I4 => \s21_reg_n_0_[6]\,
      O => \s2/s2/C_2\
    );
\out_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^out_reg[0]_0\,
      D => \out[0]_i_1__1_n_0\,
      Q => m_axis_tdata(0)
    );
\out_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^out_reg[0]_0\,
      D => \out[1]_i_1__1_n_0\,
      Q => m_axis_tdata(1)
    );
\out_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^out_reg[0]_0\,
      D => \out[2]_i_1__1_n_0\,
      Q => m_axis_tdata(2)
    );
\out_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^out_reg[0]_0\,
      D => \out[3]_i_1__1_n_0\,
      Q => m_axis_tdata(3)
    );
\out_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^out_reg[0]_0\,
      D => \out[4]_i_1__1_n_0\,
      Q => m_axis_tdata(4)
    );
\out_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^out_reg[0]_0\,
      D => \out[5]_i_1__1_n_0\,
      Q => m_axis_tdata(5)
    );
\out_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^out_reg[0]_0\,
      D => \out[6]_i_1__1_n_0\,
      Q => m_axis_tdata(6)
    );
\out_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^out_reg[0]_0\,
      D => \out[7]_i_2__1_n_0\,
      Q => m_axis_tdata(7)
    );
\s11[0]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(0),
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => \xreg1_reg_n_0_[0]\,
      O => \s11[0]_i_1__1_n_0\
    );
\s11[1]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(1),
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => \xreg1_reg_n_0_[1]\,
      O => \s11[1]_i_1__1_n_0\
    );
\s11[2]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(2),
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => \xreg1_reg_n_0_[2]\,
      O => \s11[2]_i_1__1_n_0\
    );
\s11[3]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(3),
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => \xreg1_reg_n_0_[3]\,
      O => \s11[3]_i_1__1_n_0\
    );
\s11[4]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(4),
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => \xreg1_reg_n_0_[4]\,
      O => \s11[4]_i_1__1_n_0\
    );
\s11[5]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(5),
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => \xreg1_reg_n_0_[5]\,
      O => \s11[5]_i_1__1_n_0\
    );
\s11[6]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(6),
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => \xreg1_reg_n_0_[6]\,
      O => \s11[6]_i_1__1_n_0\
    );
\s11[7]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(7),
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => \xreg1_reg_n_0_[7]\,
      O => \s11[7]_i_1__1_n_0\
    );
\s11[8]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(8),
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => \xreg1_reg_n_0_[8]\,
      O => \s11[8]_i_1__1_n_0\
    );
\s11[9]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \xreg1_reg_n_0_[9]\,
      I1 => xreg2(9),
      O => \s11[9]_i_1__1_n_0\
    );
\s11_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s11[0]_i_1__1_n_0\,
      Q => \s11_reg_n_0_[0]\
    );
\s11_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s11[1]_i_1__1_n_0\,
      Q => \s11_reg_n_0_[1]\
    );
\s11_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s11[2]_i_1__1_n_0\,
      Q => \s11_reg_n_0_[2]\
    );
\s11_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s11[3]_i_1__1_n_0\,
      Q => \s11_reg_n_0_[3]\
    );
\s11_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s11[4]_i_1__1_n_0\,
      Q => \s11_reg_n_0_[4]\
    );
\s11_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s11[5]_i_1__1_n_0\,
      Q => \s11_reg_n_0_[5]\
    );
\s11_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s11[6]_i_1__1_n_0\,
      Q => \s11_reg_n_0_[6]\
    );
\s11_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s11[7]_i_1__1_n_0\,
      Q => \s11_reg_n_0_[7]\
    );
\s11_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s11[8]_i_1__1_n_0\,
      Q => \s11_reg_n_0_[8]\
    );
\s11_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s11[9]_i_1__1_n_0\,
      Q => \s11_reg_n_0_[9]\
    );
\s12[0]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[0]\,
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => xreg2(0),
      O => \s12[0]_i_1__1_n_0\
    );
\s12[1]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[1]\,
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => xreg2(1),
      O => \s12[1]_i_1__1_n_0\
    );
\s12[2]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[2]\,
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => xreg2(2),
      O => \s12[2]_i_1__1_n_0\
    );
\s12[3]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[3]\,
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => xreg2(3),
      O => \s12[3]_i_1__1_n_0\
    );
\s12[4]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[4]\,
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => xreg2(4),
      O => \s12[4]_i_1__1_n_0\
    );
\s12[5]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[5]\,
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => xreg2(5),
      O => \s12[5]_i_1__1_n_0\
    );
\s12[6]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[6]\,
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => xreg2(6),
      O => \s12[6]_i_1__1_n_0\
    );
\s12[7]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[7]\,
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => xreg2(7),
      O => \s12[7]_i_1__1_n_0\
    );
\s12[8]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[8]\,
      I1 => \s12[8]_i_2__1_n_0\,
      I2 => xreg2(8),
      O => \s12[8]_i_1__1_n_0\
    );
\s12[8]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B200FFB2"
    )
        port map (
      I0 => \s12[8]_i_3__1_n_0\,
      I1 => \xreg1_reg_n_0_[8]\,
      I2 => xreg2(8),
      I3 => xreg2(9),
      I4 => \xreg1_reg_n_0_[9]\,
      O => \s12[8]_i_2__1_n_0\
    );
\s12[8]_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7050FFFF"
    )
        port map (
      I0 => \s12[8]_i_4__1_n_0\,
      I1 => \s12[8]_i_5__1_n_0\,
      I2 => \s12[8]_i_6__1_n_0\,
      I3 => \s12[8]_i_7__1_n_0\,
      I4 => \s12[8]_i_8__1_n_0\,
      O => \s12[8]_i_3__1_n_0\
    );
\s12[8]_i_4__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => \xreg1_reg_n_0_[4]\,
      I1 => xreg2(4),
      I2 => xreg2(5),
      I3 => \xreg1_reg_n_0_[5]\,
      O => \s12[8]_i_4__1_n_0\
    );
\s12[8]_i_5__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2F22BF2F"
    )
        port map (
      I0 => \xreg1_reg_n_0_[3]\,
      I1 => xreg2(3),
      I2 => \s12[8]_i_9__1_n_0\,
      I3 => \xreg1_reg_n_0_[2]\,
      I4 => xreg2(2),
      O => \s12[8]_i_5__1_n_0\
    );
\s12[8]_i_6__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BB2B"
    )
        port map (
      I0 => xreg2(7),
      I1 => \xreg1_reg_n_0_[7]\,
      I2 => \xreg1_reg_n_0_[6]\,
      I3 => xreg2(6),
      O => \s12[8]_i_6__1_n_0\
    );
\s12[8]_i_7__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => xreg2(4),
      I1 => \xreg1_reg_n_0_[4]\,
      I2 => \xreg1_reg_n_0_[5]\,
      I3 => xreg2(5),
      O => \s12[8]_i_7__1_n_0\
    );
\s12[8]_i_8__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B0FB"
    )
        port map (
      I0 => \xreg1_reg_n_0_[6]\,
      I1 => xreg2(6),
      I2 => \xreg1_reg_n_0_[7]\,
      I3 => xreg2(7),
      O => \s12[8]_i_8__1_n_0\
    );
\s12[8]_i_9__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40F440F4FFFF40F4"
    )
        port map (
      I0 => \xreg1_reg_n_0_[0]\,
      I1 => xreg2(0),
      I2 => xreg2(1),
      I3 => \xreg1_reg_n_0_[1]\,
      I4 => xreg2(3),
      I5 => \xreg1_reg_n_0_[3]\,
      O => \s12[8]_i_9__1_n_0\
    );
\s12[9]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000DF00"
    )
        port map (
      I0 => \full_reg_n_0_[1]\,
      I1 => m_axis_tready,
      I2 => \^m_axis_tkeep[2]\,
      I3 => \full_reg_n_0_[0]\,
      I4 => hold,
      O => s21
    );
\s12[9]_i_2__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[9]\,
      I1 => xreg2(9),
      O => \s12[9]_i_2__1_n_0\
    );
\s12_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s12[0]_i_1__1_n_0\,
      Q => s12(0)
    );
\s12_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s12[1]_i_1__1_n_0\,
      Q => s12(1)
    );
\s12_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s12[2]_i_1__1_n_0\,
      Q => s12(2)
    );
\s12_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s12[3]_i_1__1_n_0\,
      Q => s12(3)
    );
\s12_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s12[4]_i_1__1_n_0\,
      Q => s12(4)
    );
\s12_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s12[5]_i_1__1_n_0\,
      Q => s12(5)
    );
\s12_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s12[6]_i_1__1_n_0\,
      Q => s12(6)
    );
\s12_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s12[7]_i_1__1_n_0\,
      Q => s12(7)
    );
\s12_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s12[8]_i_1__1_n_0\,
      Q => s12(8)
    );
\s12_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s12[9]_i_2__1_n_0\,
      Q => s12(9)
    );
\s21[0]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(0),
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => \yreg1_reg_n_0_[0]\,
      O => \s21[0]_i_1__1_n_0\
    );
\s21[1]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(1),
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => \yreg1_reg_n_0_[1]\,
      O => \s21[1]_i_1__1_n_0\
    );
\s21[2]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(2),
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => \yreg1_reg_n_0_[2]\,
      O => \s21[2]_i_1__1_n_0\
    );
\s21[3]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(3),
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => \yreg1_reg_n_0_[3]\,
      O => \s21[3]_i_1__1_n_0\
    );
\s21[4]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(4),
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => \yreg1_reg_n_0_[4]\,
      O => \s21[4]_i_1__1_n_0\
    );
\s21[5]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(5),
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => \yreg1_reg_n_0_[5]\,
      O => \s21[5]_i_1__1_n_0\
    );
\s21[6]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(6),
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => \yreg1_reg_n_0_[6]\,
      O => \s21[6]_i_1__1_n_0\
    );
\s21[7]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(7),
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => \yreg1_reg_n_0_[7]\,
      O => \s21[7]_i_1__1_n_0\
    );
\s21[8]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(8),
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => \yreg1_reg_n_0_[8]\,
      O => \s21[8]_i_1__1_n_0\
    );
\s21[9]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \yreg1_reg_n_0_[9]\,
      I1 => yreg2(9),
      O => \s21[9]_i_1__1_n_0\
    );
\s21_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s21[0]_i_1__1_n_0\,
      Q => \s21_reg_n_0_[0]\
    );
\s21_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s21[1]_i_1__1_n_0\,
      Q => \s21_reg_n_0_[1]\
    );
\s21_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s21[2]_i_1__1_n_0\,
      Q => \s21_reg_n_0_[2]\
    );
\s21_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s21[3]_i_1__1_n_0\,
      Q => \s21_reg_n_0_[3]\
    );
\s21_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s21[4]_i_1__1_n_0\,
      Q => \s21_reg_n_0_[4]\
    );
\s21_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s21[5]_i_1__1_n_0\,
      Q => \s21_reg_n_0_[5]\
    );
\s21_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s21[6]_i_1__1_n_0\,
      Q => \s21_reg_n_0_[6]\
    );
\s21_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s21[7]_i_1__1_n_0\,
      Q => \s21_reg_n_0_[7]\
    );
\s21_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s21[8]_i_1__1_n_0\,
      Q => \s21_reg_n_0_[8]\
    );
\s21_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s21[9]_i_1__1_n_0\,
      Q => \s21_reg_n_0_[9]\
    );
\s22[0]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[0]\,
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => yreg2(0),
      O => \s22[0]_i_1__1_n_0\
    );
\s22[1]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[1]\,
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => yreg2(1),
      O => \s22[1]_i_1__1_n_0\
    );
\s22[2]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[2]\,
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => yreg2(2),
      O => \s22[2]_i_1__1_n_0\
    );
\s22[3]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[3]\,
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => yreg2(3),
      O => \s22[3]_i_1__1_n_0\
    );
\s22[4]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[4]\,
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => yreg2(4),
      O => \s22[4]_i_1__1_n_0\
    );
\s22[5]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[5]\,
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => yreg2(5),
      O => \s22[5]_i_1__1_n_0\
    );
\s22[6]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[6]\,
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => yreg2(6),
      O => \s22[6]_i_1__1_n_0\
    );
\s22[7]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[7]\,
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => yreg2(7),
      O => \s22[7]_i_1__1_n_0\
    );
\s22[8]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[8]\,
      I1 => \s22[8]_i_2__1_n_0\,
      I2 => yreg2(8),
      O => \s22[8]_i_1__1_n_0\
    );
\s22[8]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B200FFB2"
    )
        port map (
      I0 => \s22[8]_i_3__1_n_0\,
      I1 => \yreg1_reg_n_0_[8]\,
      I2 => yreg2(8),
      I3 => yreg2(9),
      I4 => \yreg1_reg_n_0_[9]\,
      O => \s22[8]_i_2__1_n_0\
    );
\s22[8]_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7050FFFF"
    )
        port map (
      I0 => \s22[8]_i_4__1_n_0\,
      I1 => \s22[8]_i_5__1_n_0\,
      I2 => \s22[8]_i_6__1_n_0\,
      I3 => \s22[8]_i_7__1_n_0\,
      I4 => \s22[8]_i_8__1_n_0\,
      O => \s22[8]_i_3__1_n_0\
    );
\s22[8]_i_4__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => \yreg1_reg_n_0_[4]\,
      I1 => yreg2(4),
      I2 => yreg2(5),
      I3 => \yreg1_reg_n_0_[5]\,
      O => \s22[8]_i_4__1_n_0\
    );
\s22[8]_i_5__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4D44DDDD"
    )
        port map (
      I0 => yreg2(3),
      I1 => \yreg1_reg_n_0_[3]\,
      I2 => yreg2(2),
      I3 => \yreg1_reg_n_0_[2]\,
      I4 => \s22[8]_i_9__1_n_0\,
      O => \s22[8]_i_5__1_n_0\
    );
\s22[8]_i_6__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BB2B"
    )
        port map (
      I0 => yreg2(7),
      I1 => \yreg1_reg_n_0_[7]\,
      I2 => \yreg1_reg_n_0_[6]\,
      I3 => yreg2(6),
      O => \s22[8]_i_6__1_n_0\
    );
\s22[8]_i_7__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => yreg2(4),
      I1 => \yreg1_reg_n_0_[4]\,
      I2 => \yreg1_reg_n_0_[5]\,
      I3 => yreg2(5),
      O => \s22[8]_i_7__1_n_0\
    );
\s22[8]_i_8__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B0FB"
    )
        port map (
      I0 => \yreg1_reg_n_0_[6]\,
      I1 => yreg2(6),
      I2 => \yreg1_reg_n_0_[7]\,
      I3 => yreg2(7),
      O => \s22[8]_i_8__1_n_0\
    );
\s22[8]_i_9__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40F440F4FFFF40F4"
    )
        port map (
      I0 => \yreg1_reg_n_0_[0]\,
      I1 => yreg2(0),
      I2 => yreg2(1),
      I3 => \yreg1_reg_n_0_[1]\,
      I4 => yreg2(2),
      I5 => \yreg1_reg_n_0_[2]\,
      O => \s22[8]_i_9__1_n_0\
    );
\s22[9]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[9]\,
      I1 => yreg2(9),
      O => \s22[9]_i_1__1_n_0\
    );
\s22_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s22[0]_i_1__1_n_0\,
      Q => s22(0)
    );
\s22_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s22[1]_i_1__1_n_0\,
      Q => s22(1)
    );
\s22_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s22[2]_i_1__1_n_0\,
      Q => s22(2)
    );
\s22_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s22[3]_i_1__1_n_0\,
      Q => s22(3)
    );
\s22_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s22[4]_i_1__1_n_0\,
      Q => s22(4)
    );
\s22_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s22[5]_i_1__1_n_0\,
      Q => s22(5)
    );
\s22_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s22[6]_i_1__1_n_0\,
      Q => s22(6)
    );
\s22_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s22[7]_i_1__1_n_0\,
      Q => s22(7)
    );
\s22_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s22[8]_i_1__1_n_0\,
      Q => s22(8)
    );
\s22_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^out_reg[0]_0\,
      D => \s22[9]_i_1__1_n_0\,
      Q => s22(9)
    );
\xreg1[9]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F7FF0000"
    )
        port map (
      I0 => \full_reg_n_0_[0]\,
      I1 => \^m_axis_tkeep[2]\,
      I2 => m_axis_tready,
      I3 => \full_reg_n_0_[1]\,
      I4 => \valid_reg[2]\(0),
      I5 => hold,
      O => xreg1
    );
\xreg1_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \inputvals_reg[119]\(0),
      Q => \xreg1_reg_n_0_[0]\
    );
\xreg1_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \inputvals_reg[119]\(1),
      Q => \xreg1_reg_n_0_[1]\
    );
\xreg1_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \inputvals_reg[119]\(2),
      Q => \xreg1_reg_n_0_[2]\
    );
\xreg1_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \inputvals_reg[119]\(3),
      Q => \xreg1_reg_n_0_[3]\
    );
\xreg1_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \inputvals_reg[119]\(4),
      Q => \xreg1_reg_n_0_[4]\
    );
\xreg1_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \inputvals_reg[119]\(5),
      Q => \xreg1_reg_n_0_[5]\
    );
\xreg1_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \inputvals_reg[119]\(6),
      Q => \xreg1_reg_n_0_[6]\
    );
\xreg1_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \inputvals_reg[119]\(7),
      Q => \xreg1_reg_n_0_[7]\
    );
\xreg1_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \inputvals_reg[119]\(8),
      Q => \xreg1_reg_n_0_[8]\
    );
\xreg1_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \inputvals_reg[119]\(9),
      Q => \xreg1_reg_n_0_[9]\
    );
\xreg2[0]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(16),
      I1 => Q(0),
      O => \^d\(0)
    );
\xreg2[1]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(1),
      I1 => Q(17),
      I2 => Q(8),
      I3 => Q(0),
      I4 => Q(16),
      O => \^d\(1)
    );
\xreg2[2]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(2),
      I1 => Q(9),
      I2 => Q(18),
      I3 => Q(8),
      I4 => Q(17),
      I5 => \c5/x12/C_1\,
      O => \^d\(2)
    );
\xreg2[2]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(16),
      I1 => Q(0),
      I2 => Q(8),
      I3 => Q(17),
      I4 => Q(1),
      O => \c5/x12/C_1\
    );
\xreg2[3]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(3),
      I1 => \c5/outx11\(3),
      I2 => \c5/x12/C_2\,
      O => \^d\(3)
    );
\xreg2[3]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(10),
      I1 => Q(19),
      I2 => Q(9),
      I3 => Q(18),
      I4 => Q(8),
      I5 => Q(17),
      O => \c5/outx11\(3)
    );
\xreg2[3]_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \c5/x12/C_1\,
      I1 => Q(17),
      I2 => Q(8),
      I3 => Q(18),
      I4 => Q(9),
      I5 => Q(2),
      O => \c5/x12/C_2\
    );
\xreg2[4]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(4),
      I1 => Q(11),
      I2 => Q(20),
      I3 => \c5/x11/C_3\,
      I4 => \c5/x12/C_3\,
      O => \^d\(4)
    );
\xreg2[5]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \c5/x12/C_3\,
      I1 => \c5/x11/C_3\,
      I2 => Q(20),
      I3 => Q(11),
      I4 => Q(4),
      I5 => \c5/x12/f6/S\,
      O => \^d\(5)
    );
\xreg2[5]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \c5/x12/C_1\,
      I1 => \c5/outx11\(2),
      I2 => Q(2),
      I3 => \c5/outx11\(3),
      I4 => Q(3),
      O => \c5/x12/C_3\
    );
\xreg2[5]_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(17),
      I1 => Q(8),
      I2 => Q(18),
      I3 => Q(9),
      I4 => Q(19),
      I5 => Q(10),
      O => \c5/x11/C_3\
    );
\xreg2[5]_i_4__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \c5/x11/C_3\,
      I1 => Q(20),
      I2 => Q(11),
      I3 => Q(21),
      I4 => Q(12),
      I5 => Q(5),
      O => \c5/x12/f6/S\
    );
\xreg2[5]_i_5__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(9),
      I1 => Q(18),
      I2 => Q(8),
      I3 => Q(17),
      O => \c5/outx11\(2)
    );
\xreg2[6]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(13),
      I2 => Q(22),
      I3 => \c5/x11/C_5\,
      I4 => \c5/x12/C_5\,
      O => \^d\(6)
    );
\xreg2[6]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \c5/x11/C_3\,
      I1 => Q(20),
      I2 => Q(11),
      I3 => Q(21),
      I4 => Q(12),
      O => \c5/x11/C_5\
    );
\xreg2[6]_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \c5/x12/C_3\,
      I1 => \c5/outx11\(4),
      I2 => Q(4),
      I3 => \c5/outx11\(5),
      I4 => Q(5),
      O => \c5/x12/C_5\
    );
\xreg2[6]_i_4__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(11),
      I1 => Q(20),
      I2 => \c5/x11/C_3\,
      O => \c5/outx11\(4)
    );
\xreg2[6]_i_5__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(12),
      I1 => Q(21),
      I2 => Q(11),
      I3 => Q(20),
      I4 => \c5/x11/C_3\,
      O => \c5/outx11\(5)
    );
\xreg2[7]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \c5/x12/f8/S\,
      I1 => \c5/x12/C_6\,
      O => \^d\(7)
    );
\xreg2[7]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \c5/x11/C_5\,
      I1 => Q(22),
      I2 => Q(13),
      I3 => Q(23),
      I4 => Q(14),
      I5 => Q(7),
      O => \c5/x12/f8/S\
    );
\xreg2[8]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(15),
      I1 => Q(7),
      I2 => Q(14),
      I3 => Q(23),
      I4 => \c5/x11/C_6\,
      I5 => \c5/x12/C_6\,
      O => \^d\(8)
    );
\xreg2[9]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \c5/x12/C_6\,
      I1 => \c5/x11/C_6\,
      I2 => Q(23),
      I3 => Q(14),
      I4 => Q(7),
      I5 => Q(15),
      O => \^d\(9)
    );
\xreg2[9]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \c5/x12/C_5\,
      I1 => \c5/x11/C_5\,
      I2 => Q(22),
      I3 => Q(13),
      I4 => Q(6),
      O => \c5/x12/C_6\
    );
\xreg2[9]_i_3__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \c5/x11/C_5\,
      I1 => Q(22),
      I2 => Q(13),
      O => \c5/x11/C_6\
    );
\xreg2_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \^d\(0),
      Q => xreg2(0)
    );
\xreg2_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \^d\(1),
      Q => xreg2(1)
    );
\xreg2_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \^d\(2),
      Q => xreg2(2)
    );
\xreg2_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \^d\(3),
      Q => xreg2(3)
    );
\xreg2_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \^d\(4),
      Q => xreg2(4)
    );
\xreg2_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \^d\(5),
      Q => xreg2(5)
    );
\xreg2_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \^d\(6),
      Q => xreg2(6)
    );
\xreg2_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \^d\(7),
      Q => xreg2(7)
    );
\xreg2_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \^d\(8),
      Q => xreg2(8)
    );
\xreg2_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^out_reg[0]_0\,
      D => \^d\(9),
      Q => xreg2(9)
    );
\yreg1[0]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(48),
      I1 => Q(16),
      O => \y12/f1/S\
    );
\yreg1[1]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(17),
      I1 => Q(49),
      I2 => Q(32),
      I3 => Q(16),
      I4 => Q(48),
      O => outy12(1)
    );
\yreg1[2]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(18),
      I1 => Q(33),
      I2 => Q(50),
      I3 => Q(32),
      I4 => Q(49),
      I5 => \y12/C_1\,
      O => outy12(2)
    );
\yreg1[2]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(48),
      I1 => Q(16),
      I2 => Q(32),
      I3 => Q(49),
      I4 => Q(17),
      O => \y12/C_1\
    );
\yreg1[3]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(19),
      I1 => outy11(3),
      I2 => \y12/C_2\,
      O => outy12(3)
    );
\yreg1[3]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(34),
      I1 => Q(51),
      I2 => Q(33),
      I3 => Q(50),
      I4 => Q(32),
      I5 => Q(49),
      O => outy11(3)
    );
\yreg1[3]_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \y12/C_1\,
      I1 => Q(49),
      I2 => Q(32),
      I3 => Q(50),
      I4 => Q(33),
      I5 => Q(18),
      O => \y12/C_2\
    );
\yreg1[4]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(20),
      I1 => Q(35),
      I2 => Q(52),
      I3 => \y11/C_3\,
      I4 => \y12/C_3\,
      O => outy12(4)
    );
\yreg1[5]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \y12/C_3\,
      I1 => \y11/C_3\,
      I2 => Q(52),
      I3 => Q(35),
      I4 => Q(20),
      I5 => \y12/f6/S\,
      O => outy12(5)
    );
\yreg1[5]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y12/C_1\,
      I1 => outy11(2),
      I2 => Q(18),
      I3 => outy11(3),
      I4 => Q(19),
      O => \y12/C_3\
    );
\yreg1[5]_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(49),
      I1 => Q(32),
      I2 => Q(50),
      I3 => Q(33),
      I4 => Q(51),
      I5 => Q(34),
      O => \y11/C_3\
    );
\yreg1[5]_i_4__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y11/C_3\,
      I1 => Q(52),
      I2 => Q(35),
      I3 => Q(53),
      I4 => Q(36),
      I5 => Q(21),
      O => \y12/f6/S\
    );
\yreg1[5]_i_5__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(33),
      I1 => Q(50),
      I2 => Q(32),
      I3 => Q(49),
      O => outy11(2)
    );
\yreg1[6]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(22),
      I1 => Q(37),
      I2 => Q(54),
      I3 => \y11/C_5\,
      I4 => \y12/C_5\,
      O => outy12(6)
    );
\yreg1[6]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y11/C_3\,
      I1 => Q(52),
      I2 => Q(35),
      I3 => Q(53),
      I4 => Q(36),
      O => \y11/C_5\
    );
\yreg1[6]_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y12/C_3\,
      I1 => outy11(4),
      I2 => Q(20),
      I3 => outy11(5),
      I4 => Q(21),
      O => \y12/C_5\
    );
\yreg1[6]_i_4__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(35),
      I1 => Q(52),
      I2 => \y11/C_3\,
      O => outy11(4)
    );
\yreg1[6]_i_5__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(36),
      I1 => Q(53),
      I2 => Q(35),
      I3 => Q(52),
      I4 => \y11/C_3\,
      O => outy11(5)
    );
\yreg1[7]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \y12/f8/S\,
      I1 => \y12/C_6\,
      O => outy12(7)
    );
\yreg1[7]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y11/C_5\,
      I1 => Q(54),
      I2 => Q(37),
      I3 => Q(55),
      I4 => Q(38),
      I5 => Q(23),
      O => \y12/f8/S\
    );
\yreg1[8]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(39),
      I1 => Q(23),
      I2 => Q(38),
      I3 => Q(55),
      I4 => \y11/C_6\,
      I5 => \y12/C_6\,
      O => outy12(8)
    );
\yreg1[9]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FF000000000000"
    )
        port map (
      I0 => \full_reg_n_0_[0]\,
      I1 => \^m_axis_tkeep[2]\,
      I2 => m_axis_tready,
      I3 => \full_reg_n_0_[1]\,
      I4 => \valid_reg[2]\(0),
      I5 => hold_reg,
      O => yreg1
    );
\yreg1[9]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \y12/C_6\,
      I1 => \y11/C_6\,
      I2 => Q(55),
      I3 => Q(38),
      I4 => Q(23),
      I5 => Q(39),
      O => outy12(9)
    );
\yreg1[9]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \y12/C_5\,
      I1 => \y11/C_5\,
      I2 => Q(54),
      I3 => Q(37),
      I4 => Q(22),
      O => \y12/C_6\
    );
\yreg1[9]_i_4__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \y11/C_5\,
      I1 => Q(54),
      I2 => Q(37),
      O => \y11/C_6\
    );
\yreg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => \y12/f1/S\,
      Q => \yreg1_reg_n_0_[0]\,
      R => '0'
    );
\yreg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(1),
      Q => \yreg1_reg_n_0_[1]\,
      R => '0'
    );
\yreg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(2),
      Q => \yreg1_reg_n_0_[2]\,
      R => '0'
    );
\yreg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(3),
      Q => \yreg1_reg_n_0_[3]\,
      R => '0'
    );
\yreg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(4),
      Q => \yreg1_reg_n_0_[4]\,
      R => '0'
    );
\yreg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(5),
      Q => \yreg1_reg_n_0_[5]\,
      R => '0'
    );
\yreg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(6),
      Q => \yreg1_reg_n_0_[6]\,
      R => '0'
    );
\yreg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(7),
      Q => \yreg1_reg_n_0_[7]\,
      R => '0'
    );
\yreg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(8),
      Q => \yreg1_reg_n_0_[8]\,
      R => '0'
    );
\yreg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(9),
      Q => \yreg1_reg_n_0_[9]\,
      R => '0'
    );
\yreg2[0]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(40),
      I1 => Q(0),
      O => \y22/f1/S\
    );
\yreg2[1]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(1),
      I1 => Q(41),
      I2 => Q(24),
      I3 => Q(0),
      I4 => Q(40),
      O => outy22(1)
    );
\yreg2[2]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(2),
      I1 => Q(25),
      I2 => Q(42),
      I3 => Q(24),
      I4 => Q(41),
      I5 => \y22/C_1\,
      O => outy22(2)
    );
\yreg2[2]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(40),
      I1 => Q(0),
      I2 => Q(24),
      I3 => Q(41),
      I4 => Q(1),
      O => \y22/C_1\
    );
\yreg2[3]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(3),
      I1 => outy21(3),
      I2 => \y22/C_2\,
      O => outy22(3)
    );
\yreg2[3]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(26),
      I1 => Q(43),
      I2 => Q(25),
      I3 => Q(42),
      I4 => Q(24),
      I5 => Q(41),
      O => outy21(3)
    );
\yreg2[3]_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \y22/C_1\,
      I1 => Q(41),
      I2 => Q(24),
      I3 => Q(42),
      I4 => Q(25),
      I5 => Q(2),
      O => \y22/C_2\
    );
\yreg2[4]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(4),
      I1 => Q(27),
      I2 => Q(44),
      I3 => \y21/C_3\,
      I4 => \y22/C_3\,
      O => outy22(4)
    );
\yreg2[5]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \y22/C_3\,
      I1 => \y21/C_3\,
      I2 => Q(44),
      I3 => Q(27),
      I4 => Q(4),
      I5 => \y22/f6/S\,
      O => outy22(5)
    );
\yreg2[5]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y22/C_1\,
      I1 => outy21(2),
      I2 => Q(2),
      I3 => outy21(3),
      I4 => Q(3),
      O => \y22/C_3\
    );
\yreg2[5]_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(41),
      I1 => Q(24),
      I2 => Q(42),
      I3 => Q(25),
      I4 => Q(43),
      I5 => Q(26),
      O => \y21/C_3\
    );
\yreg2[5]_i_4__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y21/C_3\,
      I1 => Q(44),
      I2 => Q(27),
      I3 => Q(45),
      I4 => Q(28),
      I5 => Q(5),
      O => \y22/f6/S\
    );
\yreg2[5]_i_5__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(25),
      I1 => Q(42),
      I2 => Q(24),
      I3 => Q(41),
      O => outy21(2)
    );
\yreg2[6]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(29),
      I2 => Q(46),
      I3 => \y21/C_5\,
      I4 => \y22/C_5\,
      O => outy22(6)
    );
\yreg2[6]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y21/C_3\,
      I1 => Q(44),
      I2 => Q(27),
      I3 => Q(45),
      I4 => Q(28),
      O => \y21/C_5\
    );
\yreg2[6]_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y22/C_3\,
      I1 => outy21(4),
      I2 => Q(4),
      I3 => outy21(5),
      I4 => Q(5),
      O => \y22/C_5\
    );
\yreg2[6]_i_4__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(27),
      I1 => Q(44),
      I2 => \y21/C_3\,
      O => outy21(4)
    );
\yreg2[6]_i_5__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(28),
      I1 => Q(45),
      I2 => Q(27),
      I3 => Q(44),
      I4 => \y21/C_3\,
      O => outy21(5)
    );
\yreg2[7]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \y22/f8/S\,
      I1 => \y22/C_6\,
      O => outy22(7)
    );
\yreg2[7]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y21/C_5\,
      I1 => Q(46),
      I2 => Q(29),
      I3 => Q(47),
      I4 => Q(30),
      I5 => Q(7),
      O => \y22/f8/S\
    );
\yreg2[8]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(31),
      I1 => Q(7),
      I2 => Q(30),
      I3 => Q(47),
      I4 => \y21/C_6\,
      I5 => \y22/C_6\,
      O => outy22(8)
    );
\yreg2[9]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \y22/C_6\,
      I1 => \y21/C_6\,
      I2 => Q(47),
      I3 => Q(30),
      I4 => Q(7),
      I5 => Q(31),
      O => outy22(9)
    );
\yreg2[9]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \y22/C_5\,
      I1 => \y21/C_5\,
      I2 => Q(46),
      I3 => Q(29),
      I4 => Q(6),
      O => \y22/C_6\
    );
\yreg2[9]_i_3__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \y21/C_5\,
      I1 => Q(46),
      I2 => Q(29),
      O => \y21/C_6\
    );
\yreg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => \y22/f1/S\,
      Q => yreg2(0),
      R => '0'
    );
\yreg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(1),
      Q => yreg2(1),
      R => '0'
    );
\yreg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(2),
      Q => yreg2(2),
      R => '0'
    );
\yreg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(3),
      Q => yreg2(3),
      R => '0'
    );
\yreg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(4),
      Q => yreg2(4),
      R => '0'
    );
\yreg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(5),
      Q => yreg2(5),
      R => '0'
    );
\yreg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(6),
      Q => yreg2(6),
      R => '0'
    );
\yreg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(7),
      Q => yreg2(7),
      R => '0'
    );
\yreg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(8),
      Q => yreg2(8),
      R => '0'
    );
\yreg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(9),
      Q => yreg2(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity finalproj_sobelfilteraxistream_0_0_convolution_2 is
  port (
    \full_reg[2]_0\ : out STD_LOGIC;
    \m_axis_tkeep[3]\ : out STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 55 downto 0 );
    reset : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 9 downto 0 );
    clock : in STD_LOGIC;
    hold : in STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    \valid_reg[3]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    hold_reg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of finalproj_sobelfilteraxistream_0_0_convolution_2 : entity is "convolution";
end finalproj_sobelfilteraxistream_0_0_convolution_2;

architecture STRUCTURE of finalproj_sobelfilteraxistream_0_0_convolution_2 is
  signal ans1 : STD_LOGIC_VECTOR ( 7 downto 1 );
  signal ans2 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal \final/C_1\ : STD_LOGIC;
  signal \final/C_2\ : STD_LOGIC;
  signal \final/C_4\ : STD_LOGIC;
  signal \final/C_6\ : STD_LOGIC;
  signal \final/f1/Ca\ : STD_LOGIC;
  signal \final/f2/S\ : STD_LOGIC;
  signal \final/f4/S\ : STD_LOGIC;
  signal \final/f5/S\ : STD_LOGIC;
  signal \final/f6/S\ : STD_LOGIC;
  signal \full[0]_i_1__2_n_0\ : STD_LOGIC;
  signal \full[1]_i_1__2_n_0\ : STD_LOGIC;
  signal \full[2]_i_1__2_n_0\ : STD_LOGIC;
  signal \^full_reg[2]_0\ : STD_LOGIC;
  signal \full_reg_n_0_[0]\ : STD_LOGIC;
  signal \full_reg_n_0_[1]\ : STD_LOGIC;
  signal \^m_axis_tkeep[3]\ : STD_LOGIC;
  signal \out\ : STD_LOGIC;
  signal \out[0]_i_1__2_n_0\ : STD_LOGIC;
  signal \out[0]_i_2__2_n_0\ : STD_LOGIC;
  signal \out[1]_i_1__2_n_0\ : STD_LOGIC;
  signal \out[1]_i_2__2_n_0\ : STD_LOGIC;
  signal \out[2]_i_1__2_n_0\ : STD_LOGIC;
  signal \out[2]_i_2__2_n_0\ : STD_LOGIC;
  signal \out[3]_i_1__2_n_0\ : STD_LOGIC;
  signal \out[3]_i_2__2_n_0\ : STD_LOGIC;
  signal \out[4]_i_1__2_n_0\ : STD_LOGIC;
  signal \out[4]_i_2__2_n_0\ : STD_LOGIC;
  signal \out[5]_i_1__2_n_0\ : STD_LOGIC;
  signal \out[5]_i_2__2_n_0\ : STD_LOGIC;
  signal \out[6]_i_1__2_n_0\ : STD_LOGIC;
  signal \out[6]_i_2__2_n_0\ : STD_LOGIC;
  signal \out[7]_i_26__2_n_0\ : STD_LOGIC;
  signal \out[7]_i_27__2_n_0\ : STD_LOGIC;
  signal \out[7]_i_28__2_n_0\ : STD_LOGIC;
  signal \out[7]_i_29__2_n_0\ : STD_LOGIC;
  signal \out[7]_i_2__2_n_0\ : STD_LOGIC;
  signal \out[7]_i_8__2_n_0\ : STD_LOGIC;
  signal outx21 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outx22 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal outy11 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outy12 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal outy21 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outy22 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal \s1/C2\ : STD_LOGIC;
  signal \s1/F6/S\ : STD_LOGIC;
  signal \s1/s1/C_2\ : STD_LOGIC;
  signal \s1/s2/C_0\ : STD_LOGIC;
  signal \s1/s2/C_2\ : STD_LOGIC;
  signal \s1/s2/f2/S\ : STD_LOGIC;
  signal \s11[0]_i_1__2_n_0\ : STD_LOGIC;
  signal \s11[1]_i_1__2_n_0\ : STD_LOGIC;
  signal \s11[2]_i_1__2_n_0\ : STD_LOGIC;
  signal \s11[3]_i_1__2_n_0\ : STD_LOGIC;
  signal \s11[4]_i_1__2_n_0\ : STD_LOGIC;
  signal \s11[5]_i_1__2_n_0\ : STD_LOGIC;
  signal \s11[6]_i_1__2_n_0\ : STD_LOGIC;
  signal \s11[7]_i_1__2_n_0\ : STD_LOGIC;
  signal \s11[8]_i_1__2_n_0\ : STD_LOGIC;
  signal \s11[9]_i_1__2_n_0\ : STD_LOGIC;
  signal \s11_reg_n_0_[0]\ : STD_LOGIC;
  signal \s11_reg_n_0_[1]\ : STD_LOGIC;
  signal \s11_reg_n_0_[2]\ : STD_LOGIC;
  signal \s11_reg_n_0_[3]\ : STD_LOGIC;
  signal \s11_reg_n_0_[4]\ : STD_LOGIC;
  signal \s11_reg_n_0_[5]\ : STD_LOGIC;
  signal \s11_reg_n_0_[6]\ : STD_LOGIC;
  signal \s11_reg_n_0_[7]\ : STD_LOGIC;
  signal \s11_reg_n_0_[8]\ : STD_LOGIC;
  signal \s11_reg_n_0_[9]\ : STD_LOGIC;
  signal s12 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \s12[0]_i_1__2_n_0\ : STD_LOGIC;
  signal \s12[1]_i_1__2_n_0\ : STD_LOGIC;
  signal \s12[2]_i_1__2_n_0\ : STD_LOGIC;
  signal \s12[3]_i_1__2_n_0\ : STD_LOGIC;
  signal \s12[4]_i_1__2_n_0\ : STD_LOGIC;
  signal \s12[5]_i_1__2_n_0\ : STD_LOGIC;
  signal \s12[6]_i_1__2_n_0\ : STD_LOGIC;
  signal \s12[7]_i_1__2_n_0\ : STD_LOGIC;
  signal \s12[8]_i_1__2_n_0\ : STD_LOGIC;
  signal \s12[8]_i_2__2_n_0\ : STD_LOGIC;
  signal \s12[8]_i_3__2_n_0\ : STD_LOGIC;
  signal \s12[8]_i_4__2_n_0\ : STD_LOGIC;
  signal \s12[8]_i_5__2_n_0\ : STD_LOGIC;
  signal \s12[8]_i_6__2_n_0\ : STD_LOGIC;
  signal \s12[8]_i_7__2_n_0\ : STD_LOGIC;
  signal \s12[8]_i_8__2_n_0\ : STD_LOGIC;
  signal \s12[8]_i_9__2_n_0\ : STD_LOGIC;
  signal \s12[9]_i_2__2_n_0\ : STD_LOGIC;
  signal \s2/C2\ : STD_LOGIC;
  signal \s2/F6/S\ : STD_LOGIC;
  signal \s2/f5/S\ : STD_LOGIC;
  signal \s2/s1/C_2\ : STD_LOGIC;
  signal \s2/s1/f1/S\ : STD_LOGIC;
  signal \s2/s2/C_0\ : STD_LOGIC;
  signal \s2/s2/C_2\ : STD_LOGIC;
  signal \s2/s2/f2/S\ : STD_LOGIC;
  signal s21 : STD_LOGIC;
  signal \s21[0]_i_1__2_n_0\ : STD_LOGIC;
  signal \s21[1]_i_1__2_n_0\ : STD_LOGIC;
  signal \s21[2]_i_1__2_n_0\ : STD_LOGIC;
  signal \s21[3]_i_1__2_n_0\ : STD_LOGIC;
  signal \s21[4]_i_1__2_n_0\ : STD_LOGIC;
  signal \s21[5]_i_1__2_n_0\ : STD_LOGIC;
  signal \s21[6]_i_1__2_n_0\ : STD_LOGIC;
  signal \s21[7]_i_1__2_n_0\ : STD_LOGIC;
  signal \s21[8]_i_1__2_n_0\ : STD_LOGIC;
  signal \s21[9]_i_1__2_n_0\ : STD_LOGIC;
  signal \s21_reg_n_0_[0]\ : STD_LOGIC;
  signal \s21_reg_n_0_[1]\ : STD_LOGIC;
  signal \s21_reg_n_0_[2]\ : STD_LOGIC;
  signal \s21_reg_n_0_[3]\ : STD_LOGIC;
  signal \s21_reg_n_0_[4]\ : STD_LOGIC;
  signal \s21_reg_n_0_[5]\ : STD_LOGIC;
  signal \s21_reg_n_0_[6]\ : STD_LOGIC;
  signal \s21_reg_n_0_[7]\ : STD_LOGIC;
  signal \s21_reg_n_0_[8]\ : STD_LOGIC;
  signal \s21_reg_n_0_[9]\ : STD_LOGIC;
  signal s22 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \s22[0]_i_1__2_n_0\ : STD_LOGIC;
  signal \s22[1]_i_1__2_n_0\ : STD_LOGIC;
  signal \s22[2]_i_1__2_n_0\ : STD_LOGIC;
  signal \s22[3]_i_1__2_n_0\ : STD_LOGIC;
  signal \s22[4]_i_1__2_n_0\ : STD_LOGIC;
  signal \s22[5]_i_1__2_n_0\ : STD_LOGIC;
  signal \s22[6]_i_1__2_n_0\ : STD_LOGIC;
  signal \s22[7]_i_1__2_n_0\ : STD_LOGIC;
  signal \s22[8]_i_1__2_n_0\ : STD_LOGIC;
  signal \s22[8]_i_2__2_n_0\ : STD_LOGIC;
  signal \s22[8]_i_3__2_n_0\ : STD_LOGIC;
  signal \s22[8]_i_4__2_n_0\ : STD_LOGIC;
  signal \s22[8]_i_5__2_n_0\ : STD_LOGIC;
  signal \s22[8]_i_6__2_n_0\ : STD_LOGIC;
  signal \s22[8]_i_7__2_n_0\ : STD_LOGIC;
  signal \s22[8]_i_8__2_n_0\ : STD_LOGIC;
  signal \s22[8]_i_9__2_n_0\ : STD_LOGIC;
  signal \s22[9]_i_1__2_n_0\ : STD_LOGIC;
  signal \x21/C_3\ : STD_LOGIC;
  signal \x21/C_5\ : STD_LOGIC;
  signal \x21/C_6\ : STD_LOGIC;
  signal \x22/C_1\ : STD_LOGIC;
  signal \x22/C_2\ : STD_LOGIC;
  signal \x22/C_3\ : STD_LOGIC;
  signal \x22/C_5\ : STD_LOGIC;
  signal \x22/C_6\ : STD_LOGIC;
  signal \x22/f1/S\ : STD_LOGIC;
  signal \x22/f6/S\ : STD_LOGIC;
  signal \x22/f8/S\ : STD_LOGIC;
  signal xreg1 : STD_LOGIC;
  signal \xreg1_reg_n_0_[0]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[1]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[2]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[3]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[4]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[5]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[6]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[7]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[8]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[9]\ : STD_LOGIC;
  signal xreg2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \y11/C_3\ : STD_LOGIC;
  signal \y11/C_5\ : STD_LOGIC;
  signal \y11/C_6\ : STD_LOGIC;
  signal \y12/C_1\ : STD_LOGIC;
  signal \y12/C_2\ : STD_LOGIC;
  signal \y12/C_3\ : STD_LOGIC;
  signal \y12/C_5\ : STD_LOGIC;
  signal \y12/C_6\ : STD_LOGIC;
  signal \y12/f1/S\ : STD_LOGIC;
  signal \y12/f6/S\ : STD_LOGIC;
  signal \y12/f8/S\ : STD_LOGIC;
  signal \y21/C_3\ : STD_LOGIC;
  signal \y21/C_5\ : STD_LOGIC;
  signal \y21/C_6\ : STD_LOGIC;
  signal \y22/C_1\ : STD_LOGIC;
  signal \y22/C_2\ : STD_LOGIC;
  signal \y22/C_3\ : STD_LOGIC;
  signal \y22/C_5\ : STD_LOGIC;
  signal \y22/C_6\ : STD_LOGIC;
  signal \y22/f1/S\ : STD_LOGIC;
  signal \y22/f6/S\ : STD_LOGIC;
  signal \y22/f8/S\ : STD_LOGIC;
  signal yreg1 : STD_LOGIC;
  signal \yreg1_reg_n_0_[0]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[1]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[2]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[3]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[4]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[5]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[6]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[7]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[8]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[9]\ : STD_LOGIC;
  signal yreg2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \out[0]_i_2__2\ : label is "soft_lutpair160";
  attribute SOFT_HLUTNM of \out[1]_i_2__2\ : label is "soft_lutpair160";
  attribute SOFT_HLUTNM of \out[1]_i_3__2\ : label is "soft_lutpair177";
  attribute SOFT_HLUTNM of \out[2]_i_6__2\ : label is "soft_lutpair181";
  attribute SOFT_HLUTNM of \out[4]_i_7__2\ : label is "soft_lutpair181";
  attribute SOFT_HLUTNM of \out[4]_i_9__2\ : label is "soft_lutpair177";
  attribute SOFT_HLUTNM of \out[6]_i_2__2\ : label is "soft_lutpair173";
  attribute SOFT_HLUTNM of \out[7]_i_10__2\ : label is "soft_lutpair172";
  attribute SOFT_HLUTNM of \out[7]_i_11__2\ : label is "soft_lutpair170";
  attribute SOFT_HLUTNM of \out[7]_i_13__2\ : label is "soft_lutpair201";
  attribute SOFT_HLUTNM of \out[7]_i_14__2\ : label is "soft_lutpair202";
  attribute SOFT_HLUTNM of \out[7]_i_15__2\ : label is "soft_lutpair172";
  attribute SOFT_HLUTNM of \out[7]_i_16__2\ : label is "soft_lutpair170";
  attribute SOFT_HLUTNM of \out[7]_i_21__2\ : label is "soft_lutpair162";
  attribute SOFT_HLUTNM of \out[7]_i_22__2\ : label is "soft_lutpair169";
  attribute SOFT_HLUTNM of \out[7]_i_24__2\ : label is "soft_lutpair162";
  attribute SOFT_HLUTNM of \out[7]_i_25__2\ : label is "soft_lutpair169";
  attribute SOFT_HLUTNM of \out[7]_i_27__2\ : label is "soft_lutpair201";
  attribute SOFT_HLUTNM of \out[7]_i_29__2\ : label is "soft_lutpair202";
  attribute SOFT_HLUTNM of \out[7]_i_7__2\ : label is "soft_lutpair173";
  attribute SOFT_HLUTNM of \s11[0]_i_1__2\ : label is "soft_lutpair198";
  attribute SOFT_HLUTNM of \s11[1]_i_1__2\ : label is "soft_lutpair197";
  attribute SOFT_HLUTNM of \s11[2]_i_1__2\ : label is "soft_lutpair196";
  attribute SOFT_HLUTNM of \s11[3]_i_1__2\ : label is "soft_lutpair200";
  attribute SOFT_HLUTNM of \s11[4]_i_1__2\ : label is "soft_lutpair195";
  attribute SOFT_HLUTNM of \s11[5]_i_1__2\ : label is "soft_lutpair194";
  attribute SOFT_HLUTNM of \s11[6]_i_1__2\ : label is "soft_lutpair193";
  attribute SOFT_HLUTNM of \s11[7]_i_1__2\ : label is "soft_lutpair199";
  attribute SOFT_HLUTNM of \s11[8]_i_1__2\ : label is "soft_lutpair192";
  attribute SOFT_HLUTNM of \s12[0]_i_1__2\ : label is "soft_lutpair198";
  attribute SOFT_HLUTNM of \s12[1]_i_1__2\ : label is "soft_lutpair197";
  attribute SOFT_HLUTNM of \s12[2]_i_1__2\ : label is "soft_lutpair196";
  attribute SOFT_HLUTNM of \s12[3]_i_1__2\ : label is "soft_lutpair200";
  attribute SOFT_HLUTNM of \s12[4]_i_1__2\ : label is "soft_lutpair195";
  attribute SOFT_HLUTNM of \s12[5]_i_1__2\ : label is "soft_lutpair194";
  attribute SOFT_HLUTNM of \s12[6]_i_1__2\ : label is "soft_lutpair193";
  attribute SOFT_HLUTNM of \s12[7]_i_1__2\ : label is "soft_lutpair199";
  attribute SOFT_HLUTNM of \s12[8]_i_1__2\ : label is "soft_lutpair192";
  attribute SOFT_HLUTNM of \s12[8]_i_2__2\ : label is "soft_lutpair176";
  attribute SOFT_HLUTNM of \s12[8]_i_4__2\ : label is "soft_lutpair178";
  attribute SOFT_HLUTNM of \s12[8]_i_6__2\ : label is "soft_lutpair179";
  attribute SOFT_HLUTNM of \s12[8]_i_7__2\ : label is "soft_lutpair178";
  attribute SOFT_HLUTNM of \s12[8]_i_8__2\ : label is "soft_lutpair179";
  attribute SOFT_HLUTNM of \s12[9]_i_2__2\ : label is "soft_lutpair176";
  attribute SOFT_HLUTNM of \s21[0]_i_1__2\ : label is "soft_lutpair191";
  attribute SOFT_HLUTNM of \s21[1]_i_1__2\ : label is "soft_lutpair190";
  attribute SOFT_HLUTNM of \s21[2]_i_1__2\ : label is "soft_lutpair183";
  attribute SOFT_HLUTNM of \s21[3]_i_1__2\ : label is "soft_lutpair184";
  attribute SOFT_HLUTNM of \s21[4]_i_1__2\ : label is "soft_lutpair185";
  attribute SOFT_HLUTNM of \s21[5]_i_1__2\ : label is "soft_lutpair187";
  attribute SOFT_HLUTNM of \s21[6]_i_1__2\ : label is "soft_lutpair186";
  attribute SOFT_HLUTNM of \s21[7]_i_1__2\ : label is "soft_lutpair189";
  attribute SOFT_HLUTNM of \s21[8]_i_1__2\ : label is "soft_lutpair188";
  attribute SOFT_HLUTNM of \s22[0]_i_1__2\ : label is "soft_lutpair191";
  attribute SOFT_HLUTNM of \s22[1]_i_1__2\ : label is "soft_lutpair190";
  attribute SOFT_HLUTNM of \s22[2]_i_1__2\ : label is "soft_lutpair183";
  attribute SOFT_HLUTNM of \s22[3]_i_1__2\ : label is "soft_lutpair184";
  attribute SOFT_HLUTNM of \s22[4]_i_1__2\ : label is "soft_lutpair185";
  attribute SOFT_HLUTNM of \s22[5]_i_1__2\ : label is "soft_lutpair187";
  attribute SOFT_HLUTNM of \s22[6]_i_1__2\ : label is "soft_lutpair186";
  attribute SOFT_HLUTNM of \s22[7]_i_1__2\ : label is "soft_lutpair189";
  attribute SOFT_HLUTNM of \s22[8]_i_1__2\ : label is "soft_lutpair188";
  attribute SOFT_HLUTNM of \s22[8]_i_2__2\ : label is "soft_lutpair175";
  attribute SOFT_HLUTNM of \s22[8]_i_4__2\ : label is "soft_lutpair182";
  attribute SOFT_HLUTNM of \s22[8]_i_6__2\ : label is "soft_lutpair180";
  attribute SOFT_HLUTNM of \s22[8]_i_7__2\ : label is "soft_lutpair182";
  attribute SOFT_HLUTNM of \s22[8]_i_8__2\ : label is "soft_lutpair180";
  attribute SOFT_HLUTNM of \s22[9]_i_1__2\ : label is "soft_lutpair175";
  attribute SOFT_HLUTNM of \xreg2[1]_i_1\ : label is "soft_lutpair157";
  attribute SOFT_HLUTNM of \xreg2[2]_i_2\ : label is "soft_lutpair157";
  attribute SOFT_HLUTNM of \xreg2[4]_i_1\ : label is "soft_lutpair174";
  attribute SOFT_HLUTNM of \xreg2[6]_i_1\ : label is "soft_lutpair158";
  attribute SOFT_HLUTNM of \xreg2[6]_i_2\ : label is "soft_lutpair165";
  attribute SOFT_HLUTNM of \xreg2[6]_i_4\ : label is "soft_lutpair174";
  attribute SOFT_HLUTNM of \xreg2[6]_i_5\ : label is "soft_lutpair165";
  attribute SOFT_HLUTNM of \xreg2[9]_i_2\ : label is "soft_lutpair158";
  attribute SOFT_HLUTNM of \yreg1[1]_i_1__2\ : label is "soft_lutpair163";
  attribute SOFT_HLUTNM of \yreg1[2]_i_2__2\ : label is "soft_lutpair163";
  attribute SOFT_HLUTNM of \yreg1[4]_i_1__2\ : label is "soft_lutpair167";
  attribute SOFT_HLUTNM of \yreg1[6]_i_1__2\ : label is "soft_lutpair161";
  attribute SOFT_HLUTNM of \yreg1[6]_i_2__2\ : label is "soft_lutpair159";
  attribute SOFT_HLUTNM of \yreg1[6]_i_4__2\ : label is "soft_lutpair167";
  attribute SOFT_HLUTNM of \yreg1[6]_i_5__2\ : label is "soft_lutpair159";
  attribute SOFT_HLUTNM of \yreg1[9]_i_3__1\ : label is "soft_lutpair161";
  attribute SOFT_HLUTNM of \yreg2[1]_i_1__2\ : label is "soft_lutpair164";
  attribute SOFT_HLUTNM of \yreg2[2]_i_2__2\ : label is "soft_lutpair164";
  attribute SOFT_HLUTNM of \yreg2[4]_i_1__2\ : label is "soft_lutpair171";
  attribute SOFT_HLUTNM of \yreg2[6]_i_1__2\ : label is "soft_lutpair168";
  attribute SOFT_HLUTNM of \yreg2[6]_i_2__2\ : label is "soft_lutpair166";
  attribute SOFT_HLUTNM of \yreg2[6]_i_4__2\ : label is "soft_lutpair171";
  attribute SOFT_HLUTNM of \yreg2[6]_i_5__2\ : label is "soft_lutpair166";
  attribute SOFT_HLUTNM of \yreg2[9]_i_2__2\ : label is "soft_lutpair168";
begin
  \full_reg[2]_0\ <= \^full_reg[2]_0\;
  \m_axis_tkeep[3]\ <= \^m_axis_tkeep[3]\;
\full[0]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEFEEEEE44444444"
    )
        port map (
      I0 => hold,
      I1 => \valid_reg[3]\(0),
      I2 => \full_reg_n_0_[1]\,
      I3 => m_axis_tready,
      I4 => \^m_axis_tkeep[3]\,
      I5 => \full_reg_n_0_[0]\,
      O => \full[0]_i_1__2_n_0\
    );
\full[1]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEFE4444"
    )
        port map (
      I0 => hold,
      I1 => \full_reg_n_0_[0]\,
      I2 => \^m_axis_tkeep[3]\,
      I3 => m_axis_tready,
      I4 => \full_reg_n_0_[1]\,
      O => \full[1]_i_1__2_n_0\
    );
\full[2]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFF4040"
    )
        port map (
      I0 => hold,
      I1 => reset,
      I2 => \full_reg_n_0_[1]\,
      I3 => m_axis_tready,
      I4 => \^m_axis_tkeep[3]\,
      O => \full[2]_i_1__2_n_0\
    );
\full_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => \^full_reg[2]_0\,
      D => \full[0]_i_1__2_n_0\,
      Q => \full_reg_n_0_[0]\
    );
\full_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => \^full_reg[2]_0\,
      D => \full[1]_i_1__2_n_0\,
      Q => \full_reg_n_0_[1]\
    );
\full_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => \^full_reg[2]_0\,
      D => \full[2]_i_1__2_n_0\,
      Q => \^m_axis_tkeep[3]\
    );
\out[0]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[0]_i_2__2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__2_n_0\,
      O => \out[0]_i_1__2_n_0\
    );
\out[0]_i_2__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s11_reg_n_0_[0]\,
      O => \out[0]_i_2__2_n_0\
    );
\out[1]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[1]_i_2__2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__2_n_0\,
      O => \out[1]_i_1__2_n_0\
    );
\out[1]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA9696AA"
    )
        port map (
      I0 => \final/f2/S\,
      I1 => \s11_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s21_reg_n_0_[0]\,
      I4 => s22(0),
      O => \out[1]_i_2__2_n_0\
    );
\out[1]_i_3__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D22D2DD2"
    )
        port map (
      I0 => s12(0),
      I1 => \s11_reg_n_0_[0]\,
      I2 => \s11_reg_n_0_[1]\,
      I3 => s12(1),
      I4 => ans2(1),
      O => \final/f2/S\
    );
\out[2]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[2]_i_2__2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__2_n_0\,
      O => \out[2]_i_1__2_n_0\
    );
\out[2]_i_2__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => ans1(2),
      I1 => ans2(2),
      I2 => \final/C_1\,
      O => \out[2]_i_2__2_n_0\
    );
\out[2]_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966996969666966"
    )
        port map (
      I0 => s12(2),
      I1 => \s11_reg_n_0_[2]\,
      I2 => \s11_reg_n_0_[1]\,
      I3 => s12(1),
      I4 => \s11_reg_n_0_[0]\,
      I5 => s12(0),
      O => ans1(2)
    );
\out[2]_i_4__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966996969666966"
    )
        port map (
      I0 => s22(2),
      I1 => \s21_reg_n_0_[2]\,
      I2 => \s21_reg_n_0_[1]\,
      I3 => s22(1),
      I4 => \s21_reg_n_0_[0]\,
      I5 => s22(0),
      O => ans2(2)
    );
\out[2]_i_5__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0CC0D44D4DD40CC0"
    )
        port map (
      I0 => \s2/s1/f1/S\,
      I1 => ans2(1),
      I2 => s12(1),
      I3 => \s11_reg_n_0_[1]\,
      I4 => \s11_reg_n_0_[0]\,
      I5 => s12(0),
      O => \final/C_1\
    );
\out[2]_i_6__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[0]\,
      I1 => s22(0),
      O => \s2/s1/f1/S\
    );
\out[3]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[3]_i_2__2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__2_n_0\,
      O => \out[3]_i_1__2_n_0\
    );
\out[3]_i_2__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \final/f4/S\,
      I1 => \final/C_2\,
      O => \out[3]_i_2__2_n_0\
    );
\out[3]_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => \s11_reg_n_0_[3]\,
      I2 => s12(3),
      I3 => \s2/s1/C_2\,
      I4 => \s21_reg_n_0_[3]\,
      I5 => s22(3),
      O => \final/f4/S\
    );
\out[4]_i_10__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF0DFFFF0000DF0D"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s22(1),
      I3 => \s21_reg_n_0_[1]\,
      I4 => s22(2),
      I5 => \s21_reg_n_0_[2]\,
      O => \s2/s1/C_2\
    );
\out[4]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[4]_i_2__2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__2_n_0\,
      O => \out[4]_i_1__2_n_0\
    );
\out[4]_i_2__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"711717718EE8E88E"
    )
        port map (
      I0 => \final/C_2\,
      I1 => ans2(3),
      I2 => s12(3),
      I3 => \s11_reg_n_0_[3]\,
      I4 => \s1/s1/C_2\,
      I5 => \final/f5/S\,
      O => \out[4]_i_2__2_n_0\
    );
\out[4]_i_3__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/f1/Ca\,
      I1 => ans2(1),
      I2 => ans1(1),
      I3 => ans2(2),
      I4 => ans1(2),
      O => \final/C_2\
    );
\out[4]_i_4__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(3),
      I1 => \s21_reg_n_0_[3]\,
      I2 => \s2/s1/C_2\,
      O => ans2(3)
    );
\out[4]_i_5__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF0DFFFF0000DF0D"
    )
        port map (
      I0 => s12(0),
      I1 => \s11_reg_n_0_[0]\,
      I2 => s12(1),
      I3 => \s11_reg_n_0_[1]\,
      I4 => s12(2),
      I5 => \s11_reg_n_0_[2]\,
      O => \s1/s1/C_2\
    );
\out[4]_i_6__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B24D4DB24DB2B24D"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => s12(3),
      I2 => \s11_reg_n_0_[3]\,
      I3 => \s11_reg_n_0_[4]\,
      I4 => s12(4),
      I5 => ans2(4),
      O => \final/f5/S\
    );
\out[4]_i_7__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0660"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s11_reg_n_0_[0]\,
      O => \final/f1/Ca\
    );
\out[4]_i_8__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => s22(1),
      I1 => \s21_reg_n_0_[1]\,
      I2 => \s21_reg_n_0_[0]\,
      I3 => s22(0),
      O => ans2(1)
    );
\out[4]_i_9__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => s12(1),
      I1 => \s11_reg_n_0_[1]\,
      I2 => \s11_reg_n_0_[0]\,
      I3 => s12(0),
      O => ans1(1)
    );
\out[5]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[5]_i_2__2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__2_n_0\,
      O => \out[5]_i_1__2_n_0\
    );
\out[5]_i_2__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \final/f6/S\,
      I1 => \final/C_4\,
      O => \out[5]_i_2__2_n_0\
    );
\out[5]_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \s1/s2/C_0\,
      I1 => \s11_reg_n_0_[5]\,
      I2 => s12(5),
      I3 => \s2/s2/C_0\,
      I4 => \s21_reg_n_0_[5]\,
      I5 => s22(5),
      O => \final/f6/S\
    );
\out[6]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[6]_i_2__2_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__2_n_0\,
      O => \out[6]_i_1__2_n_0\
    );
\out[6]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => ans2(6),
      I1 => ans1(6),
      I2 => ans1(5),
      I3 => ans2(5),
      I4 => \final/C_4\,
      O => \out[6]_i_2__2_n_0\
    );
\out[7]_i_10__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s2/s2/C_0\,
      I1 => s22(5),
      I2 => \s21_reg_n_0_[5]\,
      I3 => s22(6),
      I4 => \s21_reg_n_0_[6]\,
      O => \s2/s2/C_2\
    );
\out[7]_i_11__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s1/s2/C_0\,
      I1 => s12(5),
      I2 => \s11_reg_n_0_[5]\,
      I3 => s12(6),
      I4 => \s11_reg_n_0_[6]\,
      O => \s1/s2/C_2\
    );
\out[7]_i_12__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/C_2\,
      I1 => ans2(3),
      I2 => ans1(3),
      I3 => ans2(4),
      I4 => ans1(4),
      O => \final/C_4\
    );
\out[7]_i_13__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(5),
      I1 => \s21_reg_n_0_[5]\,
      I2 => \s2/s2/C_0\,
      O => ans2(5)
    );
\out[7]_i_14__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(5),
      I1 => \s11_reg_n_0_[5]\,
      I2 => \s1/s2/C_0\,
      O => ans1(5)
    );
\out[7]_i_15__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s22(6),
      I1 => \s21_reg_n_0_[6]\,
      I2 => \s21_reg_n_0_[5]\,
      I3 => s22(5),
      I4 => \s2/s2/C_0\,
      O => ans2(6)
    );
\out[7]_i_16__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s12(6),
      I1 => \s11_reg_n_0_[6]\,
      I2 => \s11_reg_n_0_[5]\,
      I3 => s12(5),
      I4 => \s1/s2/C_0\,
      O => ans1(6)
    );
\out[7]_i_17__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s11_reg_n_0_[9]\,
      I1 => s12(9),
      O => \s1/F6/S\
    );
\out[7]_i_18__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[8]\,
      I1 => s22(8),
      O => \s2/f5/S\
    );
\out[7]_i_19__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EF0EFFFF0000EF0E"
    )
        port map (
      I0 => \out[7]_i_26__2_n_0\,
      I1 => \out[7]_i_27__2_n_0\,
      I2 => s22(6),
      I3 => \s21_reg_n_0_[6]\,
      I4 => s22(7),
      I5 => \s21_reg_n_0_[7]\,
      O => \s2/C2\
    );
\out[7]_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00D0"
    )
        port map (
      I0 => \^m_axis_tkeep[3]\,
      I1 => m_axis_tready,
      I2 => \full_reg_n_0_[1]\,
      I3 => hold,
      O => \out\
    );
\out[7]_i_20__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EF0EFFFF0000EF0E"
    )
        port map (
      I0 => \out[7]_i_28__2_n_0\,
      I1 => \out[7]_i_29__2_n_0\,
      I2 => s12(6),
      I3 => \s11_reg_n_0_[6]\,
      I4 => s12(7),
      I5 => \s11_reg_n_0_[7]\,
      O => \s1/C2\
    );
\out[7]_i_21__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s2/s1/C_2\,
      I1 => s22(3),
      I2 => \s21_reg_n_0_[3]\,
      I3 => s22(4),
      I4 => \s21_reg_n_0_[4]\,
      O => \s2/s2/C_0\
    );
\out[7]_i_22__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => s12(3),
      I2 => \s11_reg_n_0_[3]\,
      I3 => s12(4),
      I4 => \s11_reg_n_0_[4]\,
      O => \s1/s2/C_0\
    );
\out[7]_i_23__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(3),
      I1 => \s11_reg_n_0_[3]\,
      I2 => \s1/s1/C_2\,
      O => ans1(3)
    );
\out[7]_i_24__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s22(4),
      I1 => \s21_reg_n_0_[4]\,
      I2 => \s21_reg_n_0_[3]\,
      I3 => s22(3),
      I4 => \s2/s1/C_2\,
      O => ans2(4)
    );
\out[7]_i_25__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s12(4),
      I1 => \s11_reg_n_0_[4]\,
      I2 => \s11_reg_n_0_[3]\,
      I3 => s12(3),
      I4 => \s1/s1/C_2\,
      O => ans1(4)
    );
\out[7]_i_26__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A088A8A08088A08"
    )
        port map (
      I0 => \s2/s2/f2/S\,
      I1 => \s21_reg_n_0_[4]\,
      I2 => s22(4),
      I3 => \s21_reg_n_0_[3]\,
      I4 => s22(3),
      I5 => \s2/s1/C_2\,
      O => \out[7]_i_26__2_n_0\
    );
\out[7]_i_27__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \s21_reg_n_0_[5]\,
      I1 => s22(5),
      O => \out[7]_i_27__2_n_0\
    );
\out[7]_i_28__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A088A8A08088A08"
    )
        port map (
      I0 => \s1/s2/f2/S\,
      I1 => \s11_reg_n_0_[4]\,
      I2 => s12(4),
      I3 => \s11_reg_n_0_[3]\,
      I4 => s12(3),
      I5 => \s1/s1/C_2\,
      O => \out[7]_i_28__2_n_0\
    );
\out[7]_i_29__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \s11_reg_n_0_[5]\,
      I1 => s12(5),
      O => \out[7]_i_29__2_n_0\
    );
\out[7]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => ans2(9),
      I1 => ans1(7),
      I2 => ans2(7),
      I3 => \final/C_6\,
      I4 => \out[7]_i_8__2_n_0\,
      O => \out[7]_i_2__2_n_0\
    );
\out[7]_i_30__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[5]\,
      I1 => s22(5),
      O => \s2/s2/f2/S\
    );
\out[7]_i_31__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s11_reg_n_0_[5]\,
      I1 => s12(5),
      O => \s1/s2/f2/S\
    );
\out[7]_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset,
      O => \^full_reg[2]_0\
    );
\out[7]_i_4__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"65A66565A6A665A6"
    )
        port map (
      I0 => \s2/F6/S\,
      I1 => \s21_reg_n_0_[8]\,
      I2 => s22(8),
      I3 => \s21_reg_n_0_[7]\,
      I4 => s22(7),
      I5 => \s2/s2/C_2\,
      O => ans2(9)
    );
\out[7]_i_5__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(7),
      I1 => \s11_reg_n_0_[7]\,
      I2 => \s1/s2/C_2\,
      O => ans1(7)
    );
\out[7]_i_6__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(7),
      I1 => \s21_reg_n_0_[7]\,
      I2 => \s2/s2/C_2\,
      O => ans2(7)
    );
\out[7]_i_7__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/C_4\,
      I1 => ans2(5),
      I2 => ans1(5),
      I3 => ans2(6),
      I4 => ans1(6),
      O => \final/C_6\
    );
\out[7]_i_8__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DFFFF7DFF7DBEFF"
    )
        port map (
      I0 => \s1/F6/S\,
      I1 => \s2/f5/S\,
      I2 => \s2/C2\,
      I3 => s12(8),
      I4 => \s11_reg_n_0_[8]\,
      I5 => \s1/C2\,
      O => \out[7]_i_8__2_n_0\
    );
\out[7]_i_9__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[9]\,
      I1 => s22(9),
      O => \s2/F6/S\
    );
\out_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^full_reg[2]_0\,
      D => \out[0]_i_1__2_n_0\,
      Q => m_axis_tdata(0)
    );
\out_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^full_reg[2]_0\,
      D => \out[1]_i_1__2_n_0\,
      Q => m_axis_tdata(1)
    );
\out_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^full_reg[2]_0\,
      D => \out[2]_i_1__2_n_0\,
      Q => m_axis_tdata(2)
    );
\out_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^full_reg[2]_0\,
      D => \out[3]_i_1__2_n_0\,
      Q => m_axis_tdata(3)
    );
\out_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^full_reg[2]_0\,
      D => \out[4]_i_1__2_n_0\,
      Q => m_axis_tdata(4)
    );
\out_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^full_reg[2]_0\,
      D => \out[5]_i_1__2_n_0\,
      Q => m_axis_tdata(5)
    );
\out_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^full_reg[2]_0\,
      D => \out[6]_i_1__2_n_0\,
      Q => m_axis_tdata(6)
    );
\out_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => \^full_reg[2]_0\,
      D => \out[7]_i_2__2_n_0\,
      Q => m_axis_tdata(7)
    );
\s11[0]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(0),
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => \xreg1_reg_n_0_[0]\,
      O => \s11[0]_i_1__2_n_0\
    );
\s11[1]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(1),
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => \xreg1_reg_n_0_[1]\,
      O => \s11[1]_i_1__2_n_0\
    );
\s11[2]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(2),
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => \xreg1_reg_n_0_[2]\,
      O => \s11[2]_i_1__2_n_0\
    );
\s11[3]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(3),
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => \xreg1_reg_n_0_[3]\,
      O => \s11[3]_i_1__2_n_0\
    );
\s11[4]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(4),
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => \xreg1_reg_n_0_[4]\,
      O => \s11[4]_i_1__2_n_0\
    );
\s11[5]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(5),
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => \xreg1_reg_n_0_[5]\,
      O => \s11[5]_i_1__2_n_0\
    );
\s11[6]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(6),
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => \xreg1_reg_n_0_[6]\,
      O => \s11[6]_i_1__2_n_0\
    );
\s11[7]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(7),
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => \xreg1_reg_n_0_[7]\,
      O => \s11[7]_i_1__2_n_0\
    );
\s11[8]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(8),
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => \xreg1_reg_n_0_[8]\,
      O => \s11[8]_i_1__2_n_0\
    );
\s11[9]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \xreg1_reg_n_0_[9]\,
      I1 => xreg2(9),
      O => \s11[9]_i_1__2_n_0\
    );
\s11_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s11[0]_i_1__2_n_0\,
      Q => \s11_reg_n_0_[0]\
    );
\s11_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s11[1]_i_1__2_n_0\,
      Q => \s11_reg_n_0_[1]\
    );
\s11_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s11[2]_i_1__2_n_0\,
      Q => \s11_reg_n_0_[2]\
    );
\s11_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s11[3]_i_1__2_n_0\,
      Q => \s11_reg_n_0_[3]\
    );
\s11_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s11[4]_i_1__2_n_0\,
      Q => \s11_reg_n_0_[4]\
    );
\s11_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s11[5]_i_1__2_n_0\,
      Q => \s11_reg_n_0_[5]\
    );
\s11_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s11[6]_i_1__2_n_0\,
      Q => \s11_reg_n_0_[6]\
    );
\s11_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s11[7]_i_1__2_n_0\,
      Q => \s11_reg_n_0_[7]\
    );
\s11_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s11[8]_i_1__2_n_0\,
      Q => \s11_reg_n_0_[8]\
    );
\s11_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s11[9]_i_1__2_n_0\,
      Q => \s11_reg_n_0_[9]\
    );
\s12[0]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[0]\,
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => xreg2(0),
      O => \s12[0]_i_1__2_n_0\
    );
\s12[1]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[1]\,
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => xreg2(1),
      O => \s12[1]_i_1__2_n_0\
    );
\s12[2]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[2]\,
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => xreg2(2),
      O => \s12[2]_i_1__2_n_0\
    );
\s12[3]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[3]\,
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => xreg2(3),
      O => \s12[3]_i_1__2_n_0\
    );
\s12[4]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[4]\,
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => xreg2(4),
      O => \s12[4]_i_1__2_n_0\
    );
\s12[5]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[5]\,
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => xreg2(5),
      O => \s12[5]_i_1__2_n_0\
    );
\s12[6]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[6]\,
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => xreg2(6),
      O => \s12[6]_i_1__2_n_0\
    );
\s12[7]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[7]\,
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => xreg2(7),
      O => \s12[7]_i_1__2_n_0\
    );
\s12[8]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[8]\,
      I1 => \s12[8]_i_2__2_n_0\,
      I2 => xreg2(8),
      O => \s12[8]_i_1__2_n_0\
    );
\s12[8]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B200FFB2"
    )
        port map (
      I0 => \s12[8]_i_3__2_n_0\,
      I1 => \xreg1_reg_n_0_[8]\,
      I2 => xreg2(8),
      I3 => xreg2(9),
      I4 => \xreg1_reg_n_0_[9]\,
      O => \s12[8]_i_2__2_n_0\
    );
\s12[8]_i_3__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7050FFFF"
    )
        port map (
      I0 => \s12[8]_i_4__2_n_0\,
      I1 => \s12[8]_i_5__2_n_0\,
      I2 => \s12[8]_i_6__2_n_0\,
      I3 => \s12[8]_i_7__2_n_0\,
      I4 => \s12[8]_i_8__2_n_0\,
      O => \s12[8]_i_3__2_n_0\
    );
\s12[8]_i_4__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => \xreg1_reg_n_0_[4]\,
      I1 => xreg2(4),
      I2 => xreg2(5),
      I3 => \xreg1_reg_n_0_[5]\,
      O => \s12[8]_i_4__2_n_0\
    );
\s12[8]_i_5__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2F22BF2F"
    )
        port map (
      I0 => \xreg1_reg_n_0_[3]\,
      I1 => xreg2(3),
      I2 => \s12[8]_i_9__2_n_0\,
      I3 => \xreg1_reg_n_0_[2]\,
      I4 => xreg2(2),
      O => \s12[8]_i_5__2_n_0\
    );
\s12[8]_i_6__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BB2B"
    )
        port map (
      I0 => xreg2(7),
      I1 => \xreg1_reg_n_0_[7]\,
      I2 => \xreg1_reg_n_0_[6]\,
      I3 => xreg2(6),
      O => \s12[8]_i_6__2_n_0\
    );
\s12[8]_i_7__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => xreg2(4),
      I1 => \xreg1_reg_n_0_[4]\,
      I2 => \xreg1_reg_n_0_[5]\,
      I3 => xreg2(5),
      O => \s12[8]_i_7__2_n_0\
    );
\s12[8]_i_8__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B0FB"
    )
        port map (
      I0 => \xreg1_reg_n_0_[6]\,
      I1 => xreg2(6),
      I2 => \xreg1_reg_n_0_[7]\,
      I3 => xreg2(7),
      O => \s12[8]_i_8__2_n_0\
    );
\s12[8]_i_9__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40F440F4FFFF40F4"
    )
        port map (
      I0 => \xreg1_reg_n_0_[0]\,
      I1 => xreg2(0),
      I2 => xreg2(1),
      I3 => \xreg1_reg_n_0_[1]\,
      I4 => xreg2(3),
      I5 => \xreg1_reg_n_0_[3]\,
      O => \s12[8]_i_9__2_n_0\
    );
\s12[9]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000DF00"
    )
        port map (
      I0 => \full_reg_n_0_[1]\,
      I1 => m_axis_tready,
      I2 => \^m_axis_tkeep[3]\,
      I3 => \full_reg_n_0_[0]\,
      I4 => hold,
      O => s21
    );
\s12[9]_i_2__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[9]\,
      I1 => xreg2(9),
      O => \s12[9]_i_2__2_n_0\
    );
\s12_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s12[0]_i_1__2_n_0\,
      Q => s12(0)
    );
\s12_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s12[1]_i_1__2_n_0\,
      Q => s12(1)
    );
\s12_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s12[2]_i_1__2_n_0\,
      Q => s12(2)
    );
\s12_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s12[3]_i_1__2_n_0\,
      Q => s12(3)
    );
\s12_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s12[4]_i_1__2_n_0\,
      Q => s12(4)
    );
\s12_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s12[5]_i_1__2_n_0\,
      Q => s12(5)
    );
\s12_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s12[6]_i_1__2_n_0\,
      Q => s12(6)
    );
\s12_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s12[7]_i_1__2_n_0\,
      Q => s12(7)
    );
\s12_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s12[8]_i_1__2_n_0\,
      Q => s12(8)
    );
\s12_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s12[9]_i_2__2_n_0\,
      Q => s12(9)
    );
\s21[0]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(0),
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => \yreg1_reg_n_0_[0]\,
      O => \s21[0]_i_1__2_n_0\
    );
\s21[1]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(1),
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => \yreg1_reg_n_0_[1]\,
      O => \s21[1]_i_1__2_n_0\
    );
\s21[2]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(2),
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => \yreg1_reg_n_0_[2]\,
      O => \s21[2]_i_1__2_n_0\
    );
\s21[3]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(3),
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => \yreg1_reg_n_0_[3]\,
      O => \s21[3]_i_1__2_n_0\
    );
\s21[4]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(4),
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => \yreg1_reg_n_0_[4]\,
      O => \s21[4]_i_1__2_n_0\
    );
\s21[5]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(5),
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => \yreg1_reg_n_0_[5]\,
      O => \s21[5]_i_1__2_n_0\
    );
\s21[6]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(6),
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => \yreg1_reg_n_0_[6]\,
      O => \s21[6]_i_1__2_n_0\
    );
\s21[7]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(7),
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => \yreg1_reg_n_0_[7]\,
      O => \s21[7]_i_1__2_n_0\
    );
\s21[8]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(8),
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => \yreg1_reg_n_0_[8]\,
      O => \s21[8]_i_1__2_n_0\
    );
\s21[9]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \yreg1_reg_n_0_[9]\,
      I1 => yreg2(9),
      O => \s21[9]_i_1__2_n_0\
    );
\s21_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s21[0]_i_1__2_n_0\,
      Q => \s21_reg_n_0_[0]\
    );
\s21_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s21[1]_i_1__2_n_0\,
      Q => \s21_reg_n_0_[1]\
    );
\s21_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s21[2]_i_1__2_n_0\,
      Q => \s21_reg_n_0_[2]\
    );
\s21_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s21[3]_i_1__2_n_0\,
      Q => \s21_reg_n_0_[3]\
    );
\s21_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s21[4]_i_1__2_n_0\,
      Q => \s21_reg_n_0_[4]\
    );
\s21_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s21[5]_i_1__2_n_0\,
      Q => \s21_reg_n_0_[5]\
    );
\s21_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s21[6]_i_1__2_n_0\,
      Q => \s21_reg_n_0_[6]\
    );
\s21_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s21[7]_i_1__2_n_0\,
      Q => \s21_reg_n_0_[7]\
    );
\s21_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s21[8]_i_1__2_n_0\,
      Q => \s21_reg_n_0_[8]\
    );
\s21_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s21[9]_i_1__2_n_0\,
      Q => \s21_reg_n_0_[9]\
    );
\s22[0]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[0]\,
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => yreg2(0),
      O => \s22[0]_i_1__2_n_0\
    );
\s22[1]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[1]\,
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => yreg2(1),
      O => \s22[1]_i_1__2_n_0\
    );
\s22[2]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[2]\,
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => yreg2(2),
      O => \s22[2]_i_1__2_n_0\
    );
\s22[3]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[3]\,
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => yreg2(3),
      O => \s22[3]_i_1__2_n_0\
    );
\s22[4]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[4]\,
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => yreg2(4),
      O => \s22[4]_i_1__2_n_0\
    );
\s22[5]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[5]\,
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => yreg2(5),
      O => \s22[5]_i_1__2_n_0\
    );
\s22[6]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[6]\,
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => yreg2(6),
      O => \s22[6]_i_1__2_n_0\
    );
\s22[7]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[7]\,
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => yreg2(7),
      O => \s22[7]_i_1__2_n_0\
    );
\s22[8]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[8]\,
      I1 => \s22[8]_i_2__2_n_0\,
      I2 => yreg2(8),
      O => \s22[8]_i_1__2_n_0\
    );
\s22[8]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B200FFB2"
    )
        port map (
      I0 => \s22[8]_i_3__2_n_0\,
      I1 => \yreg1_reg_n_0_[8]\,
      I2 => yreg2(8),
      I3 => yreg2(9),
      I4 => \yreg1_reg_n_0_[9]\,
      O => \s22[8]_i_2__2_n_0\
    );
\s22[8]_i_3__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7050FFFF"
    )
        port map (
      I0 => \s22[8]_i_4__2_n_0\,
      I1 => \s22[8]_i_5__2_n_0\,
      I2 => \s22[8]_i_6__2_n_0\,
      I3 => \s22[8]_i_7__2_n_0\,
      I4 => \s22[8]_i_8__2_n_0\,
      O => \s22[8]_i_3__2_n_0\
    );
\s22[8]_i_4__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => \yreg1_reg_n_0_[4]\,
      I1 => yreg2(4),
      I2 => yreg2(5),
      I3 => \yreg1_reg_n_0_[5]\,
      O => \s22[8]_i_4__2_n_0\
    );
\s22[8]_i_5__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4D44DDDD"
    )
        port map (
      I0 => yreg2(3),
      I1 => \yreg1_reg_n_0_[3]\,
      I2 => yreg2(2),
      I3 => \yreg1_reg_n_0_[2]\,
      I4 => \s22[8]_i_9__2_n_0\,
      O => \s22[8]_i_5__2_n_0\
    );
\s22[8]_i_6__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BB2B"
    )
        port map (
      I0 => yreg2(7),
      I1 => \yreg1_reg_n_0_[7]\,
      I2 => \yreg1_reg_n_0_[6]\,
      I3 => yreg2(6),
      O => \s22[8]_i_6__2_n_0\
    );
\s22[8]_i_7__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => yreg2(4),
      I1 => \yreg1_reg_n_0_[4]\,
      I2 => \yreg1_reg_n_0_[5]\,
      I3 => yreg2(5),
      O => \s22[8]_i_7__2_n_0\
    );
\s22[8]_i_8__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B0FB"
    )
        port map (
      I0 => \yreg1_reg_n_0_[6]\,
      I1 => yreg2(6),
      I2 => \yreg1_reg_n_0_[7]\,
      I3 => yreg2(7),
      O => \s22[8]_i_8__2_n_0\
    );
\s22[8]_i_9__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40F440F4FFFF40F4"
    )
        port map (
      I0 => \yreg1_reg_n_0_[0]\,
      I1 => yreg2(0),
      I2 => yreg2(1),
      I3 => \yreg1_reg_n_0_[1]\,
      I4 => yreg2(2),
      I5 => \yreg1_reg_n_0_[2]\,
      O => \s22[8]_i_9__2_n_0\
    );
\s22[9]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[9]\,
      I1 => yreg2(9),
      O => \s22[9]_i_1__2_n_0\
    );
\s22_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s22[0]_i_1__2_n_0\,
      Q => s22(0)
    );
\s22_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s22[1]_i_1__2_n_0\,
      Q => s22(1)
    );
\s22_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s22[2]_i_1__2_n_0\,
      Q => s22(2)
    );
\s22_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s22[3]_i_1__2_n_0\,
      Q => s22(3)
    );
\s22_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s22[4]_i_1__2_n_0\,
      Q => s22(4)
    );
\s22_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s22[5]_i_1__2_n_0\,
      Q => s22(5)
    );
\s22_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s22[6]_i_1__2_n_0\,
      Q => s22(6)
    );
\s22_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s22[7]_i_1__2_n_0\,
      Q => s22(7)
    );
\s22_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s22[8]_i_1__2_n_0\,
      Q => s22(8)
    );
\s22_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => \^full_reg[2]_0\,
      D => \s22[9]_i_1__2_n_0\,
      Q => s22(9)
    );
\xreg1[9]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F7FF0000"
    )
        port map (
      I0 => \full_reg_n_0_[0]\,
      I1 => \^m_axis_tkeep[3]\,
      I2 => m_axis_tready,
      I3 => \full_reg_n_0_[1]\,
      I4 => \valid_reg[3]\(0),
      I5 => hold,
      O => xreg1
    );
\xreg1_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => D(0),
      Q => \xreg1_reg_n_0_[0]\
    );
\xreg1_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => D(1),
      Q => \xreg1_reg_n_0_[1]\
    );
\xreg1_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => D(2),
      Q => \xreg1_reg_n_0_[2]\
    );
\xreg1_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => D(3),
      Q => \xreg1_reg_n_0_[3]\
    );
\xreg1_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => D(4),
      Q => \xreg1_reg_n_0_[4]\
    );
\xreg1_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => D(5),
      Q => \xreg1_reg_n_0_[5]\
    );
\xreg1_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => D(6),
      Q => \xreg1_reg_n_0_[6]\
    );
\xreg1_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => D(7),
      Q => \xreg1_reg_n_0_[7]\
    );
\xreg1_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => D(8),
      Q => \xreg1_reg_n_0_[8]\
    );
\xreg1_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => D(9),
      Q => \xreg1_reg_n_0_[9]\
    );
\xreg2[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(16),
      I1 => Q(0),
      O => \x22/f1/S\
    );
\xreg2[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(1),
      I1 => Q(17),
      I2 => Q(8),
      I3 => Q(0),
      I4 => Q(16),
      O => outx22(1)
    );
\xreg2[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(2),
      I1 => Q(9),
      I2 => Q(18),
      I3 => Q(8),
      I4 => Q(17),
      I5 => \x22/C_1\,
      O => outx22(2)
    );
\xreg2[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(16),
      I1 => Q(0),
      I2 => Q(8),
      I3 => Q(17),
      I4 => Q(1),
      O => \x22/C_1\
    );
\xreg2[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(3),
      I1 => outx21(3),
      I2 => \x22/C_2\,
      O => outx22(3)
    );
\xreg2[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(10),
      I1 => Q(19),
      I2 => Q(9),
      I3 => Q(18),
      I4 => Q(8),
      I5 => Q(17),
      O => outx21(3)
    );
\xreg2[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \x22/C_1\,
      I1 => Q(17),
      I2 => Q(8),
      I3 => Q(18),
      I4 => Q(9),
      I5 => Q(2),
      O => \x22/C_2\
    );
\xreg2[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(4),
      I1 => Q(11),
      I2 => Q(20),
      I3 => \x21/C_3\,
      I4 => \x22/C_3\,
      O => outx22(4)
    );
\xreg2[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \x22/C_3\,
      I1 => \x21/C_3\,
      I2 => Q(20),
      I3 => Q(11),
      I4 => Q(4),
      I5 => \x22/f6/S\,
      O => outx22(5)
    );
\xreg2[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \x22/C_1\,
      I1 => outx21(2),
      I2 => Q(2),
      I3 => outx21(3),
      I4 => Q(3),
      O => \x22/C_3\
    );
\xreg2[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(17),
      I1 => Q(8),
      I2 => Q(18),
      I3 => Q(9),
      I4 => Q(19),
      I5 => Q(10),
      O => \x21/C_3\
    );
\xreg2[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \x21/C_3\,
      I1 => Q(20),
      I2 => Q(11),
      I3 => Q(21),
      I4 => Q(12),
      I5 => Q(5),
      O => \x22/f6/S\
    );
\xreg2[5]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(9),
      I1 => Q(18),
      I2 => Q(8),
      I3 => Q(17),
      O => outx21(2)
    );
\xreg2[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(13),
      I2 => Q(22),
      I3 => \x21/C_5\,
      I4 => \x22/C_5\,
      O => outx22(6)
    );
\xreg2[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \x21/C_3\,
      I1 => Q(20),
      I2 => Q(11),
      I3 => Q(21),
      I4 => Q(12),
      O => \x21/C_5\
    );
\xreg2[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \x22/C_3\,
      I1 => outx21(4),
      I2 => Q(4),
      I3 => outx21(5),
      I4 => Q(5),
      O => \x22/C_5\
    );
\xreg2[6]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(11),
      I1 => Q(20),
      I2 => \x21/C_3\,
      O => outx21(4)
    );
\xreg2[6]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(12),
      I1 => Q(21),
      I2 => Q(11),
      I3 => Q(20),
      I4 => \x21/C_3\,
      O => outx21(5)
    );
\xreg2[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \x22/f8/S\,
      I1 => \x22/C_6\,
      O => outx22(7)
    );
\xreg2[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \x21/C_5\,
      I1 => Q(22),
      I2 => Q(13),
      I3 => Q(23),
      I4 => Q(14),
      I5 => Q(7),
      O => \x22/f8/S\
    );
\xreg2[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(15),
      I1 => Q(7),
      I2 => Q(14),
      I3 => Q(23),
      I4 => \x21/C_6\,
      I5 => \x22/C_6\,
      O => outx22(8)
    );
\xreg2[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \x22/C_6\,
      I1 => \x21/C_6\,
      I2 => Q(23),
      I3 => Q(14),
      I4 => Q(7),
      I5 => Q(15),
      O => outx22(9)
    );
\xreg2[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \x22/C_5\,
      I1 => \x21/C_5\,
      I2 => Q(22),
      I3 => Q(13),
      I4 => Q(6),
      O => \x22/C_6\
    );
\xreg2[9]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \x21/C_5\,
      I1 => Q(22),
      I2 => Q(13),
      O => \x21/C_6\
    );
\xreg2_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => \x22/f1/S\,
      Q => xreg2(0)
    );
\xreg2_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => outx22(1),
      Q => xreg2(1)
    );
\xreg2_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => outx22(2),
      Q => xreg2(2)
    );
\xreg2_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => outx22(3),
      Q => xreg2(3)
    );
\xreg2_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => outx22(4),
      Q => xreg2(4)
    );
\xreg2_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => outx22(5),
      Q => xreg2(5)
    );
\xreg2_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => outx22(6),
      Q => xreg2(6)
    );
\xreg2_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => outx22(7),
      Q => xreg2(7)
    );
\xreg2_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => outx22(8),
      Q => xreg2(8)
    );
\xreg2_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => \^full_reg[2]_0\,
      D => outx22(9),
      Q => xreg2(9)
    );
\yreg1[0]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(48),
      I1 => Q(16),
      O => \y12/f1/S\
    );
\yreg1[1]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(17),
      I1 => Q(49),
      I2 => Q(32),
      I3 => Q(16),
      I4 => Q(48),
      O => outy12(1)
    );
\yreg1[2]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(18),
      I1 => Q(33),
      I2 => Q(50),
      I3 => Q(32),
      I4 => Q(49),
      I5 => \y12/C_1\,
      O => outy12(2)
    );
\yreg1[2]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(48),
      I1 => Q(16),
      I2 => Q(32),
      I3 => Q(49),
      I4 => Q(17),
      O => \y12/C_1\
    );
\yreg1[3]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(19),
      I1 => outy11(3),
      I2 => \y12/C_2\,
      O => outy12(3)
    );
\yreg1[3]_i_2__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(34),
      I1 => Q(51),
      I2 => Q(33),
      I3 => Q(50),
      I4 => Q(32),
      I5 => Q(49),
      O => outy11(3)
    );
\yreg1[3]_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \y12/C_1\,
      I1 => Q(49),
      I2 => Q(32),
      I3 => Q(50),
      I4 => Q(33),
      I5 => Q(18),
      O => \y12/C_2\
    );
\yreg1[4]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(20),
      I1 => Q(35),
      I2 => Q(52),
      I3 => \y11/C_3\,
      I4 => \y12/C_3\,
      O => outy12(4)
    );
\yreg1[5]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \y12/C_3\,
      I1 => \y11/C_3\,
      I2 => Q(52),
      I3 => Q(35),
      I4 => Q(20),
      I5 => \y12/f6/S\,
      O => outy12(5)
    );
\yreg1[5]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y12/C_1\,
      I1 => outy11(2),
      I2 => Q(18),
      I3 => outy11(3),
      I4 => Q(19),
      O => \y12/C_3\
    );
\yreg1[5]_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(49),
      I1 => Q(32),
      I2 => Q(50),
      I3 => Q(33),
      I4 => Q(51),
      I5 => Q(34),
      O => \y11/C_3\
    );
\yreg1[5]_i_4__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y11/C_3\,
      I1 => Q(52),
      I2 => Q(35),
      I3 => Q(53),
      I4 => Q(36),
      I5 => Q(21),
      O => \y12/f6/S\
    );
\yreg1[5]_i_5__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(33),
      I1 => Q(50),
      I2 => Q(32),
      I3 => Q(49),
      O => outy11(2)
    );
\yreg1[6]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(22),
      I1 => Q(37),
      I2 => Q(54),
      I3 => \y11/C_5\,
      I4 => \y12/C_5\,
      O => outy12(6)
    );
\yreg1[6]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y11/C_3\,
      I1 => Q(52),
      I2 => Q(35),
      I3 => Q(53),
      I4 => Q(36),
      O => \y11/C_5\
    );
\yreg1[6]_i_3__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y12/C_3\,
      I1 => outy11(4),
      I2 => Q(20),
      I3 => outy11(5),
      I4 => Q(21),
      O => \y12/C_5\
    );
\yreg1[6]_i_4__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(35),
      I1 => Q(52),
      I2 => \y11/C_3\,
      O => outy11(4)
    );
\yreg1[6]_i_5__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(36),
      I1 => Q(53),
      I2 => Q(35),
      I3 => Q(52),
      I4 => \y11/C_3\,
      O => outy11(5)
    );
\yreg1[7]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \y12/f8/S\,
      I1 => \y12/C_6\,
      O => outy12(7)
    );
\yreg1[7]_i_2__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y11/C_5\,
      I1 => Q(54),
      I2 => Q(37),
      I3 => Q(55),
      I4 => Q(38),
      I5 => Q(23),
      O => \y12/f8/S\
    );
\yreg1[8]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(39),
      I1 => Q(23),
      I2 => Q(38),
      I3 => Q(55),
      I4 => \y11/C_6\,
      I5 => \y12/C_6\,
      O => outy12(8)
    );
\yreg1[9]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FF000000000000"
    )
        port map (
      I0 => \full_reg_n_0_[0]\,
      I1 => \^m_axis_tkeep[3]\,
      I2 => m_axis_tready,
      I3 => \full_reg_n_0_[1]\,
      I4 => \valid_reg[3]\(0),
      I5 => hold_reg,
      O => yreg1
    );
\yreg1[9]_i_2__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \y12/C_6\,
      I1 => \y11/C_6\,
      I2 => Q(55),
      I3 => Q(38),
      I4 => Q(23),
      I5 => Q(39),
      O => outy12(9)
    );
\yreg1[9]_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \y12/C_5\,
      I1 => \y11/C_5\,
      I2 => Q(54),
      I3 => Q(37),
      I4 => Q(22),
      O => \y12/C_6\
    );
\yreg1[9]_i_4__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \y11/C_5\,
      I1 => Q(54),
      I2 => Q(37),
      O => \y11/C_6\
    );
\yreg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => \y12/f1/S\,
      Q => \yreg1_reg_n_0_[0]\,
      R => '0'
    );
\yreg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(1),
      Q => \yreg1_reg_n_0_[1]\,
      R => '0'
    );
\yreg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(2),
      Q => \yreg1_reg_n_0_[2]\,
      R => '0'
    );
\yreg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(3),
      Q => \yreg1_reg_n_0_[3]\,
      R => '0'
    );
\yreg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(4),
      Q => \yreg1_reg_n_0_[4]\,
      R => '0'
    );
\yreg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(5),
      Q => \yreg1_reg_n_0_[5]\,
      R => '0'
    );
\yreg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(6),
      Q => \yreg1_reg_n_0_[6]\,
      R => '0'
    );
\yreg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(7),
      Q => \yreg1_reg_n_0_[7]\,
      R => '0'
    );
\yreg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(8),
      Q => \yreg1_reg_n_0_[8]\,
      R => '0'
    );
\yreg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(9),
      Q => \yreg1_reg_n_0_[9]\,
      R => '0'
    );
\yreg2[0]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(40),
      I1 => Q(0),
      O => \y22/f1/S\
    );
\yreg2[1]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(1),
      I1 => Q(41),
      I2 => Q(24),
      I3 => Q(0),
      I4 => Q(40),
      O => outy22(1)
    );
\yreg2[2]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(2),
      I1 => Q(25),
      I2 => Q(42),
      I3 => Q(24),
      I4 => Q(41),
      I5 => \y22/C_1\,
      O => outy22(2)
    );
\yreg2[2]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(40),
      I1 => Q(0),
      I2 => Q(24),
      I3 => Q(41),
      I4 => Q(1),
      O => \y22/C_1\
    );
\yreg2[3]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(3),
      I1 => outy21(3),
      I2 => \y22/C_2\,
      O => outy22(3)
    );
\yreg2[3]_i_2__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(26),
      I1 => Q(43),
      I2 => Q(25),
      I3 => Q(42),
      I4 => Q(24),
      I5 => Q(41),
      O => outy21(3)
    );
\yreg2[3]_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \y22/C_1\,
      I1 => Q(41),
      I2 => Q(24),
      I3 => Q(42),
      I4 => Q(25),
      I5 => Q(2),
      O => \y22/C_2\
    );
\yreg2[4]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(4),
      I1 => Q(27),
      I2 => Q(44),
      I3 => \y21/C_3\,
      I4 => \y22/C_3\,
      O => outy22(4)
    );
\yreg2[5]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \y22/C_3\,
      I1 => \y21/C_3\,
      I2 => Q(44),
      I3 => Q(27),
      I4 => Q(4),
      I5 => \y22/f6/S\,
      O => outy22(5)
    );
\yreg2[5]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y22/C_1\,
      I1 => outy21(2),
      I2 => Q(2),
      I3 => outy21(3),
      I4 => Q(3),
      O => \y22/C_3\
    );
\yreg2[5]_i_3__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(41),
      I1 => Q(24),
      I2 => Q(42),
      I3 => Q(25),
      I4 => Q(43),
      I5 => Q(26),
      O => \y21/C_3\
    );
\yreg2[5]_i_4__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y21/C_3\,
      I1 => Q(44),
      I2 => Q(27),
      I3 => Q(45),
      I4 => Q(28),
      I5 => Q(5),
      O => \y22/f6/S\
    );
\yreg2[5]_i_5__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(25),
      I1 => Q(42),
      I2 => Q(24),
      I3 => Q(41),
      O => outy21(2)
    );
\yreg2[6]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(29),
      I2 => Q(46),
      I3 => \y21/C_5\,
      I4 => \y22/C_5\,
      O => outy22(6)
    );
\yreg2[6]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y21/C_3\,
      I1 => Q(44),
      I2 => Q(27),
      I3 => Q(45),
      I4 => Q(28),
      O => \y21/C_5\
    );
\yreg2[6]_i_3__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y22/C_3\,
      I1 => outy21(4),
      I2 => Q(4),
      I3 => outy21(5),
      I4 => Q(5),
      O => \y22/C_5\
    );
\yreg2[6]_i_4__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(27),
      I1 => Q(44),
      I2 => \y21/C_3\,
      O => outy21(4)
    );
\yreg2[6]_i_5__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(28),
      I1 => Q(45),
      I2 => Q(27),
      I3 => Q(44),
      I4 => \y21/C_3\,
      O => outy21(5)
    );
\yreg2[7]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \y22/f8/S\,
      I1 => \y22/C_6\,
      O => outy22(7)
    );
\yreg2[7]_i_2__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y21/C_5\,
      I1 => Q(46),
      I2 => Q(29),
      I3 => Q(47),
      I4 => Q(30),
      I5 => Q(7),
      O => \y22/f8/S\
    );
\yreg2[8]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(31),
      I1 => Q(7),
      I2 => Q(30),
      I3 => Q(47),
      I4 => \y21/C_6\,
      I5 => \y22/C_6\,
      O => outy22(8)
    );
\yreg2[9]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \y22/C_6\,
      I1 => \y21/C_6\,
      I2 => Q(47),
      I3 => Q(30),
      I4 => Q(7),
      I5 => Q(31),
      O => outy22(9)
    );
\yreg2[9]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \y22/C_5\,
      I1 => \y21/C_5\,
      I2 => Q(46),
      I3 => Q(29),
      I4 => Q(6),
      O => \y22/C_6\
    );
\yreg2[9]_i_3__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \y21/C_5\,
      I1 => Q(46),
      I2 => Q(29),
      O => \y21/C_6\
    );
\yreg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => \y22/f1/S\,
      Q => yreg2(0),
      R => '0'
    );
\yreg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(1),
      Q => yreg2(1),
      R => '0'
    );
\yreg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(2),
      Q => yreg2(2),
      R => '0'
    );
\yreg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(3),
      Q => yreg2(3),
      R => '0'
    );
\yreg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(4),
      Q => yreg2(4),
      R => '0'
    );
\yreg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(5),
      Q => yreg2(5),
      R => '0'
    );
\yreg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(6),
      Q => yreg2(6),
      R => '0'
    );
\yreg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(7),
      Q => yreg2(7),
      R => '0'
    );
\yreg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(8),
      Q => yreg2(8),
      R => '0'
    );
\yreg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(9),
      Q => yreg2(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity finalproj_sobelfilteraxistream_0_0_convolution_3 is
  port (
    \yreg2_reg[0]_0\ : out STD_LOGIC;
    \m_axis_tkeep[4]\ : out STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 55 downto 0 );
    reset : in STD_LOGIC;
    hold : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 9 downto 0 );
    clock : in STD_LOGIC;
    reset_0 : in STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    \valid_reg[4]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_1 : in STD_LOGIC;
    reset_2 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of finalproj_sobelfilteraxistream_0_0_convolution_3 : entity is "convolution";
end finalproj_sobelfilteraxistream_0_0_convolution_3;

architecture STRUCTURE of finalproj_sobelfilteraxistream_0_0_convolution_3 is
  signal ans1 : STD_LOGIC_VECTOR ( 7 downto 1 );
  signal ans2 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal \final/C_1\ : STD_LOGIC;
  signal \final/C_2\ : STD_LOGIC;
  signal \final/C_4\ : STD_LOGIC;
  signal \final/C_6\ : STD_LOGIC;
  signal \final/f1/Ca\ : STD_LOGIC;
  signal \final/f2/S\ : STD_LOGIC;
  signal \final/f4/S\ : STD_LOGIC;
  signal \final/f5/S\ : STD_LOGIC;
  signal \final/f6/S\ : STD_LOGIC;
  signal \full[0]_i_1__3_n_0\ : STD_LOGIC;
  signal \full[1]_i_1__3_n_0\ : STD_LOGIC;
  signal \full[2]_i_1__3_n_0\ : STD_LOGIC;
  signal \full_reg_n_0_[0]\ : STD_LOGIC;
  signal \full_reg_n_0_[1]\ : STD_LOGIC;
  signal \^m_axis_tkeep[4]\ : STD_LOGIC;
  signal \out\ : STD_LOGIC;
  signal \out[0]_i_1__3_n_0\ : STD_LOGIC;
  signal \out[0]_i_2__3_n_0\ : STD_LOGIC;
  signal \out[1]_i_1__3_n_0\ : STD_LOGIC;
  signal \out[1]_i_2__3_n_0\ : STD_LOGIC;
  signal \out[2]_i_1__3_n_0\ : STD_LOGIC;
  signal \out[2]_i_2__3_n_0\ : STD_LOGIC;
  signal \out[3]_i_1__3_n_0\ : STD_LOGIC;
  signal \out[3]_i_2__3_n_0\ : STD_LOGIC;
  signal \out[4]_i_1__3_n_0\ : STD_LOGIC;
  signal \out[4]_i_2__3_n_0\ : STD_LOGIC;
  signal \out[5]_i_1__3_n_0\ : STD_LOGIC;
  signal \out[5]_i_2__3_n_0\ : STD_LOGIC;
  signal \out[6]_i_1__3_n_0\ : STD_LOGIC;
  signal \out[6]_i_2__3_n_0\ : STD_LOGIC;
  signal \out[7]_i_26__3_n_0\ : STD_LOGIC;
  signal \out[7]_i_27__3_n_0\ : STD_LOGIC;
  signal \out[7]_i_28__3_n_0\ : STD_LOGIC;
  signal \out[7]_i_29__3_n_0\ : STD_LOGIC;
  signal \out[7]_i_2__3_n_0\ : STD_LOGIC;
  signal \out[7]_i_8__3_n_0\ : STD_LOGIC;
  signal outx21 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outx22 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal outy11 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outy12 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal outy21 : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal outy22 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal \s1/C2\ : STD_LOGIC;
  signal \s1/F6/S\ : STD_LOGIC;
  signal \s1/s1/C_2\ : STD_LOGIC;
  signal \s1/s2/C_0\ : STD_LOGIC;
  signal \s1/s2/C_2\ : STD_LOGIC;
  signal \s1/s2/f2/S\ : STD_LOGIC;
  signal \s11[0]_i_1__3_n_0\ : STD_LOGIC;
  signal \s11[1]_i_1__3_n_0\ : STD_LOGIC;
  signal \s11[2]_i_1__3_n_0\ : STD_LOGIC;
  signal \s11[3]_i_1__3_n_0\ : STD_LOGIC;
  signal \s11[4]_i_1__3_n_0\ : STD_LOGIC;
  signal \s11[5]_i_1__3_n_0\ : STD_LOGIC;
  signal \s11[6]_i_1__3_n_0\ : STD_LOGIC;
  signal \s11[7]_i_1__3_n_0\ : STD_LOGIC;
  signal \s11[8]_i_1__3_n_0\ : STD_LOGIC;
  signal \s11[9]_i_1__3_n_0\ : STD_LOGIC;
  signal \s11_reg_n_0_[0]\ : STD_LOGIC;
  signal \s11_reg_n_0_[1]\ : STD_LOGIC;
  signal \s11_reg_n_0_[2]\ : STD_LOGIC;
  signal \s11_reg_n_0_[3]\ : STD_LOGIC;
  signal \s11_reg_n_0_[4]\ : STD_LOGIC;
  signal \s11_reg_n_0_[5]\ : STD_LOGIC;
  signal \s11_reg_n_0_[6]\ : STD_LOGIC;
  signal \s11_reg_n_0_[7]\ : STD_LOGIC;
  signal \s11_reg_n_0_[8]\ : STD_LOGIC;
  signal \s11_reg_n_0_[9]\ : STD_LOGIC;
  signal s12 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \s12[0]_i_1__3_n_0\ : STD_LOGIC;
  signal \s12[1]_i_1__3_n_0\ : STD_LOGIC;
  signal \s12[2]_i_1__3_n_0\ : STD_LOGIC;
  signal \s12[3]_i_1__3_n_0\ : STD_LOGIC;
  signal \s12[4]_i_1__3_n_0\ : STD_LOGIC;
  signal \s12[5]_i_1__3_n_0\ : STD_LOGIC;
  signal \s12[6]_i_1__3_n_0\ : STD_LOGIC;
  signal \s12[7]_i_1__3_n_0\ : STD_LOGIC;
  signal \s12[8]_i_1__3_n_0\ : STD_LOGIC;
  signal \s12[8]_i_2__3_n_0\ : STD_LOGIC;
  signal \s12[8]_i_3__3_n_0\ : STD_LOGIC;
  signal \s12[8]_i_4__3_n_0\ : STD_LOGIC;
  signal \s12[8]_i_5__3_n_0\ : STD_LOGIC;
  signal \s12[8]_i_6__3_n_0\ : STD_LOGIC;
  signal \s12[8]_i_7__3_n_0\ : STD_LOGIC;
  signal \s12[8]_i_8__3_n_0\ : STD_LOGIC;
  signal \s12[8]_i_9__3_n_0\ : STD_LOGIC;
  signal \s12[9]_i_2__3_n_0\ : STD_LOGIC;
  signal \s2/C2\ : STD_LOGIC;
  signal \s2/F6/S\ : STD_LOGIC;
  signal \s2/f5/S\ : STD_LOGIC;
  signal \s2/s1/C_2\ : STD_LOGIC;
  signal \s2/s1/f1/S\ : STD_LOGIC;
  signal \s2/s2/C_0\ : STD_LOGIC;
  signal \s2/s2/C_2\ : STD_LOGIC;
  signal \s2/s2/f2/S\ : STD_LOGIC;
  signal s21 : STD_LOGIC;
  signal \s21[0]_i_1__3_n_0\ : STD_LOGIC;
  signal \s21[1]_i_1__3_n_0\ : STD_LOGIC;
  signal \s21[2]_i_1__3_n_0\ : STD_LOGIC;
  signal \s21[3]_i_1__3_n_0\ : STD_LOGIC;
  signal \s21[4]_i_1__3_n_0\ : STD_LOGIC;
  signal \s21[5]_i_1__3_n_0\ : STD_LOGIC;
  signal \s21[6]_i_1__3_n_0\ : STD_LOGIC;
  signal \s21[7]_i_1__3_n_0\ : STD_LOGIC;
  signal \s21[8]_i_1__3_n_0\ : STD_LOGIC;
  signal \s21[9]_i_1__3_n_0\ : STD_LOGIC;
  signal \s21_reg_n_0_[0]\ : STD_LOGIC;
  signal \s21_reg_n_0_[1]\ : STD_LOGIC;
  signal \s21_reg_n_0_[2]\ : STD_LOGIC;
  signal \s21_reg_n_0_[3]\ : STD_LOGIC;
  signal \s21_reg_n_0_[4]\ : STD_LOGIC;
  signal \s21_reg_n_0_[5]\ : STD_LOGIC;
  signal \s21_reg_n_0_[6]\ : STD_LOGIC;
  signal \s21_reg_n_0_[7]\ : STD_LOGIC;
  signal \s21_reg_n_0_[8]\ : STD_LOGIC;
  signal \s21_reg_n_0_[9]\ : STD_LOGIC;
  signal s22 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \s22[0]_i_1__3_n_0\ : STD_LOGIC;
  signal \s22[1]_i_1__3_n_0\ : STD_LOGIC;
  signal \s22[2]_i_1__3_n_0\ : STD_LOGIC;
  signal \s22[3]_i_1__3_n_0\ : STD_LOGIC;
  signal \s22[4]_i_1__3_n_0\ : STD_LOGIC;
  signal \s22[5]_i_1__3_n_0\ : STD_LOGIC;
  signal \s22[6]_i_1__3_n_0\ : STD_LOGIC;
  signal \s22[7]_i_1__3_n_0\ : STD_LOGIC;
  signal \s22[8]_i_1__3_n_0\ : STD_LOGIC;
  signal \s22[8]_i_2__3_n_0\ : STD_LOGIC;
  signal \s22[8]_i_3__3_n_0\ : STD_LOGIC;
  signal \s22[8]_i_4__3_n_0\ : STD_LOGIC;
  signal \s22[8]_i_5__3_n_0\ : STD_LOGIC;
  signal \s22[8]_i_6__3_n_0\ : STD_LOGIC;
  signal \s22[8]_i_7__3_n_0\ : STD_LOGIC;
  signal \s22[8]_i_8__3_n_0\ : STD_LOGIC;
  signal \s22[8]_i_9__3_n_0\ : STD_LOGIC;
  signal \s22[9]_i_1__3_n_0\ : STD_LOGIC;
  signal \x21/C_3\ : STD_LOGIC;
  signal \x21/C_5\ : STD_LOGIC;
  signal \x21/C_6\ : STD_LOGIC;
  signal \x22/C_1\ : STD_LOGIC;
  signal \x22/C_2\ : STD_LOGIC;
  signal \x22/C_3\ : STD_LOGIC;
  signal \x22/C_5\ : STD_LOGIC;
  signal \x22/C_6\ : STD_LOGIC;
  signal \x22/f1/S\ : STD_LOGIC;
  signal \x22/f6/S\ : STD_LOGIC;
  signal \x22/f8/S\ : STD_LOGIC;
  signal xreg1 : STD_LOGIC;
  signal \xreg1_reg_n_0_[0]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[1]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[2]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[3]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[4]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[5]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[6]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[7]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[8]\ : STD_LOGIC;
  signal \xreg1_reg_n_0_[9]\ : STD_LOGIC;
  signal xreg2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \y11/C_3\ : STD_LOGIC;
  signal \y11/C_5\ : STD_LOGIC;
  signal \y11/C_6\ : STD_LOGIC;
  signal \y12/C_1\ : STD_LOGIC;
  signal \y12/C_2\ : STD_LOGIC;
  signal \y12/C_3\ : STD_LOGIC;
  signal \y12/C_5\ : STD_LOGIC;
  signal \y12/C_6\ : STD_LOGIC;
  signal \y12/f1/S\ : STD_LOGIC;
  signal \y12/f6/S\ : STD_LOGIC;
  signal \y12/f8/S\ : STD_LOGIC;
  signal \y21/C_3\ : STD_LOGIC;
  signal \y21/C_5\ : STD_LOGIC;
  signal \y21/C_6\ : STD_LOGIC;
  signal \y22/C_1\ : STD_LOGIC;
  signal \y22/C_2\ : STD_LOGIC;
  signal \y22/C_3\ : STD_LOGIC;
  signal \y22/C_5\ : STD_LOGIC;
  signal \y22/C_6\ : STD_LOGIC;
  signal \y22/f1/S\ : STD_LOGIC;
  signal \y22/f6/S\ : STD_LOGIC;
  signal \y22/f8/S\ : STD_LOGIC;
  signal yreg1 : STD_LOGIC;
  signal \yreg1_reg_n_0_[0]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[1]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[2]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[3]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[4]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[5]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[6]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[7]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[8]\ : STD_LOGIC;
  signal \yreg1_reg_n_0_[9]\ : STD_LOGIC;
  signal yreg2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \^yreg2_reg[0]_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \full[2]_i_1__3\ : label is "soft_lutpair207";
  attribute SOFT_HLUTNM of \out[0]_i_2__3\ : label is "soft_lutpair220";
  attribute SOFT_HLUTNM of \out[1]_i_2__3\ : label is "soft_lutpair220";
  attribute SOFT_HLUTNM of \out[1]_i_3__3\ : label is "soft_lutpair224";
  attribute SOFT_HLUTNM of \out[2]_i_6__3\ : label is "soft_lutpair227";
  attribute SOFT_HLUTNM of \out[4]_i_8__3\ : label is "soft_lutpair227";
  attribute SOFT_HLUTNM of \out[4]_i_9__3\ : label is "soft_lutpair224";
  attribute SOFT_HLUTNM of \out[6]_i_2__3\ : label is "soft_lutpair222";
  attribute SOFT_HLUTNM of \out[7]_i_10__3\ : label is "soft_lutpair221";
  attribute SOFT_HLUTNM of \out[7]_i_11__3\ : label is "soft_lutpair216";
  attribute SOFT_HLUTNM of \out[7]_i_13__3\ : label is "soft_lutpair246";
  attribute SOFT_HLUTNM of \out[7]_i_14__3\ : label is "soft_lutpair248";
  attribute SOFT_HLUTNM of \out[7]_i_15__3\ : label is "soft_lutpair221";
  attribute SOFT_HLUTNM of \out[7]_i_16__3\ : label is "soft_lutpair216";
  attribute SOFT_HLUTNM of \out[7]_i_21__3\ : label is "soft_lutpair219";
  attribute SOFT_HLUTNM of \out[7]_i_22__3\ : label is "soft_lutpair204";
  attribute SOFT_HLUTNM of \out[7]_i_24__3\ : label is "soft_lutpair219";
  attribute SOFT_HLUTNM of \out[7]_i_25__3\ : label is "soft_lutpair204";
  attribute SOFT_HLUTNM of \out[7]_i_27__3\ : label is "soft_lutpair246";
  attribute SOFT_HLUTNM of \out[7]_i_29__3\ : label is "soft_lutpair248";
  attribute SOFT_HLUTNM of \out[7]_i_7__3\ : label is "soft_lutpair222";
  attribute SOFT_HLUTNM of \s11[0]_i_1__3\ : label is "soft_lutpair243";
  attribute SOFT_HLUTNM of \s11[1]_i_1__3\ : label is "soft_lutpair242";
  attribute SOFT_HLUTNM of \s11[2]_i_1__3\ : label is "soft_lutpair241";
  attribute SOFT_HLUTNM of \s11[3]_i_1__3\ : label is "soft_lutpair238";
  attribute SOFT_HLUTNM of \s11[4]_i_1__3\ : label is "soft_lutpair239";
  attribute SOFT_HLUTNM of \s11[5]_i_1__3\ : label is "soft_lutpair245";
  attribute SOFT_HLUTNM of \s11[6]_i_1__3\ : label is "soft_lutpair244";
  attribute SOFT_HLUTNM of \s11[7]_i_1__3\ : label is "soft_lutpair247";
  attribute SOFT_HLUTNM of \s11[8]_i_1__3\ : label is "soft_lutpair237";
  attribute SOFT_HLUTNM of \s12[0]_i_1__3\ : label is "soft_lutpair243";
  attribute SOFT_HLUTNM of \s12[1]_i_1__3\ : label is "soft_lutpair242";
  attribute SOFT_HLUTNM of \s12[2]_i_1__3\ : label is "soft_lutpair241";
  attribute SOFT_HLUTNM of \s12[3]_i_1__3\ : label is "soft_lutpair238";
  attribute SOFT_HLUTNM of \s12[4]_i_1__3\ : label is "soft_lutpair239";
  attribute SOFT_HLUTNM of \s12[5]_i_1__3\ : label is "soft_lutpair245";
  attribute SOFT_HLUTNM of \s12[6]_i_1__3\ : label is "soft_lutpair244";
  attribute SOFT_HLUTNM of \s12[7]_i_1__3\ : label is "soft_lutpair247";
  attribute SOFT_HLUTNM of \s12[8]_i_1__3\ : label is "soft_lutpair237";
  attribute SOFT_HLUTNM of \s12[8]_i_2__3\ : label is "soft_lutpair223";
  attribute SOFT_HLUTNM of \s12[8]_i_4__3\ : label is "soft_lutpair226";
  attribute SOFT_HLUTNM of \s12[8]_i_6__3\ : label is "soft_lutpair225";
  attribute SOFT_HLUTNM of \s12[8]_i_7__3\ : label is "soft_lutpair226";
  attribute SOFT_HLUTNM of \s12[8]_i_8__3\ : label is "soft_lutpair225";
  attribute SOFT_HLUTNM of \s12[9]_i_2__3\ : label is "soft_lutpair223";
  attribute SOFT_HLUTNM of \s21[0]_i_1__3\ : label is "soft_lutpair240";
  attribute SOFT_HLUTNM of \s21[1]_i_1__3\ : label is "soft_lutpair240";
  attribute SOFT_HLUTNM of \s21[3]_i_1__3\ : label is "soft_lutpair230";
  attribute SOFT_HLUTNM of \s21[4]_i_1__3\ : label is "soft_lutpair231";
  attribute SOFT_HLUTNM of \s21[5]_i_1__3\ : label is "soft_lutpair232";
  attribute SOFT_HLUTNM of \s21[6]_i_1__3\ : label is "soft_lutpair234";
  attribute SOFT_HLUTNM of \s21[7]_i_1__3\ : label is "soft_lutpair235";
  attribute SOFT_HLUTNM of \s21[8]_i_1__3\ : label is "soft_lutpair233";
  attribute SOFT_HLUTNM of \s22[0]_i_1__3\ : label is "soft_lutpair231";
  attribute SOFT_HLUTNM of \s22[1]_i_1__3\ : label is "soft_lutpair230";
  attribute SOFT_HLUTNM of \s22[2]_i_1__3\ : label is "soft_lutpair232";
  attribute SOFT_HLUTNM of \s22[3]_i_1__3\ : label is "soft_lutpair234";
  attribute SOFT_HLUTNM of \s22[4]_i_1__3\ : label is "soft_lutpair236";
  attribute SOFT_HLUTNM of \s22[5]_i_1__3\ : label is "soft_lutpair236";
  attribute SOFT_HLUTNM of \s22[7]_i_1__3\ : label is "soft_lutpair235";
  attribute SOFT_HLUTNM of \s22[8]_i_1__3\ : label is "soft_lutpair233";
  attribute SOFT_HLUTNM of \s22[8]_i_2__3\ : label is "soft_lutpair218";
  attribute SOFT_HLUTNM of \s22[8]_i_4__3\ : label is "soft_lutpair229";
  attribute SOFT_HLUTNM of \s22[8]_i_6__3\ : label is "soft_lutpair228";
  attribute SOFT_HLUTNM of \s22[8]_i_7__3\ : label is "soft_lutpair229";
  attribute SOFT_HLUTNM of \s22[8]_i_8__3\ : label is "soft_lutpair228";
  attribute SOFT_HLUTNM of \s22[9]_i_1__3\ : label is "soft_lutpair218";
  attribute SOFT_HLUTNM of \xreg2[1]_i_1__0\ : label is "soft_lutpair206";
  attribute SOFT_HLUTNM of \xreg2[2]_i_2__0\ : label is "soft_lutpair206";
  attribute SOFT_HLUTNM of \xreg2[4]_i_1__0\ : label is "soft_lutpair217";
  attribute SOFT_HLUTNM of \xreg2[6]_i_1__0\ : label is "soft_lutpair212";
  attribute SOFT_HLUTNM of \xreg2[6]_i_2__0\ : label is "soft_lutpair203";
  attribute SOFT_HLUTNM of \xreg2[6]_i_4__0\ : label is "soft_lutpair217";
  attribute SOFT_HLUTNM of \xreg2[6]_i_5__0\ : label is "soft_lutpair203";
  attribute SOFT_HLUTNM of \xreg2[9]_i_2__0\ : label is "soft_lutpair212";
  attribute SOFT_HLUTNM of \yreg1[1]_i_1__3\ : label is "soft_lutpair205";
  attribute SOFT_HLUTNM of \yreg1[2]_i_2__3\ : label is "soft_lutpair205";
  attribute SOFT_HLUTNM of \yreg1[4]_i_1__3\ : label is "soft_lutpair214";
  attribute SOFT_HLUTNM of \yreg1[6]_i_1__3\ : label is "soft_lutpair215";
  attribute SOFT_HLUTNM of \yreg1[6]_i_2__3\ : label is "soft_lutpair210";
  attribute SOFT_HLUTNM of \yreg1[6]_i_4__3\ : label is "soft_lutpair214";
  attribute SOFT_HLUTNM of \yreg1[6]_i_5__3\ : label is "soft_lutpair210";
  attribute SOFT_HLUTNM of \yreg1[9]_i_3__2\ : label is "soft_lutpair215";
  attribute SOFT_HLUTNM of \yreg1[9]_i_3__3\ : label is "soft_lutpair207";
  attribute SOFT_HLUTNM of \yreg2[1]_i_1__3\ : label is "soft_lutpair211";
  attribute SOFT_HLUTNM of \yreg2[2]_i_2__3\ : label is "soft_lutpair211";
  attribute SOFT_HLUTNM of \yreg2[4]_i_1__3\ : label is "soft_lutpair209";
  attribute SOFT_HLUTNM of \yreg2[6]_i_1__3\ : label is "soft_lutpair208";
  attribute SOFT_HLUTNM of \yreg2[6]_i_2__3\ : label is "soft_lutpair213";
  attribute SOFT_HLUTNM of \yreg2[6]_i_4__3\ : label is "soft_lutpair209";
  attribute SOFT_HLUTNM of \yreg2[6]_i_5__3\ : label is "soft_lutpair213";
  attribute SOFT_HLUTNM of \yreg2[9]_i_2__3\ : label is "soft_lutpair208";
begin
  \m_axis_tkeep[4]\ <= \^m_axis_tkeep[4]\;
  \yreg2_reg[0]_0\ <= \^yreg2_reg[0]_0\;
\full[0]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEFEEEEE44444444"
    )
        port map (
      I0 => hold,
      I1 => \valid_reg[4]\(0),
      I2 => \full_reg_n_0_[1]\,
      I3 => m_axis_tready,
      I4 => \^m_axis_tkeep[4]\,
      I5 => \full_reg_n_0_[0]\,
      O => \full[0]_i_1__3_n_0\
    );
\full[1]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEFE4444"
    )
        port map (
      I0 => hold,
      I1 => \full_reg_n_0_[0]\,
      I2 => \^m_axis_tkeep[4]\,
      I3 => m_axis_tready,
      I4 => \full_reg_n_0_[1]\,
      O => \full[1]_i_1__3_n_0\
    );
\full[2]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFF4040"
    )
        port map (
      I0 => hold,
      I1 => reset,
      I2 => \full_reg_n_0_[1]\,
      I3 => m_axis_tready,
      I4 => \^m_axis_tkeep[4]\,
      O => \full[2]_i_1__3_n_0\
    );
\full_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => reset_1,
      D => \full[0]_i_1__3_n_0\,
      Q => \full_reg_n_0_[0]\
    );
\full_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => reset_1,
      D => \full[1]_i_1__3_n_0\,
      Q => \full_reg_n_0_[1]\
    );
\full_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      CLR => reset_1,
      D => \full[2]_i_1__3_n_0\,
      Q => \^m_axis_tkeep[4]\
    );
\out[0]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[0]_i_2__3_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__3_n_0\,
      O => \out[0]_i_1__3_n_0\
    );
\out[0]_i_2__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s11_reg_n_0_[0]\,
      O => \out[0]_i_2__3_n_0\
    );
\out[1]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[1]_i_2__3_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__3_n_0\,
      O => \out[1]_i_1__3_n_0\
    );
\out[1]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA9696AA"
    )
        port map (
      I0 => \final/f2/S\,
      I1 => \s11_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s21_reg_n_0_[0]\,
      I4 => s22(0),
      O => \out[1]_i_2__3_n_0\
    );
\out[1]_i_3__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D22D2DD2"
    )
        port map (
      I0 => s12(0),
      I1 => \s11_reg_n_0_[0]\,
      I2 => \s11_reg_n_0_[1]\,
      I3 => s12(1),
      I4 => ans2(1),
      O => \final/f2/S\
    );
\out[2]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[2]_i_2__3_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__3_n_0\,
      O => \out[2]_i_1__3_n_0\
    );
\out[2]_i_2__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => ans1(2),
      I1 => ans2(2),
      I2 => \final/C_1\,
      O => \out[2]_i_2__3_n_0\
    );
\out[2]_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966996969666966"
    )
        port map (
      I0 => s12(2),
      I1 => \s11_reg_n_0_[2]\,
      I2 => \s11_reg_n_0_[1]\,
      I3 => s12(1),
      I4 => \s11_reg_n_0_[0]\,
      I5 => s12(0),
      O => ans1(2)
    );
\out[2]_i_4__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966996969666966"
    )
        port map (
      I0 => s22(2),
      I1 => \s21_reg_n_0_[2]\,
      I2 => \s21_reg_n_0_[1]\,
      I3 => s22(1),
      I4 => \s21_reg_n_0_[0]\,
      I5 => s22(0),
      O => ans2(2)
    );
\out[2]_i_5__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0CC0D44D4DD40CC0"
    )
        port map (
      I0 => \s2/s1/f1/S\,
      I1 => ans2(1),
      I2 => s12(1),
      I3 => \s11_reg_n_0_[1]\,
      I4 => \s11_reg_n_0_[0]\,
      I5 => s12(0),
      O => \final/C_1\
    );
\out[2]_i_6__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[0]\,
      I1 => s22(0),
      O => \s2/s1/f1/S\
    );
\out[3]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[3]_i_2__3_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__3_n_0\,
      O => \out[3]_i_1__3_n_0\
    );
\out[3]_i_2__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \final/f4/S\,
      I1 => \final/C_2\,
      O => \out[3]_i_2__3_n_0\
    );
\out[3]_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => \s11_reg_n_0_[3]\,
      I2 => s12(3),
      I3 => \s2/s1/C_2\,
      I4 => \s21_reg_n_0_[3]\,
      I5 => s22(3),
      O => \final/f4/S\
    );
\out[4]_i_10__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF0DFFFF0000DF0D"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s22(1),
      I3 => \s21_reg_n_0_[1]\,
      I4 => s22(2),
      I5 => \s21_reg_n_0_[2]\,
      O => \s2/s1/C_2\
    );
\out[4]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[4]_i_2__3_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__3_n_0\,
      O => \out[4]_i_1__3_n_0\
    );
\out[4]_i_2__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"711717718EE8E88E"
    )
        port map (
      I0 => \final/C_2\,
      I1 => ans2(3),
      I2 => s12(3),
      I3 => \s11_reg_n_0_[3]\,
      I4 => \s1/s1/C_2\,
      I5 => \final/f5/S\,
      O => \out[4]_i_2__3_n_0\
    );
\out[4]_i_3__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/f1/Ca\,
      I1 => ans2(1),
      I2 => ans1(1),
      I3 => ans2(2),
      I4 => ans1(2),
      O => \final/C_2\
    );
\out[4]_i_4__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(3),
      I1 => \s21_reg_n_0_[3]\,
      I2 => \s2/s1/C_2\,
      O => ans2(3)
    );
\out[4]_i_5__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF0DFFFF0000DF0D"
    )
        port map (
      I0 => s12(0),
      I1 => \s11_reg_n_0_[0]\,
      I2 => s12(1),
      I3 => \s11_reg_n_0_[1]\,
      I4 => s12(2),
      I5 => \s11_reg_n_0_[2]\,
      O => \s1/s1/C_2\
    );
\out[4]_i_6__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B24D4DB24DB2B24D"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => s12(3),
      I2 => \s11_reg_n_0_[3]\,
      I3 => \s11_reg_n_0_[4]\,
      I4 => s12(4),
      I5 => ans2(4),
      O => \final/f5/S\
    );
\out[4]_i_7__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0660"
    )
        port map (
      I0 => s22(0),
      I1 => \s21_reg_n_0_[0]\,
      I2 => s12(0),
      I3 => \s11_reg_n_0_[0]\,
      O => \final/f1/Ca\
    );
\out[4]_i_8__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => s22(1),
      I1 => \s21_reg_n_0_[1]\,
      I2 => \s21_reg_n_0_[0]\,
      I3 => s22(0),
      O => ans2(1)
    );
\out[4]_i_9__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => s12(1),
      I1 => \s11_reg_n_0_[1]\,
      I2 => \s11_reg_n_0_[0]\,
      I3 => s12(0),
      O => ans1(1)
    );
\out[5]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[5]_i_2__3_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__3_n_0\,
      O => \out[5]_i_1__3_n_0\
    );
\out[5]_i_2__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \final/f6/S\,
      I1 => \final/C_4\,
      O => \out[5]_i_2__3_n_0\
    );
\out[5]_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \s1/s2/C_0\,
      I1 => \s11_reg_n_0_[5]\,
      I2 => s12(5),
      I3 => \s2/s2/C_0\,
      I4 => \s21_reg_n_0_[5]\,
      I5 => s22(5),
      O => \final/f6/S\
    );
\out[6]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEEE"
    )
        port map (
      I0 => \out[6]_i_2__3_n_0\,
      I1 => ans2(9),
      I2 => ans1(7),
      I3 => ans2(7),
      I4 => \final/C_6\,
      I5 => \out[7]_i_8__3_n_0\,
      O => \out[6]_i_1__3_n_0\
    );
\out[6]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => ans2(6),
      I1 => ans1(6),
      I2 => ans1(5),
      I3 => ans2(5),
      I4 => \final/C_4\,
      O => \out[6]_i_2__3_n_0\
    );
\out[7]_i_10__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s2/s2/C_0\,
      I1 => s22(5),
      I2 => \s21_reg_n_0_[5]\,
      I3 => s22(6),
      I4 => \s21_reg_n_0_[6]\,
      O => \s2/s2/C_2\
    );
\out[7]_i_11__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s1/s2/C_0\,
      I1 => s12(5),
      I2 => \s11_reg_n_0_[5]\,
      I3 => s12(6),
      I4 => \s11_reg_n_0_[6]\,
      O => \s1/s2/C_2\
    );
\out[7]_i_12__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/C_2\,
      I1 => ans2(3),
      I2 => ans1(3),
      I3 => ans2(4),
      I4 => ans1(4),
      O => \final/C_4\
    );
\out[7]_i_13__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(5),
      I1 => \s21_reg_n_0_[5]\,
      I2 => \s2/s2/C_0\,
      O => ans2(5)
    );
\out[7]_i_14__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(5),
      I1 => \s11_reg_n_0_[5]\,
      I2 => \s1/s2/C_0\,
      O => ans1(5)
    );
\out[7]_i_15__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s22(6),
      I1 => \s21_reg_n_0_[6]\,
      I2 => \s21_reg_n_0_[5]\,
      I3 => s22(5),
      I4 => \s2/s2/C_0\,
      O => ans2(6)
    );
\out[7]_i_16__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s12(6),
      I1 => \s11_reg_n_0_[6]\,
      I2 => \s11_reg_n_0_[5]\,
      I3 => s12(5),
      I4 => \s1/s2/C_0\,
      O => ans1(6)
    );
\out[7]_i_17__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s11_reg_n_0_[9]\,
      I1 => s12(9),
      O => \s1/F6/S\
    );
\out[7]_i_18__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[8]\,
      I1 => s22(8),
      O => \s2/f5/S\
    );
\out[7]_i_19__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EF0EFFFF0000EF0E"
    )
        port map (
      I0 => \out[7]_i_26__3_n_0\,
      I1 => \out[7]_i_27__3_n_0\,
      I2 => s22(6),
      I3 => \s21_reg_n_0_[6]\,
      I4 => s22(7),
      I5 => \s21_reg_n_0_[7]\,
      O => \s2/C2\
    );
\out[7]_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00D0"
    )
        port map (
      I0 => \^m_axis_tkeep[4]\,
      I1 => m_axis_tready,
      I2 => \full_reg_n_0_[1]\,
      I3 => hold,
      O => \out\
    );
\out[7]_i_20__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EF0EFFFF0000EF0E"
    )
        port map (
      I0 => \out[7]_i_28__3_n_0\,
      I1 => \out[7]_i_29__3_n_0\,
      I2 => s12(6),
      I3 => \s11_reg_n_0_[6]\,
      I4 => s12(7),
      I5 => \s11_reg_n_0_[7]\,
      O => \s1/C2\
    );
\out[7]_i_21__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s2/s1/C_2\,
      I1 => s22(3),
      I2 => \s21_reg_n_0_[3]\,
      I3 => s22(4),
      I4 => \s21_reg_n_0_[4]\,
      O => \s2/s2/C_0\
    );
\out[7]_i_22__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2FF00B2"
    )
        port map (
      I0 => \s1/s1/C_2\,
      I1 => s12(3),
      I2 => \s11_reg_n_0_[3]\,
      I3 => s12(4),
      I4 => \s11_reg_n_0_[4]\,
      O => \s1/s2/C_0\
    );
\out[7]_i_23__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(3),
      I1 => \s11_reg_n_0_[3]\,
      I2 => \s1/s1/C_2\,
      O => ans1(3)
    );
\out[7]_i_24__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s22(4),
      I1 => \s21_reg_n_0_[4]\,
      I2 => \s21_reg_n_0_[3]\,
      I3 => s22(3),
      I4 => \s2/s1/C_2\,
      O => ans2(4)
    );
\out[7]_i_25__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => s12(4),
      I1 => \s11_reg_n_0_[4]\,
      I2 => \s11_reg_n_0_[3]\,
      I3 => s12(3),
      I4 => \s1/s1/C_2\,
      O => ans1(4)
    );
\out[7]_i_26__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A088A8A08088A08"
    )
        port map (
      I0 => \s2/s2/f2/S\,
      I1 => \s21_reg_n_0_[4]\,
      I2 => s22(4),
      I3 => \s21_reg_n_0_[3]\,
      I4 => s22(3),
      I5 => \s2/s1/C_2\,
      O => \out[7]_i_26__3_n_0\
    );
\out[7]_i_27__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \s21_reg_n_0_[5]\,
      I1 => s22(5),
      O => \out[7]_i_27__3_n_0\
    );
\out[7]_i_28__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A088A8A08088A08"
    )
        port map (
      I0 => \s1/s2/f2/S\,
      I1 => \s11_reg_n_0_[4]\,
      I2 => s12(4),
      I3 => \s11_reg_n_0_[3]\,
      I4 => s12(3),
      I5 => \s1/s1/C_2\,
      O => \out[7]_i_28__3_n_0\
    );
\out[7]_i_29__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \s11_reg_n_0_[5]\,
      I1 => s12(5),
      O => \out[7]_i_29__3_n_0\
    );
\out[7]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => ans2(9),
      I1 => ans1(7),
      I2 => ans2(7),
      I3 => \final/C_6\,
      I4 => \out[7]_i_8__3_n_0\,
      O => \out[7]_i_2__3_n_0\
    );
\out[7]_i_30__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[5]\,
      I1 => s22(5),
      O => \s2/s2/f2/S\
    );
\out[7]_i_31__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s11_reg_n_0_[5]\,
      I1 => s12(5),
      O => \s1/s2/f2/S\
    );
\out[7]_i_4__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"65A66565A6A665A6"
    )
        port map (
      I0 => \s2/F6/S\,
      I1 => \s21_reg_n_0_[8]\,
      I2 => s22(8),
      I3 => \s21_reg_n_0_[7]\,
      I4 => s22(7),
      I5 => \s2/s2/C_2\,
      O => ans2(9)
    );
\out[7]_i_5__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s12(7),
      I1 => \s11_reg_n_0_[7]\,
      I2 => \s1/s2/C_2\,
      O => ans1(7)
    );
\out[7]_i_6__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => s22(7),
      I1 => \s21_reg_n_0_[7]\,
      I2 => \s2/s2/C_2\,
      O => ans2(7)
    );
\out[7]_i_7__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \final/C_4\,
      I1 => ans2(5),
      I2 => ans1(5),
      I3 => ans2(6),
      I4 => ans1(6),
      O => \final/C_6\
    );
\out[7]_i_8__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DFFFF7DFF7DBEFF"
    )
        port map (
      I0 => \s1/F6/S\,
      I1 => \s2/f5/S\,
      I2 => \s2/C2\,
      I3 => s12(8),
      I4 => \s11_reg_n_0_[8]\,
      I5 => \s1/C2\,
      O => \out[7]_i_8__3_n_0\
    );
\out[7]_i_9__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \s21_reg_n_0_[9]\,
      I1 => s22(9),
      O => \s2/F6/S\
    );
\out_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_2,
      D => \out[0]_i_1__3_n_0\,
      Q => m_axis_tdata(0)
    );
\out_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_2,
      D => \out[1]_i_1__3_n_0\,
      Q => m_axis_tdata(1)
    );
\out_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_2,
      D => \out[2]_i_1__3_n_0\,
      Q => m_axis_tdata(2)
    );
\out_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_2,
      D => \out[3]_i_1__3_n_0\,
      Q => m_axis_tdata(3)
    );
\out_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_2,
      D => \out[4]_i_1__3_n_0\,
      Q => m_axis_tdata(4)
    );
\out_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_2,
      D => \out[5]_i_1__3_n_0\,
      Q => m_axis_tdata(5)
    );
\out_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_2,
      D => \out[6]_i_1__3_n_0\,
      Q => m_axis_tdata(6)
    );
\out_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => \out\,
      CLR => reset_2,
      D => \out[7]_i_2__3_n_0\,
      Q => m_axis_tdata(7)
    );
\s11[0]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(0),
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => \xreg1_reg_n_0_[0]\,
      O => \s11[0]_i_1__3_n_0\
    );
\s11[1]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(1),
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => \xreg1_reg_n_0_[1]\,
      O => \s11[1]_i_1__3_n_0\
    );
\s11[2]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(2),
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => \xreg1_reg_n_0_[2]\,
      O => \s11[2]_i_1__3_n_0\
    );
\s11[3]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(3),
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => \xreg1_reg_n_0_[3]\,
      O => \s11[3]_i_1__3_n_0\
    );
\s11[4]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(4),
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => \xreg1_reg_n_0_[4]\,
      O => \s11[4]_i_1__3_n_0\
    );
\s11[5]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(5),
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => \xreg1_reg_n_0_[5]\,
      O => \s11[5]_i_1__3_n_0\
    );
\s11[6]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(6),
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => \xreg1_reg_n_0_[6]\,
      O => \s11[6]_i_1__3_n_0\
    );
\s11[7]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(7),
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => \xreg1_reg_n_0_[7]\,
      O => \s11[7]_i_1__3_n_0\
    );
\s11[8]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => xreg2(8),
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => \xreg1_reg_n_0_[8]\,
      O => \s11[8]_i_1__3_n_0\
    );
\s11[9]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \xreg1_reg_n_0_[9]\,
      I1 => xreg2(9),
      O => \s11[9]_i_1__3_n_0\
    );
\s11_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[0]_i_1__3_n_0\,
      Q => \s11_reg_n_0_[0]\
    );
\s11_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[1]_i_1__3_n_0\,
      Q => \s11_reg_n_0_[1]\
    );
\s11_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[2]_i_1__3_n_0\,
      Q => \s11_reg_n_0_[2]\
    );
\s11_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[3]_i_1__3_n_0\,
      Q => \s11_reg_n_0_[3]\
    );
\s11_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[4]_i_1__3_n_0\,
      Q => \s11_reg_n_0_[4]\
    );
\s11_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[5]_i_1__3_n_0\,
      Q => \s11_reg_n_0_[5]\
    );
\s11_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[6]_i_1__3_n_0\,
      Q => \s11_reg_n_0_[6]\
    );
\s11_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[7]_i_1__3_n_0\,
      Q => \s11_reg_n_0_[7]\
    );
\s11_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[8]_i_1__3_n_0\,
      Q => \s11_reg_n_0_[8]\
    );
\s11_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s11[9]_i_1__3_n_0\,
      Q => \s11_reg_n_0_[9]\
    );
\s12[0]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[0]\,
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => xreg2(0),
      O => \s12[0]_i_1__3_n_0\
    );
\s12[1]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[1]\,
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => xreg2(1),
      O => \s12[1]_i_1__3_n_0\
    );
\s12[2]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[2]\,
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => xreg2(2),
      O => \s12[2]_i_1__3_n_0\
    );
\s12[3]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[3]\,
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => xreg2(3),
      O => \s12[3]_i_1__3_n_0\
    );
\s12[4]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[4]\,
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => xreg2(4),
      O => \s12[4]_i_1__3_n_0\
    );
\s12[5]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[5]\,
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => xreg2(5),
      O => \s12[5]_i_1__3_n_0\
    );
\s12[6]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[6]\,
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => xreg2(6),
      O => \s12[6]_i_1__3_n_0\
    );
\s12[7]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[7]\,
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => xreg2(7),
      O => \s12[7]_i_1__3_n_0\
    );
\s12[8]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[8]\,
      I1 => \s12[8]_i_2__3_n_0\,
      I2 => xreg2(8),
      O => \s12[8]_i_1__3_n_0\
    );
\s12[8]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B200FFB2"
    )
        port map (
      I0 => \s12[8]_i_3__3_n_0\,
      I1 => \xreg1_reg_n_0_[8]\,
      I2 => xreg2(8),
      I3 => xreg2(9),
      I4 => \xreg1_reg_n_0_[9]\,
      O => \s12[8]_i_2__3_n_0\
    );
\s12[8]_i_3__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7050FFFF"
    )
        port map (
      I0 => \s12[8]_i_4__3_n_0\,
      I1 => \s12[8]_i_5__3_n_0\,
      I2 => \s12[8]_i_6__3_n_0\,
      I3 => \s12[8]_i_7__3_n_0\,
      I4 => \s12[8]_i_8__3_n_0\,
      O => \s12[8]_i_3__3_n_0\
    );
\s12[8]_i_4__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => \xreg1_reg_n_0_[4]\,
      I1 => xreg2(4),
      I2 => xreg2(5),
      I3 => \xreg1_reg_n_0_[5]\,
      O => \s12[8]_i_4__3_n_0\
    );
\s12[8]_i_5__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2F22BF2F"
    )
        port map (
      I0 => \xreg1_reg_n_0_[3]\,
      I1 => xreg2(3),
      I2 => \s12[8]_i_9__3_n_0\,
      I3 => \xreg1_reg_n_0_[2]\,
      I4 => xreg2(2),
      O => \s12[8]_i_5__3_n_0\
    );
\s12[8]_i_6__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BB2B"
    )
        port map (
      I0 => xreg2(7),
      I1 => \xreg1_reg_n_0_[7]\,
      I2 => \xreg1_reg_n_0_[6]\,
      I3 => xreg2(6),
      O => \s12[8]_i_6__3_n_0\
    );
\s12[8]_i_7__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => xreg2(4),
      I1 => \xreg1_reg_n_0_[4]\,
      I2 => \xreg1_reg_n_0_[5]\,
      I3 => xreg2(5),
      O => \s12[8]_i_7__3_n_0\
    );
\s12[8]_i_8__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B0FB"
    )
        port map (
      I0 => \xreg1_reg_n_0_[6]\,
      I1 => xreg2(6),
      I2 => \xreg1_reg_n_0_[7]\,
      I3 => xreg2(7),
      O => \s12[8]_i_8__3_n_0\
    );
\s12[8]_i_9__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40F440F4FFFF40F4"
    )
        port map (
      I0 => \xreg1_reg_n_0_[0]\,
      I1 => xreg2(0),
      I2 => xreg2(1),
      I3 => \xreg1_reg_n_0_[1]\,
      I4 => xreg2(3),
      I5 => \xreg1_reg_n_0_[3]\,
      O => \s12[8]_i_9__3_n_0\
    );
\s12[9]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000DF00"
    )
        port map (
      I0 => \full_reg_n_0_[1]\,
      I1 => m_axis_tready,
      I2 => \^m_axis_tkeep[4]\,
      I3 => \full_reg_n_0_[0]\,
      I4 => hold,
      O => s21
    );
\s12[9]_i_2__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \xreg1_reg_n_0_[9]\,
      I1 => xreg2(9),
      O => \s12[9]_i_2__3_n_0\
    );
\s12_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[0]_i_1__3_n_0\,
      Q => s12(0)
    );
\s12_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[1]_i_1__3_n_0\,
      Q => s12(1)
    );
\s12_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[2]_i_1__3_n_0\,
      Q => s12(2)
    );
\s12_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[3]_i_1__3_n_0\,
      Q => s12(3)
    );
\s12_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[4]_i_1__3_n_0\,
      Q => s12(4)
    );
\s12_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[5]_i_1__3_n_0\,
      Q => s12(5)
    );
\s12_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[6]_i_1__3_n_0\,
      Q => s12(6)
    );
\s12_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[7]_i_1__3_n_0\,
      Q => s12(7)
    );
\s12_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[8]_i_1__3_n_0\,
      Q => s12(8)
    );
\s12_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s12[9]_i_2__3_n_0\,
      Q => s12(9)
    );
\s21[0]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(0),
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => \yreg1_reg_n_0_[0]\,
      O => \s21[0]_i_1__3_n_0\
    );
\s21[1]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(1),
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => \yreg1_reg_n_0_[1]\,
      O => \s21[1]_i_1__3_n_0\
    );
\s21[2]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(2),
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => \yreg1_reg_n_0_[2]\,
      O => \s21[2]_i_1__3_n_0\
    );
\s21[3]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(3),
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => \yreg1_reg_n_0_[3]\,
      O => \s21[3]_i_1__3_n_0\
    );
\s21[4]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(4),
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => \yreg1_reg_n_0_[4]\,
      O => \s21[4]_i_1__3_n_0\
    );
\s21[5]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(5),
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => \yreg1_reg_n_0_[5]\,
      O => \s21[5]_i_1__3_n_0\
    );
\s21[6]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(6),
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => \yreg1_reg_n_0_[6]\,
      O => \s21[6]_i_1__3_n_0\
    );
\s21[7]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(7),
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => \yreg1_reg_n_0_[7]\,
      O => \s21[7]_i_1__3_n_0\
    );
\s21[8]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => yreg2(8),
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => \yreg1_reg_n_0_[8]\,
      O => \s21[8]_i_1__3_n_0\
    );
\s21[9]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \yreg1_reg_n_0_[9]\,
      I1 => yreg2(9),
      O => \s21[9]_i_1__3_n_0\
    );
\s21_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_2,
      D => \s21[0]_i_1__3_n_0\,
      Q => \s21_reg_n_0_[0]\
    );
\s21_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_2,
      D => \s21[1]_i_1__3_n_0\,
      Q => \s21_reg_n_0_[1]\
    );
\s21_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_2,
      D => \s21[2]_i_1__3_n_0\,
      Q => \s21_reg_n_0_[2]\
    );
\s21_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s21[3]_i_1__3_n_0\,
      Q => \s21_reg_n_0_[3]\
    );
\s21_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s21[4]_i_1__3_n_0\,
      Q => \s21_reg_n_0_[4]\
    );
\s21_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s21[5]_i_1__3_n_0\,
      Q => \s21_reg_n_0_[5]\
    );
\s21_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s21[6]_i_1__3_n_0\,
      Q => \s21_reg_n_0_[6]\
    );
\s21_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s21[7]_i_1__3_n_0\,
      Q => \s21_reg_n_0_[7]\
    );
\s21_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s21[8]_i_1__3_n_0\,
      Q => \s21_reg_n_0_[8]\
    );
\s21_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s21[9]_i_1__3_n_0\,
      Q => \s21_reg_n_0_[9]\
    );
\s22[0]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[0]\,
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => yreg2(0),
      O => \s22[0]_i_1__3_n_0\
    );
\s22[1]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[1]\,
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => yreg2(1),
      O => \s22[1]_i_1__3_n_0\
    );
\s22[2]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[2]\,
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => yreg2(2),
      O => \s22[2]_i_1__3_n_0\
    );
\s22[3]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[3]\,
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => yreg2(3),
      O => \s22[3]_i_1__3_n_0\
    );
\s22[4]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[4]\,
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => yreg2(4),
      O => \s22[4]_i_1__3_n_0\
    );
\s22[5]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[5]\,
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => yreg2(5),
      O => \s22[5]_i_1__3_n_0\
    );
\s22[6]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[6]\,
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => yreg2(6),
      O => \s22[6]_i_1__3_n_0\
    );
\s22[7]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[7]\,
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => yreg2(7),
      O => \s22[7]_i_1__3_n_0\
    );
\s22[8]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[8]\,
      I1 => \s22[8]_i_2__3_n_0\,
      I2 => yreg2(8),
      O => \s22[8]_i_1__3_n_0\
    );
\s22[8]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B200FFB2"
    )
        port map (
      I0 => \s22[8]_i_3__3_n_0\,
      I1 => \yreg1_reg_n_0_[8]\,
      I2 => yreg2(8),
      I3 => yreg2(9),
      I4 => \yreg1_reg_n_0_[9]\,
      O => \s22[8]_i_2__3_n_0\
    );
\s22[8]_i_3__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7050FFFF"
    )
        port map (
      I0 => \s22[8]_i_4__3_n_0\,
      I1 => \s22[8]_i_5__3_n_0\,
      I2 => \s22[8]_i_6__3_n_0\,
      I3 => \s22[8]_i_7__3_n_0\,
      I4 => \s22[8]_i_8__3_n_0\,
      O => \s22[8]_i_3__3_n_0\
    );
\s22[8]_i_4__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => \yreg1_reg_n_0_[4]\,
      I1 => yreg2(4),
      I2 => yreg2(5),
      I3 => \yreg1_reg_n_0_[5]\,
      O => \s22[8]_i_4__3_n_0\
    );
\s22[8]_i_5__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4D44DDDD"
    )
        port map (
      I0 => yreg2(3),
      I1 => \yreg1_reg_n_0_[3]\,
      I2 => yreg2(2),
      I3 => \yreg1_reg_n_0_[2]\,
      I4 => \s22[8]_i_9__3_n_0\,
      O => \s22[8]_i_5__3_n_0\
    );
\s22[8]_i_6__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BB2B"
    )
        port map (
      I0 => yreg2(7),
      I1 => \yreg1_reg_n_0_[7]\,
      I2 => \yreg1_reg_n_0_[6]\,
      I3 => yreg2(6),
      O => \s22[8]_i_6__3_n_0\
    );
\s22[8]_i_7__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF0B"
    )
        port map (
      I0 => yreg2(4),
      I1 => \yreg1_reg_n_0_[4]\,
      I2 => \yreg1_reg_n_0_[5]\,
      I3 => yreg2(5),
      O => \s22[8]_i_7__3_n_0\
    );
\s22[8]_i_8__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B0FB"
    )
        port map (
      I0 => \yreg1_reg_n_0_[6]\,
      I1 => yreg2(6),
      I2 => \yreg1_reg_n_0_[7]\,
      I3 => yreg2(7),
      O => \s22[8]_i_8__3_n_0\
    );
\s22[8]_i_9__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40F440F4FFFF40F4"
    )
        port map (
      I0 => \yreg1_reg_n_0_[0]\,
      I1 => yreg2(0),
      I2 => yreg2(1),
      I3 => \yreg1_reg_n_0_[1]\,
      I4 => yreg2(2),
      I5 => \yreg1_reg_n_0_[2]\,
      O => \s22[8]_i_9__3_n_0\
    );
\s22[9]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \yreg1_reg_n_0_[9]\,
      I1 => yreg2(9),
      O => \s22[9]_i_1__3_n_0\
    );
\s22_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[0]_i_1__3_n_0\,
      Q => s22(0)
    );
\s22_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[1]_i_1__3_n_0\,
      Q => s22(1)
    );
\s22_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[2]_i_1__3_n_0\,
      Q => s22(2)
    );
\s22_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[3]_i_1__3_n_0\,
      Q => s22(3)
    );
\s22_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[4]_i_1__3_n_0\,
      Q => s22(4)
    );
\s22_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[5]_i_1__3_n_0\,
      Q => s22(5)
    );
\s22_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[6]_i_1__3_n_0\,
      Q => s22(6)
    );
\s22_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[7]_i_1__3_n_0\,
      Q => s22(7)
    );
\s22_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[8]_i_1__3_n_0\,
      Q => s22(8)
    );
\s22_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => s21,
      CLR => reset_0,
      D => \s22[9]_i_1__3_n_0\,
      Q => s22(9)
    );
\xreg1[9]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F7FF0000"
    )
        port map (
      I0 => \full_reg_n_0_[0]\,
      I1 => \^m_axis_tkeep[4]\,
      I2 => m_axis_tready,
      I3 => \full_reg_n_0_[1]\,
      I4 => \valid_reg[4]\(0),
      I5 => hold,
      O => xreg1
    );
\xreg1_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => D(0),
      Q => \xreg1_reg_n_0_[0]\
    );
\xreg1_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => D(1),
      Q => \xreg1_reg_n_0_[1]\
    );
\xreg1_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => D(2),
      Q => \xreg1_reg_n_0_[2]\
    );
\xreg1_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => D(3),
      Q => \xreg1_reg_n_0_[3]\
    );
\xreg1_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => D(4),
      Q => \xreg1_reg_n_0_[4]\
    );
\xreg1_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => D(5),
      Q => \xreg1_reg_n_0_[5]\
    );
\xreg1_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => D(6),
      Q => \xreg1_reg_n_0_[6]\
    );
\xreg1_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => D(7),
      Q => \xreg1_reg_n_0_[7]\
    );
\xreg1_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => D(8),
      Q => \xreg1_reg_n_0_[8]\
    );
\xreg1_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => D(9),
      Q => \xreg1_reg_n_0_[9]\
    );
\xreg2[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(16),
      I1 => Q(0),
      O => \x22/f1/S\
    );
\xreg2[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(1),
      I1 => Q(17),
      I2 => Q(8),
      I3 => Q(0),
      I4 => Q(16),
      O => outx22(1)
    );
\xreg2[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(2),
      I1 => Q(9),
      I2 => Q(18),
      I3 => Q(8),
      I4 => Q(17),
      I5 => \x22/C_1\,
      O => outx22(2)
    );
\xreg2[2]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(16),
      I1 => Q(0),
      I2 => Q(8),
      I3 => Q(17),
      I4 => Q(1),
      O => \x22/C_1\
    );
\xreg2[3]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(3),
      I1 => outx21(3),
      I2 => \x22/C_2\,
      O => outx22(3)
    );
\xreg2[3]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(10),
      I1 => Q(19),
      I2 => Q(9),
      I3 => Q(18),
      I4 => Q(8),
      I5 => Q(17),
      O => outx21(3)
    );
\xreg2[3]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \x22/C_1\,
      I1 => Q(17),
      I2 => Q(8),
      I3 => Q(18),
      I4 => Q(9),
      I5 => Q(2),
      O => \x22/C_2\
    );
\xreg2[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(4),
      I1 => Q(11),
      I2 => Q(20),
      I3 => \x21/C_3\,
      I4 => \x22/C_3\,
      O => outx22(4)
    );
\xreg2[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \x22/C_3\,
      I1 => \x21/C_3\,
      I2 => Q(20),
      I3 => Q(11),
      I4 => Q(4),
      I5 => \x22/f6/S\,
      O => outx22(5)
    );
\xreg2[5]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \x22/C_1\,
      I1 => outx21(2),
      I2 => Q(2),
      I3 => outx21(3),
      I4 => Q(3),
      O => \x22/C_3\
    );
\xreg2[5]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(17),
      I1 => Q(8),
      I2 => Q(18),
      I3 => Q(9),
      I4 => Q(19),
      I5 => Q(10),
      O => \x21/C_3\
    );
\xreg2[5]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \x21/C_3\,
      I1 => Q(20),
      I2 => Q(11),
      I3 => Q(21),
      I4 => Q(12),
      I5 => Q(5),
      O => \x22/f6/S\
    );
\xreg2[5]_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(9),
      I1 => Q(18),
      I2 => Q(8),
      I3 => Q(17),
      O => outx21(2)
    );
\xreg2[6]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(13),
      I2 => Q(22),
      I3 => \x21/C_5\,
      I4 => \x22/C_5\,
      O => outx22(6)
    );
\xreg2[6]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \x21/C_3\,
      I1 => Q(20),
      I2 => Q(11),
      I3 => Q(21),
      I4 => Q(12),
      O => \x21/C_5\
    );
\xreg2[6]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \x22/C_3\,
      I1 => outx21(4),
      I2 => Q(4),
      I3 => outx21(5),
      I4 => Q(5),
      O => \x22/C_5\
    );
\xreg2[6]_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(11),
      I1 => Q(20),
      I2 => \x21/C_3\,
      O => outx21(4)
    );
\xreg2[6]_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(12),
      I1 => Q(21),
      I2 => Q(11),
      I3 => Q(20),
      I4 => \x21/C_3\,
      O => outx21(5)
    );
\xreg2[7]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \x22/f8/S\,
      I1 => \x22/C_6\,
      O => outx22(7)
    );
\xreg2[7]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \x21/C_5\,
      I1 => Q(22),
      I2 => Q(13),
      I3 => Q(23),
      I4 => Q(14),
      I5 => Q(7),
      O => \x22/f8/S\
    );
\xreg2[8]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(15),
      I1 => Q(7),
      I2 => Q(14),
      I3 => Q(23),
      I4 => \x21/C_6\,
      I5 => \x22/C_6\,
      O => outx22(8)
    );
\xreg2[9]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \x22/C_6\,
      I1 => \x21/C_6\,
      I2 => Q(23),
      I3 => Q(14),
      I4 => Q(7),
      I5 => Q(15),
      O => outx22(9)
    );
\xreg2[9]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \x22/C_5\,
      I1 => \x21/C_5\,
      I2 => Q(22),
      I3 => Q(13),
      I4 => Q(6),
      O => \x22/C_6\
    );
\xreg2[9]_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \x21/C_5\,
      I1 => Q(22),
      I2 => Q(13),
      O => \x21/C_6\
    );
\xreg2_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => \x22/f1/S\,
      Q => xreg2(0)
    );
\xreg2_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => outx22(1),
      Q => xreg2(1)
    );
\xreg2_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => outx22(2),
      Q => xreg2(2)
    );
\xreg2_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => outx22(3),
      Q => xreg2(3)
    );
\xreg2_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => outx22(4),
      Q => xreg2(4)
    );
\xreg2_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => outx22(5),
      Q => xreg2(5)
    );
\xreg2_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => outx22(6),
      Q => xreg2(6)
    );
\xreg2_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => outx22(7),
      Q => xreg2(7)
    );
\xreg2_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => outx22(8),
      Q => xreg2(8)
    );
\xreg2_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clock,
      CE => xreg1,
      CLR => reset_0,
      D => outx22(9),
      Q => xreg2(9)
    );
\yreg1[0]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(48),
      I1 => Q(16),
      O => \y12/f1/S\
    );
\yreg1[1]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(17),
      I1 => Q(49),
      I2 => Q(32),
      I3 => Q(16),
      I4 => Q(48),
      O => outy12(1)
    );
\yreg1[2]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(18),
      I1 => Q(33),
      I2 => Q(50),
      I3 => Q(32),
      I4 => Q(49),
      I5 => \y12/C_1\,
      O => outy12(2)
    );
\yreg1[2]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(48),
      I1 => Q(16),
      I2 => Q(32),
      I3 => Q(49),
      I4 => Q(17),
      O => \y12/C_1\
    );
\yreg1[3]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(19),
      I1 => outy11(3),
      I2 => \y12/C_2\,
      O => outy12(3)
    );
\yreg1[3]_i_2__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(34),
      I1 => Q(51),
      I2 => Q(33),
      I3 => Q(50),
      I4 => Q(32),
      I5 => Q(49),
      O => outy11(3)
    );
\yreg1[3]_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \y12/C_1\,
      I1 => Q(49),
      I2 => Q(32),
      I3 => Q(50),
      I4 => Q(33),
      I5 => Q(18),
      O => \y12/C_2\
    );
\yreg1[4]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(20),
      I1 => Q(35),
      I2 => Q(52),
      I3 => \y11/C_3\,
      I4 => \y12/C_3\,
      O => outy12(4)
    );
\yreg1[5]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \y12/C_3\,
      I1 => \y11/C_3\,
      I2 => Q(52),
      I3 => Q(35),
      I4 => Q(20),
      I5 => \y12/f6/S\,
      O => outy12(5)
    );
\yreg1[5]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y12/C_1\,
      I1 => outy11(2),
      I2 => Q(18),
      I3 => outy11(3),
      I4 => Q(19),
      O => \y12/C_3\
    );
\yreg1[5]_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(49),
      I1 => Q(32),
      I2 => Q(50),
      I3 => Q(33),
      I4 => Q(51),
      I5 => Q(34),
      O => \y11/C_3\
    );
\yreg1[5]_i_4__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y11/C_3\,
      I1 => Q(52),
      I2 => Q(35),
      I3 => Q(53),
      I4 => Q(36),
      I5 => Q(21),
      O => \y12/f6/S\
    );
\yreg1[5]_i_5__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(33),
      I1 => Q(50),
      I2 => Q(32),
      I3 => Q(49),
      O => outy11(2)
    );
\yreg1[6]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(22),
      I1 => Q(37),
      I2 => Q(54),
      I3 => \y11/C_5\,
      I4 => \y12/C_5\,
      O => outy12(6)
    );
\yreg1[6]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y11/C_3\,
      I1 => Q(52),
      I2 => Q(35),
      I3 => Q(53),
      I4 => Q(36),
      O => \y11/C_5\
    );
\yreg1[6]_i_3__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y12/C_3\,
      I1 => outy11(4),
      I2 => Q(20),
      I3 => outy11(5),
      I4 => Q(21),
      O => \y12/C_5\
    );
\yreg1[6]_i_4__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(35),
      I1 => Q(52),
      I2 => \y11/C_3\,
      O => outy11(4)
    );
\yreg1[6]_i_5__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(36),
      I1 => Q(53),
      I2 => Q(35),
      I3 => Q(52),
      I4 => \y11/C_3\,
      O => outy11(5)
    );
\yreg1[7]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \y12/f8/S\,
      I1 => \y12/C_6\,
      O => outy12(7)
    );
\yreg1[7]_i_2__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y11/C_5\,
      I1 => Q(54),
      I2 => Q(37),
      I3 => Q(55),
      I4 => Q(38),
      I5 => Q(23),
      O => \y12/f8/S\
    );
\yreg1[8]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(39),
      I1 => Q(23),
      I2 => Q(38),
      I3 => Q(55),
      I4 => \y11/C_6\,
      I5 => \y12/C_6\,
      O => outy12(8)
    );
\yreg1[9]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FF000000000000"
    )
        port map (
      I0 => \full_reg_n_0_[0]\,
      I1 => \^m_axis_tkeep[4]\,
      I2 => m_axis_tready,
      I3 => \full_reg_n_0_[1]\,
      I4 => \valid_reg[4]\(0),
      I5 => \^yreg2_reg[0]_0\,
      O => yreg1
    );
\yreg1[9]_i_2__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \y12/C_6\,
      I1 => \y11/C_6\,
      I2 => Q(55),
      I3 => Q(38),
      I4 => Q(23),
      I5 => Q(39),
      O => outy12(9)
    );
\yreg1[9]_i_3__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \y12/C_5\,
      I1 => \y11/C_5\,
      I2 => Q(54),
      I3 => Q(37),
      I4 => Q(22),
      O => \y12/C_6\
    );
\yreg1[9]_i_3__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => reset,
      I1 => hold,
      O => \^yreg2_reg[0]_0\
    );
\yreg1[9]_i_4__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \y11/C_5\,
      I1 => Q(54),
      I2 => Q(37),
      O => \y11/C_6\
    );
\yreg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => \y12/f1/S\,
      Q => \yreg1_reg_n_0_[0]\,
      R => '0'
    );
\yreg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(1),
      Q => \yreg1_reg_n_0_[1]\,
      R => '0'
    );
\yreg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(2),
      Q => \yreg1_reg_n_0_[2]\,
      R => '0'
    );
\yreg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(3),
      Q => \yreg1_reg_n_0_[3]\,
      R => '0'
    );
\yreg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(4),
      Q => \yreg1_reg_n_0_[4]\,
      R => '0'
    );
\yreg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(5),
      Q => \yreg1_reg_n_0_[5]\,
      R => '0'
    );
\yreg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(6),
      Q => \yreg1_reg_n_0_[6]\,
      R => '0'
    );
\yreg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(7),
      Q => \yreg1_reg_n_0_[7]\,
      R => '0'
    );
\yreg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(8),
      Q => \yreg1_reg_n_0_[8]\,
      R => '0'
    );
\yreg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy12(9),
      Q => \yreg1_reg_n_0_[9]\,
      R => '0'
    );
\yreg2[0]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(40),
      I1 => Q(0),
      O => \y22/f1/S\
    );
\yreg2[1]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(1),
      I1 => Q(41),
      I2 => Q(24),
      I3 => Q(0),
      I4 => Q(40),
      O => outy22(1)
    );
\yreg2[2]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696969969696"
    )
        port map (
      I0 => Q(2),
      I1 => Q(25),
      I2 => Q(42),
      I3 => Q(24),
      I4 => Q(41),
      I5 => \y22/C_1\,
      O => outy22(2)
    );
\yreg2[2]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FF80880"
    )
        port map (
      I0 => Q(40),
      I1 => Q(0),
      I2 => Q(24),
      I3 => Q(41),
      I4 => Q(1),
      O => \y22/C_1\
    );
\yreg2[3]_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(3),
      I1 => outy21(3),
      I2 => \y22/C_2\,
      O => outy22(3)
    );
\yreg2[3]_i_2__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996966696669666"
    )
        port map (
      I0 => Q(26),
      I1 => Q(43),
      I2 => Q(25),
      I3 => Q(42),
      I4 => Q(24),
      I5 => Q(41),
      O => outy21(3)
    );
\yreg2[3]_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EABFBFEA802A2A80"
    )
        port map (
      I0 => \y22/C_1\,
      I1 => Q(41),
      I2 => Q(24),
      I3 => Q(42),
      I4 => Q(25),
      I5 => Q(2),
      O => \y22/C_2\
    );
\yreg2[4]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(4),
      I1 => Q(27),
      I2 => Q(44),
      I3 => \y21/C_3\,
      I4 => \y22/C_3\,
      O => outy22(4)
    );
\yreg2[5]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14417DD7EBBE8228"
    )
        port map (
      I0 => \y22/C_3\,
      I1 => \y21/C_3\,
      I2 => Q(44),
      I3 => Q(27),
      I4 => Q(4),
      I5 => \y22/f6/S\,
      O => outy22(5)
    );
\yreg2[5]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y22/C_1\,
      I1 => outy21(2),
      I2 => Q(2),
      I3 => outy21(3),
      I4 => Q(3),
      O => \y22/C_3\
    );
\yreg2[5]_i_3__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF880F8800000"
    )
        port map (
      I0 => Q(41),
      I1 => Q(24),
      I2 => Q(42),
      I3 => Q(25),
      I4 => Q(43),
      I5 => Q(26),
      O => \y21/C_3\
    );
\yreg2[5]_i_4__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y21/C_3\,
      I1 => Q(44),
      I2 => Q(27),
      I3 => Q(45),
      I4 => Q(28),
      I5 => Q(5),
      O => \y22/f6/S\
    );
\yreg2[5]_i_5__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => Q(25),
      I1 => Q(42),
      I2 => Q(24),
      I3 => Q(41),
      O => outy21(2)
    );
\yreg2[6]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(29),
      I2 => Q(46),
      I3 => \y21/C_5\,
      I4 => \y22/C_5\,
      O => outy22(6)
    );
\yreg2[6]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y21/C_3\,
      I1 => Q(44),
      I2 => Q(27),
      I3 => Q(45),
      I4 => Q(28),
      O => \y21/C_5\
    );
\yreg2[6]_i_3__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \y22/C_3\,
      I1 => outy21(4),
      I2 => Q(4),
      I3 => outy21(5),
      I4 => Q(5),
      O => \y22/C_5\
    );
\yreg2[6]_i_4__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(27),
      I1 => Q(44),
      I2 => \y21/C_3\,
      O => outy21(4)
    );
\yreg2[6]_i_5__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => Q(28),
      I1 => Q(45),
      I2 => Q(27),
      I3 => Q(44),
      I4 => \y21/C_3\,
      O => outy21(5)
    );
\yreg2[7]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \y22/f8/S\,
      I1 => \y22/C_6\,
      O => outy22(7)
    );
\yreg2[7]_i_2__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \y21/C_5\,
      I1 => Q(46),
      I2 => Q(29),
      I3 => Q(47),
      I4 => Q(30),
      I5 => Q(7),
      O => \y22/f8/S\
    );
\yreg2[8]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A99595569556566A"
    )
        port map (
      I0 => Q(31),
      I1 => Q(7),
      I2 => Q(30),
      I3 => Q(47),
      I4 => \y21/C_6\,
      I5 => \y22/C_6\,
      O => outy22(8)
    );
\yreg2[9]_i_1__3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEE8E8808000"
    )
        port map (
      I0 => \y22/C_6\,
      I1 => \y21/C_6\,
      I2 => Q(47),
      I3 => Q(30),
      I4 => Q(7),
      I5 => Q(31),
      O => outy22(9)
    );
\yreg2[9]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBE8228"
    )
        port map (
      I0 => \y22/C_5\,
      I1 => \y21/C_5\,
      I2 => Q(46),
      I3 => Q(29),
      I4 => Q(6),
      O => \y22/C_6\
    );
\yreg2[9]_i_3__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \y21/C_5\,
      I1 => Q(46),
      I2 => Q(29),
      O => \y21/C_6\
    );
\yreg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => \y22/f1/S\,
      Q => yreg2(0),
      R => '0'
    );
\yreg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(1),
      Q => yreg2(1),
      R => '0'
    );
\yreg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(2),
      Q => yreg2(2),
      R => '0'
    );
\yreg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(3),
      Q => yreg2(3),
      R => '0'
    );
\yreg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(4),
      Q => yreg2(4),
      R => '0'
    );
\yreg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(5),
      Q => yreg2(5),
      R => '0'
    );
\yreg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(6),
      Q => yreg2(6),
      R => '0'
    );
\yreg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(7),
      Q => yreg2(7),
      R => '0'
    );
\yreg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(8),
      Q => yreg2(8),
      R => '0'
    );
\yreg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => yreg1,
      D => outy22(9),
      Q => yreg2(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity finalproj_sobelfilteraxistream_0_0_sobelfilteraxistream is
  port (
    \m_axis_tkeep[0]\ : out STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 39 downto 0 );
    \m_axis_tkeep[1]\ : out STD_LOGIC;
    \m_axis_tkeep[2]\ : out STD_LOGIC;
    \m_axis_tkeep[3]\ : out STD_LOGIC;
    \m_axis_tkeep[4]\ : out STD_LOGIC;
    m_axis_tlast : out STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    m_axis_tvalid : out STD_LOGIC;
    s_axis_tstrb : in STD_LOGIC_VECTOR ( 14 downto 0 );
    clock : in STD_LOGIC;
    reset : in STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of finalproj_sobelfilteraxistream_0_0_sobelfilteraxistream : entity is "sobelfilteraxistream";
end finalproj_sobelfilteraxistream_0_0_sobelfilteraxistream;

architecture STRUCTURE of finalproj_sobelfilteraxistream_0_0_sobelfilteraxistream is
  signal c1_n_10 : STD_LOGIC;
  signal c1_n_11 : STD_LOGIC;
  signal c1_n_15 : STD_LOGIC;
  signal c1_n_16 : STD_LOGIC;
  signal c1_n_18 : STD_LOGIC;
  signal c3_n_10 : STD_LOGIC;
  signal c4_n_0 : STD_LOGIC;
  signal c5_n_0 : STD_LOGIC;
  signal f : STD_LOGIC;
  signal f_i_2_n_0 : STD_LOGIC;
  signal f_i_3_n_0 : STD_LOGIC;
  signal hold : STD_LOGIC;
  signal hold0 : STD_LOGIC;
  signal inputvals : STD_LOGIC_VECTOR ( 167 downto 0 );
  signal \^m_axis_tkeep[0]\ : STD_LOGIC;
  signal \^m_axis_tkeep[2]\ : STD_LOGIC;
  signal \^m_axis_tkeep[3]\ : STD_LOGIC;
  signal \^m_axis_tkeep[4]\ : STD_LOGIC;
  signal \^m_axis_tlast\ : STD_LOGIC;
  signal outx12 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal outx12_1 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal outx12_3 : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal p_0_in : STD_LOGIC_VECTOR ( 167 downto 48 );
  signal p_1_out : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal temp0 : STD_LOGIC;
  signal \temp[47]_i_2_n_0\ : STD_LOGIC;
  signal \temp_reg_n_0_[0]\ : STD_LOGIC;
  signal \temp_reg_n_0_[10]\ : STD_LOGIC;
  signal \temp_reg_n_0_[11]\ : STD_LOGIC;
  signal \temp_reg_n_0_[12]\ : STD_LOGIC;
  signal \temp_reg_n_0_[13]\ : STD_LOGIC;
  signal \temp_reg_n_0_[14]\ : STD_LOGIC;
  signal \temp_reg_n_0_[15]\ : STD_LOGIC;
  signal \temp_reg_n_0_[16]\ : STD_LOGIC;
  signal \temp_reg_n_0_[17]\ : STD_LOGIC;
  signal \temp_reg_n_0_[18]\ : STD_LOGIC;
  signal \temp_reg_n_0_[19]\ : STD_LOGIC;
  signal \temp_reg_n_0_[1]\ : STD_LOGIC;
  signal \temp_reg_n_0_[20]\ : STD_LOGIC;
  signal \temp_reg_n_0_[21]\ : STD_LOGIC;
  signal \temp_reg_n_0_[22]\ : STD_LOGIC;
  signal \temp_reg_n_0_[23]\ : STD_LOGIC;
  signal \temp_reg_n_0_[24]\ : STD_LOGIC;
  signal \temp_reg_n_0_[25]\ : STD_LOGIC;
  signal \temp_reg_n_0_[26]\ : STD_LOGIC;
  signal \temp_reg_n_0_[27]\ : STD_LOGIC;
  signal \temp_reg_n_0_[28]\ : STD_LOGIC;
  signal \temp_reg_n_0_[29]\ : STD_LOGIC;
  signal \temp_reg_n_0_[2]\ : STD_LOGIC;
  signal \temp_reg_n_0_[30]\ : STD_LOGIC;
  signal \temp_reg_n_0_[31]\ : STD_LOGIC;
  signal \temp_reg_n_0_[32]\ : STD_LOGIC;
  signal \temp_reg_n_0_[33]\ : STD_LOGIC;
  signal \temp_reg_n_0_[34]\ : STD_LOGIC;
  signal \temp_reg_n_0_[35]\ : STD_LOGIC;
  signal \temp_reg_n_0_[36]\ : STD_LOGIC;
  signal \temp_reg_n_0_[37]\ : STD_LOGIC;
  signal \temp_reg_n_0_[38]\ : STD_LOGIC;
  signal \temp_reg_n_0_[39]\ : STD_LOGIC;
  signal \temp_reg_n_0_[3]\ : STD_LOGIC;
  signal \temp_reg_n_0_[40]\ : STD_LOGIC;
  signal \temp_reg_n_0_[41]\ : STD_LOGIC;
  signal \temp_reg_n_0_[42]\ : STD_LOGIC;
  signal \temp_reg_n_0_[43]\ : STD_LOGIC;
  signal \temp_reg_n_0_[44]\ : STD_LOGIC;
  signal \temp_reg_n_0_[45]\ : STD_LOGIC;
  signal \temp_reg_n_0_[46]\ : STD_LOGIC;
  signal \temp_reg_n_0_[47]\ : STD_LOGIC;
  signal \temp_reg_n_0_[4]\ : STD_LOGIC;
  signal \temp_reg_n_0_[5]\ : STD_LOGIC;
  signal \temp_reg_n_0_[6]\ : STD_LOGIC;
  signal \temp_reg_n_0_[7]\ : STD_LOGIC;
  signal \temp_reg_n_0_[8]\ : STD_LOGIC;
  signal \temp_reg_n_0_[9]\ : STD_LOGIC;
  signal valid : STD_LOGIC;
  signal \valid[1]_i_2_n_0\ : STD_LOGIC;
  signal \valid[1]_i_3_n_0\ : STD_LOGIC;
  signal \valid[2]_i_1_n_0\ : STD_LOGIC;
  signal \valid[2]_i_2_n_0\ : STD_LOGIC;
  signal \valid_reg_n_0_[0]\ : STD_LOGIC;
  signal \valid_reg_n_0_[1]\ : STD_LOGIC;
  signal \valid_reg_n_0_[2]\ : STD_LOGIC;
  signal \valid_reg_n_0_[3]\ : STD_LOGIC;
  signal \valid_reg_n_0_[4]\ : STD_LOGIC;
  signal \x12/f1/S\ : STD_LOGIC;
  signal \x12/f1/S_0\ : STD_LOGIC;
  signal \x12/f1/S_2\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \inputvals[100]_i_1\ : label is "soft_lutpair270";
  attribute SOFT_HLUTNM of \inputvals[101]_i_1\ : label is "soft_lutpair269";
  attribute SOFT_HLUTNM of \inputvals[102]_i_1\ : label is "soft_lutpair268";
  attribute SOFT_HLUTNM of \inputvals[103]_i_1\ : label is "soft_lutpair267";
  attribute SOFT_HLUTNM of \inputvals[104]_i_1\ : label is "soft_lutpair266";
  attribute SOFT_HLUTNM of \inputvals[105]_i_1\ : label is "soft_lutpair265";
  attribute SOFT_HLUTNM of \inputvals[106]_i_1\ : label is "soft_lutpair264";
  attribute SOFT_HLUTNM of \inputvals[107]_i_1\ : label is "soft_lutpair263";
  attribute SOFT_HLUTNM of \inputvals[108]_i_1\ : label is "soft_lutpair262";
  attribute SOFT_HLUTNM of \inputvals[109]_i_1\ : label is "soft_lutpair261";
  attribute SOFT_HLUTNM of \inputvals[110]_i_1\ : label is "soft_lutpair260";
  attribute SOFT_HLUTNM of \inputvals[111]_i_1\ : label is "soft_lutpair259";
  attribute SOFT_HLUTNM of \inputvals[112]_i_1\ : label is "soft_lutpair258";
  attribute SOFT_HLUTNM of \inputvals[113]_i_1\ : label is "soft_lutpair257";
  attribute SOFT_HLUTNM of \inputvals[114]_i_1\ : label is "soft_lutpair256";
  attribute SOFT_HLUTNM of \inputvals[115]_i_1\ : label is "soft_lutpair255";
  attribute SOFT_HLUTNM of \inputvals[116]_i_1\ : label is "soft_lutpair254";
  attribute SOFT_HLUTNM of \inputvals[117]_i_1\ : label is "soft_lutpair253";
  attribute SOFT_HLUTNM of \inputvals[118]_i_1\ : label is "soft_lutpair252";
  attribute SOFT_HLUTNM of \inputvals[119]_i_1\ : label is "soft_lutpair251";
  attribute SOFT_HLUTNM of \inputvals[120]_i_1\ : label is "soft_lutpair298";
  attribute SOFT_HLUTNM of \inputvals[121]_i_1\ : label is "soft_lutpair297";
  attribute SOFT_HLUTNM of \inputvals[122]_i_1\ : label is "soft_lutpair296";
  attribute SOFT_HLUTNM of \inputvals[123]_i_1\ : label is "soft_lutpair295";
  attribute SOFT_HLUTNM of \inputvals[124]_i_1\ : label is "soft_lutpair294";
  attribute SOFT_HLUTNM of \inputvals[125]_i_1\ : label is "soft_lutpair293";
  attribute SOFT_HLUTNM of \inputvals[126]_i_1\ : label is "soft_lutpair292";
  attribute SOFT_HLUTNM of \inputvals[127]_i_1\ : label is "soft_lutpair291";
  attribute SOFT_HLUTNM of \inputvals[128]_i_1\ : label is "soft_lutpair290";
  attribute SOFT_HLUTNM of \inputvals[129]_i_1\ : label is "soft_lutpair289";
  attribute SOFT_HLUTNM of \inputvals[130]_i_1\ : label is "soft_lutpair288";
  attribute SOFT_HLUTNM of \inputvals[131]_i_1\ : label is "soft_lutpair287";
  attribute SOFT_HLUTNM of \inputvals[132]_i_1\ : label is "soft_lutpair286";
  attribute SOFT_HLUTNM of \inputvals[133]_i_1\ : label is "soft_lutpair285";
  attribute SOFT_HLUTNM of \inputvals[134]_i_1\ : label is "soft_lutpair284";
  attribute SOFT_HLUTNM of \inputvals[135]_i_1\ : label is "soft_lutpair283";
  attribute SOFT_HLUTNM of \inputvals[136]_i_1\ : label is "soft_lutpair282";
  attribute SOFT_HLUTNM of \inputvals[137]_i_1\ : label is "soft_lutpair281";
  attribute SOFT_HLUTNM of \inputvals[138]_i_1\ : label is "soft_lutpair280";
  attribute SOFT_HLUTNM of \inputvals[139]_i_1\ : label is "soft_lutpair279";
  attribute SOFT_HLUTNM of \inputvals[140]_i_1\ : label is "soft_lutpair278";
  attribute SOFT_HLUTNM of \inputvals[141]_i_1\ : label is "soft_lutpair277";
  attribute SOFT_HLUTNM of \inputvals[142]_i_1\ : label is "soft_lutpair276";
  attribute SOFT_HLUTNM of \inputvals[143]_i_1\ : label is "soft_lutpair275";
  attribute SOFT_HLUTNM of \inputvals[144]_i_1\ : label is "soft_lutpair274";
  attribute SOFT_HLUTNM of \inputvals[145]_i_1\ : label is "soft_lutpair273";
  attribute SOFT_HLUTNM of \inputvals[146]_i_1\ : label is "soft_lutpair272";
  attribute SOFT_HLUTNM of \inputvals[147]_i_1\ : label is "soft_lutpair271";
  attribute SOFT_HLUTNM of \inputvals[148]_i_1\ : label is "soft_lutpair270";
  attribute SOFT_HLUTNM of \inputvals[149]_i_1\ : label is "soft_lutpair269";
  attribute SOFT_HLUTNM of \inputvals[150]_i_1\ : label is "soft_lutpair268";
  attribute SOFT_HLUTNM of \inputvals[151]_i_1\ : label is "soft_lutpair267";
  attribute SOFT_HLUTNM of \inputvals[152]_i_1\ : label is "soft_lutpair266";
  attribute SOFT_HLUTNM of \inputvals[153]_i_1\ : label is "soft_lutpair265";
  attribute SOFT_HLUTNM of \inputvals[154]_i_1\ : label is "soft_lutpair264";
  attribute SOFT_HLUTNM of \inputvals[155]_i_1\ : label is "soft_lutpair263";
  attribute SOFT_HLUTNM of \inputvals[156]_i_1\ : label is "soft_lutpair262";
  attribute SOFT_HLUTNM of \inputvals[157]_i_1\ : label is "soft_lutpair261";
  attribute SOFT_HLUTNM of \inputvals[158]_i_1\ : label is "soft_lutpair260";
  attribute SOFT_HLUTNM of \inputvals[159]_i_1\ : label is "soft_lutpair259";
  attribute SOFT_HLUTNM of \inputvals[160]_i_1\ : label is "soft_lutpair258";
  attribute SOFT_HLUTNM of \inputvals[161]_i_1\ : label is "soft_lutpair257";
  attribute SOFT_HLUTNM of \inputvals[162]_i_1\ : label is "soft_lutpair256";
  attribute SOFT_HLUTNM of \inputvals[163]_i_1\ : label is "soft_lutpair255";
  attribute SOFT_HLUTNM of \inputvals[164]_i_1\ : label is "soft_lutpair254";
  attribute SOFT_HLUTNM of \inputvals[165]_i_1\ : label is "soft_lutpair253";
  attribute SOFT_HLUTNM of \inputvals[166]_i_1\ : label is "soft_lutpair252";
  attribute SOFT_HLUTNM of \inputvals[167]_i_1\ : label is "soft_lutpair251";
  attribute SOFT_HLUTNM of \inputvals[48]_i_1\ : label is "soft_lutpair310";
  attribute SOFT_HLUTNM of \inputvals[49]_i_1\ : label is "soft_lutpair310";
  attribute SOFT_HLUTNM of \inputvals[50]_i_1\ : label is "soft_lutpair309";
  attribute SOFT_HLUTNM of \inputvals[51]_i_1\ : label is "soft_lutpair309";
  attribute SOFT_HLUTNM of \inputvals[52]_i_1\ : label is "soft_lutpair308";
  attribute SOFT_HLUTNM of \inputvals[53]_i_1\ : label is "soft_lutpair308";
  attribute SOFT_HLUTNM of \inputvals[54]_i_1\ : label is "soft_lutpair307";
  attribute SOFT_HLUTNM of \inputvals[55]_i_1\ : label is "soft_lutpair307";
  attribute SOFT_HLUTNM of \inputvals[56]_i_1\ : label is "soft_lutpair306";
  attribute SOFT_HLUTNM of \inputvals[57]_i_1\ : label is "soft_lutpair306";
  attribute SOFT_HLUTNM of \inputvals[58]_i_1\ : label is "soft_lutpair305";
  attribute SOFT_HLUTNM of \inputvals[59]_i_1\ : label is "soft_lutpair305";
  attribute SOFT_HLUTNM of \inputvals[60]_i_1\ : label is "soft_lutpair304";
  attribute SOFT_HLUTNM of \inputvals[61]_i_1\ : label is "soft_lutpair304";
  attribute SOFT_HLUTNM of \inputvals[62]_i_1\ : label is "soft_lutpair303";
  attribute SOFT_HLUTNM of \inputvals[63]_i_1\ : label is "soft_lutpair303";
  attribute SOFT_HLUTNM of \inputvals[64]_i_1\ : label is "soft_lutpair302";
  attribute SOFT_HLUTNM of \inputvals[65]_i_1\ : label is "soft_lutpair302";
  attribute SOFT_HLUTNM of \inputvals[66]_i_1\ : label is "soft_lutpair301";
  attribute SOFT_HLUTNM of \inputvals[67]_i_1\ : label is "soft_lutpair301";
  attribute SOFT_HLUTNM of \inputvals[68]_i_1\ : label is "soft_lutpair300";
  attribute SOFT_HLUTNM of \inputvals[69]_i_1\ : label is "soft_lutpair300";
  attribute SOFT_HLUTNM of \inputvals[70]_i_1\ : label is "soft_lutpair299";
  attribute SOFT_HLUTNM of \inputvals[71]_i_1\ : label is "soft_lutpair299";
  attribute SOFT_HLUTNM of \inputvals[72]_i_1\ : label is "soft_lutpair298";
  attribute SOFT_HLUTNM of \inputvals[73]_i_1\ : label is "soft_lutpair297";
  attribute SOFT_HLUTNM of \inputvals[74]_i_1\ : label is "soft_lutpair296";
  attribute SOFT_HLUTNM of \inputvals[75]_i_1\ : label is "soft_lutpair295";
  attribute SOFT_HLUTNM of \inputvals[76]_i_1\ : label is "soft_lutpair294";
  attribute SOFT_HLUTNM of \inputvals[77]_i_1\ : label is "soft_lutpair293";
  attribute SOFT_HLUTNM of \inputvals[78]_i_1\ : label is "soft_lutpair292";
  attribute SOFT_HLUTNM of \inputvals[79]_i_1\ : label is "soft_lutpair291";
  attribute SOFT_HLUTNM of \inputvals[80]_i_1\ : label is "soft_lutpair290";
  attribute SOFT_HLUTNM of \inputvals[81]_i_1\ : label is "soft_lutpair289";
  attribute SOFT_HLUTNM of \inputvals[82]_i_1\ : label is "soft_lutpair288";
  attribute SOFT_HLUTNM of \inputvals[83]_i_1\ : label is "soft_lutpair287";
  attribute SOFT_HLUTNM of \inputvals[84]_i_1\ : label is "soft_lutpair286";
  attribute SOFT_HLUTNM of \inputvals[85]_i_1\ : label is "soft_lutpair285";
  attribute SOFT_HLUTNM of \inputvals[86]_i_1\ : label is "soft_lutpair284";
  attribute SOFT_HLUTNM of \inputvals[87]_i_1\ : label is "soft_lutpair283";
  attribute SOFT_HLUTNM of \inputvals[88]_i_1\ : label is "soft_lutpair282";
  attribute SOFT_HLUTNM of \inputvals[89]_i_1\ : label is "soft_lutpair281";
  attribute SOFT_HLUTNM of \inputvals[90]_i_1\ : label is "soft_lutpair280";
  attribute SOFT_HLUTNM of \inputvals[91]_i_1\ : label is "soft_lutpair279";
  attribute SOFT_HLUTNM of \inputvals[92]_i_1\ : label is "soft_lutpair278";
  attribute SOFT_HLUTNM of \inputvals[93]_i_1\ : label is "soft_lutpair277";
  attribute SOFT_HLUTNM of \inputvals[94]_i_1\ : label is "soft_lutpair276";
  attribute SOFT_HLUTNM of \inputvals[95]_i_1\ : label is "soft_lutpair275";
  attribute SOFT_HLUTNM of \inputvals[96]_i_1\ : label is "soft_lutpair274";
  attribute SOFT_HLUTNM of \inputvals[97]_i_1\ : label is "soft_lutpair273";
  attribute SOFT_HLUTNM of \inputvals[98]_i_1\ : label is "soft_lutpair272";
  attribute SOFT_HLUTNM of \inputvals[99]_i_1\ : label is "soft_lutpair271";
  attribute SOFT_HLUTNM of \temp[47]_i_2\ : label is "soft_lutpair250";
  attribute SOFT_HLUTNM of \valid[1]_i_2\ : label is "soft_lutpair249";
  attribute SOFT_HLUTNM of \valid[2]_i_1\ : label is "soft_lutpair249";
  attribute SOFT_HLUTNM of \valid[3]_i_1\ : label is "soft_lutpair250";
begin
  \m_axis_tkeep[0]\ <= \^m_axis_tkeep[0]\;
  \m_axis_tkeep[2]\ <= \^m_axis_tkeep[2]\;
  \m_axis_tkeep[3]\ <= \^m_axis_tkeep[3]\;
  \m_axis_tkeep[4]\ <= \^m_axis_tkeep[4]\;
  m_axis_tlast <= \^m_axis_tlast\;
c1: entity work.finalproj_sobelfilteraxistream_0_0_convolution
     port map (
      D(9 downto 1) => outx12(9 downto 1),
      D(0) => \x12/f1/S\,
      E(0) => c1_n_16,
      Q(63 downto 32) => inputvals(167 downto 136),
      Q(31 downto 0) => inputvals(127 downto 96),
      clock => clock,
      f => f,
      f_reg => c1_n_18,
      hold => hold,
      hold0 => hold0,
      hold_reg => c5_n_0,
      lastpck_reg => c1_n_15,
      m_axis_tdata(7 downto 0) => m_axis_tdata(7 downto 0),
      \m_axis_tkeep[0]\ => \^m_axis_tkeep[0]\,
      m_axis_tlast => \^m_axis_tlast\,
      m_axis_tready => m_axis_tready,
      reset => reset,
      reset_0 => c4_n_0,
      s_axis_tlast => s_axis_tlast,
      s_axis_tready => s_axis_tready,
      s_axis_tstrb(5 downto 3) => s_axis_tstrb(14 downto 12),
      s_axis_tstrb(2 downto 0) => s_axis_tstrb(2 downto 0),
      \s_axis_tstrb[6]\ => f_i_2_n_0,
      \s_axis_tstrb[9]\ => \temp[47]_i_2_n_0\,
      s_axis_tvalid => s_axis_tvalid,
      temp0 => temp0,
      \temp_reg[0]\ => c1_n_11,
      \valid_reg[0]\(0) => \valid_reg_n_0_[0]\,
      \xreg1_reg[8]_0\ => c1_n_10
    );
c2: entity work.finalproj_sobelfilteraxistream_0_0_convolution_0
     port map (
      D(9 downto 1) => outx12_1(9 downto 1),
      D(0) => \x12/f1/S_0\,
      Q(63 downto 32) => inputvals(143 downto 112),
      Q(31 downto 0) => inputvals(103 downto 72),
      clock => clock,
      \full_reg[2]_0\ => \^m_axis_tkeep[4]\,
      \full_reg[2]_1\ => \^m_axis_tkeep[0]\,
      \full_reg[2]_2\ => \^m_axis_tkeep[3]\,
      \full_reg[2]_3\ => \^m_axis_tkeep[2]\,
      hold => hold,
      hold_reg => c5_n_0,
      m_axis_tdata(7 downto 0) => m_axis_tdata(15 downto 8),
      \m_axis_tkeep[1]\ => \m_axis_tkeep[1]\,
      m_axis_tready => m_axis_tready,
      m_axis_tvalid => m_axis_tvalid,
      reset => reset,
      reset_0 => c4_n_0,
      reset_1 => c3_n_10,
      \valid_reg[1]\(0) => \valid_reg_n_0_[1]\
    );
c3: entity work.finalproj_sobelfilteraxistream_0_0_convolution_1
     port map (
      D(9 downto 1) => outx12_3(9 downto 1),
      D(0) => \x12/f1/S_2\,
      Q(55 downto 48) => inputvals(119 downto 112),
      Q(47 downto 32) => inputvals(103 downto 88),
      Q(31 downto 0) => inputvals(79 downto 48),
      clock => clock,
      hold => hold,
      hold_reg => c5_n_0,
      \inputvals_reg[119]\(9 downto 1) => outx12(9 downto 1),
      \inputvals_reg[119]\(0) => \x12/f1/S\,
      m_axis_tdata(7 downto 0) => m_axis_tdata(23 downto 16),
      \m_axis_tkeep[2]\ => \^m_axis_tkeep[2]\,
      m_axis_tready => m_axis_tready,
      \out_reg[0]_0\ => c3_n_10,
      reset => reset,
      reset_0 => c4_n_0,
      \valid_reg[2]\(0) => \valid_reg_n_0_[2]\
    );
c4: entity work.finalproj_sobelfilteraxistream_0_0_convolution_2
     port map (
      D(9 downto 1) => outx12_1(9 downto 1),
      D(0) => \x12/f1/S_0\,
      Q(55 downto 48) => inputvals(95 downto 88),
      Q(47 downto 32) => inputvals(79 downto 64),
      Q(31 downto 0) => inputvals(55 downto 24),
      clock => clock,
      \full_reg[2]_0\ => c4_n_0,
      hold => hold,
      hold_reg => c5_n_0,
      m_axis_tdata(7 downto 0) => m_axis_tdata(31 downto 24),
      \m_axis_tkeep[3]\ => \^m_axis_tkeep[3]\,
      m_axis_tready => m_axis_tready,
      reset => reset,
      \valid_reg[3]\(0) => \valid_reg_n_0_[3]\
    );
c5: entity work.finalproj_sobelfilteraxistream_0_0_convolution_3
     port map (
      D(9 downto 1) => outx12_3(9 downto 1),
      D(0) => \x12/f1/S_2\,
      Q(55 downto 48) => inputvals(71 downto 64),
      Q(47 downto 32) => inputvals(55 downto 40),
      Q(31 downto 0) => inputvals(31 downto 0),
      clock => clock,
      hold => hold,
      m_axis_tdata(7 downto 0) => m_axis_tdata(39 downto 32),
      \m_axis_tkeep[4]\ => \^m_axis_tkeep[4]\,
      m_axis_tready => m_axis_tready,
      reset => reset,
      reset_0 => c1_n_10,
      reset_1 => c4_n_0,
      reset_2 => c3_n_10,
      \valid_reg[4]\(0) => \valid_reg_n_0_[4]\,
      \yreg2_reg[0]_0\ => c5_n_0
    );
f_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEAAA"
    )
        port map (
      I0 => \valid[1]_i_3_n_0\,
      I1 => s_axis_tdata(121),
      I2 => s_axis_tdata(120),
      I3 => f_i_3_n_0,
      I4 => \valid[2]_i_2_n_0\,
      I5 => \valid[1]_i_2_n_0\,
      O => f_i_2_n_0
    );
f_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => s_axis_tdata(124),
      I1 => s_axis_tdata(125),
      I2 => s_axis_tdata(122),
      I3 => s_axis_tdata(123),
      I4 => s_axis_tdata(127),
      I5 => s_axis_tdata(126),
      O => f_i_3_n_0
    );
f_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clock,
      CE => '1',
      D => c1_n_18,
      Q => f,
      R => '0'
    );
hold_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => temp0,
      D => hold0,
      Q => hold,
      R => '0'
    );
\inputvals[100]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(68),
      I1 => f,
      I2 => s_axis_tdata(20),
      O => p_0_in(100)
    );
\inputvals[101]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(69),
      I1 => f,
      I2 => s_axis_tdata(21),
      O => p_0_in(101)
    );
\inputvals[102]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(70),
      I1 => f,
      I2 => s_axis_tdata(22),
      O => p_0_in(102)
    );
\inputvals[103]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(71),
      I1 => f,
      I2 => s_axis_tdata(23),
      O => p_0_in(103)
    );
\inputvals[104]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(56),
      I1 => f,
      I2 => s_axis_tdata(8),
      O => p_0_in(104)
    );
\inputvals[105]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(57),
      I1 => f,
      I2 => s_axis_tdata(9),
      O => p_0_in(105)
    );
\inputvals[106]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(58),
      I1 => f,
      I2 => s_axis_tdata(10),
      O => p_0_in(106)
    );
\inputvals[107]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(59),
      I1 => f,
      I2 => s_axis_tdata(11),
      O => p_0_in(107)
    );
\inputvals[108]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(60),
      I1 => f,
      I2 => s_axis_tdata(12),
      O => p_0_in(108)
    );
\inputvals[109]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(61),
      I1 => f,
      I2 => s_axis_tdata(13),
      O => p_0_in(109)
    );
\inputvals[110]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(62),
      I1 => f,
      I2 => s_axis_tdata(14),
      O => p_0_in(110)
    );
\inputvals[111]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(63),
      I1 => f,
      I2 => s_axis_tdata(15),
      O => p_0_in(111)
    );
\inputvals[112]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(48),
      I1 => f,
      I2 => s_axis_tdata(0),
      O => p_0_in(112)
    );
\inputvals[113]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(49),
      I1 => f,
      I2 => s_axis_tdata(1),
      O => p_0_in(113)
    );
\inputvals[114]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(50),
      I1 => f,
      I2 => s_axis_tdata(2),
      O => p_0_in(114)
    );
\inputvals[115]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(51),
      I1 => f,
      I2 => s_axis_tdata(3),
      O => p_0_in(115)
    );
\inputvals[116]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(52),
      I1 => f,
      I2 => s_axis_tdata(4),
      O => p_0_in(116)
    );
\inputvals[117]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(53),
      I1 => f,
      I2 => s_axis_tdata(5),
      O => p_0_in(117)
    );
\inputvals[118]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(54),
      I1 => f,
      I2 => s_axis_tdata(6),
      O => p_0_in(118)
    );
\inputvals[119]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(55),
      I1 => f,
      I2 => s_axis_tdata(7),
      O => p_0_in(119)
    );
\inputvals[120]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(40),
      I1 => f,
      I2 => \temp_reg_n_0_[0]\,
      O => p_0_in(120)
    );
\inputvals[121]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(41),
      I1 => f,
      I2 => \temp_reg_n_0_[1]\,
      O => p_0_in(121)
    );
\inputvals[122]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(42),
      I1 => f,
      I2 => \temp_reg_n_0_[2]\,
      O => p_0_in(122)
    );
\inputvals[123]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(43),
      I1 => f,
      I2 => \temp_reg_n_0_[3]\,
      O => p_0_in(123)
    );
\inputvals[124]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(44),
      I1 => f,
      I2 => \temp_reg_n_0_[4]\,
      O => p_0_in(124)
    );
\inputvals[125]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(45),
      I1 => f,
      I2 => \temp_reg_n_0_[5]\,
      O => p_0_in(125)
    );
\inputvals[126]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(46),
      I1 => f,
      I2 => \temp_reg_n_0_[6]\,
      O => p_0_in(126)
    );
\inputvals[127]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(47),
      I1 => f,
      I2 => \temp_reg_n_0_[7]\,
      O => p_0_in(127)
    );
\inputvals[128]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(32),
      I1 => f,
      I2 => \temp_reg_n_0_[8]\,
      O => p_0_in(128)
    );
\inputvals[129]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(33),
      I1 => f,
      I2 => \temp_reg_n_0_[9]\,
      O => p_0_in(129)
    );
\inputvals[130]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(34),
      I1 => f,
      I2 => \temp_reg_n_0_[10]\,
      O => p_0_in(130)
    );
\inputvals[131]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(35),
      I1 => f,
      I2 => \temp_reg_n_0_[11]\,
      O => p_0_in(131)
    );
\inputvals[132]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(36),
      I1 => f,
      I2 => \temp_reg_n_0_[12]\,
      O => p_0_in(132)
    );
\inputvals[133]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(37),
      I1 => f,
      I2 => \temp_reg_n_0_[13]\,
      O => p_0_in(133)
    );
\inputvals[134]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(38),
      I1 => f,
      I2 => \temp_reg_n_0_[14]\,
      O => p_0_in(134)
    );
\inputvals[135]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(39),
      I1 => f,
      I2 => \temp_reg_n_0_[15]\,
      O => p_0_in(135)
    );
\inputvals[136]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(24),
      I1 => f,
      I2 => \temp_reg_n_0_[16]\,
      O => p_0_in(136)
    );
\inputvals[137]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(25),
      I1 => f,
      I2 => \temp_reg_n_0_[17]\,
      O => p_0_in(137)
    );
\inputvals[138]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(26),
      I1 => f,
      I2 => \temp_reg_n_0_[18]\,
      O => p_0_in(138)
    );
\inputvals[139]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(27),
      I1 => f,
      I2 => \temp_reg_n_0_[19]\,
      O => p_0_in(139)
    );
\inputvals[140]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(28),
      I1 => f,
      I2 => \temp_reg_n_0_[20]\,
      O => p_0_in(140)
    );
\inputvals[141]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(29),
      I1 => f,
      I2 => \temp_reg_n_0_[21]\,
      O => p_0_in(141)
    );
\inputvals[142]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(30),
      I1 => f,
      I2 => \temp_reg_n_0_[22]\,
      O => p_0_in(142)
    );
\inputvals[143]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(31),
      I1 => f,
      I2 => \temp_reg_n_0_[23]\,
      O => p_0_in(143)
    );
\inputvals[144]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(16),
      I1 => f,
      I2 => \temp_reg_n_0_[24]\,
      O => p_0_in(144)
    );
\inputvals[145]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(17),
      I1 => f,
      I2 => \temp_reg_n_0_[25]\,
      O => p_0_in(145)
    );
\inputvals[146]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(18),
      I1 => f,
      I2 => \temp_reg_n_0_[26]\,
      O => p_0_in(146)
    );
\inputvals[147]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(19),
      I1 => f,
      I2 => \temp_reg_n_0_[27]\,
      O => p_0_in(147)
    );
\inputvals[148]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(20),
      I1 => f,
      I2 => \temp_reg_n_0_[28]\,
      O => p_0_in(148)
    );
\inputvals[149]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(21),
      I1 => f,
      I2 => \temp_reg_n_0_[29]\,
      O => p_0_in(149)
    );
\inputvals[150]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(22),
      I1 => f,
      I2 => \temp_reg_n_0_[30]\,
      O => p_0_in(150)
    );
\inputvals[151]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(23),
      I1 => f,
      I2 => \temp_reg_n_0_[31]\,
      O => p_0_in(151)
    );
\inputvals[152]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(8),
      I1 => f,
      I2 => \temp_reg_n_0_[32]\,
      O => p_0_in(152)
    );
\inputvals[153]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(9),
      I1 => f,
      I2 => \temp_reg_n_0_[33]\,
      O => p_0_in(153)
    );
\inputvals[154]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(10),
      I1 => f,
      I2 => \temp_reg_n_0_[34]\,
      O => p_0_in(154)
    );
\inputvals[155]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(11),
      I1 => f,
      I2 => \temp_reg_n_0_[35]\,
      O => p_0_in(155)
    );
\inputvals[156]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(12),
      I1 => f,
      I2 => \temp_reg_n_0_[36]\,
      O => p_0_in(156)
    );
\inputvals[157]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(13),
      I1 => f,
      I2 => \temp_reg_n_0_[37]\,
      O => p_0_in(157)
    );
\inputvals[158]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(14),
      I1 => f,
      I2 => \temp_reg_n_0_[38]\,
      O => p_0_in(158)
    );
\inputvals[159]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(15),
      I1 => f,
      I2 => \temp_reg_n_0_[39]\,
      O => p_0_in(159)
    );
\inputvals[160]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(0),
      I1 => f,
      I2 => \temp_reg_n_0_[40]\,
      O => p_0_in(160)
    );
\inputvals[161]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(1),
      I1 => f,
      I2 => \temp_reg_n_0_[41]\,
      O => p_0_in(161)
    );
\inputvals[162]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(2),
      I1 => f,
      I2 => \temp_reg_n_0_[42]\,
      O => p_0_in(162)
    );
\inputvals[163]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(3),
      I1 => f,
      I2 => \temp_reg_n_0_[43]\,
      O => p_0_in(163)
    );
\inputvals[164]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(4),
      I1 => f,
      I2 => \temp_reg_n_0_[44]\,
      O => p_0_in(164)
    );
\inputvals[165]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(5),
      I1 => f,
      I2 => \temp_reg_n_0_[45]\,
      O => p_0_in(165)
    );
\inputvals[166]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(6),
      I1 => f,
      I2 => \temp_reg_n_0_[46]\,
      O => p_0_in(166)
    );
\inputvals[167]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(7),
      I1 => f,
      I2 => \temp_reg_n_0_[47]\,
      O => p_0_in(167)
    );
\inputvals[48]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(112),
      I1 => f,
      I2 => s_axis_tdata(64),
      O => p_0_in(48)
    );
\inputvals[49]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(113),
      I1 => f,
      I2 => s_axis_tdata(65),
      O => p_0_in(49)
    );
\inputvals[50]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(114),
      I1 => f,
      I2 => s_axis_tdata(66),
      O => p_0_in(50)
    );
\inputvals[51]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(115),
      I1 => f,
      I2 => s_axis_tdata(67),
      O => p_0_in(51)
    );
\inputvals[52]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(116),
      I1 => f,
      I2 => s_axis_tdata(68),
      O => p_0_in(52)
    );
\inputvals[53]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(117),
      I1 => f,
      I2 => s_axis_tdata(69),
      O => p_0_in(53)
    );
\inputvals[54]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(118),
      I1 => f,
      I2 => s_axis_tdata(70),
      O => p_0_in(54)
    );
\inputvals[55]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(119),
      I1 => f,
      I2 => s_axis_tdata(71),
      O => p_0_in(55)
    );
\inputvals[56]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(104),
      I1 => f,
      I2 => s_axis_tdata(56),
      O => p_0_in(56)
    );
\inputvals[57]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(105),
      I1 => f,
      I2 => s_axis_tdata(57),
      O => p_0_in(57)
    );
\inputvals[58]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(106),
      I1 => f,
      I2 => s_axis_tdata(58),
      O => p_0_in(58)
    );
\inputvals[59]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(107),
      I1 => f,
      I2 => s_axis_tdata(59),
      O => p_0_in(59)
    );
\inputvals[60]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(108),
      I1 => f,
      I2 => s_axis_tdata(60),
      O => p_0_in(60)
    );
\inputvals[61]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(109),
      I1 => f,
      I2 => s_axis_tdata(61),
      O => p_0_in(61)
    );
\inputvals[62]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(110),
      I1 => f,
      I2 => s_axis_tdata(62),
      O => p_0_in(62)
    );
\inputvals[63]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(111),
      I1 => f,
      I2 => s_axis_tdata(63),
      O => p_0_in(63)
    );
\inputvals[64]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(96),
      I1 => f,
      I2 => s_axis_tdata(48),
      O => p_0_in(64)
    );
\inputvals[65]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(97),
      I1 => f,
      I2 => s_axis_tdata(49),
      O => p_0_in(65)
    );
\inputvals[66]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(98),
      I1 => f,
      I2 => s_axis_tdata(50),
      O => p_0_in(66)
    );
\inputvals[67]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(99),
      I1 => f,
      I2 => s_axis_tdata(51),
      O => p_0_in(67)
    );
\inputvals[68]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(100),
      I1 => f,
      I2 => s_axis_tdata(52),
      O => p_0_in(68)
    );
\inputvals[69]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(101),
      I1 => f,
      I2 => s_axis_tdata(53),
      O => p_0_in(69)
    );
\inputvals[70]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(102),
      I1 => f,
      I2 => s_axis_tdata(54),
      O => p_0_in(70)
    );
\inputvals[71]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(103),
      I1 => f,
      I2 => s_axis_tdata(55),
      O => p_0_in(71)
    );
\inputvals[72]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(88),
      I1 => f,
      I2 => s_axis_tdata(40),
      O => p_0_in(72)
    );
\inputvals[73]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(89),
      I1 => f,
      I2 => s_axis_tdata(41),
      O => p_0_in(73)
    );
\inputvals[74]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(90),
      I1 => f,
      I2 => s_axis_tdata(42),
      O => p_0_in(74)
    );
\inputvals[75]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(91),
      I1 => f,
      I2 => s_axis_tdata(43),
      O => p_0_in(75)
    );
\inputvals[76]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(92),
      I1 => f,
      I2 => s_axis_tdata(44),
      O => p_0_in(76)
    );
\inputvals[77]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(93),
      I1 => f,
      I2 => s_axis_tdata(45),
      O => p_0_in(77)
    );
\inputvals[78]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(94),
      I1 => f,
      I2 => s_axis_tdata(46),
      O => p_0_in(78)
    );
\inputvals[79]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(95),
      I1 => f,
      I2 => s_axis_tdata(47),
      O => p_0_in(79)
    );
\inputvals[80]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(80),
      I1 => f,
      I2 => s_axis_tdata(32),
      O => p_0_in(80)
    );
\inputvals[81]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(81),
      I1 => f,
      I2 => s_axis_tdata(33),
      O => p_0_in(81)
    );
\inputvals[82]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(82),
      I1 => f,
      I2 => s_axis_tdata(34),
      O => p_0_in(82)
    );
\inputvals[83]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(83),
      I1 => f,
      I2 => s_axis_tdata(35),
      O => p_0_in(83)
    );
\inputvals[84]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(84),
      I1 => f,
      I2 => s_axis_tdata(36),
      O => p_0_in(84)
    );
\inputvals[85]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(85),
      I1 => f,
      I2 => s_axis_tdata(37),
      O => p_0_in(85)
    );
\inputvals[86]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(86),
      I1 => f,
      I2 => s_axis_tdata(38),
      O => p_0_in(86)
    );
\inputvals[87]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(87),
      I1 => f,
      I2 => s_axis_tdata(39),
      O => p_0_in(87)
    );
\inputvals[88]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(72),
      I1 => f,
      I2 => s_axis_tdata(24),
      O => p_0_in(88)
    );
\inputvals[89]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(73),
      I1 => f,
      I2 => s_axis_tdata(25),
      O => p_0_in(89)
    );
\inputvals[90]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(74),
      I1 => f,
      I2 => s_axis_tdata(26),
      O => p_0_in(90)
    );
\inputvals[91]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(75),
      I1 => f,
      I2 => s_axis_tdata(27),
      O => p_0_in(91)
    );
\inputvals[92]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(76),
      I1 => f,
      I2 => s_axis_tdata(28),
      O => p_0_in(92)
    );
\inputvals[93]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(77),
      I1 => f,
      I2 => s_axis_tdata(29),
      O => p_0_in(93)
    );
\inputvals[94]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(78),
      I1 => f,
      I2 => s_axis_tdata(30),
      O => p_0_in(94)
    );
\inputvals[95]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(79),
      I1 => f,
      I2 => s_axis_tdata(31),
      O => p_0_in(95)
    );
\inputvals[96]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(64),
      I1 => f,
      I2 => s_axis_tdata(16),
      O => p_0_in(96)
    );
\inputvals[97]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(65),
      I1 => f,
      I2 => s_axis_tdata(17),
      O => p_0_in(97)
    );
\inputvals[98]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(66),
      I1 => f,
      I2 => s_axis_tdata(18),
      O => p_0_in(98)
    );
\inputvals[99]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axis_tdata(67),
      I1 => f,
      I2 => s_axis_tdata(19),
      O => p_0_in(99)
    );
\inputvals_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(112),
      Q => inputvals(0),
      R => '0'
    );
\inputvals_reg[100]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(100),
      Q => inputvals(100),
      R => '0'
    );
\inputvals_reg[101]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(101),
      Q => inputvals(101),
      R => '0'
    );
\inputvals_reg[102]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(102),
      Q => inputvals(102),
      R => '0'
    );
\inputvals_reg[103]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(103),
      Q => inputvals(103),
      R => '0'
    );
\inputvals_reg[104]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(104),
      Q => inputvals(104),
      R => '0'
    );
\inputvals_reg[105]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(105),
      Q => inputvals(105),
      R => '0'
    );
\inputvals_reg[106]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(106),
      Q => inputvals(106),
      R => '0'
    );
\inputvals_reg[107]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(107),
      Q => inputvals(107),
      R => '0'
    );
\inputvals_reg[108]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(108),
      Q => inputvals(108),
      R => '0'
    );
\inputvals_reg[109]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(109),
      Q => inputvals(109),
      R => '0'
    );
\inputvals_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(106),
      Q => inputvals(10),
      R => '0'
    );
\inputvals_reg[110]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(110),
      Q => inputvals(110),
      R => '0'
    );
\inputvals_reg[111]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(111),
      Q => inputvals(111),
      R => '0'
    );
\inputvals_reg[112]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(112),
      Q => inputvals(112),
      R => '0'
    );
\inputvals_reg[113]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(113),
      Q => inputvals(113),
      R => '0'
    );
\inputvals_reg[114]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(114),
      Q => inputvals(114),
      R => '0'
    );
\inputvals_reg[115]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(115),
      Q => inputvals(115),
      R => '0'
    );
\inputvals_reg[116]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(116),
      Q => inputvals(116),
      R => '0'
    );
\inputvals_reg[117]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(117),
      Q => inputvals(117),
      R => '0'
    );
\inputvals_reg[118]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(118),
      Q => inputvals(118),
      R => '0'
    );
\inputvals_reg[119]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(119),
      Q => inputvals(119),
      R => '0'
    );
\inputvals_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(107),
      Q => inputvals(11),
      R => '0'
    );
\inputvals_reg[120]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(120),
      Q => inputvals(120),
      R => '0'
    );
\inputvals_reg[121]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(121),
      Q => inputvals(121),
      R => '0'
    );
\inputvals_reg[122]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(122),
      Q => inputvals(122),
      R => '0'
    );
\inputvals_reg[123]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(123),
      Q => inputvals(123),
      R => '0'
    );
\inputvals_reg[124]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(124),
      Q => inputvals(124),
      R => '0'
    );
\inputvals_reg[125]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(125),
      Q => inputvals(125),
      R => '0'
    );
\inputvals_reg[126]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(126),
      Q => inputvals(126),
      R => '0'
    );
\inputvals_reg[127]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(127),
      Q => inputvals(127),
      R => '0'
    );
\inputvals_reg[128]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(128),
      Q => inputvals(128),
      R => '0'
    );
\inputvals_reg[129]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(129),
      Q => inputvals(129),
      R => '0'
    );
\inputvals_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(108),
      Q => inputvals(12),
      R => '0'
    );
\inputvals_reg[130]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(130),
      Q => inputvals(130),
      R => '0'
    );
\inputvals_reg[131]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(131),
      Q => inputvals(131),
      R => '0'
    );
\inputvals_reg[132]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(132),
      Q => inputvals(132),
      R => '0'
    );
\inputvals_reg[133]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(133),
      Q => inputvals(133),
      R => '0'
    );
\inputvals_reg[134]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(134),
      Q => inputvals(134),
      R => '0'
    );
\inputvals_reg[135]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(135),
      Q => inputvals(135),
      R => '0'
    );
\inputvals_reg[136]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(136),
      Q => inputvals(136),
      R => '0'
    );
\inputvals_reg[137]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(137),
      Q => inputvals(137),
      R => '0'
    );
\inputvals_reg[138]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(138),
      Q => inputvals(138),
      R => '0'
    );
\inputvals_reg[139]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(139),
      Q => inputvals(139),
      R => '0'
    );
\inputvals_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(109),
      Q => inputvals(13),
      R => '0'
    );
\inputvals_reg[140]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(140),
      Q => inputvals(140),
      R => '0'
    );
\inputvals_reg[141]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(141),
      Q => inputvals(141),
      R => '0'
    );
\inputvals_reg[142]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(142),
      Q => inputvals(142),
      R => '0'
    );
\inputvals_reg[143]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(143),
      Q => inputvals(143),
      R => '0'
    );
\inputvals_reg[144]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(144),
      Q => inputvals(144),
      R => '0'
    );
\inputvals_reg[145]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(145),
      Q => inputvals(145),
      R => '0'
    );
\inputvals_reg[146]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(146),
      Q => inputvals(146),
      R => '0'
    );
\inputvals_reg[147]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(147),
      Q => inputvals(147),
      R => '0'
    );
\inputvals_reg[148]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(148),
      Q => inputvals(148),
      R => '0'
    );
\inputvals_reg[149]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(149),
      Q => inputvals(149),
      R => '0'
    );
\inputvals_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(110),
      Q => inputvals(14),
      R => '0'
    );
\inputvals_reg[150]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(150),
      Q => inputvals(150),
      R => '0'
    );
\inputvals_reg[151]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(151),
      Q => inputvals(151),
      R => '0'
    );
\inputvals_reg[152]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(152),
      Q => inputvals(152),
      R => '0'
    );
\inputvals_reg[153]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(153),
      Q => inputvals(153),
      R => '0'
    );
\inputvals_reg[154]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(154),
      Q => inputvals(154),
      R => '0'
    );
\inputvals_reg[155]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(155),
      Q => inputvals(155),
      R => '0'
    );
\inputvals_reg[156]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(156),
      Q => inputvals(156),
      R => '0'
    );
\inputvals_reg[157]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(157),
      Q => inputvals(157),
      R => '0'
    );
\inputvals_reg[158]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(158),
      Q => inputvals(158),
      R => '0'
    );
\inputvals_reg[159]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(159),
      Q => inputvals(159),
      R => '0'
    );
\inputvals_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(111),
      Q => inputvals(15),
      R => '0'
    );
\inputvals_reg[160]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(160),
      Q => inputvals(160),
      R => '0'
    );
\inputvals_reg[161]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(161),
      Q => inputvals(161),
      R => '0'
    );
\inputvals_reg[162]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(162),
      Q => inputvals(162),
      R => '0'
    );
\inputvals_reg[163]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(163),
      Q => inputvals(163),
      R => '0'
    );
\inputvals_reg[164]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(164),
      Q => inputvals(164),
      R => '0'
    );
\inputvals_reg[165]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(165),
      Q => inputvals(165),
      R => '0'
    );
\inputvals_reg[166]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(166),
      Q => inputvals(166),
      R => '0'
    );
\inputvals_reg[167]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(167),
      Q => inputvals(167),
      R => '0'
    );
\inputvals_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(96),
      Q => inputvals(16),
      R => '0'
    );
\inputvals_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(97),
      Q => inputvals(17),
      R => '0'
    );
\inputvals_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(98),
      Q => inputvals(18),
      R => '0'
    );
\inputvals_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(99),
      Q => inputvals(19),
      R => '0'
    );
\inputvals_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(113),
      Q => inputvals(1),
      R => '0'
    );
\inputvals_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(100),
      Q => inputvals(20),
      R => '0'
    );
\inputvals_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(101),
      Q => inputvals(21),
      R => '0'
    );
\inputvals_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(102),
      Q => inputvals(22),
      R => '0'
    );
\inputvals_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(103),
      Q => inputvals(23),
      R => '0'
    );
\inputvals_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(88),
      Q => inputvals(24),
      R => '0'
    );
\inputvals_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(89),
      Q => inputvals(25),
      R => '0'
    );
\inputvals_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(90),
      Q => inputvals(26),
      R => '0'
    );
\inputvals_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(91),
      Q => inputvals(27),
      R => '0'
    );
\inputvals_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(92),
      Q => inputvals(28),
      R => '0'
    );
\inputvals_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(93),
      Q => inputvals(29),
      R => '0'
    );
\inputvals_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(114),
      Q => inputvals(2),
      R => '0'
    );
\inputvals_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(94),
      Q => inputvals(30),
      R => '0'
    );
\inputvals_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(95),
      Q => inputvals(31),
      R => '0'
    );
\inputvals_reg[32]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(80),
      Q => inputvals(32),
      R => '0'
    );
\inputvals_reg[33]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(81),
      Q => inputvals(33),
      R => '0'
    );
\inputvals_reg[34]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(82),
      Q => inputvals(34),
      R => '0'
    );
\inputvals_reg[35]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(83),
      Q => inputvals(35),
      R => '0'
    );
\inputvals_reg[36]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(84),
      Q => inputvals(36),
      R => '0'
    );
\inputvals_reg[37]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(85),
      Q => inputvals(37),
      R => '0'
    );
\inputvals_reg[38]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(86),
      Q => inputvals(38),
      R => '0'
    );
\inputvals_reg[39]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(87),
      Q => inputvals(39),
      R => '0'
    );
\inputvals_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(115),
      Q => inputvals(3),
      R => '0'
    );
\inputvals_reg[40]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(72),
      Q => inputvals(40),
      R => '0'
    );
\inputvals_reg[41]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(73),
      Q => inputvals(41),
      R => '0'
    );
\inputvals_reg[42]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(74),
      Q => inputvals(42),
      R => '0'
    );
\inputvals_reg[43]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(75),
      Q => inputvals(43),
      R => '0'
    );
\inputvals_reg[44]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(76),
      Q => inputvals(44),
      R => '0'
    );
\inputvals_reg[45]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(77),
      Q => inputvals(45),
      R => '0'
    );
\inputvals_reg[46]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(78),
      Q => inputvals(46),
      R => '0'
    );
\inputvals_reg[47]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(79),
      Q => inputvals(47),
      R => '0'
    );
\inputvals_reg[48]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(48),
      Q => inputvals(48),
      R => '0'
    );
\inputvals_reg[49]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(49),
      Q => inputvals(49),
      R => '0'
    );
\inputvals_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(116),
      Q => inputvals(4),
      R => '0'
    );
\inputvals_reg[50]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(50),
      Q => inputvals(50),
      R => '0'
    );
\inputvals_reg[51]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(51),
      Q => inputvals(51),
      R => '0'
    );
\inputvals_reg[52]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(52),
      Q => inputvals(52),
      R => '0'
    );
\inputvals_reg[53]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(53),
      Q => inputvals(53),
      R => '0'
    );
\inputvals_reg[54]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(54),
      Q => inputvals(54),
      R => '0'
    );
\inputvals_reg[55]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(55),
      Q => inputvals(55),
      R => '0'
    );
\inputvals_reg[56]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(56),
      Q => inputvals(56),
      R => '0'
    );
\inputvals_reg[57]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(57),
      Q => inputvals(57),
      R => '0'
    );
\inputvals_reg[58]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(58),
      Q => inputvals(58),
      R => '0'
    );
\inputvals_reg[59]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(59),
      Q => inputvals(59),
      R => '0'
    );
\inputvals_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(117),
      Q => inputvals(5),
      R => '0'
    );
\inputvals_reg[60]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(60),
      Q => inputvals(60),
      R => '0'
    );
\inputvals_reg[61]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(61),
      Q => inputvals(61),
      R => '0'
    );
\inputvals_reg[62]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(62),
      Q => inputvals(62),
      R => '0'
    );
\inputvals_reg[63]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(63),
      Q => inputvals(63),
      R => '0'
    );
\inputvals_reg[64]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(64),
      Q => inputvals(64),
      R => '0'
    );
\inputvals_reg[65]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(65),
      Q => inputvals(65),
      R => '0'
    );
\inputvals_reg[66]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(66),
      Q => inputvals(66),
      R => '0'
    );
\inputvals_reg[67]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(67),
      Q => inputvals(67),
      R => '0'
    );
\inputvals_reg[68]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(68),
      Q => inputvals(68),
      R => '0'
    );
\inputvals_reg[69]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(69),
      Q => inputvals(69),
      R => '0'
    );
\inputvals_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(118),
      Q => inputvals(6),
      R => '0'
    );
\inputvals_reg[70]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(70),
      Q => inputvals(70),
      R => '0'
    );
\inputvals_reg[71]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(71),
      Q => inputvals(71),
      R => '0'
    );
\inputvals_reg[72]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(72),
      Q => inputvals(72),
      R => '0'
    );
\inputvals_reg[73]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(73),
      Q => inputvals(73),
      R => '0'
    );
\inputvals_reg[74]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(74),
      Q => inputvals(74),
      R => '0'
    );
\inputvals_reg[75]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(75),
      Q => inputvals(75),
      R => '0'
    );
\inputvals_reg[76]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(76),
      Q => inputvals(76),
      R => '0'
    );
\inputvals_reg[77]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(77),
      Q => inputvals(77),
      R => '0'
    );
\inputvals_reg[78]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(78),
      Q => inputvals(78),
      R => '0'
    );
\inputvals_reg[79]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(79),
      Q => inputvals(79),
      R => '0'
    );
\inputvals_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(119),
      Q => inputvals(7),
      R => '0'
    );
\inputvals_reg[80]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(80),
      Q => inputvals(80),
      R => '0'
    );
\inputvals_reg[81]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(81),
      Q => inputvals(81),
      R => '0'
    );
\inputvals_reg[82]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(82),
      Q => inputvals(82),
      R => '0'
    );
\inputvals_reg[83]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(83),
      Q => inputvals(83),
      R => '0'
    );
\inputvals_reg[84]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(84),
      Q => inputvals(84),
      R => '0'
    );
\inputvals_reg[85]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(85),
      Q => inputvals(85),
      R => '0'
    );
\inputvals_reg[86]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(86),
      Q => inputvals(86),
      R => '0'
    );
\inputvals_reg[87]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(87),
      Q => inputvals(87),
      R => '0'
    );
\inputvals_reg[88]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(88),
      Q => inputvals(88),
      R => '0'
    );
\inputvals_reg[89]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(89),
      Q => inputvals(89),
      R => '0'
    );
\inputvals_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(104),
      Q => inputvals(8),
      R => '0'
    );
\inputvals_reg[90]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(90),
      Q => inputvals(90),
      R => '0'
    );
\inputvals_reg[91]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(91),
      Q => inputvals(91),
      R => '0'
    );
\inputvals_reg[92]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(92),
      Q => inputvals(92),
      R => '0'
    );
\inputvals_reg[93]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(93),
      Q => inputvals(93),
      R => '0'
    );
\inputvals_reg[94]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(94),
      Q => inputvals(94),
      R => '0'
    );
\inputvals_reg[95]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(95),
      Q => inputvals(95),
      R => '0'
    );
\inputvals_reg[96]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(96),
      Q => inputvals(96),
      R => '0'
    );
\inputvals_reg[97]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(97),
      Q => inputvals(97),
      R => '0'
    );
\inputvals_reg[98]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(98),
      Q => inputvals(98),
      R => '0'
    );
\inputvals_reg[99]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_0_in(99),
      Q => inputvals(99),
      R => '0'
    );
\inputvals_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => c1_n_16,
      D => s_axis_tdata(105),
      Q => inputvals(9),
      R => '0'
    );
lastpck_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clock,
      CE => '1',
      D => c1_n_15,
      Q => \^m_axis_tlast\,
      R => '0'
    );
\temp[47]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => s_axis_tstrb(11),
      I1 => s_axis_tstrb(10),
      I2 => s_axis_tstrb(9),
      O => \temp[47]_i_2_n_0\
    );
\temp_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(112),
      Q => \temp_reg_n_0_[0]\,
      R => c1_n_11
    );
\temp_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(106),
      Q => \temp_reg_n_0_[10]\,
      R => c1_n_11
    );
\temp_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(107),
      Q => \temp_reg_n_0_[11]\,
      R => c1_n_11
    );
\temp_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(108),
      Q => \temp_reg_n_0_[12]\,
      R => c1_n_11
    );
\temp_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(109),
      Q => \temp_reg_n_0_[13]\,
      R => c1_n_11
    );
\temp_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(110),
      Q => \temp_reg_n_0_[14]\,
      R => c1_n_11
    );
\temp_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(111),
      Q => \temp_reg_n_0_[15]\,
      R => c1_n_11
    );
\temp_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(96),
      Q => \temp_reg_n_0_[16]\,
      R => c1_n_11
    );
\temp_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(97),
      Q => \temp_reg_n_0_[17]\,
      R => c1_n_11
    );
\temp_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(98),
      Q => \temp_reg_n_0_[18]\,
      R => c1_n_11
    );
\temp_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(99),
      Q => \temp_reg_n_0_[19]\,
      R => c1_n_11
    );
\temp_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(113),
      Q => \temp_reg_n_0_[1]\,
      R => c1_n_11
    );
\temp_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(100),
      Q => \temp_reg_n_0_[20]\,
      R => c1_n_11
    );
\temp_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(101),
      Q => \temp_reg_n_0_[21]\,
      R => c1_n_11
    );
\temp_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(102),
      Q => \temp_reg_n_0_[22]\,
      R => c1_n_11
    );
\temp_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(103),
      Q => \temp_reg_n_0_[23]\,
      R => c1_n_11
    );
\temp_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(88),
      Q => \temp_reg_n_0_[24]\,
      R => c1_n_11
    );
\temp_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(89),
      Q => \temp_reg_n_0_[25]\,
      R => c1_n_11
    );
\temp_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(90),
      Q => \temp_reg_n_0_[26]\,
      R => c1_n_11
    );
\temp_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(91),
      Q => \temp_reg_n_0_[27]\,
      R => c1_n_11
    );
\temp_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(92),
      Q => \temp_reg_n_0_[28]\,
      R => c1_n_11
    );
\temp_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(93),
      Q => \temp_reg_n_0_[29]\,
      R => c1_n_11
    );
\temp_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(114),
      Q => \temp_reg_n_0_[2]\,
      R => c1_n_11
    );
\temp_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(94),
      Q => \temp_reg_n_0_[30]\,
      R => c1_n_11
    );
\temp_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(95),
      Q => \temp_reg_n_0_[31]\,
      R => c1_n_11
    );
\temp_reg[32]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(80),
      Q => \temp_reg_n_0_[32]\,
      R => c1_n_11
    );
\temp_reg[33]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(81),
      Q => \temp_reg_n_0_[33]\,
      R => c1_n_11
    );
\temp_reg[34]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(82),
      Q => \temp_reg_n_0_[34]\,
      R => c1_n_11
    );
\temp_reg[35]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(83),
      Q => \temp_reg_n_0_[35]\,
      R => c1_n_11
    );
\temp_reg[36]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(84),
      Q => \temp_reg_n_0_[36]\,
      R => c1_n_11
    );
\temp_reg[37]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(85),
      Q => \temp_reg_n_0_[37]\,
      R => c1_n_11
    );
\temp_reg[38]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(86),
      Q => \temp_reg_n_0_[38]\,
      R => c1_n_11
    );
\temp_reg[39]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(87),
      Q => \temp_reg_n_0_[39]\,
      R => c1_n_11
    );
\temp_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(115),
      Q => \temp_reg_n_0_[3]\,
      R => c1_n_11
    );
\temp_reg[40]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(72),
      Q => \temp_reg_n_0_[40]\,
      R => c1_n_11
    );
\temp_reg[41]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(73),
      Q => \temp_reg_n_0_[41]\,
      R => c1_n_11
    );
\temp_reg[42]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(74),
      Q => \temp_reg_n_0_[42]\,
      R => c1_n_11
    );
\temp_reg[43]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(75),
      Q => \temp_reg_n_0_[43]\,
      R => c1_n_11
    );
\temp_reg[44]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(76),
      Q => \temp_reg_n_0_[44]\,
      R => c1_n_11
    );
\temp_reg[45]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(77),
      Q => \temp_reg_n_0_[45]\,
      R => c1_n_11
    );
\temp_reg[46]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(78),
      Q => \temp_reg_n_0_[46]\,
      R => c1_n_11
    );
\temp_reg[47]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(79),
      Q => \temp_reg_n_0_[47]\,
      R => c1_n_11
    );
\temp_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(116),
      Q => \temp_reg_n_0_[4]\,
      R => c1_n_11
    );
\temp_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(117),
      Q => \temp_reg_n_0_[5]\,
      R => c1_n_11
    );
\temp_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(118),
      Q => \temp_reg_n_0_[6]\,
      R => c1_n_11
    );
\temp_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(119),
      Q => \temp_reg_n_0_[7]\,
      R => c1_n_11
    );
\temp_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(104),
      Q => \temp_reg_n_0_[8]\,
      R => c1_n_11
    );
\temp_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => s_axis_tdata(105),
      Q => \temp_reg_n_0_[9]\,
      R => c1_n_11
    );
\valid[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080008000808080"
    )
        port map (
      I0 => s_axis_tstrb(0),
      I1 => s_axis_tstrb(1),
      I2 => s_axis_tstrb(2),
      I3 => f,
      I4 => \valid[1]_i_3_n_0\,
      I5 => \valid[1]_i_2_n_0\,
      O => p_1_out(0)
    );
\valid[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000004000FFFF"
    )
        port map (
      I0 => \valid[1]_i_2_n_0\,
      I1 => s_axis_tstrb(9),
      I2 => s_axis_tstrb(10),
      I3 => s_axis_tstrb(11),
      I4 => f,
      I5 => \valid[1]_i_3_n_0\,
      O => p_1_out(1)
    );
\valid[1]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => s_axis_tstrb(8),
      I1 => s_axis_tstrb(7),
      I2 => s_axis_tstrb(6),
      O => \valid[1]_i_2_n_0\
    );
\valid[1]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => s_axis_tstrb(5),
      I1 => s_axis_tstrb(4),
      I2 => s_axis_tstrb(3),
      O => \valid[1]_i_3_n_0\
    );
\valid[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"70000000"
    )
        port map (
      I0 => f,
      I1 => \valid[2]_i_2_n_0\,
      I2 => s_axis_tstrb(8),
      I3 => s_axis_tstrb(7),
      I4 => s_axis_tstrb(6),
      O => \valid[2]_i_1_n_0\
    );
\valid[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => s_axis_tstrb(12),
      I1 => s_axis_tstrb(13),
      I2 => s_axis_tstrb(14),
      I3 => s_axis_tstrb(9),
      I4 => s_axis_tstrb(10),
      I5 => s_axis_tstrb(11),
      O => \valid[2]_i_2_n_0\
    );
\valid[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => s_axis_tstrb(9),
      I1 => s_axis_tstrb(10),
      I2 => s_axis_tstrb(11),
      I3 => f,
      O => p_1_out(3)
    );
\valid[4]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s_axis_tvalid,
      O => valid
    );
\valid[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => s_axis_tstrb(12),
      I1 => s_axis_tstrb(13),
      I2 => s_axis_tstrb(14),
      I3 => f,
      O => p_1_out(4)
    );
\valid_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_1_out(0),
      Q => \valid_reg_n_0_[0]\,
      R => valid
    );
\valid_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_1_out(1),
      Q => \valid_reg_n_0_[1]\,
      R => valid
    );
\valid_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => \valid[2]_i_1_n_0\,
      Q => \valid_reg_n_0_[2]\,
      R => valid
    );
\valid_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_1_out(3),
      Q => \valid_reg_n_0_[3]\,
      R => valid
    );
\valid_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clock,
      CE => temp0,
      D => p_1_out(4),
      Q => \valid_reg_n_0_[4]\,
      R => valid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity finalproj_sobelfilteraxistream_0_0 is
  port (
    clock : in STD_LOGIC;
    reset : in STD_LOGIC;
    s_axis_tlast : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    s_axis_tstrb : in STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_tlast : out STD_LOGIC;
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 39 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of finalproj_sobelfilteraxistream_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of finalproj_sobelfilteraxistream_0_0 : entity is "finalproj_sobelfilteraxistream_0_0,sobelfilteraxistream,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of finalproj_sobelfilteraxistream_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of finalproj_sobelfilteraxistream_0_0 : entity is "sobelfilteraxistream,Vivado 2017.4";
end finalproj_sobelfilteraxistream_0_0;

architecture STRUCTURE of finalproj_sobelfilteraxistream_0_0 is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clock : signal is "xilinx.com:signal:clock:1.0 clock CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clock : signal is "XIL_INTERFACENAME clock, ASSOCIATED_BUSIF m_axis:s_axis, ASSOCIATED_RESET reset, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN finalproj_processing_system7_0_0_FCLK_CLK0";
  attribute X_INTERFACE_INFO of m_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m_axis TLAST";
  attribute X_INTERFACE_INFO of m_axis_tready : signal is "xilinx.com:interface:axis:1.0 m_axis TREADY";
  attribute X_INTERFACE_INFO of m_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m_axis TVALID";
  attribute X_INTERFACE_INFO of reset : signal is "xilinx.com:signal:reset:1.0 reset RST";
  attribute X_INTERFACE_PARAMETER of reset : signal is "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of s_axis_tlast : signal is "xilinx.com:interface:axis:1.0 s_axis TLAST";
  attribute X_INTERFACE_INFO of s_axis_tready : signal is "xilinx.com:interface:axis:1.0 s_axis TREADY";
  attribute X_INTERFACE_INFO of s_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 s_axis TVALID";
  attribute X_INTERFACE_INFO of m_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m_axis TDATA";
  attribute X_INTERFACE_INFO of m_axis_tkeep : signal is "xilinx.com:interface:axis:1.0 m_axis TKEEP";
  attribute X_INTERFACE_PARAMETER of m_axis_tkeep : signal is "XIL_INTERFACENAME m_axis, TDATA_NUM_BYTES 5, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN finalproj_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of s_axis_tdata : signal is "xilinx.com:interface:axis:1.0 s_axis TDATA";
  attribute X_INTERFACE_INFO of s_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 s_axis TSTRB";
  attribute X_INTERFACE_PARAMETER of s_axis_tstrb : signal is "XIL_INTERFACENAME s_axis, TDATA_NUM_BYTES 16, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN finalproj_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef";
begin
inst: entity work.finalproj_sobelfilteraxistream_0_0_sobelfilteraxistream
     port map (
      clock => clock,
      m_axis_tdata(39 downto 0) => m_axis_tdata(39 downto 0),
      \m_axis_tkeep[0]\ => m_axis_tkeep(0),
      \m_axis_tkeep[1]\ => m_axis_tkeep(1),
      \m_axis_tkeep[2]\ => m_axis_tkeep(2),
      \m_axis_tkeep[3]\ => m_axis_tkeep(3),
      \m_axis_tkeep[4]\ => m_axis_tkeep(4),
      m_axis_tlast => m_axis_tlast,
      m_axis_tready => m_axis_tready,
      m_axis_tvalid => m_axis_tvalid,
      reset => reset,
      s_axis_tdata(127 downto 0) => s_axis_tdata(127 downto 0),
      s_axis_tlast => s_axis_tlast,
      s_axis_tready => s_axis_tready,
      s_axis_tstrb(14 downto 0) => s_axis_tstrb(14 downto 0),
      s_axis_tvalid => s_axis_tvalid
    );
end STRUCTURE;
